import { Component, OnInit } from '@angular/core';
import { HttpClient} from '@angular/common/http';;
import { Router, ActivatedRoute, Params } from '@angular/router';
import { map } from "rxjs/operators";
import 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';
import { Meta, Title } from "@angular/platform-browser";

@Component({
	selector: 'app-search',
	templateUrl: './search.component.html',
	styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

	searchTxt: any;
	private apiUrl = 'https://api.finalrevise.com/finalrevise/v1/search/';
	pageData: any = {};
	keys: any = [];
	nav: any = [];
	navIds: any = [];
	offset: number = 10;
	count: number = 0;
	meta: Meta;
	type: number = 0;
	nameList: any;
	nameId: Number;
	valueSelected: any;

	constructor(private http: HttpClient, private activatedRoute: ActivatedRoute, private router: Router, meta: Meta, title: Title) {
		title.setTitle("FinalRevise  - Previous Year Papers");
		this.meta = meta;
	}

	ngOnInit() {
		// subscribe to router event
		this.activatedRoute.params.subscribe((params: Params) => {
			this.searchTxt = params['text'];
			this.count = params['count'];
			this.type = params['type'];
		});
		this.apiUrl = this.apiUrl + this.searchTxt + "/" + this.offset + "/" + this.count + "/" + this.type;
		this.getData()
		this.getPageData()
	}

	getData() {
		return this.http.get<any>(this.apiUrl)
	}

	getPageData() {
		this.meta.addTags([
			{ name: 'author', content: 'finalrevise.com' },
			{ name: 'keywords', content: 'All Papers related to : ' + this.searchTxt },
			{
				name: 'description', content: 'All Previous Year Question Papers for Different Competitive Exams, ' +
					'Universities : Delhi University/JNU/BHU/Amity/IP etc..., International Papers : ELETS/TOFEL/GMAT/GRE etc..., Company interview questions, School 10th 12th exams, Government Jobs Exams : Bank Papers/SSC papers/graduate level exams'
			}
		]);
		this.getData().subscribe(data => {
			this.pageData = data
			this.nav = String('Home#' + this.searchTxt).split("#");
			this.navIds = String('1#10').split("#");
		})
	}
	openPageFromNav(pageId) {
		if (pageId > 0 && pageId < 15) {
			this.router.navigateByUrl('');
		}
	}

	openPreviousPage() {
		if (this.count > 0) {
			this.count = Number(this.count) - 1;
			window.open('/search/' + this.searchTxt + '/' + this.count + '/' + this.type, "_self")
		}
	}

	openNextPage() {
		this.count = Number(this.count) + 1;
		window.open('/search/' + this.searchTxt + '/' + this.count + '/' + this.type, "_self")
	}

	openPage(id, type, child) {
		if (child == 1) {
			window.open('detail-list/' + id, "_self")
		} else if (child == 2) {
			window.open('detail/' + id, "_self")
		}
	}

	replaceAll(str, replacement) {
		str = str.split('(').join('')
		str = str.split(')').join('')
		str = str.split('.').join('')
		str = str.split('/').join('')
		str = str.split('|').join('')
		str = str.split(' ').join(replacement)
		str = str.split('---').join(replacement)
		return str.split('--').join(replacement)
	};

	selectName() {
		window.open('/search/' + this.searchTxt + '/' + this.count + '/' + this.nameId, "_self")
	}
}
