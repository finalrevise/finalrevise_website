import { Component, OnInit } from '@angular/core';
import { Http, Response} from '@angular/http';
import 'rxjs/Rx';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { map } from "rxjs/operators";
import { Observable } from 'rxjs/Observable';

@Component({
	selector: '404',
	templateUrl: './not-found.component.html',
	styleUrls: ['./not-found.component.css']
})

export class NotFoundComponent {
	
	constructor() {
	}
	
}
