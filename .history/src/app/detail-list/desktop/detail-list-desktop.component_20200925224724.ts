import { Component, OnInit } from '@angular/core';
import { HttpClient} from '@angular/common/http';;
import 'rxjs/Rx';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Meta, Title } from "@angular/platform-browser";
import { HeaderService } from '../../app-header/header.service';
import { DomSanitizer } from '@angular/platform-browser'

@Component({
	selector: 'app-detail-list',
	templateUrl: './detail-list-desktop.component.html',
	styleUrls: ['./detail-list-desktop.component.css']
})
export class DetailListDesktopComponent implements OnInit {

	selectedId: any;
	private apiUrl = 'https://api.finalrevise.com/finalrevise/v1/subcat/';
	pageData: any = {};
	keys: any = [];
	nav: any = [];
	navIds: any = [];
	title: Title;
	meta: Meta;
	private tabUrl = 'https://api.finalrevise.com/finalrevise/v1/relatedlinks/';
	tabs: any;
	private whatsAppUrl = 'https://api.finalrevise.com/finalrevise/v1/whatsApp/';
	whatsAppData: any;
	updateMasonryLayout: any;
	isClickedShare: any;
	aboutExamContent: any;
	isArticle: any;

	ngOnInit() {
		// subscribe to router event
		this.activatedRoute.params.subscribe((params: Params) => {
			this.selectedId = params['title'];
		});
		this.apiUrl = this.apiUrl + this.selectedId;
		this.tabUrl = this.tabUrl + this.selectedId;
		// this.whatsAppUrl = this.whatsAppUrl + this.selectedId;
		this.getPageData()
		this.getTabData()
		// this.getWhatsAppUrl()
		this.selectedId = this.selectedId.split(' ').join('-');
	}

	shareClicked(){
		if(this.isClickedShare){
			this.isClickedShare = false;
		}else{
			this.isClickedShare = true;
		}
	}

	constructor(private http: HttpClient, private activatedRoute: ActivatedRoute, private router: Router, meta: Meta, title: Title,
		private headerService: HeaderService, private sanitizer: DomSanitizer) {
		this.router.routeReuseStrategy.shouldReuseRoute = function () {
			return false;
		};
		this.title = title;
		this.meta = meta;
	}

	openNextPage(isChild, metaTitle) {
			if (isChild) {
				this.router.navigateByUrl('/detail-list/' + metaTitle)
			} else if(metaTitle.includes('book')){
				this.router.navigateByUrl('/study-met/' + metaTitle)
			}
			else {
				this.router.navigateByUrl('/detail/' + metaTitle);
			}
	}

	openPageFromNav(pageId, index) {
		if (index == 0) {
			this.router.navigateByUrl('#');
		}
		else {
			this.router.navigateByUrl('/detail-list/' + pageId)
		}
	}

	getData() {
		return this.http.get<any>(this.apiUrl)
	}

	getPageData() {
		this.getData().subscribe(data => {
			this.pageData = data
			if (this.pageData.data.isChild) {
				this.nav = String(this.pageData.data.pagePath).split("#");
				this.navIds = String(this.pageData.data.pagePathId).split("#");
				this.getKeys(this.pageData.data.groupedPageItems);
				this.aboutExamContent = this.sanitizer.bypassSecurityTrustHtml(this.pageData.data.article);
				this.isArticle = this.pageData.data.article;
				this.setTitleMeta();
			} else {
				this.openNextPage(false, this.pageData.data.cat);
			}
		})
	}

	getSvgs(urlSvgs : string){
		if(urlSvgs.includes(".png")){
			return false
		}else
			true
	}

	getWhatsAppUrl() {
		this.getWhatsAppData().subscribe(data => {
			this.whatsAppData = data.data
		})
	}

	getWhatsAppData() {
		return this.http.get<any>(this.whatsAppUrl)
	}

	getTData() {
		return this.http.get<any>(this.tabUrl)
	}

	getTabData() {
		this.getTData().subscribe(data => {
			if(data){
				this.headerService.setRelatedTopics(data.data.tabs);
			}
		})
	}

	getKeys(obj) {
		var r = []
		for (var k in obj) {
			if (!obj.hasOwnProperty(k))
				continue
			r.push(k)
		}
		this.keys = r;
	}

	setTitleMeta() {
		this.title.setTitle(this.pageData.data.tag);
		this.meta.addTags([
			{ name: 'author', content: 'finalrevise.com' },
			{ name: 'keywords', content: this.pageData.data.keywords },
			{
				name: 'description', content: this.pageData.data.description
			}
		]);
	}

	replaceAll(str, replacement) {
		str = str.split('(').join('')
		str = str.split(')').join('')
		str = str.split('.').join('')
		str = str.split('/').join('')
		str = str.split('|').join(':')
		str = str.split(' ').join(replacement)
		str = str.split('---').join(replacement)
		return str.split('--').join(replacement)
	};

	replaceAllSocial(str) {
		var find = '-';
		var re = new RegExp(find, 'g');
		return str.replace(re, ' ');
	}

	openFacebook() {
		window.open("https://www.facebook.com/sharer/sharer.php?u=http://finalrevise.com/detail-list/" + this.selectedId);
	}

	openWhatsApp() {
		window.open("https://api.whatsapp.com/send?text=*Download%20" + this.selectedId+ "*%0a%0a" + "http://finalrevise.com/detail-list/" + this.selectedId +
			"%0a%0a*Download%20FinalRevise%20App%20:%20https://play.google.com/store/apps/details?id%3Dcom.finalrevise.android*")
	}

	openMail() {
		window.open("mailto:?subject=FinalRevise%20Study%20Material%20for%20" + this.selectedId +
			"&body=Download%20" + this.replaceAllSocial(this.selectedId) + "%0a%0a" + "http://finalrevise.com/detail-list/" + this.selectedId +
			"%0a%0a Download FinalRevise App : https://play.google.com/store/apps/details?id=com.finalrevise.android")
	}

	openMessenger() {
			window.open('fb-messenger://share?link=' + encodeURIComponent("http://finalrevise.com/detail-list/" + this.selectedId) + '&app_id='
				+ encodeURIComponent("358637287840510"))
	}

}
