
import 'rxjs/Rx';
import { Component } from '@angular/core';
import { HttpClient} from '@angular/common/http';;
import 'rxjs/Rx';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Meta, Title } from "@angular/platform-browser";
import { isPlatformBrowser } from '@angular/common';
import { DomSanitizer } from '@angular/platform-browser'
import { HeaderService } from '../../app-header/header.service';

@Component({
	selector: 'pdf-viewer-mobile',
	templateUrl: './mobile-pdf-viewer.component.html',
	styleUrls: ['./mobile-pdf-viewer.component.css']
})

export class PdfViewerComponentMobile {
	
	// selectedId: any;
	titleText: any;
	subId: any;
	pdfUrl: any;
	keys: any = [];
	nav: any = [];
	navIds: any = [];
	navCount: any = [];
	title: Title;
	meta: Meta;
	aboutExamContent: any;
	deviceInfo: any = {};
	private tabUrl = 'https://api.finalrevise.com/finalrevise/v1/relatedlinks/';
	tags: any;
	private whatsAppUrl = 'https://api.finalrevise.com/finalrevise/v1/whatsApp/';
	private metaUrl = 'https://api.finalrevise.com/finalrevise/v1/meta/';
	tabs: any;
	isClickedShare: any;
	urlTitle: any;
	groupUrl: any;
	metaData: any;

	transform(url) {
		return this.sanitizer.bypassSecurityTrustResourceUrl(url);
	}

	constructor(private http: HttpClient, private activatedRoute: ActivatedRoute, private router: Router, meta: Meta,
		title: Title, private sanitizer: DomSanitizer, private headerService: HeaderService) {
		this.title = title;
		this.meta = meta;
	}

	ngOnInit() {
		// subscribe to router event
		this.activatedRoute.params.subscribe((params: Params) => {
			this.titleText = params['title'];
			this.urlTitle = params['urlTitle'];
			this.subId = params['subId'];
			this.pdfUrl = "https://drive.google.com/file/d/"+this.subId+"/preview";
		});
		this.tabUrl = this.tabUrl + this.urlTitle;
		this.whatsAppUrl = this.whatsAppUrl + this.urlTitle;
		this.metaUrl = this.metaUrl + this.subId;
		this.getTabData()
		this.getWhatsAppUrl()
		this.getMetaUrl()
	}

	shareClicked(){
		if(this.isClickedShare){
			this.isClickedShare = false;
		}else{
			this.isClickedShare = true;
		}
	}

	getWhatsAppUrl() {
		this.getWhatsAppData().subscribe(data => {
			this.groupUrl = data.data.whatsAppUrl 
		})
	}

	getWhatsAppData() {
		return this.http.get<any>(this.whatsAppUrl)
	}

	getTData() {
		return this.http.get<any>(this.tabUrl)
	}

	getTabData(){
		this.getTData().subscribe(data => {
			this.tags = data.data.related
			if(data){
				this.headerService.setRelatedTopics(data);
			}
		})
	}

	openTag(tab) {
		if (tab.isChild == 1) {
			this.router.navigateByUrl('/detail-list/' + tab.linkTitle)
		}
		else {
			if (tab.linkTitle.includes('book')) {
				this.router.navigateByUrl('/study-met/' + tab.linkTitle)
			}else{
				this.router.navigateByUrl('/detail/' + tab.linkTitle);
			}
		}
	}

	getMetaUrl() {
		this.getMetaData().subscribe(data => {
			this.metaData = data
			this.setTitleMeta()
		})
	}

	getMetaData() {
		return this.http.get<any>(this.metaUrl)
	}

	replaceAllSocial(str) {
		var find = '-';
		var re = new RegExp(find, 'g');
		return str.replace(re, ' ');
	}

	openFacebook() {
		window.open("https://www.facebook.com/sharer/sharer.php?u=http://finalrevise.com/detail/" + this.titleText);
	}

	openWhatsApp() {
		window.open("https://api.whatsapp.com/send?text=*Download%20" + this.titleText + "*%0a%0a" + "http://finalrevise.com/detail/" + this.titleText + "/" + 
		"%0a%0a*Download%20FinalRevise%20App%20:%20https://play.google.com/store/apps/details?id%3Dcom.finalrevise.android*")
	}

	openMail() {
		window.open("mailto:?subject=FinalRevise%20Study%20Material%20for%20" + this.nav[this.navCount - 1] +
			"&body=Download%20" + this.replaceAllSocial(this.titleText) + "%0a%0a" + "http://finalrevise.com/detail/" + this.titleText+
			"%0a%0a Download FinalRevise App : https://play.google.com/store/apps/details?id=com.finalrevise.android")
	}

	openMessenger() {
		if (isPlatformBrowser) {
			window.open("https://m.me/finalrevise?u=http://finalrevise.com/detail/" + this.titleText);
		} else {
			window.open('fb-messenger://share?link=' + encodeURIComponent("http://finalrevise.com/detail/" + this.titleText) + '&app_id='
				+ encodeURIComponent("358637287840510"))
		}
	}

	setTitleMeta() {
		this.title.setTitle(this.metaData.title);
		this.meta.addTags([
			{ name: 'author', content: 'finalrevise.com' },
			{ name: 'keywords', content: this.metaData.metaKeywords },
			{
				name: 'description', content: this.metaData.metaDesc
			}
		]);
	}

	replaceAll(str, replacement) {
		str = str.split('(').join('')
		str = str.split(')').join('')
		str = str.split('.').join('')
		str = str.split('/').join('')
		str = str.split('|').join(':')
		str = str.split(' ').join(replacement)
		str = str.split('---').join(replacement)
		return str.split('--').join(replacement)
	};
}

