import { Injectable } from '@angular/core';

@Injectable() 
export class CounterService {
	count: number = 0;

	public getCount() : number{
		let temp = parseInt(localStorage.getItem('count'));
		return  temp? temp : 0;
	}

	public addCount(){
		let temp = parseInt(localStorage.getItem('count'));
		this.count = temp? temp : 0;
		this.count = this.count + 1;
		localStorage.setItem('count', ""+this.count);
	}

}