import { Component } from '@angular/core';
import 'rxjs/Rx';
import { Meta, Title } from "@angular/platform-browser";

@Component({
	selector: 'about',
	templateUrl: './about.component.html',
	styleUrls: ['./about.component.css']
})
export class AboutComponent {
	constructor(meta: Meta, title: Title) {
		title.setTitle("FinalRevise-Previous Year questions/notes/study material/placement & interview question");

		meta.addTags([
			{ name: 'author', content: 'finalrevise.com' },
			{ name: 'keywords', content: 'State and Centre Boards Previous Year question Papers, Competitive Exams Previous Year Papers and answer, Entrance Exams question paper, University Semesters Previous Year question Papers, Companies Placement Papers download, Companies Interview Questions answer, International Exams Paper' },
			{ name: 'description', content: 'Get study material for competitive exams, universities semesters, international competitive exam, school board, government job papers and placement papers.' }
		]);
	}
}
