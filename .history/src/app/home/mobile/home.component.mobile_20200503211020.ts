import { Component, OnInit } from '@angular/core';
import 'rxjs/Rx';
import { FormGroup, FormControl } from '@angular/forms';
import { Meta, Title } from "@angular/platform-browser";
import { HeaderService } from '../../app-header/header.service';
import { Router} from '@angular/router';

@Component({
	selector: 'home-mobile',
	templateUrl: './home.component.mobile.html',
	styleUrls: ['./home.component.mobile.scss']
})

export class HomeComponentMobile implements OnInit{

	homePageData: any = {};
	title = 'FinalRevise';

	signupfrm = new FormGroup({
		name: new FormControl()
	});

	constructor(meta: Meta, title: Title, private router: Router,private headerService: HeaderService) {
		title.setTitle(this.title + " - Previous Year Papers/Notes/Books/Study material");

		meta.addTags([
			{ name: 'author', content: 'finalrevise.com' },
			{ name: 'keywords', content: 'State and Centre Boards Previous Year Papers, Competitive Exams Previous Year Papers, Entrance Exams Previous Year Papers, University Semesters Previous Year Papers, Companies Placement Papers, Companies Interview Questions, International Competition Papers, International Exams Papers' },
			{ name: 'description', content: 'All Previous Year Question Papers for Different Competitive Exams, Universities semseters, International Exam Papers, Company interview questions, School Borad exams, Government Jobs Papers' }
		]);
	}

	ngOnInit(): void {
		this.headerService.setIsHome(true);
	}

	getSearchResult() {
		this.router.navigateByUrl('/search/' + this.signupfrm.value.name + '/0' +"/0")
	}

	openNextPage(titleText){
		this.router.navigateByUrl('/subcat/' + this.replaceAll(titleText, '-'))
	}

	replaceAll(str, replacement) {
    	return str.split(' ').join(replacement)
	};

	scrollToPrevious() {
		var elmnt = document.getElementById("previous");
		elmnt.scrollIntoView();
	}
}