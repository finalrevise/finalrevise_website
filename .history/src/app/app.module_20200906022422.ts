import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AppRoutingModule} from './app.routing.module';
import {AboutComponent} from './about/about.component';
import {CoreModule} from './core.module';
import {MatDialogModule} from '@angular/material/dialog';
import { AppHeaderDesktopComponent } from './app-header/app-header.component';
import { HomeComponentDesktop } from './home/desktop/home.component.desktop';
import { HomeComponentMobile } from './home/mobile/home.component.mobile';
import { HttpModule } from '@angular/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { FooterComponent } from './footer/footer.component';
import { SubcatDesktopComponent } from './subcategory/desktop/subcat-desktop.component';
import { SubcatComponentMobile } from './subcategory/mobile/subcat-mobile.component';
import { HeaderService } from './app-header/header.service';
import { MatMenuModule } from '@angular/material/menu';
// import { MatIconModule } from '@angular/material/icon';
// import { MatToolbarModule } from '@angular/material/toolbar';
import { PdfViewerComponentMobile } from './pdf-viewer/mobile/mobile-pdf-viewer.component';
import { StudyMetDesktopComponent } from './study-met/desktop/desktop-study-met.component';
import {DialogContentDialog, StudyMetMobileComponent} from './study-met/mobile/mobile-study-met.component';
import { DetailComponentDesktop } from './detail/desktop/desktop-detail.component';
import { DetailListDesktopComponent } from './detail-list/desktop/detail-list-desktop.component';
import {DialogContentExampleDialog} from './detail/mobile/mobile-detail.component';
import { NgxMasonryModule } from 'ngx-masonry';
// import { DeviceDetectorModule } from 'ngx-device-detector';
import { DetailComponentMobile } from './detail/mobile/mobile-detail.component';
import { PdfViewerComponentDesktop } from './pdf-viewer/desktop/desktop-pdf-viewer.component';
import { DetailListMobileComponent } from './detail-list/mobile/detail-list-mobile.component';
import { CustomUrlSerializer } from './CustomUrlSerializer';
import { UrlSerializer } from '@angular/router';
import { ContactComponent } from './contact/contact.component';
import { ServicesComponent } from './our-services/services.component';
import { SearchComponent } from './search/search.component';
import { UploadFileService } from './footer/fileUpload.service';
// import { AngularSvgIconModule } from 'angular-svg-icon';
import { MatCardModule } from '@angular/material/card';

@NgModule({
  declarations: [
    AppComponent,
    AboutComponent,
    AppHeaderDesktopComponent,
    HomeComponentDesktop,
    HomeComponentMobile,
    FooterComponent,
    SubcatDesktopComponent,
    SubcatComponentMobile,
    PdfViewerComponentMobile,
    StudyMetDesktopComponent,
    DialogContentDialog,
    DetailComponentDesktop,
    DetailComponentMobile,
    DetailListDesktopComponent,
    DialogContentExampleDialog,
    PdfViewerComponentDesktop,
    DetailListMobileComponent,
    AboutComponent,
    ContactComponent,
    ServicesComponent,
    SearchComponent,
    StudyMetMobileComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'angular-multi-view' }),
    BrowserAnimationsModule,
    CoreModule,
    AppRoutingModule,
    HttpModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    // MatToolbarModule,
    MatMenuModule,
    // MatIconModule,
    MatDialogModule,
    NgxMasonryModule,
    MatCardModule,
    // AngularSvgIconModule.forRoot(),
    // DeviceDetectorModule.forRoot()
  ],
  entryComponents: [
    HomeComponentMobile,
    SubcatComponentMobile,
    DialogContentExampleDialog, 
    DialogContentDialog,
    DetailComponentMobile,
    PdfViewerComponentMobile,
    DetailListMobileComponent,
    StudyMetMobileComponent
  ],
  bootstrap: [
    AppComponent
  ],
  providers: [HeaderService,UploadFileService,
    { provide: UrlSerializer, useClass: CustomUrlSerializer }
  ]
})
export class AppModule {
}
