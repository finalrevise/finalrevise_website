import { NgModule } from '@angular/core';
import { PreloadAllModules, Router, RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './about/about.component';
import { ApplicationStateService } from './application-state.service';
import { HomeComponentDesktop } from './home/desktop/home.component.desktop';
import { HomeComponentMobile } from './home/mobile/home.component.mobile';
import { SubcatDesktopComponent } from './subcategory/desktop/subcat-desktop.component';
import { SubcatComponentMobile } from './subcategory/mobile/subcat-mobile.component';
import { PdfViewerComponentMobile } from './pdf-viewer/mobile/mobile-pdf-viewer.component';
import { DetailComponentDesktop } from './detail/desktop/desktop-detail.component';
import { DetailListDesktopComponent } from './detail-list/desktop/detail-list-desktop.component';
import { DetailComponentMobile } from './detail/mobile/mobile-detail.component';
import { PdfViewerComponentDesktop } from './pdf-viewer/desktop/desktop-pdf-viewer.component';
import { DetailListMobileComponent } from './detail-list/mobile/detail-list-mobile.component';
import { ContactComponent } from './contact/contact.component';
import { ServicesComponent } from './our-services/services.component';
import { SearchComponent } from './search/search.component';
import { StudyMetDesktopComponent } from './study-met/desktop/desktop-study-met.component';
import { StudyMetMobileComponent } from './study-met/mobile/mobile-study-met.component';
import { NotFoundComponent } from './notfound/not-found.component';

const desktop_routes: Routes = [
  {
    path: '', component: HomeComponentDesktop
  },
  { path: 'about', component: AboutComponent },
  { path: 'home', component: HomeComponentDesktop },
  { path: 'subcat/:title', component: SubcatDesktopComponent },
  {
    path: "detail-list/:title",
    component: DetailListDesktopComponent,
    pathMatch: 'full'
  },
  {
    path: "detail/:title",
    component: DetailComponentDesktop,
    pathMatch: 'full'
  },
  {
    path: "pdf-viewer/:urlTitle/:title/:subId",
    component: PdfViewerComponentDesktop
  },
  {
    path: "about",
    component: AboutComponent
  },
  {
    path: "contact",
    component: ContactComponent
  },
  {
    path: "our-services",
    component: ServicesComponent
  },
  {
    path: "search/:text/:count/:type",
    component: SearchComponent,
  },
  {
    path: "study-met/:title",
    component: StudyMetDesktopComponent
  },
  // directs all other routes to the main page
  {
    path: 'not-found',
    loadChildren: './routes/not-found/not-found.module#NotFoundModule'
  },
  {
    path: '**',
    redirectTo: 'not-found'
  }
];

const mobile_routes: Routes = [
  { path: '', component: HomeComponentMobile },
  { path: 'about', component: AboutComponent },
  { path: 'home', component: HomeComponentMobile },
  { path: 'subcat/:title', component: SubcatComponentMobile },
  {
    path: "detail-list/:title",
    component: DetailListMobileComponent,
    pathMatch: 'full'
  },
  {
    path: "detail/:title",
    component: DetailComponentMobile,
    pathMatch: 'full'
  },
  {
    path: "pdf-viewer/:urlTitle/:title/:subId",
    component: PdfViewerComponentMobile
  },
  {
    path: "about",
    component: AboutComponent
  },
  {
    path: "contact",
    component: ContactComponent
  },
  {
    path: "our-services",
    component: ServicesComponent
  },
  {
    path: "search/:text/:count/:type",
    component: SearchComponent,
  },
  {
    path: "study-met/:title",
    component: StudyMetMobileComponent
  },
  // directs all other routes to the main page
  {
    path: 'not-found',
    loadChildren: './routes/not-found/not-found.module#NotFoundModule'
  },
  {
    path: '**',
    redirectTo: 'not-found'
  }
];

@NgModule({
  // as default we set the desktop routing configuration. if mobile will be started it will be replaced below.
  // note that we must specify some routes here (not an empty array) otherwise the trick below doesn't work...
  imports: [RouterModule.forRoot(desktop_routes, { preloadingStrategy: PreloadAllModules, scrollPositionRestoration: 'enabled' })],
  exports: [RouterModule]
})
export class AppRoutingModule {

  public constructor(private router: Router,
    private applicationStateService: ApplicationStateService) {

    if (applicationStateService.getIsMobileResolution()) {
      router.resetConfig(mobile_routes);
    }
  }

  /**
   * this function inject new routes for the given module instead the current routes. the operation happens on the given current routes object so after
   * this method a call to reset routes on router should be called with the the current routes object.
   * @param currentRoutes
   * @param routesToInject
   * @param childNameToReplaceRoutesUnder - the module name to replace its routes.
   */
  private injectModuleRoutes(currentRoutes: Routes, routesToInject: Routes, childNameToReplaceRoutesUnder: string): void {
    for (let i = 0; i < currentRoutes.length; i++) {
      if (currentRoutes[i].loadChildren != null &&
        currentRoutes[i].loadChildren.toString().indexOf(childNameToReplaceRoutesUnder) != -1) {
        // we found it. taking the route prefix
        let prefixRoute: string = currentRoutes[i].path;
        // first removing the module line
        currentRoutes.splice(i, 1);
        // now injecting the new routes
        // we need to add the prefix route first
        this.addPrefixToRoutes(routesToInject, prefixRoute);
        for (let route of routesToInject) {
          currentRoutes.push(route);
        }
        // since we found it we can break the injection
        return;
      }

      if (currentRoutes[i].children != null) {
        this.injectModuleRoutes(currentRoutes[i].children, routesToInject, childNameToReplaceRoutesUnder);
      }
    }
  }

  private addPrefixToRoutes(routes: Routes, prefix: string) {
    for (let i = 0; i < routes.length; i++) {
      routes[i].path = prefix + '/' + routes[i].path;
    }
  }
}


