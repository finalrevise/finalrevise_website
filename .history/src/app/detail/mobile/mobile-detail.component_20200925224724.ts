import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { HttpClient} from '@angular/common/http';;
import 'rxjs/Rx';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Meta, Title } from "@angular/platform-browser";
import { isPlatformBrowser } from '@angular/common';
import { DomSanitizer } from '@angular/platform-browser'
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CounterService } from '../data.service';
import { TimerObservable } from "rxjs/observable/TimerObservable";
import { Subscription } from "rxjs";
import { HeaderService } from '../../app-header/header.service';
import { DeviceDetectorService } from 'ngx-device-detector';

@Component({
	selector: 'app-detail-mobile',
	templateUrl: './mobile-detail.component.html',
	styleUrls: ['./mobile-detail.component.css'],
	providers: [CounterService]
})
export class DetailComponentMobile implements OnInit {

	private apiUrl = 'https://api.finalrevise.com/finalrevise/v1/content/';
	pageData: any = {};
	keys: any = [];
	nav: any = [];
	navIds: any = [];
	navCount: any = [];
	title: Title;
	meta: Meta;
	aboutExamContent: any;
	titleText: any;
	private tabUrl = 'https://api.finalrevise.com/finalrevise/v1/relatedlinks/';
	tags: any;
	tabs: any;
	private whatsAppUrl = 'https://api.finalrevise.com/finalrevise/v1/whatsApp/';
	isClickedShare: any;
	groupUrl: any;
	linkedPageItems: any;
	pageTitle: any;
	deviceInfo: any = {};
	dujat: any;

	constructor(private http: HttpClient, private activatedRoute: ActivatedRoute, private router: Router, meta: Meta,
		title: Title, private sanitizer: DomSanitizer, public dialog: MatDialog, private deviceService: DeviceDetectorService,
		public dataService: CounterService, private headerService: HeaderService) {
		this.title = title;
		this.meta = meta;
	}

	ngOnInit() {
		// subscribe to router event
		this.activatedRoute.params.subscribe((params: Params) => {
			this.titleText = params['title'];
		});
		this.apiUrl = this.apiUrl + this.titleText;
		this.tabUrl = this.tabUrl + this.titleText;
		this.whatsAppUrl = this.whatsAppUrl + this.titleText.split('/').join('-');
		this.getPageData()
		this.getTabData()
		this.getWhatsAppUrl()
	}

	shareClicked() {
		if (this.isClickedShare) {
			this.isClickedShare = false;
		} else {
			this.isClickedShare = true;
		}
	}

	getData() {
		return this.http.get<any>(this.apiUrl)
	}

	getTData() {
		return this.http.get<any>(this.tabUrl)
	}

	getTabData() {
		this.getTData().subscribe(data => {
			this.tags = data.data.related
			this.tabs = data
			this.headerService.setRelatedTopics(this.tabs.data.tabs);
		})
	}

	getWhatsAppUrl() {
		this.getWhatsAppData().subscribe(data => {
			this.groupUrl = data.data.whatsAppUrl
		})
	}

	getWhatsAppData() {
		return this.http.get<any>(this.whatsAppUrl)
	}

	getPageData() {
		this.getData().subscribe(data => {
			this.pageData = data
			this.linkedPageItems = this.pageData.data.linkedPageItems;
			this.aboutExamContent = this.sanitizer.bypassSecurityTrustHtml(this.pageData.data.pageIntro);
			this.pageTitle = this.pageData.data.pageTitle
			// For DU-JAT only
			if (this.pageTitle.includes('DU-JAT')) {
				this.dujat = true
			}
			this.setTitleMeta();
		})
	}

	openNextPage(link, paperName) {
		this.deviceInfo = this.deviceService.getDeviceInfo();
		paperName = this.replaceAllString(paperName, ",", " ")
		paperName = this.replaceAllString(paperName, "(", " ")
		paperName = this.replaceAllString(paperName, ")", " ")
		paperName = this.replaceAllString(paperName, "?", " ")
		this.dataService.count = this.dataService.getCount();
		this.dataService.addCount();
		// console.log(this.dataService.count);
		if (this.dataService.count % 3 != 0 && this.dataService.count != 0) {
			this.openPdfViewer(link, paperName)
		} else if ("ANDROID" == (this.deviceInfo.os).toUpperCase()) {
			this.openDialog(link, this.titleText, paperName);
		} else {
			this.openPdfViewer(link, paperName)
		}
	}

	openPdfViewer(link, paperName) {
		if (link.includes('open')) {
			this.router.navigateByUrl('/pdf-viewer/' + this.titleText + "/" + paperName + "/" + link.substring(link.indexOf("?id=") + 4));
		}
		else if (link.includes("/view")) {
			this.router.navigateByUrl('/pdf-viewer/' + this.titleText + "/" + paperName + "/" + link.replace("/view", ").substring(link.indexOf("/d/") + 3));
		}
		else {
			window.open(link)
		}
	}

	replaceAllString(str, find, replace) {
		return str.split(find).join(replace);
	}

	openTag(tab) {
		if (tab.isChild == 1) {
			this.router.navigateByUrl('/detail-list/' + tab.linkTitle)
		}
		else {
			if (tab.linkTitle.includes('book')) {
				this.router.navigateByUrl('/study-met/' + tab.linkTitle)
			}else{
				this.router.navigateByUrl('/detail/' + tab.linkTitle);
			}
		}
	}

	replaceAllSocial(str) {
		var find = '-';
		var re = new RegExp(find, 'g');
		return str.replace(re, ' ');
	}

	openFacebook() {
		window.open("https://www.facebook.com/sharer/sharer.php?u=http://finalrevise.com/detail/" + this.titleText);
	}

	openWhatsApp() {
		window.open("https://api.whatsapp.com/send?text=*Download%20" + this.titleText + "*%0a%0a" + "http://finalrevise.com/detail/" + this.titleText +
			"%0a%0a*Download%20FinalRevise%20App%20:%20https://play.google.com/store/apps/details?id%3Dcom.finalrevise.android*")
	}

	openMail() {
		window.open("mailto:?subject=FinalRevise%20Study%20Material%20for%20" + this.nav[this.navCount - 1] +
			"&body=Download%20" + this.replaceAllSocial(this.titleText) + "%0a%0a" + "http://finalrevise.com/detail/" + this.titleText +
			"%0a%0a Download FinalRevise App : https://play.google.com/store/apps/details?id=com.finalrevise.android")
	}

	openMessenger() {
		if (isPlatformBrowser) {
			window.open("https://m.me/finalrevise?u=http://finalrevise.com/detail/" + this.titleText);
		} else {
			window.open('fb-messenger://share?link=' + encodeURIComponent("http://finalrevise.com/detail/" + this.titleText) + '&app_id='
				+ encodeURIComponent("358637287840510"))
		}
	}

	openTabPage(pageId, isChild, tag, cat) {
		if (pageId != null) {
			if (cat == 10 && !isChild) {
				window.open('/study-met/' + this.replaceAll(tag, '-') + '/' + pageId, "_self")
			}
			else if (pageId == 1573689600004 || pageId == 10 || pageId == 13 || pageId == 1561798213495
				|| pageId == 14 || pageId == 1561798213494 || pageId == 1561658659435) {
				window.open('/main-cat-list/' + this.replaceAll(tag, '-') + '/' + pageId, "_self")
			}
			else if (cat == 11) {
				window.open('/image-text-list/' + this.replaceAll(tag, '-') + '/' + pageId, "_self")
			}
			else if (isChild && pageId != null) {
				window.open('/detail-list/' + this.replaceAll(tag, '-') + '/' + pageId, "_self")
			}
			else {
				this.router.navigateByUrl('/detail/' + this.replaceAll(tag, '-') + '/' + pageId);
			}
		}
	}

	setTitleMeta() {
		this.title.setTitle(this.pageData.data.tag);
		this.meta.addTags([
			{ name: 'author', content: 'finalrevise.com' },
			{ name: 'keywords', content: this.pageData.data.keywords },
			{
				name: 'description', content: this.pageData.data.description
			}
		]);
	}

	replaceAll(str, replacement) {
		str = str.split('(').join('')
		str = str.split(')').join('')
		str = str.split('.').join('')
		str = str.split('/').join('')
		str = str.split('|').join(':')
		str = str.split(' ').join(replacement)
		str = str.split('---').join(replacement)
		return str.split('--').join(replacement)
	};

	openDialog(link, titleText, paperName) {
		const dialogRef = this.dialog.open(DialogContentExampleDialog, {
			data: {
				link: link,
				titleText: titleText,
				paperName: paperName
			}
		});
	}
}

export interface DialogData {
	link: string;
	titleText: string;
	paperName: string;
}

@Component({
	selector: 'dialog-content-example-dialog',
	templateUrl: 'dialog-app-install.html',
})
export class DialogContentExampleDialog implements OnInit, OnDestroy {

	public tick: Number;
	private subscription: Subscription;
	public isValid: boolean = false;

	ngOnInit() {

	}

	ngOnDestroy() {
		this.subscription.unsubscribe();
	}

	constructor(
		public dialogRef: MatDialogRef<DialogContentExampleDialog>,
		@Inject(MAT_DIALOG_DATA) public data: DialogData) {
		let timer = TimerObservable.create(0, 1000);
		this.subscription = timer.subscribe(t => {
			this.tick = t;
			if (this.tick > 15) {
				this.isValid = true;
			}
		});
	}

	openDialog() {
		this.dialogRef.close()
		this.openPdfViewer(this.data.link, this.data.titleText, this.data.paperName)
		// window.open(this.data.link)
	}

	openPdfViewer(link, titleText, paperName) {
		if (link.includes('open')) {
			window.open('/pdf-viewer/' + titleText + "/" + paperName + "/" + link.substring(link.indexOf("?id=") + 4), "_self");
		}
		else if (link.includes("/view")) {
			window.open('/pdf-viewer/' + titleText + "/" + paperName + "/" + link.replace("/view", ").substring(link.indexOf("/d/") + 3), "_self");
		}
		else {
			window.open(link)
		}
	}

	openFacebook() {
		window.open("https://www.facebook.com/sharer/sharer.php?u=http://finalrevise.com/detail/" + this.data.titleText);
	}

	openWhatsApp() {
		window.open("https://api.whatsapp.com/send?text=*Download%20" + this.data.titleText + "*%0a%0a" + "http://finalrevise.com/detail/" + this.data.titleText +
			"%0a%0a*Download%20FinalRevise%20App%20:%20https://play.google.com/store/apps/details?id%3Dcom.finalrevise.android*")
	}

	openMail() {
		window.open("mailto:?subject=FinalRevise%20Study%20material%20related%20to%20" + this.data.paperName +
			"&body=Download%20all%20Study%20material%20related%20to%20" + this.data.paperName + "%20:%20" +
			this.data.paperName + "%20" + "http://finalrevise.com/detail/" + this.data.titleText)
		this.openDialog();
	}

	openMessenger() {
		window.open('fb-messenger://share?link=' + encodeURIComponent("http://finalrevise.com/detail/" + this.data.titleText) + '&app_id='
			+ encodeURIComponent("358637287840510"))
		this.openDialog();
	}
}
