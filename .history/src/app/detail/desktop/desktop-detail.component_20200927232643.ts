import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';;
import 'rxjs/Rx';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Meta, Title } from "@angular/platform-browser";
import { isPlatformBrowser } from '@angular/common';
import { DomSanitizer } from '@angular/platform-browser';
import { HeaderService } from '../../app-header/header.service';

@Component({
	selector: 'app-detail-desktop',
	templateUrl: './desktop-detail.component.html',
	styleUrls: ['./desktop-detail.component.css']
})
export class DetailComponentDesktop implements OnInit {

	private apiUrl = 'https://api.finalrevise.com/finalrevise/v1/content/';
	pageData: any = {};
	keys: any = [];
	nav: any = [];
	navIds: any = [];
	navCount: number;
	title: Title;
	meta: Meta;
	aboutExamContent: any;
	titleText: any;
	private tabUrl = 'https://api.finalrevise.com/finalrevise/v1/relatedlinks/';
	tags: any;
	private whatsAppUrl = 'https://api.finalrevise.com/finalrevise/v1/whatsApp/';
	groupUrl: any;
	isClickedShare: any;
	linkedPageItems: any;
	pageTitle: any;
	tabs: any;
	dujat: any;

	constructor(private http: HttpClient, private activatedRoute: ActivatedRoute, private router: Router, meta: Meta,
		title: Title, private sanitizer: DomSanitizer, private headerService: HeaderService) {
		this.router.routeReuseStrategy.shouldReuseRoute = function () {
			return false;
		};
		this.title = title;
		this.meta = meta;
	}

	ngOnInit() {
		// subscribe to router event
		this.activatedRoute.params.subscribe((params: Params) => {
			this.titleText = params['title'];
		});
		this.apiUrl = this.apiUrl + this.titleText;
		this.tabUrl = this.tabUrl + this.titleText;
		this.whatsAppUrl = this.whatsAppUrl + this.titleText.split('/').join('-');
		this.getWhatsAppUrl()
		this.getPageData()
		this.getTabData()
	}

	shareClicked() {
		if (this.isClickedShare) {
			this.isClickedShare = false;
		} else {
			this.isClickedShare = true;
		}
	}

	getData() {
		return this.http.get<any>(this.apiUrl)
	}

	getTData() {
		return this.http.get<any>(this.tabUrl)
	}

	getTabData() {
		this.getTData().subscribe(data => {
			this.tags = data.data.related
			this.tabs = data
			this.headerService.setRelatedTopics(this.tabs.data.tabs);
		})
	}

	getWhatsAppUrl() {
		this.getWhatsAppData().subscribe(data => {
			this.groupUrl = data.data.whatsAppUrl
		})
	}

	getWhatsAppData() {
		return this.http.get<any>(this.whatsAppUrl)
	}

	getPageData() {
		this.getData().subscribe(data => {
			console.log(data.meta.statusCode);
			if (data.meta.statusCode == 200) {
				this.pageData = data
				this.nav = String(this.pageData.data.pagePath).split("#");
				this.navCount = this.nav.length;
				this.navIds = String(this.pageData.data.pagePathId).split("#");
				this.aboutExamContent = this.sanitizer.bypassSecurityTrustHtml(this.pageData.data.pageIntro);
				this.linkedPageItems = this.pageData.data.linkedPageItems;
				this.pageTitle = this.pageData.data.pageTitle
				// For DU-JAT only
				if (this.pageTitle.includes('DU-JAT')) {
					this.dujat = true
				}
				this.setTitleMeta();
			} else {
				this.router.navigateByUrl('/404')
			}
		})
	}

	openPageFromNav(pageId, index) {
		if (index == 0) {
			this.router.navigateByUrl('#');
		}
		else {
			this.router.navigateByUrl('/detail-list/' + pageId)
		}
	}

	openNextPage(link, paperName) {
		paperName = this.replaceAllString(paperName, ",", " ")
		paperName = this.replaceAllString(paperName, "(", " ")
		paperName = this.replaceAllString(paperName, ")", " ")
		paperName = this.replaceAllString(paperName, "?", " ")
		this.openPdfViewer(link, paperName)
	}

	openPdfViewer(link, paperName) {
		if (link.includes('open')) {
			this.router.navigateByUrl('/pdf-viewer/' + this.titleText + "/" + paperName + "/" + link.substring(link.indexOf("?id=") + 4));
		}
		else if (link.includes("/view")) {
			this.router.navigateByUrl('/pdf-viewer/' + this.titleText + "/" + paperName + "/" + link.replace("/view", ").substring(link.indexOf("/d/") + 3));
		}
		else {
			window.open(link)
		}
	}

	replaceAllString(str, find, replace) {
		return str.split(find).join(replace);
	}

	openTag(tab) {
		if (tab.isChild == 1) {
			this.router.navigateByUrl('/detail-list/' + tab.linkTitle)
		}
		else {
			if (tab.linkTitle.includes('book')) {
				this.router.navigateByUrl('/study-met/' + tab.linkTitle)
			} else {
				this.router.navigateByUrl('/detail/' + tab.linkTitle);
			}
		}
	}

	replaceAllSocial(str) {
		var find = '-';
		var re = new RegExp(find, 'g');
		return str.replace(re, ' ');
	}

	openFacebook() {
		window.open("https://www.facebook.com/sharer/sharer.php?u=http://finalrevise.com/detail/" + this.titleText);
	}

	openWhatsApp() {
		window.open("https://api.whatsapp.com/send?text=*Download%20" + this.titleText + "*%0a%0a" + "http://finalrevise.com/detail/" + this.titleText +
			"%0a%0a*Download%20FinalRevise%20App%20:%20https://play.google.com/store/apps/details?id%3Dcom.finalrevise.android*")
	}

	openMail() {
		window.open("mailto:?subject=FinalRevise%20Study%20Material%20for%20" + this.nav[this.navCount - 1] +
			"&body=Download%20" + this.replaceAllSocial(this.titleText) + "%0a%0a" + "http://finalrevise.com/detail/" + this.titleText +
			"%0a%0a Download FinalRevise App : https://play.google.com/store/apps/details?id=com.finalrevise.android")
	}

	openMessenger() {
		if (isPlatformBrowser) {
			window.open("https://m.me/finalrevise?u=http://finalrevise.com/detail/" + this.titleText);
		} else {
			window.open('fb-messenger://share?link=' + encodeURIComponent("http://finalrevise.com/detail/" + this.titleText) + '&app_id='
				+ encodeURIComponent("358637287840510"))
		}
	}

	setTitleMeta() {
		this.title.setTitle(this.pageData.data.tag);
		this.meta.addTags([
			{ name: 'author', content: 'finalrevise.com' },
			{ name: 'keywords', content: this.pageData.data.keywords },
			{
				name: 'description', content: this.pageData.data.description
			}
		]);
	}

	replaceAll(str, replacement) {
		str = str.split('(').join('')
		str = str.split(')').join('')
		str = str.split('.').join('')
		str = str.split('/').join('')
		str = str.split('|').join(':')
		str = str.split(' ').join(replacement)
		str = str.split('---').join(replacement)
		return str.split('--').join(replacement)
	};
}
