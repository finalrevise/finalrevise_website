import { Component, OnInit } from '@angular/core';
import 'rxjs/Rx';
import { Meta, Title } from "@angular/platform-browser";

@Component({
	selector: 'contact',
	templateUrl: './contact.component.html',
	styleUrls: ['./contact.component.css']
})

export class ContactComponent {
	
	constructor(meta: Meta, title: Title) {
		title.setTitle("All information about FinalRevise.com");

		meta.addTags([
			{ name: 'author', content: 'finalrevise.com' },
			{ name: 'keywords', content: 'About us, finalrevise' },
			{ name: 'description', content: 'Get all information about finalrevise.com' }
		]);
	}
	
}
