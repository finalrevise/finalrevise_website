import { Component, OnInit } from '@angular/core';
import { Http, Response} from '@angular/http';
import 'rxjs/Rx';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { map } from "rxjs/operators";
import { Observable } from 'rxjs/Observable';
// import {NgbCarouselConfig} from '@ng-bootstrap/ng-bootstrap';

@Component({
	selector: 'our-services',
	templateUrl: './services.component.html',
	styleUrls: ['./services.component.css']
})
export class ServicesComponent {

	event_list = [
		{
		  event: ' Event 1',
		  eventLocation: 'Fox',
		  eventImage: 'https://images.unsplash.com/photo-1563210080-dfe35c83e2eb?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjExMDk0fQ&auto=format&fit=crop&w=500&q=60',
		  eventDescription: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
		  eventStartDate: new Date(),
		  eventEndingDate: new Date()
		},
		{
		  event: ' Event 2',
		  eventLocation: 'Cat',
		  eventImage: 'https://images.unsplash.com/photo-1564012948891-27965af38a81?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60',
		  eventDescription: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
		  eventStartDate: new Date(),
		  eventEndingDate: new Date()
		},
		{
		  event: ' Event 3',
		  eventLocation: 'Monkey',
		  eventImage: "https://images.unsplash.com/photo-1563049009-2fc38eb88540?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60",
		  eventDescription: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\' standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
		  eventStartDate: new Date(),
		  eventEndingDate: new Date()
		}
	  ];

	current_events =  this.event_list.filter( event => (event.eventStartDate >= new Date() && (event.eventEndingDate <= new Date())));

	constructor() {
		// customize default values of carousels used by this component tree
		// config.showNavigationArrows = false;
		// config.wrap = true;
		// config.showNavigationIndicators = true;
	  }

}