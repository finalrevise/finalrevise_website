(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/about/about.component.html":
/*!**********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/about/about.component.html ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"main-content\">\n\t<div class=\"jumbotron\">\n\t\t<h3>Who we are</h3>\n\t\t<p>\n\t\t\tFinalRevise is the website made to help the students while preparing for their exams. It's having variety of\n\t\t\tsections, covering\n\t\t\talmost all type of exams.\n\t\t</p>\n\t\t<p>\n\t\t\tWe are constantly adding new papers on our platform. Still if you don't find any paper which you need, email\n\t\t\tus :\n\t\t\t<a style=\"margin-left: 2px;\" href=\"mailto:admin@finalrevise.com\">admin@finalrevise.com</a>\n\t\t</p>\n\t\t<p>\n\t\t\tWe are always happy to help you. Stay connected. Happy to hear your suggestions.\n\t\t</p>\n\t\t<br>\n\t\t<h3>Disclaimer</h3>\n\t\t<br>\n\t\t<h4>Use of information</h4>\n\t\t<p>The materials and information provided on this website are for reference purposes only. For official\n\t\t\tinformation please\n\t\t\trefere to the official website or contact the organization.</p>\n\t\t<br>\n\t\t<h4>No warrants as to accuracy</h4>\n\t\t<p>Information may, as a result of the passage of time, change due to law, human error and/or system failure and\n\t\t\tbecome no\n\t\t\tlonger accurate. While we strive for accuracy, FinalRevise makes no warranty as to the accuracy of any\n\t\t\tinformation contained\n\t\t\tin and/or on this website.</p>\n\t\t<p>Any link found anywhere in and / or on this website could be inaccurate or misdirected and FinalRevise will\n\t\t\tnot be liable\n\t\t\tfor any use of these informatio or links in any manner whatsoever and this entire disclaimer applies to\n\t\t\tthose information\n\t\t\t&#038; links.</p>\n\t\t<br>\n\t\t<h4>Make no action or ommission</h4>\n\t\t<p>Anyone using this website should not take any action and/or omit to take any action based in whole or in part\n\t\t\ton information\n\t\t\tfound on this website without first seeking the advice of the actual orgainzation other appropriate advisor.\n\t\t</p>\n\t\t<br>\n\t\t<h4>No transmittal of confidential information</h4>\n\t\t<p>This website shall not be used to transmit confidential information, personal information or information\n\t\t\tsubject to individual\n\t\t\tprivilege.\n\t\t</p>\n\t\t<br>\n\t\t<h4>Disclaimer of and limit of liability</h4>\n\t\t<p>FinalRevise does not accept any liability whatsoever for use of this website. Any use of this website is at\n\t\t\tyour own risk.\n\t\t\tInformation contained in and/or on this website is provided on an “as is” and “as available” basis.\n\t\t\tFinalRevise makes no\n\t\t\trepresentations, warranties or conditions of any kind, whether express or implied and including without\n\t\t\tlimiting the generality\n\t\t\tof the foregoing: express or implied representations, warranties or conditions of title, non-infringement,\n\t\t\tmerchantability,\n\t\t\tfitness for particular purpose, performance, durability, availability, timeliness, accuracy or completeness\n\t\t\tall of which\n\t\t\tare hereby disclaimed to the fullest extent permitted by law. FinalRevise will not be liable to you or any\n\t\t\tother person\n\t\t\tin relation to loss of any kind or damages of any kind arising from, in connection with, or in relation to\n\t\t\tthe use of this\n\t\t\twebsite by you or any other person.</p>\n\t</div>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/app-header/app-header.component.html":
/*!********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app-header/app-header.component.html ***!
  \********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"navbar bg-primary fixed-top\">\n\t<ul class=\"list-inline\">\n\t\t<li class=\"list-inline-item\">\n\t\t\t<a routerLink=\"\">\n\t\t\t\t<img class=\"image-style\" alt=\"logo finalrevise finalrevise company\" src=\"/assets/images/logo_r1.png\" />\n\t\t\t</a>\n\t\t</li>\n\t\t<li class=\"list-inline-item\">\n\t\t\t<a class=\"navbar-brand\" routerLink=\"\">FinalRevise</a>\n\t\t</li>\n\t</ul>\n\t<div *ngIf=\"isMobileResolution && !isHome && isRelatedTopicsPresent\">\n\t\t<button class=\"matBtn\" mat-button [matMenuTriggerFor]=\"beforeMenu\">\n\t\t\t\t<img alt=\"finalrevise menu\" height=\"40\" width=\"35\" src=\"/assets/images/menu.png\" alt=\"Card image\">\n\t\t</button>\n\t\t<mat-menu #beforeMenu=\"matMenu\" xPosition=\"before\">\n\t\t\t<button *ngFor=\"let el of relatedTopics;\" mat-menu-item\n\t\t\tstyle=\"color: #2c3e50!important;\" (click)=\"openTabPage(el.isChild, el.linkTitle, el.relatedLinksType)\"><b>{{el.relatedLinksHeading}}</b></button>\n\t\t</mat-menu>\n\t</div>\n\t<div *ngIf=\"!isMobileResolution\">\n\t\t<div *ngIf=\"isRelatedTopicsPresent\">\n\t\t\t<ul class=\"list-inline margin-right\">\n\t\t\t\t<li class=\"list-inline-item\" *ngFor=\"let el of relatedTopics;\">\n\t\t\t\t\t<a class=\"nav-link\" style=\"color: white;\" [routerLink]=\"\"\n\t\t\t\t\t\t(click)=\"openTabPage(el.isChild, el.linkTitle, el.relatedLinksType)\"><b>{{el.relatedLinksHeading}}</b></a>\n\t\t\t\t</li>\n\t\t\t</ul>\n\t\t</div>\n\t</div>\n\t<div *ngIf=\"isHome\">\n\t\t<ul class=\"list-inline margin-right\">\n\t\t\t<li class=\"list-inline-item\">\n\t\t\t\t<a href=\"https://www.linkedin.com/company/finalrevise/\" target=\"_blank\">\n\t\t\t\t\t<img alt=\"finalrevise linkedin account\" border=\"0\" id=\"fixed-width-flamingo\" src=\"/assets/images/linkedin.png\" />\n\t\t\t\t</a>\n\t\t\t</li>\n\t\t\t<li class=\"list-inline-item\">\n\t\t\t\t<a href=\"https://www.youtube.com/channel/UCODEmMV_ka_Xb5hKoTGOEHQ\" target=\"_blank\">\n\t\t\t\t\t<img alt=\"finalrevise youtube channel\" border=\"0\" id=\"fixed-width-flamingo\" src=\"/assets/images/youtube.png\" />\n\t\t\t\t</a>\n\t\t\t</li>\n\t\t\t<li class=\"list-inline-item\">\n\t\t\t\t<a href=\"https://www.facebook.com/finalrevise/\" target=\"_blank\">\n\t\t\t\t\t<img alt=\"finalrevise facebook page\" border=\"0\" id=\"fixed-width-flamingo\" src=\"/assets/images/social_facebook.png\" />\n\t\t\t\t</a>\n\t\t\t</li>\n\t\t\t<li class=\"list-inline-item\">\n\t\t\t\t<a href=\"https://www.instagram.com/finalrevise/\" target=\"_blank\">\n\t\t\t\t\t<img alt=\"finalrevise instagram profile\" border=\"0\" id=\"fixed-width-flamingo\" src=\"/assets/images/instagram.png\" />\n\t\t\t\t</a>\n\t\t\t</li>\n\t\t</ul>\n\t</div>\n\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-header></app-header>\n    <router-outlet>\n    </router-outlet>\n<app-footer></app-footer>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/contact/contact.component.html":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/contact/contact.component.html ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"main-content\">\n\t<div class=\"jumbotron\">\n\t\t<div class=\"grid_4\">\n\t\t\t<div class=\"grid_2 preffix_1\">\n\t\t\t\t<ul class=\"iphone\">\n\t\t\t\t\t<i class=\"phone\"> </i>\n\t\t\t\t\t<li class=\"phone_desc\">\n\t\t\t\t\t\t<b>Phone No :</b> +91 9068758150 </li>\n\t\t\t\t\t<div class=\"clearfix\"> </div>\n\t\t\t\t</ul>\n\t\t\t\t<ul class=\"iphone\">\n\t\t\t\t\t<i class=\"flag\"> </i>\n\t\t\t\t\t<li class=\"phone_desc\">\n\t\t\t\t\t\t<b>Website :</b>\n\t\t\t\t\t\t<a routerLink=\"http://finalrevise.com\"> http://finalrevise.com</a>\n\t\t\t\t\t</li>\n\t\t\t\t\t<div class=\"clearfix\"> </div>\n\t\t\t\t</ul>\n\t\t\t</div>\n\t\t\t<div class=\"grid_3\">\n\t\t\t\t<ul class=\"iphone\">\n\t\t\t\t\t<i class=\"msg\"> </i>\n\t\t\t\t\t<li class=\"phone_desc\">\n\t\t\t\t\t\t<b>Email :</b>\n\t\t\t\t\t\t<a style=\"margin-left: 2px;\" href=\"mailto:admin@finalrevise.com\">admin@finalrevise.com</a>\n\t\t\t\t\t</li>\n\t\t\t\t\t<div class=\"clearfix\"> </div>\n\t\t\t\t</ul>\n\t\t\t\t<ul class=\"iphone\">\n\t\t\t\t\t<i class=\"home\"> </i>\n\t\t\t\t\t<li class=\"phone_desc\">\n\t\t\t\t\t\t<b>Follow us :</b>\n\t\t\t\t\t\t<a href=\"https://www.facebook.com/finalrevise/\" target=\"_blank\">\n\t\t\t\t\t\t\t<img border=\"0\" data-original-height=\"40\" data-original-width=\"40\"\n\t\t\t\t\t\t\t\tsrc=\"/assets/images/social_facebook.png\" style=\"margin-left: 1%;\" />\n\t\t\t\t\t\t</a>\n\t\t\t\t\t\t<a href=\"https://www.instagram.com/finalrevise/\" target=\"_blank\" style=\"margin-left: .5%;\">\n\t\t\t\t\t\t\t<img border=\"0\" data-original-height=\"40\" data-original-width=\"40\"\n\t\t\t\t\t\t\t\tsrc=\"/assets/images/instagram.png\" />\n\t\t\t\t\t\t</a>\n\t\t\t\t\t</li>\n\t\t\t\t</ul>\n\t\t\t\t<ul class=\"iphone\">\n\t\t\t\t\t<a href=\"https://play.google.com/store/apps/details?id=com.finalrevise.android\" target=\"_blank\">\n\t\t\t\t\t\t<img class=\"play-img\" data-original-width=\"40\" src=\"/assets/images/playstore.png\"\n\t\t\t\t\t\t\tstyle=\"margin-left: 1%;\" />\n\t\t\t\t\t</a>\n\t\t\t\t</ul>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/detail-list/desktop/detail-list-desktop.component.html":
/*!**************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/detail-list/desktop/detail-list-desktop.component.html ***!
  \**************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div id=\"loader\" class=\"loader\" *ngIf=\"!pageData.data\"></div>\n<div style=\"background-image: url('assets/images/bg.jpg');\">\n\t<div class=\"main-content\">\n\t\t<div class=\"row\">\n\t\t\t<ol *ngFor=\"let headerNav of nav; let i = index\" [attr.data-index]=\"i\">\n\t\t\t\t<li class=\"row\">\n\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openPageFromNav(navIds[i], i)\"\n\t\t\t\t\t\ttitle=\"{{headerNav}} Study Material\"><b>{{headerNav}}</b></a>\n\t\t\t\t\t\t<div *ngIf=\"i!=navIds.length-1\" style=\"color: #8b0000;\">&nbsp;&nbsp;&gt;</div>\n\t\t\t\t</li>\n\t\t\t</ol>\n\t\t</div>\n\t\t<div *ngIf=\"isArticle\" class=\"jumbotron\" [innerHtml]=\"aboutExamContent\"> </div>\n\t\t<ngx-masonry [updateLayout]=\"updateMasonryLayout\" [options]=\"{ transitionDuration: '0.0s' }\">\n\t\t\t<ngxMasonryItem class=\"masonry-item\" *ngFor=\"let key of keys;\">\n\t\t\t\t<div class=\"card mb-3 bg-light\">\n\t\t\t\t\t<div class=\"card-body\">\n\t\t\t\t\t\t<h4 class=\"card-title\">{{key}}</h4>\n\t\t\t\t\t\t<div *ngFor=\"let el of pageData.data.groupedPageItems[key]; let i=index\" style=\"padding-bottom: 2%;\">\n\t\t\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openNextPage(el.isChild, el.metaTitle)\"\n\t\t\t\t\t\t\t\tstyle=\"text-decoration: none;color: inherit;\">\n\t\t\t\t\t\t\t\t<tr class=\"card-subtitle\" title=\"{{el.tag}}\">\n\t\t\t\t\t\t\t\t\t<td style=\"width: 15%;height: 15%;padding-left: 2%;\">\n\t\t\t\t\t\t\t\t\t\t<!-- <svg-icon src=\"{{el.coverUrlSvg}}\" [svgStyle]=\"{ 'width.px':60, 'height.px':60  }\" *ngIf=\"el.coverUrlSvg\" (load)=\"updateMasonryLayout = !updateMasonryLayout\"></svg-icon>\n\t\t\t\t\t\t\t\t\t\t<svg-icon src=\"/assets/images/logosq_light.png\" *ngIf=\"!el.coverUrl\" (load)=\"updateMasonryLayout = !updateMasonryLayout\"></svg-icon> -->\n\t\t\t\t\t\t\t\t\t\t<img src=\"{{el.coverUrl}}\" *ngIf=\"el.coverUrl\" (load)=\"updateMasonryLayout = !updateMasonryLayout\">\n\t\t\t\t\t\t\t\t\t\t<img src=\"/assets/images/logosq_light.png\" *ngIf=\"!el.coverUrl\" (load)=\"updateMasonryLayout = !updateMasonryLayout\">\n\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t<td style=\"padding-left: 2%;color: #2c3e50!important;\">\n\t\t\t\t\t\t\t\t\t\t{{el.pageItemName}}\n\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</ngxMasonryItem>\n\t\t</ngx-masonry>\n\n\n\t\t<div class=\"sbuttons\">\n\t\t\t<div *ngIf=\"isClickedShare\">\n\t\t\t\t<a [routerLink]=\"\" (click)=\"openWhatsApp()\" class=\"sbutton whatsapp\" tooltip=\"WhatsApp\"><i\n\t\t\t\t\t\tclass=\"fab fa-whatsapp\"></i></a>\n\n\t\t\t\t<a [routerLink]=\"\" (click)=\"openFacebook()\" class=\"sbutton fb\" tooltip=\"Facebook\"><i\n\t\t\t\t\t\tclass=\"fab fa-facebook-f\"></i></a>\n\n\t\t\t\t<a [routerLink]=\"\" (click)=\"openMail()\" class=\"sbutton gplus\" tooltip=\"Mail\"><i\n\t\t\t\t\t\tclass=\"fab fa-google-plus-g\"></i></a>\n\n\t\t\t\t<a [routerLink]=\"\" (click)=\"openMessenger()\" class=\"sbutton twitt\" tooltip=\"Fb messenger\"><i\n\t\t\t\t\t\tclass=\"fab fa-twitter\"></i></a>\n\t\t\t</div>\n\t\t\t<a [routerLink]=\"\" (click)=\"shareClicked()\" class=\"sbutton mainsbutton\"\n\t\t\t\ttooltip=\"Share this page with your friends\"><i class=\"fas fa-share-alt\"></i></a>\n\t\t</div>\n\t</div>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/detail-list/mobile/detail-list-mobile.component.html":
/*!************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/detail-list/mobile/detail-list-mobile.component.html ***!
  \************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div id=\"loader\" class=\"loader\" *ngIf=\"!pageData.data\"></div>\n<div class=\"main-content\">\n\t<h5 class=\"row goBack\" onclick=\"window.history.back()\">\n\t\t<div style=\"margin-top: -.4%;padding-right: 2%;\"><b>&nbsp; &lt;</b></div><b>Back</b>\n\t</h5>\n\t<div *ngIf=\"isArticle\" class=\"jumbotron\" [innerHtml]=\"aboutExamContent\"> </div>\n\t<ngx-masonry [updateLayout]=\"updateMasonryLayout\" [options]=\"{ transitionDuration: '0.0s' }\">\n\t\t<ngxMasonryItem class=\"masonry-item\" *ngFor=\"let key of keys;\">\n\t\t\t<div class=\"card mb-3 bg-light\">\n\t\t\t\t<div class=\"card-body\">\n\t\t\t\t\t<h4 class=\"card-title\">{{key}}</h4>\n\t\t\t\t\t<div *ngFor=\"let el of pageData.data.groupedPageItems[key]; let i=index\">\n\t\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openNextPage(el.isChild, el.metaTitle)\"\n\t\t\t\t\t\t\tstyle=\"text-decoration: none;color: inherit;\">\n\t\t\t\t\t\t\t<tr class=\"card-subtitle\" title=\"{{el.tag}}\">\n\t\t\t\t\t\t\t\t <td style=\"padding-left: 4%;\">\n\t\t\t\t\t\t\t\t\t<!-- <svg-icon src=\"{{el.coverUrlSvg}}\" [svgStyle]=\"{ 'width.px':50, 'height.px':50  }\" *ngIf=\"el.coverUrlSvg\" (load)=\"updateMasonryLayout = !updateMasonryLayout\"></svg-icon>\n\t\t\t\t\t\t\t\t\t\t<svg-icon src=\"/assets/images/logosq_light.png\" *ngIf=\"!el.coverUrlSvg\" (load)=\"updateMasonryLayout = !updateMasonryLayout\"></svg-icon> -->\n\t\t\t\t\t\t\t\t<!-- <img src=\"{{el.coverUrl}}\" *ngIf=\"el.coverUrl\"\n\t\t\t\t\t\t\t\t\t\t(load)=\"updateMasonryLayout = !updateMasonryLayout\">\n\t\t\t\t\t\t\t\t\t<img src=\"/assets/images/logo3.png\" *ngIf=\"!el.coverUrl\"\n\t\t\t\t\t\t\t\t\t\t(load)=\"updateMasonryLayout = !updateMasonryLayout\">  -->\n\t\t\t\t\t\t\t\t\t\t<li></li>\n\t\t\t\t\t\t\t\t</td>  \n\t\t\t\t\t\t\t\t<td style=\"color: #2c3e50!important; width: 100%;\">\n\t\t\t\t\t\t\t\t\t{{el.pageItemName}}\n\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t</a>\n\t\t\t\t\t\t<br>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</ngxMasonryItem>\n\t</ngx-masonry>\n\n\n\t<div class=\"sbuttons\">\n\t\t<div *ngIf=\"isClickedShare\">\n\t\t\t<a [routerLink]=\"\" (click)=\"openWhatsApp()\" class=\"sbutton whatsapp\" tooltip=\"WhatsApp\"><i\n\t\t\t\t\tclass=\"fab fa-whatsapp\"></i></a>\n\n\t\t\t<a [routerLink]=\"\" (click)=\"openFacebook()\" class=\"sbutton fb\" tooltip=\"Facebook\"><i\n\t\t\t\t\tclass=\"fab fa-facebook-f\"></i></a>\n\n\t\t\t<a [routerLink]=\"\" (click)=\"openMail()\" class=\"sbutton gplus\" tooltip=\"Mail\"><i\n\t\t\t\t\tclass=\"fab fa-google-plus-g\"></i></a>\n\n\t\t\t<a [routerLink]=\"\" (click)=\"openMessenger()\" class=\"sbutton twitt\" tooltip=\"Fb messenger\"><i\n\t\t\t\t\tclass=\"fab fa-twitter\"></i></a>\n\t\t</div>\n\t\t<a [routerLink]=\"\" (click)=\"shareClicked()\" class=\"sbutton mainsbutton\"\n\t\t\ttooltip=\"Share this page with your friends\"><i class=\"fas fa-share-alt\"></i></a>\n\t</div>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/detail/desktop/desktop-detail.component.html":
/*!****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/detail/desktop/desktop-detail.component.html ***!
  \****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div id=\"loader\" class=\"loader\" *ngIf=\"!pageData.data\"></div>\n<div style=\"padding-bottom: 5%;\">\n\t<div class=\"main-content\">\n\t\t<div class=\"row\">\n\t\t\t<ol *ngFor=\"let headerNav of nav; let i = index\" [attr.data-index]=\"i\">\n\t\t\t\t<li class=\"row\">\n\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openPageFromNav(navIds[i], i)\"\n\t\t\t\t\t\ttitle=\"{{headerNav}} Study Material\"><b>{{headerNav}}</b></a>\n\t\t\t\t\t<div *ngIf=\"i!=navCount-1\" style=\"color: #008f72;\">&nbsp;&nbsp;&gt;</div>\n\t\t\t\t</li>\n\t\t\t</ol>\n\t\t</div>\n\t\t<hr>\n\t\t<div *ngIf=\"!tags\" style=\"margin-left: 5%; margin-right: 5%;\">\n\t\t\t<table class=\"table1\" *ngIf=\"groupUrl\">\n\t\t\t\t<tr>\n\t\t\t\t\t<td style=\"width: 75%;\">\n\t\t\t\t\t\t<h1 style=\"font-size: 2rem; color: #8b0000; text-shadow: 2px 2px 4px  rgba(0, 0, 0, 0.18);\"\n\t\t\t\t\t\t\ttitle=\"{{titleText}}\">\n\t\t\t\t\t\t\t<b>{{pageTitle}}</b></h1>\n\t\t\t\t\t</td>\n\t\t\t\t\t<td class=\"blink\">\n\t\t\t\t\t\t<a href=\"{{groupUrl}}\" target=\"_blank\">\n\t\t\t\t\t\t\t<img src=\"/assets/images/whatsapp_join.png\">\n\t\t\t\t\t\t</a>\n\t\t\t\t\t</td>\n\t\t\t\t</tr>\n\t\t\t</table>\n\t\t\t<table *ngIf=\"!groupUrl\">\n\t\t\t\t<tr>\n\t\t\t\t\t<td>\n\t\t\t\t\t\t<h1 style=\"font-size: 2rem; color: #8b0000; text-shadow: 2px 2px 4px  rgba(0, 0, 0, 0.18);\"\n\t\t\t\t\t\t\ttitle=\"{{titleText}}\">\n\t\t\t\t\t\t\t<b>{{pageTitle}}</b></h1>\n\t\t\t\t\t</td>\n\t\t\t\t</tr>\n\t\t\t</table>\n\t\t\t<h1 *ngIf=\"dujat\" style=\"font-size: 2rem;\n\t\ttext-shadow: 2px 2px 4px  rgba(0, 0, 0, 0.18); text-align: center;\" title=\"Mock Test Paper DUJAT\">\n\t\t\t\t<a style=\"color: blue;\" href=\"https://forms.gle/LqxtE6LjA1iupdUG6\" target=\"_blank\"><b><u>2 Mock Test of\n\t\t\t\t\t\tDUJAT 2020 in Rs 30 for BMS, BBE and BBA (Click here)</u></b></a></h1>\n\t\t\t<div class=\"jumbotron\">\n\t\t\t\t<div [innerHtml]=\"aboutExamContent\"> </div>\n\t\t\t\t<hr class=\"my-4\">\n\t\t\t\t<div *ngFor=\"let el of linkedPageItems\">\n\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openNextPage(el.pageDocLink, el.pageItemName)\">\n\t\t\t\t\t\t<p>{{el.pageItemName}}</p>\n\t\t\t\t\t</a>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"navbar bg-primary\">\n\t\t\t\t<ul class=\"list-inline\">\n\t\t\t\t\t<li class=\"list-inline-item\">\n\t\t\t\t\t\t<b>\n\t\t\t\t\t\t\t<font size=\"3\" color=\"white\">Share {{pageTitle}} page with your friends :\n\t\t\t\t\t\t\t</font>\n\t\t\t\t\t\t</b>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li class=\"list-inline-item\">\n\t\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openFacebook()\">\n\t\t\t\t\t\t\t<img border=\"0\" id=\"fixed-width-flamingo\" src=\"/assets/images/facebook60.png\" />\n\t\t\t\t\t\t</a>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li class=\"list-inline-item\">\n\t\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openWhatsApp()\">\n\t\t\t\t\t\t\t<img border=\"0\" id=\"fixed-width-flamingo\" src=\"/assets/images/whatsapp60.png\" />\n\t\t\t\t\t\t</a>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li class=\"list-inline-item\">\n\t\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openMail()\">\n\t\t\t\t\t\t\t<img border=\"0\" id=\"fixed-width-flamingo\" src=\"/assets/images/gmail60.png\" />\n\t\t\t\t\t\t</a>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li class=\"list-inline-item\">\n\t\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openMessenger()\">\n\t\t\t\t\t\t\t<img border=\"0\" id=\"fixed-width-flamingo\" src=\"/assets/images/mess60.png\" />\n\t\t\t\t\t\t</a>\n\t\t\t\t\t</li>\n\t\t\t\t</ul>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"row\" style=\"margin-top: 0%\" *ngIf=\"tags\">\n\t\t\t<table class=\"table1\" *ngIf=\"groupUrl\">\n\t\t\t\t<tr>\n\t\t\t\t\t<td style=\"width: 75%;\">\n\t\t\t\t\t\t<h1 style=\"font-size: 2rem; color: #8b0000; text-shadow: 2px 2px 4px  rgba(0, 0, 0, 0.18);\"\n\t\t\t\t\t\t\ttitle=\"{{titleText}}\">\n\t\t\t\t\t\t\t<b>{{pageTitle}}</b></h1>\n\t\t\t\t\t</td>\n\t\t\t\t\t<td class=\"blink\">\n\t\t\t\t\t\t<a href=\"{{groupUrl}}\" target=\"_blank\">\n\t\t\t\t\t\t\t<img class=\"center\" src=\"/assets/images/whatsapp_join.png\">\n\t\t\t\t\t\t</a>\n\t\t\t\t\t</td>\n\t\t\t\t</tr>\n\t\t\t</table>\n\t\t\t<table *ngIf=\"!groupUrl\">\n\t\t\t\t<tr>\n\t\t\t\t\t<td>\n\t\t\t\t\t\t<h1 style=\"font-size: 2rem; color: #8b0000; text-shadow: 2px 2px 4px  rgba(0, 0, 0, 0.18);\"\n\t\t\t\t\t\t\ttitle=\"{{titleText}}\">\n\t\t\t\t\t\t\t<b>{{pageTitle}}</b></h1>\n\t\t\t\t\t</td>\n\t\t\t\t</tr>\n\t\t\t</table>\n\t\t\t<h1 *ngIf=\"dujat\" style=\"font-size: 2rem;\n\t\ttext-shadow: 2px 2px 4px  rgba(0, 0, 0, 0.18); text-align: center;\" title=\"Mock Test Paper DUJAT\">\n\t\t&nbsp;&nbsp;\n\t\t\t\t<a style=\"color: blue;\" href=\"https://forms.gle/LqxtE6LjA1iupdUG6\" target=\"_blank\"><b><u>2 Mock Test of\n\t\t\t\t\t\tDUJAT 2020 in Rs 30 for BMS, BBE and BBA (Click here)</u></b></a></h1>\n\t\t\t<div class=\"col-sm-8\">\n\t\t\t\t<div class=\"jumbotron\" style=\"padding: 1.25rem 1.2rem;\">\n\t\t\t\t\t<div [innerHtml]=\"aboutExamContent\"> </div>\n\t\t\t\t\t<hr class=\"my-4\">\n\t\t\t\t\t<div *ngFor=\"let el of linkedPageItems\">\n\t\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openNextPage(el.pageDocLink, el.pageItemName)\">\n\t\t\t\t\t\t\t<p>{{el.pageItemName}}</p>\n\t\t\t\t\t\t</a>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"navbar bg-primary\">\n\t\t\t\t\t<ul class=\"list-inline\">\n\t\t\t\t\t\t<li class=\"list-inline-item\">\n\t\t\t\t\t\t\t<b>\n\t\t\t\t\t\t\t\t<font size=\"3\" color=\"white\">Share {{pageTitle}} page with your friends :\n\t\t\t\t\t\t\t\t</font>\n\t\t\t\t\t\t\t</b>\n\t\t\t\t\t\t</li>\n\t\t\t\t\t\t<li class=\"list-inline-item\">\n\t\t\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openFacebook()\">\n\t\t\t\t\t\t\t\t<img border=\"0\" id=\"fixed-width-flamingo\" src=\"/assets/images/facebook60.png\" />\n\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t</li>\n\t\t\t\t\t\t<li class=\"list-inline-item\">\n\t\t\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openWhatsApp()\">\n\t\t\t\t\t\t\t\t<img border=\"0\" id=\"fixed-width-flamingo\" src=\"/assets/images/whatsapp60.png\" />\n\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t</li>\n\t\t\t\t\t\t<li class=\"list-inline-item\">\n\t\t\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openMail()\">\n\t\t\t\t\t\t\t\t<img border=\"0\" id=\"fixed-width-flamingo\" src=\"/assets/images/gmail60.png\" />\n\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t</li>\n\t\t\t\t\t\t<li class=\"list-inline-item\">\n\t\t\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openMessenger()\">\n\t\t\t\t\t\t\t\t<img border=\"0\" id=\"fixed-width-flamingo\" src=\"/assets/images/mess60.png\" />\n\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t</li>\n\t\t\t\t\t</ul>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"col-sm-4\">\n\t\t\t\t<div class=\"jumbotron\">\n\t\t\t\t\t<h5 style=\"text-align: center; color: #8b0000;\"><b>Related links</b></h5>\n\t\t\t\t\t<hr class=\"my-3\">\n\t\t\t\t\t<table>\n\t\t\t\t\t\t<tr *ngFor=\"let el of tags\">\n\t\t\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openTag(el)\">\n\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t<img src=\"{{el.relatedLinksImgUrl}}\" />\n\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t<td style=\"padding: 2%; text-align: center;\" valign=\"top\">\n\t\t\t\t\t\t\t\t\t<b>\n\t\t\t\t\t\t\t\t\t\t{{el.relatedLinksHeading}}\n\t\t\t\t\t\t\t\t\t</b>\n\t\t\t\t\t\t\t\t\t<br>\n\t\t\t\t\t\t\t\t\t<br>\n\t\t\t\t\t\t\t\t\t{{el.relatedLinksSmallDesc}}\n\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t<hr class=\"my-3\">\n\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t</tr>\n\t\t\t\t\t</table>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\n\n\t\t<div class=\"sbuttons\">\n\t\t\t<div *ngIf=\"isClickedShare\">\n\t\t\t\t<a [routerLink]=\"\" (click)=\"openWhatsApp()\" class=\"sbutton whatsapp\" tooltip=\"WhatsApp\"><i\n\t\t\t\t\t\tclass=\"fab fa-whatsapp\"></i></a>\n\n\t\t\t\t<a [routerLink]=\"\" (click)=\"openFacebook()\" class=\"sbutton fb\" tooltip=\"Facebook\"><i\n\t\t\t\t\t\tclass=\"fab fa-facebook-f\"></i></a>\n\n\t\t\t\t<a [routerLink]=\"\" (click)=\"openMail()\" class=\"sbutton gplus\" tooltip=\"Mail\"><i\n\t\t\t\t\t\tclass=\"fab fa-google-plus-g\"></i></a>\n\n\t\t\t\t<a [routerLink]=\"\" (click)=\"openMessenger()\" class=\"sbutton twitt\" tooltip=\"Fb messenger\"><i\n\t\t\t\t\t\tclass=\"fab fa-twitter\"></i></a>\n\t\t\t</div>\n\t\t\t<a [routerLink]=\"\" (click)=\"shareClicked()\" class=\"sbutton mainsbutton\"\n\t\t\t\ttooltip=\"Share this page with your friends\"><i class=\"fas fa-share-alt\"></i></a>\n\t\t</div>\n\t</div>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/detail/mobile/dialog-app-install.html":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/detail/mobile/dialog-app-install.html ***!
  \*********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<mat-dialog-content class=\"mat-typography\">\n\t<ul class=\"list-inline\">\n\t\t<li class=\"list-inline-item\">\n\t\t\t<h3 mat-dialog-title align=\"center\" style=\"color: black\"><b>Share {{data.paperName}} page with your friends.</b></h3>\n    </li>\n    <div align=\"center\">\n\t\t<li class=\"list-inline-item\">\n\t\t\t<a [routerLink]=\"\" (click)=\"openFacebook()\" >\n\t\t\t\t<img border=\"0\" id=\"fixed-width-flamingo\" style=\"height: 30px; width: 30px\" src=\"/assets/images/facebook60.png\" />\n\t\t\t</a>\n\t\t</li>\n\t\t<li class=\"list-inline-item\">\n\t\t\t<a [routerLink]=\"\" (click)=\"openWhatsApp()\">\n\t\t\t\t<img border=\"0\" id=\"fixed-width-flamingo\" style=\"height: 30px; width: 30px\" src=\"/assets/images/whatsapp60.png\" />\n\t\t\t</a>\n\t\t</li>\n\t\t<li class=\"list-inline-item\">\n\t\t\t<a [routerLink]=\"\" (click)=\"openMail()\">\n\t\t\t\t<img border=\"0\" id=\"fixed-width-flamingo\" style=\"height: 30px; width: 30px\" src=\"/assets/images/gmail60.png\" />\n\t\t\t</a>\n\t\t</li>\n\t\t<li class=\"list-inline-item\">\n\t\t\t<a [routerLink]=\"\" (click)=\"openMessenger()\">\n\t\t\t\t<img border=\"0\" id=\"fixed-width-flamingo\" style=\"height: 30px; width: 30px\" src=\"/assets/images/mess60.png\" />\n\t\t\t</a>\n    </li>\n  </div>\n\t</ul>\n</mat-dialog-content>\n<h2 mat-dialog-title align=\"center\">---- OR ----</h2>\n<mat-dialog-content class=\"mat-typography\">\n    <h4 mat-dialog-title align=\"center\" style=\"color: #2c3e50\"><b>Manage your study material in a easy way</b></h4>\n  <p align=\"justify\">Now you can bookmark and download unlimited papers, notes, books and assignment for free.</p>\n  <p> Download FinalRevise mobile App from Google Play store.</p>\n</mat-dialog-content>\n<p style=\"color: red\">Timer for 15 seconds : {{tick}}</p>\n<mat-dialog-actions align=\"end\">\n  <button [hidden]=\"!isValid\" (click)=\"isValid && openDialog()\" mat-button>Cancel</button>\n  <!-- <button (click)=\"openShare()\" mat-button>Share</button> -->\n  <button mat-button [mat-dialog-close]=\"true\" cdkFocusInitial onclick=\"window.location.href='https://play.google.com/store/apps/details?id=com.finalrevise.android'\"\n    target=\"_blank\">Install</button>\n</mat-dialog-actions>\n\n\n<!-- Copyright 2019 Google Inc. All Rights Reserved.\n    Use of this source code is governed by an MIT-style license that\n    can be found in the LICENSE file at http://angular.io/license -->");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/detail/mobile/mobile-detail.component.html":
/*!**************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/detail/mobile/mobile-detail.component.html ***!
  \**************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div id=\"loader\" class=\"loader\" *ngIf=\"!pageData.data\"></div>\n<div style=\"background-image: url('assets/images/bg.jpg'); padding-bottom: 5%\">\n\t<div class=\"main-content\">\n\t\t<h5 class=\"row goBack\" onclick=\"window.history.back()\">\n\t\t\t<div style=\"margin-top: -.4%;padding-right: 2%;\"><b>&nbsp; &lt;</b></div><b>Back</b>\n\t\t</h5>\n\t\t<hr>\n\t\t<h1 style=\"font-size: 1.6rem; color: #8b0000;\n\t\ttext-shadow: 2px 2px 4px  rgba(0, 0, 0, 0.18); text-align: center;\" title=\"{{titleText}}\">\n\t\t\t<b>{{pageTitle}}</b></h1>\n\t\t<a href=\"{{groupUrl}}\" target=\"_blank\" *ngIf=\"groupUrl\">\n\t\t\t<img class=\"center\" src=\"/assets/images/whatsapp_join.png\">\n\t\t</a>\n\t\t<h1 *ngIf=\"dujat\" style=\"font-size: 1.6rem;\n\t\ttext-shadow: 2px 2px 4px  rgba(0, 0, 0, 0.18); text-align: center;\" title=\"Mock Test Paper DUJAT\">\n\t\t\t<a style=\"color: blue;\" href=\"https://forms.gle/LqxtE6LjA1iupdUG6\" target=\"_blank\"><b><u>2 Mock Test of\n\t\t\t\t\t\tDUJAT 2020 in Rs 30 for BMS, BBE and BBA (Click here)</u></b></a></h1>\n\t\t<div class=\"jumbotron\">\n\t\t\t<div [innerHtml]=\"aboutExamContent\"> </div>\n\t\t\t<hr class=\"my-4\">\n\t\t\t<div *ngFor=\"let el of linkedPageItems\">\n\t\t\t\t<a [routerLink]=\"\" (click)=\"openNextPage(el.pageDocLink, el.pageItemName)\">\n\t\t\t\t\t<p>{{el.pageItemName}}</p>\n\t\t\t\t</a>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"navbar bg-primary\">\n\t\t\t<ul class=\"list-inline\">\n\t\t\t\t<li class=\"list-inline-item\">\n\t\t\t\t\t<b>\n\t\t\t\t\t\t<font size=\"3\" color=\"white\">Share {{pageTitle}} page with your friends :\n\t\t\t\t\t\t</font>\n\t\t\t\t\t</b>\n\t\t\t\t</li>\n\t\t\t\t<li class=\"list-inline-item\">\n\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openFacebook()\">\n\t\t\t\t\t\t<img border=\"0\" id=\"fixed-width-flamingo\" src=\"/assets/images/facebook60.png\" />\n\t\t\t\t\t</a>\n\t\t\t\t</li>\n\t\t\t\t<li class=\"list-inline-item\">\n\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openWhatsApp()\">\n\t\t\t\t\t\t<img border=\"0\" id=\"fixed-width-flamingo\" src=\"/assets/images/whatsapp60.png\" />\n\t\t\t\t\t</a>\n\t\t\t\t</li>\n\t\t\t\t<li class=\"list-inline-item\">\n\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openMail()\">\n\t\t\t\t\t\t<img border=\"0\" id=\"fixed-width-flamingo\" src=\"/assets/images/gmail60.png\" />\n\t\t\t\t\t</a>\n\t\t\t\t</li>\n\t\t\t\t<li class=\"list-inline-item\">\n\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openMessenger()\">\n\t\t\t\t\t\t<img border=\"0\" id=\"fixed-width-flamingo\" src=\"/assets/images/mess60.png\" />\n\t\t\t\t\t</a>\n\t\t\t\t</li>\n\t\t\t</ul>\n\t\t</div>\n\t\t<div class=\"jumbotron\" style=\"margin-top: 5%;\" *ngIf=\"tags\">\n\t\t\t<h5 style=\"text-align: center; color: #8b0000;\"><b>Related links</b></h5>\n\t\t\t<hr class=\"my-3\">\n\t\t\t<table>\n\t\t\t\t<tr *ngFor=\"let el of tags\">\n\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openTag(el)\">\n\t\t\t\t\t\t<!-- <td>\n\t\t\t\t\t\t\t<img src=\"{{el.relatedLinksImgUrl}}\" />\n\t\t\t\t\t\t</td> -->\n\t\t\t\t\t\t<td style=\"padding: 2%; text-align: center;\" valign=\"top\">\n\t\t\t\t\t\t\t<b style=\"color: #2c3e50;\">\n\t\t\t\t\t\t\t\t<u>\n\t\t\t\t\t\t\t\t\t{{el.relatedLinksHeading}}\n\t\t\t\t\t\t\t\t</u>\n\t\t\t\t\t\t\t</b>\n\t\t\t\t\t\t\t<br>\n\t\t\t\t\t\t\t<br>\n\t\t\t\t\t\t\t{{el.relatedLinksSmallDesc}}\n\t\t\t\t\t\t</td>\n\t\t\t\t\t\t<hr class=\"my-3\">\n\t\t\t\t\t</a>\n\t\t\t\t</tr>\n\t\t\t</table>\n\t\t</div>\n\n\t\t<div class=\"sbuttons\">\n\t\t\t<div *ngIf=\"isClickedShare\">\n\t\t\t\t<a [routerLink]=\"\" (click)=\"openWhatsApp()\" class=\"sbutton whatsapp\" tooltip=\"WhatsApp\"><i\n\t\t\t\t\t\tclass=\"fab fa-whatsapp\"></i></a>\n\n\t\t\t\t<a [routerLink]=\"\" (click)=\"openFacebook()\" class=\"sbutton fb\" tooltip=\"Facebook\"><i\n\t\t\t\t\t\tclass=\"fab fa-facebook-f\"></i></a>\n\n\t\t\t\t<a [routerLink]=\"\" (click)=\"openMail()\" class=\"sbutton gplus\" tooltip=\"Mail\"><i\n\t\t\t\t\t\tclass=\"fab fa-google-plus-g\"></i></a>\n\n\t\t\t\t<a [routerLink]=\"\" (click)=\"openMessenger()\" class=\"sbutton twitt\" tooltip=\"Fb messenger\"><i\n\t\t\t\t\t\tclass=\"fab fa-twitter\"></i></a>\n\t\t\t</div>\n\t\t\t<a [routerLink]=\"\" (click)=\"shareClicked()\" class=\"sbutton mainsbutton\"\n\t\t\t\ttooltip=\"Share this page with your friends\"><i class=\"fas fa-share-alt\"></i></a>\n\t\t</div>\n\t</div>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/footer/footer.component.html":
/*!************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/footer/footer.component.html ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"bg-primary\">\n\t<div style=\"margin-top: 10px; text-align: center\">\n\t\t<ul class=\"list-inline\">\n\t\t\t<b>\n\t\t\t\t<font color=\"#2c3e50\" style=\"font-size: 1.2rem\">*Upload papers/notes and help others*</font>\n\t\t\t</b>\n\t\t</ul>\n\t</div>\n\t<div style=\"text-align:center; margin-top: 10px;\">\n\t\t<label>\n\t\t\t<input style=\"width: 50%;\" type='file' (change)=\"selectFile($event)\" multiple>\n\t\t</label>\n\t\t<button class=\"button2\" [disabled]=\"!selectedFiles\" (click)=\"upload()\">Upload</button>\n\t\t<ul class=\"list-inline\">\n\t\t\t<b>\n\t\t\t\t<font color=\"red\" style=\"font-size: 1rem\">{{uploadMsg}}</font>\n\t\t\t</b>\n\t\t</ul>\n\t</div>\n\t<ul class=\"list-inline\" style=\"text-align: -webkit-center; margin-top: 10px;\">\n\t\t<li class=\"list-inline-item\">\n\t\t\t<b style=\"color: #2c3e50\">\n\t\t\t\tFollow us :&nbsp;&nbsp;\n\t\t\t</b>\n\t\t</li>\n\t\t<li class=\"list-inline-item\">\n\t\t\t<a href=\"https://www.linkedin.com/company/finalrevise/\" target=\"_blank\">\n\t\t\t\t<img alt=\"finalrevise linkedin account\" style=\"padding-right: 10%;\" border=\"0\" id=\"fixed-width-flamingo\"\n\t\t\t\t\tsrc=\"/assets/images/linkedin.png\" />\n\t\t\t</a>\n\t\t</li>\n\t\t<li class=\"list-inline-item\">\n\t\t\t<a href=\"https://www.youtube.com/channel/UCODEmMV_ka_Xb5hKoTGOEHQ\" target=\"_blank\">\n\t\t\t\t<img alt=\"finalrevise youtube channel\" style=\"padding-right: 10%;\" border=\"0\" id=\"fixed-width-flamingo\" src=\"/assets/images/youtube.png\" />\n\t\t\t</a>\n\t\t</li>\n\t\t<li class=\"list-inline-item\">\n\t\t\t<a href=\"https://www.facebook.com/finalrevise/\" target=\"_blank\">\n\t\t\t\t<img alt=\"finalrevise facebook page\" style=\"padding-right: 10%;\" border=\"0\" id=\"fixed-width-flamingo\"\n\t\t\t\t\tsrc=\"/assets/images/social_facebook.png\" />\n\t\t\t</a>\n\t\t</li>\n\t\t<li class=\"list-inline-item\">\n\t\t\t<a href=\"https://www.instagram.com/finalrevise/\" target=\"_blank\">\n\t\t\t\t<img alt=\"finalrevise instagram profile\" style=\"padding-right: 10%;\" border=\"0\" id=\"fixed-width-flamingo\"\n\t\t\t\t\tsrc=\"/assets/images/instagram.png\" />\n\t\t\t</a>\n\t\t</li>\n\t</ul>\n\t<ul class=\"list-inline\">\n\t\t<li class=\"list-inline-item inline1\">\n\t\t\t<ul class=\"list\">\n\t\t\t\t<li class=\"list-item\">\n\t\t\t\t\t<a routerLink=\"\" style=\"color: #8b0000\"><b>COMPANY</b></a>\n\t\t\t\t</li>\n\t\t\t\t<li class=\"list-item\">\n\t\t\t\t\t<a routerLink=\"/our-services\">Our Services</a>\n\t\t\t\t</li>\n\t\t\t\t<li class=\"list-item\">\n\t\t\t\t\t<a routerLink=\"/about\">About</a>\n\t\t\t\t</li>\n\t\t\t\t<li class=\"list-item\">\n\t\t\t\t\t<a routerLink=\"/contact\">Contact us</a>\n\t\t\t\t</li>\n\t\t\t</ul>\n\t\t</li>\n\t\t<li class=\"list-inline-item inline1\">\n\t\t</li>\n\t\t<li class=\"list-inline-item inline2\">\n\t\t\t<div class=\"jumbotron\">\n\t\t\t<a href=\"https://play.google.com/store/apps/details?id=com.finalrevise.android\" target=\"_blank\">\n\t\t\t\t<img alt=\"finalrevise app\" class=\"play-img\" border=\"0\" src=\"/assets/images/playstore.png\" />\n\t\t\t</a>\n\t\t\t</div>\n\t\t</li>\n\t</ul>\n</div>\n<div class=\"align-center\">\n\t<font color=\"#ecf0f1\">Copyright © FinalRevise 2018-20. All rights reserved.</font>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/desktop/home.component.desktop.html":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home/desktop/home.component.desktop.html ***!
  \************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!--The content below is only a placeholder and can be replaced.-->\n<div class=\"parallax\">\n\t<div class=\"main-content\">\n\t\t<div class=\"container\">\n\t\t\t<div style=\"text-align:center; padding-top: 0%;color: #8b0000; text-shadow: 2px 2px 4px #868686;\">\n\t\t\t\t<h1>\n\t\t\t\t\t<b>Question Papers & Study Material</b>\n\t\t\t\t</h1>\n\t\t\t</div>\n\t\t\t<form [formGroup]=\"signupfrm\" (ngSubmit)=\"getSearchResult()\">\n\t\t\t\t<div style=\"align-content:center;margin-left:5%; margin-top:3%; margin-right:5%; margin-bottom:4%;\n\t\t\t\t\tbox-shadow: 0px 5px 11px -2px rgba(0, 0, 0, 0.18), 0px 4px 12px -7px rgba(0, 0, 0, 0.15);\">\n\t\t\t\t\t<input name=\"name\" formControlName=\"name\" class=\"form-control mr-sm-2\" type=\"text\"\n\t\t\t\t\t\tplaceholder=\"Search any study material\">\n\t\t\t\t</div>\n\t\t\t</form>\n\t\t\t<table cellpadding=\"10\">\n\t\t\t\t<tr>\n\t\t\t\t\t<td>\n\t\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openNextPage('school-boards')\">\n\t\t\t\t\t\t\t<mat-card>\n\t\t\t\t\t\t\t\t<img alt=\"school books, previous year paper with notes and syllabus\" src=\"/assets/pngs/pic/school.png\">\n\t\t\t\t\t\t\t\t<mat-card-content>\n\t\t\t\t\t\t\t\t\t<h2 class=\"h5-style\">\n\t\t\t\t\t\t\t\t\t\t<b>School Boards</b>\n\t\t\t\t\t\t\t\t\t</h2>\n\t\t\t\t\t\t\t\t</mat-card-content>\n\t\t\t\t\t\t\t</mat-card>\n\t\t\t\t\t\t</a>\n\t\t\t\t\t</td>\n\t\t\t\t\t<td>\n\t\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openNextPage('all-competitive-exams')\">\n\t\t\t\t\t\t\t<mat-card>\n\t\t\t\t\t\t\t\t<img alt=\"entrance exam papers with free study materials and notes\" src=\"/assets/pngs/pic/entrance.png\">\n\t\t\t\t\t\t\t\t<mat-card-content>\n\t\t\t\t\t\t\t\t\t<h2 class=\"h5-style\">\n\t\t\t\t\t\t\t\t\t\t<b>Entrance Exams</b>\n\t\t\t\t\t\t\t\t\t</h2>\n\t\t\t\t\t\t\t\t</mat-card-content>\n\t\t\t\t\t\t\t</mat-card>\n\t\t\t\t\t\t</a>\n\t\t\t\t\t</td>\n\t\t\t\t\t<td>\n\t\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openNextPage('university')\">\n\t\t\t\t\t\t\t<mat-card>\n\t\t\t\t\t\t\t\t<img alt=\"semester papers, notes books and assignments\" src=\"/assets/pngs/pic/univ.png\">\n\t\t\t\t\t\t\t\t<mat-card-content>\n\t\t\t\t\t\t\t\t\t<h2 class=\"h5-style\">\n\t\t\t\t\t\t\t\t\t\t<b>University</b>\n\t\t\t\t\t\t\t\t\t</h2>\n\t\t\t\t\t\t\t\t</mat-card-content>\n\t\t\t\t\t\t\t</mat-card>\n\t\t\t\t\t\t</a>\n\t\t\t\t\t</td>\n\t\t\t\t</tr>\n\t\t\t\t<tr>\n\t\t\t\t\t<td>\n\t\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openNextPage('placement-papers')\">\n\t\t\t\t\t\t\t<mat-card>\n\t\t\t\t\t\t\t\t<img alt=\"placement paper, all question and answer\" src=\"/assets/pngs/pic/placement.png\">\n\t\t\t\t\t\t\t\t<mat-card-content>\n\t\t\t\t\t\t\t\t\t<h2 class=\"h5-style\">\n\t\t\t\t\t\t\t\t\t\t<b>Placement Papers</b>\n\t\t\t\t\t\t\t\t\t</h2>\n\t\t\t\t\t\t\t\t</mat-card-content>\n\t\t\t\t\t\t\t</mat-card>\n\t\t\t\t\t\t</a>\n\t\t\t\t\t</td>\n\t\t\t\t\t<td>\n\t\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openNextPage('government-jobs')\">\n\t\t\t\t\t\t\t<mat-card>\n\t\t\t\t\t\t\t\t<img alt=\"government job paper question answer, books and notes\" src=\"/assets/pngs/pic/govt.png\">\n\n\t\t\t\t\t\t\t\t<mat-card-content>\n\t\t\t\t\t\t\t\t\t<h2 class=\"h5-style\">\n\t\t\t\t\t\t\t\t\t\t<b>Government Jobs</b>\n\t\t\t\t\t\t\t\t\t</h2>\n\t\t\t\t\t\t\t\t</mat-card-content>\n\t\t\t\t\t\t\t</mat-card>\n\t\t\t\t\t\t</a>\n\t\t\t\t\t</td>\n\t\t\t\t\t<td>\n\t\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openNextPage('international-competition')\">\n\t\t\t\t\t\t\t<mat-card>\n\t\t\t\t\t\t\t\t<img alt=\"international exams previous year papers, question answer and study notes\" src=\"/assets/pngs/pic/international.png\">\n\t\t\t\t\t\t\t\t<mat-card-content>\n\t\t\t\t\t\t\t\t\t<h2 class=\"h5-style\">\n\t\t\t\t\t\t\t\t\t\t<b>International Competition</b>\n\t\t\t\t\t\t\t\t\t</h2>\n\t\t\t\t\t\t\t\t</mat-card-content>\n\t\t\t\t\t\t\t</mat-card>\n\t\t\t\t\t\t</a>\n\t\t\t\t\t</td>\n\t\t\t\t</tr>\n\t\t\t</table>\n\t\t\t<div id=\"carousel\" class=\"carousel slide\" data-ride=\"carousel\" style=\"margin-top: 40px; \">\n\t\t\t\t<div class=\"container\">\n\t\t\t\t\t<div class=\"jumbotron p-3\">\n\t\t\t\t\t\t<div class=\"carousel-inner\">\n\t\t\t\t\t\t\t<div class=\"carousel-item active\">\n\t\t\t\t\t\t\t\t<div class=\"d-none d-lg-block\">\n\t\t\t\t\t\t\t\t\t<div class=\"media\">\n\t\t\t\t\t\t\t\t\t\t<div class=\"media-body my-auto\">\n\t\t\t\t\t\t\t\t\t\t\t<h5><b style=\"color: #8b0000\">FinalRevise</b> helps\n\t\t\t\t\t\t\t\t\t\t\t\tstudents\n\t\t\t\t\t\t\t\t\t\t\t\tstudents in preparation of their exams. Multiple sections include Semester Exam question & answer /Entrance Exam paper & answer key / School Board Exam / Government Job Exam papers /Placement Papers /GRE /GMAT and all competitive exam paper & solutions.</h5>\n\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"d-lg-none d-xl-none\">\n\t\t\t\t\t\t\t\t\t<div class=\"card bg-transparent border-0\">\n\t\t\t\t\t\t\t\t\t\t<div class=\"card-body\">\n\t\t\t\t\t\t\t\t\t\t\t<h5><b style=\"color: #8b0000\">FinalRevise</b> helps\n\t\t\t\t\t\t\t\t\t\t\t\tstudents\n\t\t\t\t\t\t\t\t\t\t\t\tstudents in preparation of their exams. Multiple sections include Semester Exam question & answer /Entrance Exam paper & answer key / School Board Exam / Government Job Exam papers /Placement Papers /GRE /GMAT and all competitive exam paper & solutions.</h5>\n\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"carousel-item\">\n\t\t\t\t\t\t\t\t<div class=\"d-none d-lg-block\">\n\t\t\t\t\t\t\t\t\t<div class=\"media\">\n\t\t\t\t\t\t\t\t\t\t<div class=\"media-body my-auto\">\n\t\t\t\t\t\t\t\t\t\t\t<h5>The largest collection of papers, notes, free e-books, question & answer available in pdf. <b\n\t\t\t\t\t\t\t\t\t\t\t\t\tstyle=\"color: #8b0000\">10M+ downloads of study material. </b>Appreciated by more than 1M+ users.Updating new features and material daily on student's demands.</h5>\n\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"d-lg-none d-xl-none\">\n\t\t\t\t\t\t\t\t\t<div class=\"card bg-transparent border-0\">\n\t\t\t\t\t\t\t\t\t\t<div class=\"card-body\">\n\t\t\t\t\t\t\t\t\t\t\t<h5>The largest collection of papers, notes, free e-books, question & answer available in pdf. <b\n\t\t\t\t\t\t\t\t\t\t\t\t\tstyle=\"color: #8b0000\">10M+ downloads of study material. </b>Appreciated by more than 1M+ users. Updating new features and material daily on student's demands.</h5>\n\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"carousel-item\">\n\t\t\t\t\t\t\t\t<div class=\"d-none d-lg-block\">\n\t\t\t\t\t\t\t\t\t<div class=\"media\">\n\t\t\t\t\t\t\t\t\t\t<div class=\"media-body my-auto\">\n\t\t\t\t\t\t\t\t\t\t\t<h5>We are also providing a wide range of services and facilities for students like <b\n\t\t\t\t\t\t\t\t\t\t\t\t\tstyle=\"color: #8b0000\">Advertise with FinalRevise, much Affordable Tour & Traveling packages, Best Hostels & PGs, Event Management and Personalised printing on T-shirts, mugs etc.. </b>\n\t\t\t\t\t\t\t\t\t\t\t\tFor more check the ‘other services’ section.</h5>\n\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"d-lg-none d-xl-none\">\n\t\t\t\t\t\t\t\t\t<div class=\"card bg-transparent border-0\">\n\t\t\t\t\t\t\t\t\t\t<div class=\"card-body\">\n\t\t\t\t\t\t\t\t\t\t\t<h5>We are also providing a wide range of services and facilities for students like <b\n\t\t\t\t\t\t\t\t\t\t\t\t\tstyle=\"color: #8b0000\">Advertise with FinalRevise, much Affordable Tour & Traveling packages, Best Hostels & PGs, Event Management and Personalised printing on T-shirts, mugs etc.. </b>\n\t\t\t\t\t\t\t\t\t\t\t\t\tFor more check the ‘other services’ section.</h5>\n\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t<div class=\"container\">\n\t\t<div class=\"row\" style=\"text-align: -webkit-center\">\n\t\t\t<div class=\"col-lg-2\">\n\t\t\t\t<a [routerLink]=\"\" (click)=\"openNextPage('Past Papers')\" style=\"text-decoration: none;color: inherit\">\n\t\t\t\t\t<img alt=\"past papers of government job exams, universities and placement paeras\" src=\"/assets/images/previous.png\">\n\t\t\t\t\t<h3 class=\"h5-style\">\n\t\t\t\t\t\t<b>Past Papers</b>\n\t\t\t\t\t</h3>\n\t\t\t\t</a>\n\t\t\t</div>\n\t\t\t<div class=\"col-lg-2\">\n\t\t\t\t<a [routerLink]=\"\" (click)=\"openNextPage('Free Books')\" style=\"text-decoration: none;color: inherit\">\n\t\t\t\t\t<img alt=\"ebooks of all schools boards and competitive exams with notes\" src=\"/assets/images/books.png\">\n\t\t\t\t\t<h3 class=\"h5-style\">\n\t\t\t\t\t\t<b>E-Books</b>\n\t\t\t\t\t</h3>\n\t\t\t\t</a>\n\t\t\t</div>\n\t\t\t<div class=\"col-lg-2\">\n\t\t\t\t<a [routerLink]=\"\" (click)=\"openNextPage('Notes')\" style=\"text-decoration: none;color: inherit\">\n\t\t\t\t\t<img alt=\"notes for school boards, government jobs and universities\" src=\"/assets/images/notes.png\" alt=\"Card image\">\n\t\t\t\t\t<h3 class=\"h5-style\">\n\t\t\t\t\t\t<b>Notes</b>\n\t\t\t\t\t</h3>\n\t\t\t\t</a>\n\t\t\t</div>\n\t\t\t<div class=\"col-lg-2\">\n\t\t\t\t<a [routerLink]=\"\" (click)=\"openNextPage('Assignments')\" style=\"text-decoration: none;color: inherit\">\n\t\t\t\t\t<img alt=\"assignments for school boards and universities\" src=\"/assets/images/assignment.png\" alt=\"Card image\">\n\t\t\t\t\t<h3 class=\"h5-style\">\n\t\t\t\t\t\t<b>Assignments</b>\n\t\t\t\t\t</h3>\n\t\t\t\t</a>\n\t\t\t</div>\n\t\t\t<div class=\"col-lg-2\">\n\t\t\t\t<a [routerLink]=\"\" (click)=\"openNextPage('Syllabus')\">\n\t\t\t\t\t<img alt=\"syllabus of 10th & 12th class, universities and entrance exams\" src=\"/assets/images/syllabus.png\" alt=\"Card image\">\n\t\t\t\t\t<h3 class=\"h5-style\">\n\t\t\t\t\t\t<b>Syllabus</b>\n\t\t\t\t\t</h3>\n\t\t\t\t</a>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\n\t<div style=\"text-align:center;margin:40px;\">\n\t\t<h4>\n\t\t\t<div class=\"hr-sect\">\n\t\t\t\t<b>Testimonials</b>\n\t\t\t</div>\n\t\t</h4>\n\t</div>\n\t<div id=\"carousel\" class=\"carousel slide\" data-ride=\"carousel\">\n\t\t<ol class=\"carousel-indicators\">\n\t\t\t<li data-target=\"#carousel\" data-slide-to=\"0\"></li>\n\t\t\t<li data-target=\"#carousel\" data-slide-to=\"1\"></li>\n\t\t\t<li data-target=\"#carousel\" data-slide-to=\"2\"></li>\n\t\t</ol>\n\t\t<div class=\"container\">\n\t\t\t<div class=\"jumbotron p-3\">\n\t\t\t\t<div class=\"carousel-inner\">\n\t\t\t\t\t<div class=\"carousel-item active\">\n\t\t\t\t\t\t<div class=\"d-none d-lg-block\">\n\t\t\t\t\t\t\t<div class=\"media\"> <img src=\"/assets/images/tpic1.jpeg\"\n\t\t\t\t\t\t\t\t\tclass=\"align-self-center mr-3 rounded-circle\" alt=\"...\"\n\t\t\t\t\t\t\t\t\tstyle=\"width: 150px;height: 150px;\">\n\t\t\t\t\t\t\t\t<div class=\"media-body my-auto\">\n\t\t\t\t\t\t\t\t\t<h6 style=\"color: #2c3e50\"><b><i>While preparing for NIFT entrance exam, I found this website which provide me all books related to Designs, previous year papers ,notes and study material which help me a lot. I recommend FinalRevise for all NID/NIFT/CEED aspirants for a complete preparation.\n\t\t\t\t\t\t\t\t\t\t<br>\n\t\t\t\t\t\t\t\t\t\t\t\tKeep the good work going “FinalRevise”.</i></b></h6>\n\t\t\t\t\t\t\t\t\t<h6 style=\"text-align: right; color: #8b0000; padding-top: 3%; padding-right: 3%\">\n\t\t\t\t\t\t\t\t\t\tShivani Jain</h6>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"d-lg-none d-xl-none\">\n\t\t\t\t\t\t\t<div class=\"card bg-transparent border-0\"> <img src=\"/assets/images/tpic1.jpeg\"\n\t\t\t\t\t\t\t\t\tstyle=\"width: 150px;height: 150px\"\n\t\t\t\t\t\t\t\t\tclass=\"card-img-top img-thumbnail rounded mx-auto \" alt=\"...\">\n\t\t\t\t\t\t\t\t<div class=\"card-body\">\n\t\t\t\t\t\t\t\t\t<h6 style=\"color: #2c3e50\"><b><i>While preparing for NIFT entrance exam, I found this website which provide me all books related to Designs, previous year papers ,notes and study material which help me a lot. I recommend FinalRevise for all NID/NIFT/CEED aspirants for a complete preparation.\n\t\t\t\t\t\t\t\t\t\t<br>\n\t\t\t\t\t\t\t\t\t\t\t\tKeep the good work going “FinalRevise”.</i></b></h6>\n\t\t\t\t\t\t\t\t\t<h6 style=\"text-align: right; color: #8b0000; padding-top: 3%; padding-right: 3%\">\n\t\t\t\t\t\t\t\t\t\tShivani Jain</h6>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"carousel-item\">\n\t\t\t\t\t\t<div class=\"d-none d-lg-block\">\n\t\t\t\t\t\t\t<div class=\"media\"> <img src=\"/assets/images/tpic2.jpeg\"\n\t\t\t\t\t\t\t\t\tclass=\"align-self-center mr-3 rounded-circle\" alt=\"...\"\n\t\t\t\t\t\t\t\t\tstyle=\"width: 150px;height: 150px;\">\n\t\t\t\t\t\t\t\t<div class=\"media-body my-auto\">\n\t\t\t\t\t\t\t\t\t<h6 style=\"color: #2c3e50\"><b><i>At the time of exam, the best way of last minute study is previous year question papers and study notes. FinalRevise provides almost all type of question papers for free , you can download question paper and their solution.<br>\n\t\t\t\t\t\t\t\t\t\tThis helped me a lot in exam time and save my time.</i></b></h6>\n\t\t\t\t\t\t\t\t\t<h6 style=\"text-align: right; color: #8b0000; padding-top: 3%; padding-right: 3%\">\n\t\t\t\t\t\t\t\t\t\tPrakhar Kumar</h6>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"d-lg-none d-xl-none\">\n\t\t\t\t\t\t\t<div class=\"card bg-transparent border-0\"> <img src=\"/assets/images/tpic2.jpeg\"\n\t\t\t\t\t\t\t\t\tstyle=\"width: 150px;height: 150px\"\n\t\t\t\t\t\t\t\t\tclass=\"card-img-top img-thumbnail rounded mx-auto \" alt=\"...\">\n\t\t\t\t\t\t\t\t<div class=\"card-body\">\n\t\t\t\t\t\t\t\t\t<h6 style=\"color: #2c3e50\"><b><i>At the time of exam, the best way of last minute study is previous year question papers and study notes. FinalRevise provides almost all type of question papers for free , you can download question paper and their solution.<br>\n\t\t\t\t\t\t\t\t\t\tThis helped me a lot in exam time and save my time.</i></b></h6>\n\t\t\t\t\t\t\t\t\t<h6 style=\"text-align: right; color: #8b0000; padding-top: 3%; padding-right: 3%\">\n\t\t\t\t\t\t\t\t\t\tPrakhar Kumar</h6>\n\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"carousel-item\">\n\t\t\t\t\t\t<div class=\"d-none d-lg-block\">\n\t\t\t\t\t\t\t<div class=\"media\"> <img src=\"/assets/images/tpic3.jpeg\"\n\t\t\t\t\t\t\t\t\tclass=\"align-self-center mr-3 rounded-circle\" alt=\"...\"\n\t\t\t\t\t\t\t\t\tstyle=\"width: 150px;height: 150px;\">\n\t\t\t\t\t\t\t\t<div class=\"media-body my-auto\">\n\t\t\t\t\t\t\t\t\t<h6 style=\"color: #2c3e50\"><b><i>Finalrevise provided us best package with affordable rate for Chandrasheela track. It was a fantastic trip with my DU friends. They have arranged everything and it was a hassle free ride.\n\t\t\t\t\t\t\t\t\t\t<br>Thanks a lot FinalRevise.</i></b></h6>\n\t\t\t\t\t\t\t\t\t<h6 style=\"text-align: right; color: #8b0000; padding-top: 3%; padding-right: 3%\">\n\t\t\t\t\t\t\t\t\t\tNeeraj Agarwal</h6>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"d-lg-none d-xl-none\">\n\t\t\t\t\t\t\t<div class=\"card bg-transparent border-0\"> <img src=\"/assets/images/tpic3.jpeg\"\n\t\t\t\t\t\t\t\t\tstyle=\"width: 150px;height: 150px\"\n\t\t\t\t\t\t\t\t\tclass=\"card-img-top img-thumbnail rounded mx-auto \" alt=\"...\">\n\t\t\t\t\t\t\t\t<div class=\"card-body\">\n\t\t\t\t\t\t\t\t\t<h6 style=\"color: #2c3e50\"><b><i>Finalrevise provided us best package with affordable rate for Chandrasheela track. It was a fantastic trip with my DU friends. They have arranged everything and it was a hassle free ride.\n\t\t\t\t\t\t\t\t\t\t<br>Thanks a lot\n\t\t\t\t\t\t\t\t\t\t\t\tFinalRevise.</i></b></h6>\n\t\t\t\t\t\t\t\t\t<h6 style=\"text-align: right; color: #8b0000; padding-top: 3%; padding-right: 3%\">\n\t\t\t\t\t\t\t\t\t\tNeeraj Agarwal</h6>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/mobile/home.component.mobile.html":
/*!**********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home/mobile/home.component.mobile.html ***!
  \**********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!--The content below is only a placeholder and can be replaced.-->\n<div class=\"main-content\">\n\t<div class=\"container\">\n\t\t<div style=\"text-align:center; padding-top: 3%;color: #8b0000; text-shadow: 2px 2px 4px #868686;\">\n\t\t\t<h1 style=\"font-size:270%;\">\n\t\t\t\t<b>Question Papers & Study Material</b>\n\t\t\t</h1>\n\t\t</div>\n\t\t<form [formGroup]=\"signupfrm\" (ngSubmit)=\"getSearchResult()\">\n\t\t\t<div style=\"align-content:center;margin:30px;\">\n\t\t\t\t<input name=\"name\" formControlName=\"name\" class=\"form-control mr-sm-2\" type=\"text\"\n\t\t\t\t\tplaceholder=\"Search any study material\">\n\t\t\t</div>\n\t\t</form>\n\t\t<table cellpadding=\"5\" style=\"margin-bottom: 8%;\">\n\t\t\t<tr>\n\t\t\t\t<td>\n\t\t\t\t\t<a style=\"margin-bottom: .5%;\" [routerLink]=\"\" (click)=\"openNextPage('school-boards')\">\n\t\t\t\t\t\t<mat-card>\n\t\t\t\t\t\t\t<img alt=\"school books, previous year paper with notes and syllabus\" src=\"/assets/pngs/pic/school.png\">\n\t\t\t\t\t\t\t<mat-card-content>\n\t\t\t\t\t\t\t\t<h2 class=\"h5-style\">\n\t\t\t\t\t\t\t\t\t<b>School Boards</b>\n\t\t\t\t\t\t\t\t</h2>\n\t\t\t\t\t\t\t</mat-card-content>\n\t\t\t\t\t\t</mat-card>\n\t\t\t\t\t</a>\n\t\t\t\t</td>\n\t\t\t\t<td>\n\t\t\t\t\t<a style=\"margin-top:.5%;margin-bottom: .5%;\" [routerLink]=\"\"\n\t\t\t\t\t\t(click)=\"openNextPage('all-competitive-exams')\">\n\t\t\t\t\t\t<mat-card>\n\t\t\t\t\t\t\t<img alt=\"entrance exam papers with free study materials and notes\" src=\"/assets/pngs/pic/entrance.png\"\n\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t<mat-card-content>\n\t\t\t\t\t\t\t\t<h2 class=\"h5-style\">\n\t\t\t\t\t\t\t\t\t<b>Entrance Exams</b>\n\t\t\t\t\t\t\t\t</h2>\n\t\t\t\t\t\t\t</mat-card-content>\n\t\t\t\t\t\t</mat-card>\n\t\t\t\t\t</a>\n\t\t\t\t</td>\n\t\t\t</tr>\n\t\t\t<tr>\n\t\t\t\t<td>\n\t\t\t\t\t<a style=\"margin-bottom: .5%;\" [routerLink]=\"\" (click)=\"openNextPage('placement-papers')\">\n\t\t\t\t\t\t<mat-card>\n\t\t\t\t\t\t\t<img alt=\"placement paper, all question and answer\" src=\"/assets/pngs/pic/placement.png\"\n\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t<mat-card-content>\n\t\t\t\t\t\t\t\t<h2 class=\"h5-style\">\n\t\t\t\t\t\t\t\t\t<b>Placement Papers</b>\n\t\t\t\t\t\t\t\t</h2>\n\t\t\t\t\t\t\t</mat-card-content>\n\t\t\t\t\t\t</mat-card>\n\t\t\t\t\t</a>\n\t\t\t\t</td>\n\t\t\t\t<td>\n\t\t\t\t\t<a style=\"margin-top: .5%; margin-bottom: .5%;\" [routerLink]=\"\"\n\t\t\t\t\t\t(click)=\"openNextPage('government-jobs')\">\n\t\t\t\t\t\t<mat-card>\n\t\t\t\t\t\t\t<img alt=\"government job paper question answer, books and notes\" src=\"/assets/pngs/pic/govt.png\" >\n\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t<mat-card-content>\n\t\t\t\t\t\t\t\t<h2 class=\"h5-style\">\n\t\t\t\t\t\t\t\t\t<b>Government Jobs</b>\n\t\t\t\t\t\t\t\t</h2>\n\t\t\t\t\t\t\t</mat-card-content>\n\t\t\t\t\t\t</mat-card>\n\t\t\t\t\t</a>\n\t\t\t\t</td>\n\t\t\t</tr>\n\t\t\t<tr>\n\t\t\t\t<td>\n\t\t\t\t\t<a style=\"margin-top: .5%;\" [routerLink]=\"\" (click)=\"openNextPage('university')\">\n\t\t\t\t\t\t<mat-card>\n\t\t\t\t\t\t\t<img alt=\"semester papers, notes books and assignments\" src=\"/assets/pngs/pic/univ.png\" >\n\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t<mat-card-content>\n\t\t\t\t\t\t\t\t<h2 class=\"h5-style\">\n\t\t\t\t\t\t\t\t\t<b>University Semesters</b>\n\t\t\t\t\t\t\t\t</h2>\n\t\t\t\t\t\t\t</mat-card-content>\n\t\t\t\t\t\t</mat-card>\n\t\t\t\t\t</a>\n\t\t\t\t</td>\n\t\t\t\t<td>\n\t\t\t\t\t<a style=\"margin-top: .5%;\" [routerLink]=\"\" (click)=\"openNextPage('international-competition')\">\n\t\t\t\t\t\t<mat-card>\n\t\t\t\t\t\t\t<img alt=\"international exams previous year papers, question answer and study notes\" src=\"/assets/pngs/pic/international.png\"\n\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t<mat-card-content>\n\t\t\t\t\t\t\t\t<h2 class=\"h5-style\">\n\t\t\t\t\t\t\t\t\t<b>International Competition</b>\n\t\t\t\t\t\t\t\t</h2>\n\t\t\t\t\t\t\t</mat-card-content>\n\t\t\t\t\t\t</mat-card>\n\t\t\t\t\t</a>\n\t\t\t\t</td>\n\t\t\t</tr>\n\t\t</table>\n\t\t<div style=\"text-align:center;margin:10px;\">\n\t\t\t<h4>\n\t\t\t\t<div class=\"hr-sect\">\n\t\t\t\t\t<b>Category</b>\n\t\t\t\t</div>\n\t\t\t</h4>\n\t\t</div>\n\t\t<div>\n\t\t\t<div class=\"row\">\n\t\t\t\t<div class=\"col-lg-3\">\n\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openNextPage('Past Papers')\">\n\t\t\t\t\t\t<img alt=\"past papers of government job exams, universities and placement paeras\" src=\"/assets/images/previous.png\">\n\t\t\t\t\t\t<h3 class=\"h5-style\">\n\t\t\t\t\t\t\t<b>Past Papers</b>\n\t\t\t\t\t\t</h3>\n\t\t\t\t\t</a>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col-lg-3\">\n\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openNextPage('Free Books')\">\n\t\t\t\t\t\t<img alt=\"ebooks of all schools boards and competitive exams with notes\" src=\"/assets/images/books.png\">\n\t\t\t\t\t\t<h3 class=\"h5-style\">\n\t\t\t\t\t\t\t<b>E-Books</b>\n\t\t\t\t\t\t</h3>\n\t\t\t\t\t</a>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col-lg-3\">\n\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openNextPage('Notes')\">\n\t\t\t\t\t\t<img alt=\"notes for school boards, government jobs and universities\" src=\"/assets/images/notes.png\" alt=\"Card image\">\n\t\t\t\t\t\t<h3 class=\"h5-style\">\n\t\t\t\t\t\t\t<b>Notes</b>\n\t\t\t\t\t\t</h3>\n\t\t\t\t\t</a>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"row\" style=\"text-align: -webkit-center;\">\n\t\t\t\t<div class=\"col-lg-3\" style=\"padding-right: 5%; margin-left: 15%\">\n\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openNextPage('Assignments')\">\n\t\t\t\t\t\t<img alt=\"assignments for school boards and universities\" src=\"/assets/images/assignment.png\" alt=\"Card image\">\n\t\t\t\t\t\t<h3 class=\"h5-style\">\n\t\t\t\t\t\t\t<b>Assignments</b>\n\t\t\t\t\t\t</h3>\n\t\t\t\t\t</a>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col-lg-3\" style=\"padding-left: 5%;\">\n\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openNextPage('Syllabus')\">\n\t\t\t\t\t\t<img alt=\"syllabus of 10th & 12th class, universities and entrance exams\" src=\"/assets/images/syllabus.png\" alt=\"Card image\">\n\t\t\t\t\t\t<h3 class=\"h5-style\">\n\t\t\t\t\t\t\t<b>Syllabus</b>\n\t\t\t\t\t\t</h3>\n\t\t\t\t\t</a>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<div id=\"carousel\" class=\"carousel slide\" data-ride=\"carousel\" style=\"margin-top: 40px; \">\n\t\t\t<div class=\"container\">\n\t\t\t\t<div class=\"jumbotron p-3\">\n\t\t\t\t\t<div class=\"carousel-inner\">\n\t\t\t\t\t\t<div class=\"carousel-item active\">\n\t\t\t\t\t\t\t<div class=\"d-none d-lg-block\">\n\t\t\t\t\t\t\t\t<div class=\"media\">\n\t\t\t\t\t\t\t\t\t<div class=\"media-body my-auto\">\n\t\t\t\t\t\t\t\t\t\t<h5><b style=\"color: #8b0000\">FinalRevise</b> helps\n\t\t\t\t\t\t\t\t\t\t\tstudents\n\t\t\t\t\t\t\t\t\t\t\tstudents in preparation of their exams. Multiple sections include Semester Exam question & answer /Entrance Exam paper & answer key / School Board Exam / Government Job Exam papers /Placement Papers /GRE /GMAT and all competitive exam paper & solutions.</h5>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"d-lg-none d-xl-none\">\n\t\t\t\t\t\t\t\t<div class=\"card bg-transparent border-0\">\n\t\t\t\t\t\t\t\t\t<div class=\"card-body\">\n\t\t\t\t\t\t\t\t\t\t<h5><b style=\"color: #8b0000\">FinalRevise</b> helps\n\t\t\t\t\t\t\t\t\t\t\tstudents\n\t\t\t\t\t\t\t\t\t\t\tstudents in preparation of their exams. Multiple sections include Semester Exam question & answer /Entrance Exam paper & answer key / School Board Exam / Government Job Exam papers /Placement Papers /GRE /GMAT and all competitive exam paper & solutions.</h5>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"carousel-item\">\n\t\t\t\t\t\t\t<div class=\"d-none d-lg-block\">\n\t\t\t\t\t\t\t\t<div class=\"media\">\n\t\t\t\t\t\t\t\t\t<div class=\"media-body my-auto\">\n\t\t\t\t\t\t\t\t\t\t<h5>The largest collection of papers, notes, free e-books, question & answer available in pdf. <b\n\t\t\t\t\t\t\t\t\t\t\t\tstyle=\"color: #8b0000\">10M+ downloads of study material. </b>Appreciated by more than 1M+ users.Updating new features and material daily on student's demands.</h5>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"d-lg-none d-xl-none\">\n\t\t\t\t\t\t\t\t<div class=\"card bg-transparent border-0\">\n\t\t\t\t\t\t\t\t\t<div class=\"card-body\">\n\t\t\t\t\t\t\t\t\t\t<h5>The largest collection of papers, notes, free e-books, question & answer available in pdf. <b\n\t\t\t\t\t\t\t\t\t\t\t\tstyle=\"color: #8b0000\">10M+ downloads of study material. </b>Appreciated by more than 1M+ users. Updating new features and material daily on student's demands.</h5>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"carousel-item\">\n\t\t\t\t\t\t\t<div class=\"d-none d-lg-block\">\n\t\t\t\t\t\t\t\t<div class=\"media\">\n\t\t\t\t\t\t\t\t\t<div class=\"media-body my-auto\">\n\t\t\t\t\t\t\t\t\t\t<h5>We are also providing a wide range of services and facilities for students like <b\n\t\t\t\t\t\t\t\t\t\t\t\tstyle=\"color: #8b0000\">Advertise with FinalRevise, much Affordable Tour & Traveling packages, Best Hostels & PGs, Event Management and Personalised printing on T-shirts, mugs etc.. </b>\n\t\t\t\t\t\t\t\t\t\t\tFor more check the ‘other services’ section.</h5>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"d-lg-none d-xl-none\">\n\t\t\t\t\t\t\t\t<div class=\"card bg-transparent border-0\">\n\t\t\t\t\t\t\t\t\t<div class=\"card-body\">\n\t\t\t\t\t\t\t\t\t\t<h5>We are also providing a wide range of services and facilities for students like <b\n\t\t\t\t\t\t\t\t\t\t\t\tstyle=\"color: #8b0000\">Advertise with FinalRevise, much Affordable Tour & Traveling packages, Best Hostels & PGs, Event Management and Personalised printing on T-shirts, mugs etc.. </b>\n\t\t\t\t\t\t\t\t\t\t\t\tFor more check the ‘other services’ section.</h5>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/our-services/services.component.html":
/*!********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/our-services/services.component.html ***!
  \********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"main-content\">\n\t<div style=\"text-align:center;margin-bottom: 30px\">\n\t\t<h3>\n\t\t\t<div class=\"hr-sect\">\n\t\t\t\t<b>Our Services</b>\n\t\t\t</div>\n\t\t</h3>\n\t</div>\n\t<div class=\"jumbotron\">\n\t\t<div style=\"text-align: center; padding: 10px; color: #8b0000;\">\n\t\t\t<h4><u><b><i>Advertise with FinalRevise</i></b></u></h4>\n\t\t</div>\n\t\t<div class=\"row\" style=\"text-align: -webkit-center; display: flex\">\n\t\t\t<div class=\"col-lg-3\">\n\t\t\t\t<img src=\"/assets/images/advertise.png\">\n\t\t\t</div>\n\t\t\t<div class=\"col-lg-9\" style=\"text-align: left;\">\n\t\t\t\t<h5>Finalrevise.com is an educational platform which is already served useful study material to <b>over\n\t\t\t\t\t\t1M+\n\t\t\t\t\t\tstudents.</b> We are growing rapidly and adding more useful features every day.\n\t\t\t\t\t<b><br><br>We are providing wide range of advertising options for advertisers at FinalRevise as well\n\t\t\t\t\t\tas\n\t\t\t\t\t\tour social media channels.<br><br></b>\n\t\t\t\t\t<b><i>- Facebook/Instagram</i><br></b>\n\t\t\t\t\t<b><i>- WhatsApp</i><br></b>\n\t\t\t\t\t<b><i>- FinalRevise website</i><br></b>\n\t\t\t\t</h5>\n\t\t\t\t<div style=\"text-align: end; padding: 10px\">\n\t\t\t\t\t<h4><b><u><a routerLink=\"/contact\" target=\"_blank\">(Contact us)</a></u></b></h4>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t<hr>\n\t<div class=\"jumbotron\">\n\t\t<div style=\"text-align: center; padding: 10px; color: #8b0000;\">\n\t\t\t<h4><u><b><i>Affordable Tour and Traveling</i></b></u></h4>\n\t\t</div>\n\t\t<div class=\"row\" style=\"text-align: -webkit-center; display: flex\">\n\t\t\t<div class=\"col-lg-3\">\n\t\t\t\t<img src=\"/assets/images/travel.png\">\n\t\t\t</div>\n\t\t\t<div class=\"col-lg-9\" style=\"text-align: left;\">\n\t\t\t\t<h5>Bored after taking exams. Want to do something adventurous, we are here for you.<br>\n\t\t\t\t\tWe will provide you the best travelling experience you ever had by giving you the best facilities in\n\t\t\t\t\taffordable price.<br><br>\n\t\t\t\t\t<b><i>- Full transportation</i><br></b>\n\t\t\t\t\t<b><i>- Group discounts</i><br></b>\n\t\t\t\t\t<b><i>- DJ night</i><br></b>\n\t\t\t\t\t<b><i>- Bon Fire</i><br></b>\n\t\t\t\t\t<b><i>- Luxury Rooms</i><br></b>\n\t\t\t\t</h5>\n\t\t\t\t<div style=\"text-align: end; padding: 10px\">\n\t\t\t\t\t<h4><b><u><a routerLink=\"/contact\" target=\"_blank\">(Contact us)</a></u></b></h4>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t<hr>\n\t<div class=\"jumbotron\">\n\t\t<div style=\"text-align: center; padding: 10px; color: #8b0000;\">\n\t\t\t<h4><u><b><i>Best Hostels and PGs</i></b></u></h4>\n\t\t</div>\n\t\t<div class=\"row\" style=\"text-align: -webkit-center; display: flex\">\n\t\t\t<div class=\"col-lg-3\">\n\t\t\t\t<img src=\"/assets/images/hostel.png\">\n\t\t\t</div>\n\t\t\t<div class=\"col-lg-9\" style=\"text-align: left;\">\n\t\t\t\t<h5>As we are solving every problems of college campus so how can we forget about PG.<br>\n\t\t\t\t\tAfter taking admission the second thing you need is a good pg with good facilities.<br>\n\t\t\t\t\tPG is the second home away from home that you need to live in with all full amenities-<br><br>\n\t\t\t\t\t<b><i>- Wi-Fi</i></b><br>\n\t\t\t\t\t<b><i>- Refrigerator/Air Conditioner</i><br></b>\n\t\t\t\t\t<b><i>- Good environment</i><br></b>\n\t\t\t\t\t<b><i>- Affordable price</i> <br></b>\n\t\t\t\t\t<b><i>- Near your college</i><br></b>\n\t\t\t\t</h5>\n\t\t\t\t<div style=\"text-align: end; padding: 10px\">\n\t\t\t\t\t<h4><b><u><a routerLink=\"/contact\" target=\"_blank\">(Contact us)</a></u></b></h4>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t<hr>\n\t<div class=\"jumbotron\">\n\t\t<div style=\"text-align: center; padding: 10px; color: #8b0000;\">\n\t\t\t<h4><u><b><i>Event Management and Printing</i></b></u></h4>\n\t\t</div>\n\t\t<div class=\"row\" style=\"text-align: -webkit-center; display: flex\">\n\t\t\t<div class=\"col-lg-3\">\n\t\t\t\t<img src=\"/assets/images/fest.png\">\n\t\t\t</div>\n\t\t\t<div class=\"col-lg-9\" style=\"text-align: left;\">\n\t\t\t\t<h5>Finalrevise.com is providing wide range of service options for societies and colleges students\n\t\t\t\t\tarranging\n\t\t\t\t\ttechnical/cultural college festivals. So, they will focus on execution not on arrangements.<br><br>\n\t\t\t\t\t<b><i>- Posters and Banners</i></b><br>\n\t\t\t\t\t<b><i>- Printed hoodies, t-shirts</i><br></b>\n\t\t\t\t\t<b><i>- Medals And Momentos</i><br></b>\n\t\t\t\t\t<b><i>- Photography Services</i> <br></b>\n\t\t\t\t\t<b><i>- Anchors, Comedians and Professional DJs</i><br></b>\n\t\t\t\t\t<b><i>- Sponsorship</i> <br></b>\n\t\t\t\t</h5>\n\t\t\t\t<div style=\"text-align: end; padding: 10px\">\n\t\t\t\t\t<h4><b><u><a routerLink=\"/contact\" target=\"_blank\">(Contact us)</a></u></b></h4>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t<div style=\"text-align:center;margin-top:30px;margin-bottom: 30px\">\n\t\t<h3>\n\t\t\t<div class=\"hr-sect\">\n\t\t\t\t<b>Our Apps</b>\n\t\t\t</div>\n\t\t</h3>\n\t</div>\n\t<!-- <div class=\"jumbotron\">\n\t<div style=\"text-align: center; padding: 10px; color: #8b0000;\">\n\t\t<h4><u><b><i>The Funify</i></b></u></h4>\n\t</div>\n\t<div class=\"row\" style=\"text-align: -webkit-center; display: flex\">\n\t\t<div class=\"col-lg-3\">\n\t\t\t<img src=\"assets/icon_app_funify.png\">\n\t\t</div>\n\t\t<div class=\"col-lg-9\" style=\"text-align: left;\">\n\t\t\t<h5>Getting bored and looking for some funny jokes, hilarious jokes, funny animal pictures, Troll face,\n\t\t\t\thumor images, inspirational quotes or thug life videos\n\t\t\t\tby just scrolling down your phone, then here you are ending up at the right place.<br>\n\t\t\t\tBecame the first spot light of your group to share the top trending viral videos, cat comedy, rage\n\t\t\t\tfaces, reaction images, whatsapp status or humor images with your friends on whatsapp group or on\n\t\t\t\tfacebook.<br>\n\t\t\t\tFunify is the App providing the best memes, jokes, motivational quotes, videos and so on...\n\t\t\t</h5>\n\t\t\t<div style=\"text-align: end; padding: 10px\">\n\t\t\t\t<h4><b><u><a [routerLink]=\"https://play.google.com/store/apps/details?id=com.fun.thefunify\" target=\"_blank\"><img\n\t\t\t\t\t\t\t\t\tstyle=\"width: 20%; height: 25%; text-align: end\"\n\t\t\t\t\t\t\t\t\tsrc=\"assets/playstore.png\"></a></u></b></h4>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n<hr> -->\n\t<div class=\"jumbotron\">\n\t\t<div style=\"text-align: center; padding: 10px; color: #8b0000;\">\n\t\t\t<h4><u><b><i>FinalRevise - Old papers/Notes/Books/Study material</i></b></u></h4>\n\t\t</div>\n\t\t<div class=\"row\" style=\"text-align: -webkit-center; display: flex\">\n\t\t\t<div class=\"col-lg-3\">\n\t\t\t\t<img src=\"/assets/images/logo1.png\">\n\t\t\t</div>\n\t\t\t<div class=\"col-lg-9\" style=\"text-align: left;\">\n\t\t\t\t<h5>FinalRevise is a solution for all the hazels of exam preparations whether it include Entrance exam\n\t\t\t\t\tpreparations, Board exam preparations or University exams. It is a platform where you can find\n\t\t\t\t\tprevious\n\t\t\t\t\tyear question papers of different exams. Our vision is to include previous year papers of all\n\t\t\t\t\tcourses,\n\t\t\t\t\texams, semesters,\n\t\t\t\t\ttopics so that students across the world can benefit from this source.<br>\n\t\t\t\t\tWith the intent of providing all possible study material, we strive towards making studies and last\n\t\t\t\t\tminute preparations easy and comfortable, emerging confident inside student before the examination.\n\t\t\t\t\tWe\n\t\t\t\t\tare constantly adding new material on our platform.\n\t\t\t\t</h5>\n\t\t\t\t<div style=\"text-align: end; padding: 10px\">\n\t\t\t\t\t<h4><b><u><a href=\"https://play.google.com/store/apps/details?id=com.finalrevise.android\"\n\t\t\t\t\t\t\t\t\ttarget=\"_blank\"><img style=\"width: 20%; height: 25%; text-align: end\"\n\t\t\t\t\t\t\t\t\t\tsrc=\"/assets/images/playstore.png\"></a></u></b></h4>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pdf-viewer/desktop/desktop-pdf-viewer.component.html":
/*!************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pdf-viewer/desktop/desktop-pdf-viewer.component.html ***!
  \************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div style=\"background-image: url('assets/images/bg.jpg'); padding-bottom: 5%\">\n\t<div id=\"loader\" class=\"loader\" *ngIf=\"!metaData\"></div>\n\t<div class=\"main-content\">\n\t\t<table class=\"table1\" *ngIf=\"groupUrl\">\n\t\t\t<tr>\n\t\t\t\t<td style=\"width: 75%;\">\n\t\t\t\t\t<h1 style=\"font-size: 2rem; color: #8b0000; text-shadow: 2px 2px 4px  rgba(0, 0, 0, 0.18);\"\n\t\t\t\t\t\ttitle=\"{{titleText}}\">\n\t\t\t\t\t\t<b>{{titleText}}</b></h1>\n\t\t\t\t</td>\n\t\t\t\t<td class=\"blink\">\n\t\t\t\t\t<a>\n\t\t\t\t\t\t<img src=\"/assets/images/whatsapp_join.png\">\n\t\t\t\t\t</a>\n\t\t\t\t</td>\n\t\t\t</tr>\n\t\t</table>\n\t\t<table *ngIf=\"!groupUrl\" style=\"margin-left: 5%; margin-right: 5%;\">\n\t\t\t<tr>\n\t\t\t\t<td>\n\t\t\t\t\t<h1 style=\"font-size: 2rem; color: #8b0000; text-shadow: 2px 2px 4px  rgba(0, 0, 0, 0.18);\"\n\t\t\t\t\t\ttitle=\"{{titleText}}\">\n\t\t\t\t\t\t<b>{{titleText}}</b></h1>\n\t\t\t\t</td>\n\t\t\t</tr>\n\t\t</table>\n\t\t<div *ngIf=\"!tags\" style=\"margin-left: 5%; margin-right: 5%;\">\n\t\t\t<div class=\"jumbotron\">\n\t\t\t\t<iframe [src]=\"transform(pdfUrl)\"></iframe>\n\t\t\t</div>\n\t\t\t<div class=\"navbar bg-primary\">\n\t\t\t\t<ul class=\"list-inline\">\n\t\t\t\t\t<li class=\"list-inline-item\">\n\t\t\t\t\t\t<b>\n\t\t\t\t\t\t\t<font size=\"3\" color=\"white\"><b>Share {{titleText}} page with your friends :</b></font>\n\t\t\t\t\t\t</b>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li class=\"list-inline-item\">\n\t\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openFacebook()\">\n\t\t\t\t\t\t\t<img border=\"0\" id=\"fixed-width-flamingo\" src=\"/assets/images/facebook60.png\" />\n\t\t\t\t\t\t</a>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li class=\"list-inline-item\">\n\t\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openWhatsApp()\">\n\t\t\t\t\t\t\t<img border=\"0\" id=\"fixed-width-flamingo\" src=\"/assets/images/whatsapp60.png\" />\n\t\t\t\t\t\t</a>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li class=\"list-inline-item\">\n\t\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openMail()\">\n\t\t\t\t\t\t\t<img border=\"0\" id=\"fixed-width-flamingo\" src=\"/assets/images/gmail60.png\" />\n\t\t\t\t\t\t</a>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li class=\"list-inline-item\">\n\t\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openMessenger()\">\n\t\t\t\t\t\t\t<img border=\"0\" id=\"fixed-width-flamingo\" src=\"/assets/images/mess60.png\" />\n\t\t\t\t\t\t</a>\n\t\t\t\t\t</li>\n\t\t\t\t</ul>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"row\" *ngIf=\"tags\">\n\t\t\t<div class=\"col-sm-8\">\n\t\t\t\t<div class=\"jumbotron\" style=\"padding: 1.25rem 1.2rem;\">\n\t\t\t\t\t<iframe [src]=\"transform(pdfUrl)\"></iframe>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"navbar bg-primary\">\n\t\t\t\t\t<ul class=\"list-inline\">\n\t\t\t\t\t\t<li class=\"list-inline-item\">\n\t\t\t\t\t\t\t<font size=\"3\" color=\"white\"><b>Share {{titleText}} page with your friends :</b></font>\n\t\t\t\t\t\t</li>\n\t\t\t\t\t\t<li class=\"list-inline-item\">\n\t\t\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openFacebook()\">\n\t\t\t\t\t\t\t\t<img border=\"0\" id=\"fixed-width-flamingo\" src=\"/assets/images/facebook60.png\" />\n\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t</li>\n\t\t\t\t\t\t<li class=\"list-inline-item\">\n\t\t\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openWhatsApp()\">\n\t\t\t\t\t\t\t\t<img border=\"0\" id=\"fixed-width-flamingo\" src=\"/assets/images/whatsapp60.png\" />\n\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t</li>\n\t\t\t\t\t\t<li class=\"list-inline-item\">\n\t\t\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openMail()\">\n\t\t\t\t\t\t\t\t<img border=\"0\" id=\"fixed-width-flamingo\" src=\"/assets/images/gmail60.png\" />\n\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t</li>\n\t\t\t\t\t\t<li class=\"list-inline-item\">\n\t\t\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openMessenger()\">\n\t\t\t\t\t\t\t\t<img border=\"0\" id=\"fixed-width-flamingo\" src=\"/assets/images/mess60.png\" />\n\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t</li>\n\t\t\t\t\t</ul>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"col-sm-4\">\n\t\t\t\t<div class=\"jumbotron\" style=\"padding: 2%;\">\n\t\t\t\t\t<h5 style=\"text-align: center; color: #8b0000;\"><b>Related links</b></h5>\n\t\t\t\t\t<hr class=\"my-3\">\n\t\t\t\t\t<table>\n\t\t\t\t\t\t<tr *ngFor=\"let el of tags\">\n\t\t\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openTag(el)\">\n\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t<img src=\"{{el.relatedLinksImgUrl}}\" />\n\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t<td style=\"padding: 2%; text-align: center;\" valign=\"top\">\n\t\t\t\t\t\t\t\t\t<b>\n\t\t\t\t\t\t\t\t\t\t{{el.relatedLinksHeading}}\n\t\t\t\t\t\t\t\t\t</b>\n\t\t\t\t\t\t\t\t\t<br>\n\t\t\t\t\t\t\t\t\t<br>\n\t\t\t\t\t\t\t\t\t{{el.relatedLinksSmallDesc}}\n\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t</tr>\n\t\t\t\t\t</table>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"sbuttons\">\n\t\t\t<div *ngIf=\"isClickedShare\">\n\t\t\t\t<a [routerLink]=\"\" (click)=\"openWhatsApp()\" class=\"sbutton whatsapp\" tooltip=\"WhatsApp\"><i\n\t\t\t\t\t\tclass=\"fab fa-whatsapp\"></i></a>\n\n\t\t\t\t<a [routerLink]=\"\" (click)=\"openFacebook()\" class=\"sbutton fb\" tooltip=\"Facebook\"><i\n\t\t\t\t\t\tclass=\"fab fa-facebook-f\"></i></a>\n\n\t\t\t\t<a [routerLink]=\"\" (click)=\"openMail()\" class=\"sbutton gplus\" tooltip=\"Mail\"><i\n\t\t\t\t\t\tclass=\"fab fa-google-plus-g\"></i></a>\n\n\t\t\t\t<a [routerLink]=\"\" (click)=\"openMessenger()\" class=\"sbutton twitt\" tooltip=\"Fb messenger\"><i\n\t\t\t\t\t\tclass=\"fab fa-twitter\"></i></a>\n\t\t\t</div>\n\t\t\t<a [routerLink]=\"\" (click)=\"shareClicked()\" class=\"sbutton mainsbutton\"\n\t\t\t\ttooltip=\"Share this page with your friends\"><i class=\"fas fa-share-alt\"></i></a>\n\t\t</div>\n\t</div>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pdf-viewer/mobile/mobile-pdf-viewer.component.html":
/*!**********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pdf-viewer/mobile/mobile-pdf-viewer.component.html ***!
  \**********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"main-content\">\n\t<div id=\"loader\" class=\"loader\" *ngIf=\"!metaData\"></div>\n\t<h5 class=\"row goBack\" onclick=\"window.history.back()\">\n\t\t<div style=\"margin-top: -.4%;padding-right: 2%;\"><b>&nbsp; &lt;</b></div><b>Back</b>\n\t</h5>\n\t<hr>\n\t<h1 style=\"font-size: 1.6rem; color: #8b0000;\n\ttext-shadow: 2px 2px 4px  rgba(0, 0, 0, 0.18); text-align: center;\" title=\"{{titleText}}\">\n\t\t<b>{{titleText}}</b></h1>\n\t<a href=\"{{groupUrl}}\" target=\"_blank\" *ngIf=\"groupUrl\">\n\t\t<img class=\"center\" src=\"/assets/images/whatsapp_join.png\">\n\t</a>\n\t<div *ngIf=\"!tags\" style=\"margin-top: 2%\">\n\t\t<div class=\"jumbotron\">\n\t\t\t<iframe [src]=\"transform(pdfUrl)\"></iframe>\n\t\t</div>\n\t\t<div class=\"navbar bg-primary\">\n\t\t\t<ul class=\"list-inline\">\n\t\t\t\t<li class=\"list-inline-item\">\n\t\t\t\t\t<b>\n\t\t\t\t\t\t<font size=\"3\" color=\"white\"><b>Share {{titleText}} page with your friends :</b></font>\n\t\t\t\t\t</b>\n\t\t\t\t</li>\n\t\t\t\t<li class=\"list-inline-item\">\n\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openFacebook()\">\n\t\t\t\t\t\t<img border=\"0\" id=\"fixed-width-flamingo\" src=\"/assets/images/facebook60.png\" />\n\t\t\t\t\t</a>\n\t\t\t\t</li>\n\t\t\t\t<li class=\"list-inline-item\">\n\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openWhatsApp()\">\n\t\t\t\t\t\t<img border=\"0\" id=\"fixed-width-flamingo\" src=\"/assets/images/whatsapp60.png\" />\n\t\t\t\t\t</a>\n\t\t\t\t</li>\n\t\t\t\t<li class=\"list-inline-item\">\n\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openMail()\">\n\t\t\t\t\t\t<img border=\"0\" id=\"fixed-width-flamingo\" src=\"/assets/images/gmail60.png\" />\n\t\t\t\t\t</a>\n\t\t\t\t</li>\n\t\t\t\t<li class=\"list-inline-item\">\n\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openMessenger()\">\n\t\t\t\t\t\t<img border=\"0\" id=\"fixed-width-flamingo\" src=\"/assets/images/mess60.png\" />\n\t\t\t\t\t</a>\n\t\t\t\t</li>\n\t\t\t</ul>\n\t\t</div>\n\t</div>\n\t<div class=\"row\" *ngIf=\"tags\">\n\t\t<div class=\"col-sm-8\">\n\t\t\t<div class=\"jumbotron\">\n\t\t\t\t<iframe [src]=\"transform(pdfUrl)\"></iframe>\n\t\t\t</div>\n\t\t\t<div class=\"navbar bg-primary\">\n\t\t\t\t<ul class=\"list-inline\">\n\t\t\t\t\t<li class=\"list-inline-item\">\n\t\t\t\t\t\t<font size=\"3\" color=\"white\"><b>Share {{titleText}} page with your friends :</b></font>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li class=\"list-inline-item\">\n\t\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openFacebook()\">\n\t\t\t\t\t\t\t<img border=\"0\" id=\"fixed-width-flamingo\" src=\"/assets/images/facebook60.png\" />\n\t\t\t\t\t\t</a>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li class=\"list-inline-item\">\n\t\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openWhatsApp()\">\n\t\t\t\t\t\t\t<img border=\"0\" id=\"fixed-width-flamingo\" src=\"/assets/images/whatsapp60.png\" />\n\t\t\t\t\t\t</a>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li class=\"list-inline-item\">\n\t\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openMail()\">\n\t\t\t\t\t\t\t<img border=\"0\" id=\"fixed-width-flamingo\" src=\"/assets/images/gmail60.png\" />\n\t\t\t\t\t\t</a>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li class=\"list-inline-item\">\n\t\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openMessenger()\">\n\t\t\t\t\t\t\t<img border=\"0\" id=\"fixed-width-flamingo\" src=\"/assets/images/mess60.png\" />\n\t\t\t\t\t\t</a>\n\t\t\t\t\t</li>\n\t\t\t\t</ul>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"jumbotron\" style=\"margin: 3%;\">\n\t\t\t<h5 style=\"text-align: center; color: #8b0000;\"><b>Related links</b></h5>\n\t\t\t<hr class=\"my-3\">\n\t\t\t<table>\n\t\t\t\t<tr *ngFor=\"let el of tags\">\n\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openTag(el)\">\n\t\t\t\t\t\t<!-- <td>\n\t\t\t\t\t\t\t<img src=\"{{el.relatedLinksImgUrl}}\" />\n\t\t\t\t\t\t</td> -->\n\t\t\t\t\t\t<td style=\"padding: 2%; text-align: center;\" valign=\"top\">\n\t\t\t\t\t\t\t<b style=\"color: #2c3e50;\">\n\t\t\t\t\t\t\t\t<u>\n\t\t\t\t\t\t\t\t\t{{el.relatedLinksHeading}}\n\t\t\t\t\t\t\t\t</u>\n\t\t\t\t\t\t\t</b>\n\t\t\t\t\t\t\t<br>\n\t\t\t\t\t\t\t<br>\n\t\t\t\t\t\t\t{{el.relatedLinksSmallDesc}}\n\t\t\t\t\t\t</td>\n\t\t\t\t\t\t<hr class=\"my-3\">\n\t\t\t\t\t</a>\n\t\t\t\t</tr>\n\t\t\t</table>\n\t\t</div>\n\t\t<div class=\"sbuttons\">\n\t\t\t<div *ngIf=\"isClickedShare\">\n\t\t\t\t<a [routerLink]=\"\" (click)=\"openWhatsApp()\" class=\"sbutton whatsapp\" tooltip=\"WhatsApp\"><i\n\t\t\t\t\t\tclass=\"fab fa-whatsapp\"></i></a>\n\n\t\t\t\t<a [routerLink]=\"\" (click)=\"openFacebook()\" class=\"sbutton fb\" tooltip=\"Facebook\"><i\n\t\t\t\t\t\tclass=\"fab fa-facebook-f\"></i></a>\n\n\t\t\t\t<a [routerLink]=\"\" (click)=\"openMail()\" class=\"sbutton gplus\" tooltip=\"Mail\"><i\n\t\t\t\t\t\tclass=\"fab fa-google-plus-g\"></i></a>\n\n\t\t\t\t<a [routerLink]=\"\" (click)=\"openMessenger()\" class=\"sbutton twitt\" tooltip=\"Fb messenger\"><i\n\t\t\t\t\t\tclass=\"fab fa-twitter\"></i></a>\n\t\t\t</div>\n\t\t\t<a [routerLink]=\"\" (click)=\"shareClicked()\" class=\"sbutton mainsbutton\"\n\t\t\t\ttooltip=\"Share this page with your friends\"><i class=\"fas fa-share-alt\"></i></a>\n\t\t</div>\n\t</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/search/search.component.html":
/*!************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/search/search.component.html ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div id=\"loader\" class=\"loader\" *ngIf=\"!pageData.data\"></div>\n<div class=\"main-content\">\n\t<div class=\"row\" style=\"padding-top: 2%\">\n\t\t<ol class=\"breadcrumb\" *ngFor=\"let headerNav of nav; let i = index\" [attr.data-index]=\"i\">\n\t\t\t<li class=\"breadcrumb-item\">\n\t\t\t\t<a [routerLink]=\"\" (click)=\"openPageFromNav(navIds[i])\">{{headerNav}}</a>\n\t\t\t</li>\n\t\t</ol>\n\t</div>\n\n\t<h3>Seach result for : {{searchTxt}}</h3>\n\t<div class=\"jumbotron\">\n\t\t\t<div *ngFor=\"let el of pageData.data\">\n\t\t\t\t<a [routerLink]=\"\" (click)=\"openPage(el.metaTitle, el.type, el.isChild)\">\n\t\t\t\t\t<p>{{el.title}}</p>\n\t\t\t\t</a>\n\t\t</div>\n\t</div>\n\t<div style=\"float: right;\">\n\t\t<a [routerLink]=\"\" (click)=\"openPreviousPage()\" class=\"previous\">&laquo; Previous</a>\n\t\t<a [routerLink]=\"\" (click)=\"openNextPage()\" class=\"next\" style=\"margin-left: 2em\">Next &raquo;</a>\n\t</div>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/study-met/desktop/desktop-study-met.component.html":
/*!**********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/study-met/desktop/desktop-study-met.component.html ***!
  \**********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div id=\"loader\" class=\"loader\" *ngIf=\"!pageData.data\"></div>\n<div style=\"background-image: url('assets/images/bg.jpg'); padding-bottom: 5%;\">\n\t<div class=\"main-content\">\n\t\t<div class=\"row\">\n\t\t\t<ol *ngFor=\"let headerNav of nav; let i = index\" [attr.data-index]=\"i\">\n\t\t\t\t<li class=\"row\">\n\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openPageFromNav(navIds[i], i)\"\n\t\t\t\t\t\ttitle=\"{{headerNav}} Study Material\"><b>{{headerNav}}</b></a>\n\t\t\t\t\t<div *ngIf=\"i!=navCount-1\" style=\"color: #008f72;\">&nbsp;&nbsp;&gt;</div>\n\t\t\t\t</li>\n\t\t\t</ol>\n\t\t</div>\n\t\t<hr>\n\t\t<div *ngIf=\"!tags\" style=\"margin-left: 5%; margin-right: 5%;\">\n\t\t\t<table class=\"table1\" *ngIf=\"groupUrl\">\n\t\t\t\t<tr>\n\t\t\t\t\t<td style=\"width: 75%;\">\n\t\t\t\t\t\t<h1 style=\"font-size: 2rem; color: #8b0000; text-shadow: 2px 2px 4px  rgba(0, 0, 0, 0.18);\"\n\t\t\t\t\t\t\ttitle=\"{{titleText}}\">\n\t\t\t\t\t\t\t<b>{{pageTitle}}</b></h1>\n\t\t\t\t\t</td>\n\t\t\t\t\t<td class=\"blink\">\n\t\t\t\t\t\t<a href=\"{{groupUrl}}\" target=\"_blank\">\n\t\t\t\t\t\t\t<img src=\"/assets/images/whatsapp_join.png\">\n\t\t\t\t\t\t</a>\n\t\t\t\t\t</td>\n\t\t\t\t</tr>\n\t\t\t</table>\n\t\t\t<table *ngIf=\"!groupUrl\">\n\t\t\t\t<tr>\n\t\t\t\t\t<td>\n\t\t\t\t\t\t<h1 style=\"font-size: 2rem; color: #8b0000; text-shadow: 2px 2px 4px  rgba(0, 0, 0, 0.18);\"\n\t\t\t\t\t\t\ttitle=\"{{titleText}}\">\n\t\t\t\t\t\t\t<b>{{pageTitle}}</b></h1>\n\t\t\t\t\t</td>\n\t\t\t\t</tr>\n\t\t\t</table>\n\t\t\t<div class=\"jumbotron\">\n\t\t\t\t<div class=\"container-fluid\" style=\"text-align: -webkit-center\">\n\t\t\t\t\t<div [innerHtml]=\"aboutExamContent\"> </div>\n\t\t\t\t\t<hr class=\"my-4\">\n\t\t\t\t\t<div *ngFor=\"let el of linkedPageItems\" class=\"card\">\n\t\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openNextPage(el.pageDocLink, el.pageItemName)\">\n\t\t\t\t\t\t\t<div class=\"banner\" *ngIf=\"el.coverUrl\"><img src=\"{{el.coverUrl}}\" alt=\"id {{el.pageId}}\">\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"banner\" *ngIf=\"!el.coverUrl\"><img src=\"/assets/images/logo1.png\"\n\t\t\t\t\t\t\t\t\talt=\"id {{el.pageId }}\"></div>\n\t\t\t\t\t\t\t<div class=\"descri\">{{el.pageItemName}}</div>\n\t\t\t\t\t\t</a>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"navbar bg-primary\">\n\t\t\t\t<ul class=\"list-inline\">\n\t\t\t\t\t<li class=\"list-inline-item\">\n\t\t\t\t\t\t<b>\n\t\t\t\t\t\t\t<font size=\"3\" color=\"white\">Share {{pageTitle}} page with your friends :\n\t\t\t\t\t\t\t</font>\n\t\t\t\t\t\t</b>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li class=\"list-inline-item\">\n\t\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openFacebook()\">\n\t\t\t\t\t\t\t<img border=\"0\" id=\"fixed-width-flamingo\" src=\"/assets/images/facebook60.png\" />\n\t\t\t\t\t\t</a>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li class=\"list-inline-item\">\n\t\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openWhatsApp()\">\n\t\t\t\t\t\t\t<img border=\"0\" id=\"fixed-width-flamingo\" src=\"/assets/images/whatsapp60.png\" />\n\t\t\t\t\t\t</a>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li class=\"list-inline-item\">\n\t\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openMail()\">\n\t\t\t\t\t\t\t<img border=\"0\" id=\"fixed-width-flamingo\" src=\"/assets/images/gmail60.png\" />\n\t\t\t\t\t\t</a>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li class=\"list-inline-item\">\n\t\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openMessenger()\">\n\t\t\t\t\t\t\t<img border=\"0\" id=\"fixed-width-flamingo\" src=\"/assets/images/mess60.png\" />\n\t\t\t\t\t\t</a>\n\t\t\t\t\t</li>\n\t\t\t\t</ul>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"row\" style=\"margin-top: 0%\" *ngIf=\"tags\">\n\t\t\t<table class=\"table1\" *ngIf=\"groupUrl\">\n\t\t\t\t<tr>\n\t\t\t\t\t<td style=\"width: 75%;\">\n\t\t\t\t\t\t<h1 style=\"font-size: 2rem; color: #8b0000; text-shadow: 2px 2px 4px  rgba(0, 0, 0, 0.18);\"\n\t\t\t\t\t\t\ttitle=\"{{titleText}}\">\n\t\t\t\t\t\t\t<b>{{pageTitle}}</b></h1>\n\t\t\t\t\t</td>\n\t\t\t\t\t<td class=\"blink\">\n\t\t\t\t\t\t<a href=\"{{groupUrl}}\" target=\"_blank\">\n\t\t\t\t\t\t\t<img src=\"/assets/images/whatsapp_join.png\">\n\t\t\t\t\t\t</a>\n\t\t\t\t\t</td>\n\t\t\t\t</tr>\n\t\t\t</table>\n\t\t\t<table *ngIf=\"!groupUrl\">\n\t\t\t\t<tr>\n\t\t\t\t\t<td>\n\t\t\t\t\t\t<h1 style=\"font-size: 2rem; color: #8b0000; text-shadow: 2px 2px 4px  rgba(0, 0, 0, 0.18);\"\n\t\t\t\t\t\t\ttitle=\"{{titleText}}\">\n\t\t\t\t\t\t\t<b>{{pageTitle}}</b></h1>\n\t\t\t\t\t</td>\n\t\t\t\t</tr>\n\t\t\t</table>\n\t\t\t<div class=\"col-sm-8\">\n\t\t\t\t<div class=\"container-fluid jumbotron\" style=\"padding: 1.25rem 1.2rem; text-align: -webkit-center\">\n\t\t\t\t\t<div [innerHtml]=\"aboutExamContent\"> </div>\n\t\t\t\t\t<hr class=\"my-4\">\n\t\t\t\t\t<div *ngFor=\"let el of linkedPageItems\" class=\"card\">\n\t\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openNextPage(el.pageDocLink, el.pageItemName)\">\n\t\t\t\t\t\t\t<div class=\"banner\" *ngIf=\"el.coverUrl\"><img src=\"{{el.coverUrl}}\" alt=\"id {{el.pageId }}\">\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"banner\" *ngIf=\"!el.coverUrl\"><img src=\"assets/finalrevise_book_cover.jpg\"\n\t\t\t\t\t\t\t\t\talt=\"id {{el.pageId }}\"></div>\n\t\t\t\t\t\t\t<div class=\"descri\">{{el.pageItemName}}</div>\n\t\t\t\t\t\t</a>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"navbar bg-primary\">\n\t\t\t\t\t<ul class=\"list-inline\">\n\t\t\t\t\t\t<li class=\"list-inline-item\">\n\t\t\t\t\t\t\t<b>\n\t\t\t\t\t\t\t\t<font size=\"3\" color=\"white\">Share {{pageTitle}} page with your friends :\n\t\t\t\t\t\t\t\t</font>\n\t\t\t\t\t\t\t</b>\n\t\t\t\t\t\t</li>\n\t\t\t\t\t\t<li class=\"list-inline-item\">\n\t\t\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openFacebook()\">\n\t\t\t\t\t\t\t\t<img border=\"0\" id=\"fixed-width-flamingo\" src=\"/assets/images/facebook60.png\" />\n\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t</li>\n\t\t\t\t\t\t<li class=\"list-inline-item\">\n\t\t\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openWhatsApp()\">\n\t\t\t\t\t\t\t\t<img border=\"0\" id=\"fixed-width-flamingo\" src=\"/assets/images/whatsapp60.png\" />\n\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t</li>\n\t\t\t\t\t\t<li class=\"list-inline-item\">\n\t\t\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openMail()\">\n\t\t\t\t\t\t\t\t<img border=\"0\" id=\"fixed-width-flamingo\" src=\"/assets/images/gmail60.png\" />\n\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t</li>\n\t\t\t\t\t\t<li class=\"list-inline-item\">\n\t\t\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openMessenger()\">\n\t\t\t\t\t\t\t\t<img border=\"0\" id=\"fixed-width-flamingo\" src=\"/assets/images/mess60.png\" />\n\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t</li>\n\t\t\t\t\t</ul>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"col-sm-3\">\n\t\t\t\t<div class=\"container-fluid jumbotron\" style=\"padding-left: 1.25rem; \n\t\tpadding-right: 1.25rem; padding-top: .25rem; padding-bottom: .25rem\">\n\t\t\t\t\t<h5 style=\"text-align: center; color: #8b0000;\"><b>Related links</b></h5>\n\t\t\t\t\t<hr class=\"my-3\">\n\t\t\t\t\t<div *ngFor=\"let el of tags\">\n\t\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openTag(el)\">\n\t\t\t\t\t\t\t<i>\n\t\t\t\t\t\t\t\t<h5>\n\t\t\t\t\t\t\t\t\t<li>{{el.pageTitle}}</li>\n\t\t\t\t\t\t\t\t</h5>\n\t\t\t\t\t\t\t</i>\n\t\t\t\t\t\t</a>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"sbuttons\">\n\t\t\t<div *ngIf=\"isClickedShare\">\n\t\t\t\t<a [routerLink]=\"\" (click)=\"openWhatsApp()\" class=\"sbutton whatsapp\" tooltip=\"WhatsApp\"><i\n\t\t\t\t\t\tclass=\"fab fa-whatsapp\"></i></a>\n\n\t\t\t\t<a [routerLink]=\"\" (click)=\"openFacebook()\" class=\"sbutton fb\" tooltip=\"Facebook\"><i\n\t\t\t\t\t\tclass=\"fab fa-facebook-f\"></i></a>\n\n\t\t\t\t<a [routerLink]=\"\" (click)=\"openMail()\" class=\"sbutton gplus\" tooltip=\"Mail\"><i\n\t\t\t\t\t\tclass=\"fab fa-google-plus-g\"></i></a>\n\n\t\t\t\t<a [routerLink]=\"\" (click)=\"openMessenger()\" class=\"sbutton twitt\" tooltip=\"Fb messenger\"><i\n\t\t\t\t\t\tclass=\"fab fa-twitter\"></i></a>\n\t\t\t</div>\n\t\t\t<a [routerLink]=\"\" (click)=\"shareClicked()\" class=\"sbutton mainsbutton\"\n\t\t\t\ttooltip=\"Share this page with your friends\"><i class=\"fas fa-share-alt\"></i></a>\n\t\t</div>\n\t</div>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/study-met/mobile/dialog-app-install.html":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/study-met/mobile/dialog-app-install.html ***!
  \************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<mat-dialog-content class=\"mat-typography\">\n\t<ul class=\"list-inline\">\n\t\t<li class=\"list-inline-item\">\n\t\t\t<h3 mat-dialog-title align=\"center\" style=\"color: black\"><b>Share {{data.paperName}} page with your friends.</b></h3>\n    </li>\n    <div align=\"center\">\n\t\t<li class=\"list-inline-item\">\n\t\t\t<a [routerLink]=\"\" (click)=\"openFacebook()\" >\n\t\t\t\t<img border=\"0\" id=\"fixed-width-flamingo\" style=\"height: 30px; width: 30px\" src=\"/assets/images/facebook60.png\" />\n\t\t\t</a>\n\t\t</li>\n\t\t<li class=\"list-inline-item\">\n\t\t\t<a [routerLink]=\"\" (click)=\"openWhatsApp()\">\n\t\t\t\t<img border=\"0\" id=\"fixed-width-flamingo\" style=\"height: 30px; width: 30px\" src=\"/assets/images/whatsapp60.png\" />\n\t\t\t</a>\n\t\t</li>\n\t\t<li class=\"list-inline-item\">\n\t\t\t<a [routerLink]=\"\" (click)=\"openMail()\">\n\t\t\t\t<img border=\"0\" id=\"fixed-width-flamingo\" style=\"height: 30px; width: 30px\" src=\"/assets/images/gmail60.png\" />\n\t\t\t</a>\n\t\t</li>\n\t\t<li class=\"list-inline-item\">\n\t\t\t<a [routerLink]=\"\" (click)=\"openMessenger()\">\n\t\t\t\t<img border=\"0\" id=\"fixed-width-flamingo\" style=\"height: 30px; width: 30px\" src=\"/assets/images/mess60.png\" />\n\t\t\t</a>\n    </li>\n  </div>\n\t</ul>\n</mat-dialog-content>\n<h2 mat-dialog-title align=\"center\">---- OR ----</h2>\n<mat-dialog-content class=\"mat-typography\">\n    <h4 mat-dialog-title align=\"center\" style=\"color: #2c3e50\"><b>Manage your study material in a easy way</b></h4>\n  <p align=\"justify\">Now you can bookmark and download unlimited papers, notes, books and assignment for free.</p>\n  <p> Download FinalRevise mobile App from Google Play store.</p>\n</mat-dialog-content>\n<p style=\"color: red\">Timer for 15 seconds : {{tick}}</p>\n<mat-dialog-actions align=\"end\">\n  <button [hidden]=\"!isValid\" (click)=\"isValid && openDialog()\" mat-button>Cancel</button>\n  <!-- <button (click)=\"openShare()\" mat-button>Share</button> -->\n  <button mat-button [mat-dialog-close]=\"true\" cdkFocusInitial onclick=\"window.location.href='https://play.google.com/store/apps/details?id=com.finalrevise.android'\"\n    target=\"_blank\">Install</button>\n</mat-dialog-actions>\n\n\n<!-- Copyright 2019 Google Inc. All Rights Reserved.\n    Use of this source code is governed by an MIT-style license that\n    can be found in the LICENSE file at http://angular.io/license -->");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/study-met/mobile/mobile-study-met.component.html":
/*!********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/study-met/mobile/mobile-study-met.component.html ***!
  \********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div id=\"loader\" class=\"loader\" *ngIf=\"!pageData.data\"></div>\n<div style=\"background-image: url('assets/images/bg.jpg'); padding-bottom: 5%;\">\n\t<div class=\"main-content\">\n\t\t<h5 class=\"row goBack\" onclick=\"window.history.back()\">\n\t\t\t<div style=\"margin-top: -.4%;padding-right: 2%;\"><b>&nbsp; &lt;</b></div><b>Back</b>\n\t\t</h5>\n\t\t<hr>\n\t\t<h1 style=\"font-size: 1.6rem; color: #8b0000;\n\t\ttext-shadow: 2px 2px 4px  rgba(0, 0, 0, 0.18); text-align: center;\" title=\"{{titleText}}\">\n\t\t\t<b>{{pageTitle}}</b></h1>\n\t\t<a *ngIf=\"groupUrl\">\n\t\t\t<img class=\"center\" src=\"/assets/images/whatsapp_join.png\">\n\t\t</a>\n\t\t<div class=\"row\" style=\"margin-top: 0%\">\n\t\t\t<div class=\"col-sm-8\">\n\t\t\t\t<div class=\"container-fluid jumbotron\" style=\"padding: 1.25rem 1.2rem; text-align: -webkit-center\">\n\t\t\t\t\t<div [innerHtml]=\"aboutExamContent\"> </div>\n\t\t\t\t\t<hr class=\"my-4\">\n\t\t\t\t\t<div *ngFor=\"let el of linkedPageItems\" class=\"card\">\n\t\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openNextPage(el.pageDocLink, el.pageItemName)\">\n\t\t\t\t\t\t\t<div class=\"banner\" *ngIf=\"el.coverUrl\"><img src=\"{{el.coverUrl}}\" alt=\"id {{el.pageId }}\">\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"banner\" *ngIf=\"!el.coverUrl\"><img src=\"/assets/images/logo1.png\"\n\t\t\t\t\t\t\t\t\talt=\"id {{el.pageId }}\"></div>\n\t\t\t\t\t\t\t<div class=\"descri\">{{el.pageItemName}}</div>\n\t\t\t\t\t\t</a>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"navbar bg-primary\">\n\t\t\t\t\t<ul class=\"list-inline\">\n\t\t\t\t\t\t<li class=\"list-inline-item\">\n\t\t\t\t\t\t\t<b>\n\t\t\t\t\t\t\t\t<font size=\"3\" color=\"white\">Share {{pageTitle}} page with your friends :\n\t\t\t\t\t\t\t\t</font>\n\t\t\t\t\t\t\t</b>\n\t\t\t\t\t\t</li>\n\t\t\t\t\t\t<li class=\"list-inline-item\">\n\t\t\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openFacebook()\">\n\t\t\t\t\t\t\t\t<img border=\"0\" id=\"fixed-width-flamingo\" src=\"/assets/images/facebook60.png\" />\n\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t</li>\n\t\t\t\t\t\t<li class=\"list-inline-item\">\n\t\t\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openWhatsApp()\">\n\t\t\t\t\t\t\t\t<img border=\"0\" id=\"fixed-width-flamingo\" src=\"/assets/images/whatsapp60.png\" />\n\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t</li>\n\t\t\t\t\t\t<li class=\"list-inline-item\">\n\t\t\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openMail()\">\n\t\t\t\t\t\t\t\t<img border=\"0\" id=\"fixed-width-flamingo\" src=\"/assets/images/gmail60.png\" />\n\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t</li>\n\t\t\t\t\t\t<li class=\"list-inline-item\">\n\t\t\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openMessenger()\">\n\t\t\t\t\t\t\t\t<img border=\"0\" id=\"fixed-width-flamingo\" src=\"/assets/images/mess60.png\" />\n\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t</li>\n\t\t\t\t\t</ul>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"col-sm-3\" *ngIf=\"tags\">\n\t\t\t\t<div class=\"container-fluid jumbotron\" style=\"padding-left: 1.25rem; \n\t\tpadding-right: 1.25rem; padding-top: .25rem; padding-bottom: .25rem\">\n\t\t\t\t\t<h5 style=\"text-align: center; color: #8b0000;\"><b>Related links</b></h5>\n\t\t\t\t\t<hr class=\"my-3\">\n\t\t\t\t\t<div *ngFor=\"let el of tags\">\n\t\t\t\t\t\t<a [routerLink]=\"\" (click)=\"openTag(el)\">\n\t\t\t\t\t\t\t<i>\n\t\t\t\t\t\t\t\t<h5>\n\t\t\t\t\t\t\t\t\t<li>{{el.pageTitle}}</li>\n\t\t\t\t\t\t\t\t</h5>\n\t\t\t\t\t\t\t</i>\n\t\t\t\t\t\t</a>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"sbuttons\">\n\t\t\t<div *ngIf=\"isClickedShare\">\n\t\t\t\t<a [routerLink]=\"\" (click)=\"openWhatsApp()\" class=\"sbutton whatsapp\" tooltip=\"WhatsApp\"><i\n\t\t\t\t\t\tclass=\"fab fa-whatsapp\"></i></a>\n\n\t\t\t\t<a [routerLink]=\"\" (click)=\"openFacebook()\" class=\"sbutton fb\" tooltip=\"Facebook\"><i\n\t\t\t\t\t\tclass=\"fab fa-facebook-f\"></i></a>\n\n\t\t\t\t<a [routerLink]=\"\" (click)=\"openMail()\" class=\"sbutton gplus\" tooltip=\"Mail\"><i\n\t\t\t\t\t\tclass=\"fab fa-google-plus-g\"></i></a>\n\n\t\t\t\t<a [routerLink]=\"\" (click)=\"openMessenger()\" class=\"sbutton twitt\" tooltip=\"Fb messenger\"><i\n\t\t\t\t\t\tclass=\"fab fa-twitter\"></i></a>\n\t\t\t</div>\n\t\t\t<a [routerLink]=\"\" (click)=\"shareClicked()\" class=\"sbutton mainsbutton\"\n\t\t\t\ttooltip=\"Share this page with your friends\"><i class=\"fas fa-share-alt\"></i></a>\n\t\t</div>\n\t</div>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/subcategory/desktop/subcat-desktop.component.html":
/*!*********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/subcategory/desktop/subcat-desktop.component.html ***!
  \*********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div id=\"loader\" class=\"loader\" *ngIf=\"!pageData.data\"></div>\n<div class=\"parallax\">\n    <div style=\"text-align: center\">\n      <div *ngFor=\"let el of pageData.data\" class=\"card\">\n        <div class=\"hover01 column\">\n        <a [routerLink]=\"\" (click)=\"openNextPage(el.tag)\"\n          style=\"text-decoration: none;color: inherit\">\n          <figure><img src=\"{{el.entityImage}}\" alt=\"Card image\"></figure>\n          <div class=\"descri\"><b>{{el.entityName}}</b></div>\n        </a>\n      </div>\n    </div>\n    </div>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/subcategory/mobile/subcat-mobile.component.html":
/*!*******************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/subcategory/mobile/subcat-mobile.component.html ***!
  \*******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div id=\"loader\" class=\"loader\" *ngIf=\"!pageData.data\"></div>\n<div class=\"main-content\">\n  <div style=\"text-align: center\">\n    <div *ngFor=\"let el of pageData.data\" class=\"card\">\n      <a [routerLink]=\"\" (click)=\"openNextPage(el.tag)\"\n        style=\"text-decoration: none;color: inherit\">\n        <img src=\"{{el.entityImage}}\" alt=\"Card image\">\n        <div class=\"descri\"><b>{{el.entityName}}</b></div>\n      </a>\n    </div>\n  </div>\n</div>");

/***/ }),

/***/ "./node_modules/tslib/tslib.es6.js":
/*!*****************************************!*\
  !*** ./node_modules/tslib/tslib.es6.js ***!
  \*****************************************/
/*! exports provided: __extends, __assign, __rest, __decorate, __param, __metadata, __awaiter, __generator, __exportStar, __values, __read, __spread, __spreadArrays, __await, __asyncGenerator, __asyncDelegator, __asyncValues, __makeTemplateObject, __importStar, __importDefault */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__extends", function() { return __extends; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__assign", function() { return __assign; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__rest", function() { return __rest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__decorate", function() { return __decorate; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__param", function() { return __param; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__metadata", function() { return __metadata; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__awaiter", function() { return __awaiter; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__generator", function() { return __generator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__exportStar", function() { return __exportStar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__values", function() { return __values; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__read", function() { return __read; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__spread", function() { return __spread; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__spreadArrays", function() { return __spreadArrays; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__await", function() { return __await; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncGenerator", function() { return __asyncGenerator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncDelegator", function() { return __asyncDelegator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncValues", function() { return __asyncValues; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__makeTemplateObject", function() { return __makeTemplateObject; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__importStar", function() { return __importStar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__importDefault", function() { return __importDefault; });
/*! *****************************************************************************
Copyright (c) Microsoft Corporation. All rights reserved.
Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at http://www.apache.org/licenses/LICENSE-2.0

THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
MERCHANTABLITY OR NON-INFRINGEMENT.

See the Apache Version 2.0 License for specific language governing permissions
and limitations under the License.
***************************************************************************** */
/* global Reflect, Promise */

var extendStatics = function(d, b) {
    extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return extendStatics(d, b);
};

function __extends(d, b) {
    extendStatics(d, b);
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
}

var __assign = function() {
    __assign = Object.assign || function __assign(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
    }
    return __assign.apply(this, arguments);
}

function __rest(s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
}

function __decorate(decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
}

function __param(paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
}

function __metadata(metadataKey, metadataValue) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
}

function __awaiter(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
}

function __generator(thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
}

function __exportStar(m, exports) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}

function __values(o) {
    var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
    if (m) return m.call(o);
    return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
}

function __read(o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
}

function __spread() {
    for (var ar = [], i = 0; i < arguments.length; i++)
        ar = ar.concat(__read(arguments[i]));
    return ar;
}

function __spreadArrays() {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};

function __await(v) {
    return this instanceof __await ? (this.v = v, this) : new __await(v);
}

function __asyncGenerator(thisArg, _arguments, generator) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var g = generator.apply(thisArg, _arguments || []), i, q = [];
    return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
    function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
    function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
    function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
    function fulfill(value) { resume("next", value); }
    function reject(value) { resume("throw", value); }
    function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
}

function __asyncDelegator(o) {
    var i, p;
    return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
    function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
}

function __asyncValues(o) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var m = o[Symbol.asyncIterator], i;
    return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
    function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
    function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
}

function __makeTemplateObject(cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};

function __importStar(mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result.default = mod;
    return result;
}

function __importDefault(mod) {
    return (mod && mod.__esModule) ? mod : { default: mod };
}


/***/ }),

/***/ "./src/app/CustomUrlSerializer.ts":
/*!****************************************!*\
  !*** ./src/app/CustomUrlSerializer.ts ***!
  \****************************************/
/*! exports provided: CustomUrlSerializer */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomUrlSerializer", function() { return CustomUrlSerializer; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");



let CustomUrlSerializer = class CustomUrlSerializer {
    parse(url) {
        const dus = new _angular_router__WEBPACK_IMPORTED_MODULE_1__["DefaultUrlSerializer"]();
        return dus.parse(url.replace(/\&/g, '%26').replace("-/", "-%2F"));
    }
    serialize(tree) {
        const dus = new _angular_router__WEBPACK_IMPORTED_MODULE_1__["DefaultUrlSerializer"]();
        return dus.serialize(tree).replace(/%26/g, '&').replace("-/", "-%2F");
    }
};
CustomUrlSerializer = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])()
], CustomUrlSerializer);



/***/ }),

/***/ "./src/app/about/about.component.css":
/*!*******************************************!*\
  !*** ./src/app/about/about.component.css ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".jumbotron{\n    padding:0rem 0rem;margin-bottom:0rem;background-color:#ecf0f1;border-radius:0.3rem;\n}\n@media (min-width: 576px){\n    .jumbotron{padding:1rem 1rem}\n}\n.main-content {\n    padding-top: 7.2%;\n    padding-left: 2%;\n    padding-right: 2%;\n    min-height: 80vh;\n    padding-bottom: 2%;\n    background-image: url('/assets/images/bg.jpg');\n  }\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYWJvdXQvYWJvdXQuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLGlCQUFpQixDQUFDLGtCQUFrQixDQUFDLHdCQUF3QixDQUFDLG9CQUFvQjtBQUN0RjtBQUNBO0lBQ0ksV0FBVyxpQkFBaUI7QUFDaEM7QUFFQTtJQUNJLGlCQUFpQjtJQUNqQixnQkFBZ0I7SUFDaEIsaUJBQWlCO0lBQ2pCLGdCQUFnQjtJQUNoQixrQkFBa0I7SUFDbEIsOENBQThDO0VBQ2hEIiwiZmlsZSI6InNyYy9hcHAvYWJvdXQvYWJvdXQuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5qdW1ib3Ryb257XG4gICAgcGFkZGluZzowcmVtIDByZW07bWFyZ2luLWJvdHRvbTowcmVtO2JhY2tncm91bmQtY29sb3I6I2VjZjBmMTtib3JkZXItcmFkaXVzOjAuM3JlbTtcbn1cbkBtZWRpYSAobWluLXdpZHRoOiA1NzZweCl7XG4gICAgLmp1bWJvdHJvbntwYWRkaW5nOjFyZW0gMXJlbX1cbn1cblxuLm1haW4tY29udGVudCB7XG4gICAgcGFkZGluZy10b3A6IDcuMiU7XG4gICAgcGFkZGluZy1sZWZ0OiAyJTtcbiAgICBwYWRkaW5nLXJpZ2h0OiAyJTtcbiAgICBtaW4taGVpZ2h0OiA4MHZoO1xuICAgIHBhZGRpbmctYm90dG9tOiAyJTtcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoJy9hc3NldHMvaW1hZ2VzL2JnLmpwZycpO1xuICB9Il19 */");

/***/ }),

/***/ "./src/app/about/about.component.ts":
/*!******************************************!*\
  !*** ./src/app/about/about.component.ts ***!
  \******************************************/
/*! exports provided: AboutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AboutComponent", function() { return AboutComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var rxjs_Rx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/Rx */ "./node_modules/rxjs-compat/_esm2015/Rx.js");



let AboutComponent = class AboutComponent {
};
AboutComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'about',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./about.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/about/about.component.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./about.component.css */ "./src/app/about/about.component.css")).default]
    })
], AboutComponent);



/***/ }),

/***/ "./src/app/app-header/app-header.component.scss":
/*!******************************************************!*\
  !*** ./src/app/app-header/app-header.component.scss ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("@media (max-width: 700px) {\n  .navbar {\n    display: flex;\n    flex-wrap: wrap;\n    align-items: center;\n    justify-content: space-between;\n    padding: .5%;\n  }\n  .image-style {\n    border: 0;\n    height: 40px;\n    width: 40px;\n  }\n  ul {\n    margin: 0;\n  }\n  a {\n    color: white;\n    text-decoration: none;\n    background-color: transparent;\n    -webkit-text-decoration-skip: objects;\n    -webkit-text-size-adjust: inherit;\n       -moz-text-size-adjust: inherit;\n        -ms-text-size-adjust: inherit;\n            text-size-adjust: inherit;\n  }\n  .list-inline-item:not(:last-child) {\n    margin-right: 2px;\n  }\n  #fixed-width-flamingo {\n    width: 20px;\n    height: 20px;\n  }\n}\n\n@media (min-width: 700px) {\n  .image-style {\n    border: 0;\n    height: 50px;\n    width: 50px;\n  }\n  .navbar {\n    display: flex;\n    flex-wrap: wrap;\n    align-items: center;\n    justify-content: space-between;\n    padding: .8%;\n  }\n  ul {\n    margin: 0;\n  }\n  a {\n    color: white;\n    text-decoration: none;\n    background-color: transparent;\n    -webkit-text-decoration-skip: objects;\n    -webkit-text-size-adjust: inherit;\n       -moz-text-size-adjust: inherit;\n        -ms-text-size-adjust: inherit;\n            text-size-adjust: inherit;\n  }\n  #fixed-width-flamingo {\n    width: 30px;\n    height: 30px;\n  }\n}\n\n.bg-primary {\n  background-color: #2c3e50 !important;\n  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);\n}\n\n.navbar-brand {\n  color: white;\n}\n\n.matBtn {\n  -webkit-appearance: button;\n  text-rendering: auto;\n  color: buttontext;\n  letter-spacing: normal;\n  word-spacing: normal;\n  text-transform: none;\n  text-indent: 0px;\n  text-shadow: none;\n  display: inline-block;\n  text-align: center;\n  align-items: flex-start;\n  cursor: default;\n  background-color: buttonface;\n  box-sizing: border-box;\n  margin: 0em;\n  font: 400 11px system-ui;\n  padding: 1px 7px 2px;\n  border-width: 1px;\n  border-style: solid;\n  -o-border-image: initial;\n     border-image: initial;\n  background-color: #2c3e50;\n  border-color: #2c3e50;\n  color: white;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Wb2x1bWVzL0ZpbmFsUmV2aXNlL2ZpbmFscmV2aXNlX3dlYnNpdGUvc3JjL2FwcC9hcHAtaGVhZGVyL2FwcC1oZWFkZXIuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2FwcC1oZWFkZXIvYXBwLWhlYWRlci5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUVBO0lBQ0ksYUFBYTtJQUNiLGVBQWU7SUFDZixtQkFBbUI7SUFDbkIsOEJBQThCO0lBQzlCLFlBQVk7RUNBZDtFREVGO0lBQ0ksU0FBUztJQUNULFlBQVk7SUFDWixXQUFXO0VDQWI7RURFRjtJQUNJLFNBQVM7RUNBWDtFREVGO0lBQ0ksWUFBWTtJQUNaLHFCQUFxQjtJQUNyQiw2QkFBNkI7SUFDN0IscUNBQXFDO0lBQ3JDLGlDQUF5QjtPQUF6Qiw4QkFBeUI7UUFBekIsNkJBQXlCO1lBQXpCLHlCQUF5QjtFQ0EzQjtFREVGO0lBQ0ksaUJBQWlCO0VDQW5CO0VERUY7SUFDSSxXQUFXO0lBQ1gsWUFBWTtFQ0FkO0FBQ0Y7O0FER0E7RUFFQTtJQUNJLFNBQVM7SUFDVCxZQUFZO0lBQ1osV0FBVztFQ0RiO0VER0Y7SUFDSSxhQUFhO0lBQ2IsZUFBZTtJQUNmLG1CQUFtQjtJQUNuQiw4QkFBOEI7SUFDOUIsWUFBWTtFQ0RkO0VER0Y7SUFDSSxTQUFTO0VDRFg7RURHRjtJQUNJLFlBQVk7SUFDWixxQkFBcUI7SUFDckIsNkJBQTZCO0lBQzdCLHFDQUFxQztJQUNyQyxpQ0FBeUI7T0FBekIsOEJBQXlCO1FBQXpCLDZCQUF5QjtZQUF6Qix5QkFBeUI7RUNEM0I7RURHRjtJQUNJLFdBQVc7SUFDWCxZQUFZO0VDRGQ7QUFDRjs7QURHQTtFQUNJLG9DQUFtQztFQUNuQyw0RUFBNEU7QUNBaEY7O0FERUE7RUFDSSxZQUFZO0FDQ2hCOztBRENBO0VBQ0ksMEJBQTBCO0VBQzFCLG9CQUFvQjtFQUNwQixpQkFBaUI7RUFDakIsc0JBQXNCO0VBQ3RCLG9CQUFvQjtFQUNwQixvQkFBb0I7RUFDcEIsZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQixxQkFBcUI7RUFDckIsa0JBQWtCO0VBQ2xCLHVCQUF1QjtFQUN2QixlQUFlO0VBQ2YsNEJBQTRCO0VBQzVCLHNCQUFzQjtFQUN0QixXQUFXO0VBQ1gsd0JBQXdCO0VBQ3hCLG9CQUFvQjtFQUNwQixpQkFBaUI7RUFDakIsbUJBQW1CO0VBQ25CLHdCQUFxQjtLQUFyQixxQkFBcUI7RUFDckIseUJBQXlCO0VBQ3pCLHFCQUFxQjtFQUNyQixZQUFZO0FDRWhCIiwiZmlsZSI6InNyYy9hcHAvYXBwLWhlYWRlci9hcHAtaGVhZGVyLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQG1lZGlhIChtYXgtd2lkdGg6IDcwMHB4KSBcbntcbi5uYXZiYXIge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC13cmFwOiB3cmFwO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgIHBhZGRpbmc6IC41JTtcbn1cbi5pbWFnZS1zdHlsZXtcbiAgICBib3JkZXI6IDA7XG4gICAgaGVpZ2h0OiA0MHB4O1xuICAgIHdpZHRoOiA0MHB4O1xufVxudWwge1xuICAgIG1hcmdpbjogMDtcbn1cbmEge1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gICAgLXdlYmtpdC10ZXh0LWRlY29yYXRpb24tc2tpcDogb2JqZWN0cztcbiAgICB0ZXh0LXNpemUtYWRqdXN0OiBpbmhlcml0O1xufVxuLmxpc3QtaW5saW5lLWl0ZW06bm90KDpsYXN0LWNoaWxkKSB7XG4gICAgbWFyZ2luLXJpZ2h0OiAycHg7XG59XG4jZml4ZWQtd2lkdGgtZmxhbWluZ28geyAgXG4gICAgd2lkdGg6IDIwcHg7IFxuICAgIGhlaWdodDogMjBweDtcbn0gXG59XG5cbkBtZWRpYSAobWluLXdpZHRoOiA3MDBweCkgXG57XG4uaW1hZ2Utc3R5bGV7XG4gICAgYm9yZGVyOiAwO1xuICAgIGhlaWdodDogNTBweDtcbiAgICB3aWR0aDogNTBweDtcbn1cbi5uYXZiYXIge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC13cmFwOiB3cmFwO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgIHBhZGRpbmc6IC44JTtcbn1cbnVsIHtcbiAgICBtYXJnaW46IDA7XG59XG5hIHtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xuICAgIC13ZWJraXQtdGV4dC1kZWNvcmF0aW9uLXNraXA6IG9iamVjdHM7XG4gICAgdGV4dC1zaXplLWFkanVzdDogaW5oZXJpdDtcbn1cbiNmaXhlZC13aWR0aC1mbGFtaW5nbyB7ICBcbiAgICB3aWR0aDogMzBweDsgXG4gICAgaGVpZ2h0OiAzMHB4O1xufVxufVxuLmJnLXByaW1hcnl7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzJjM2U1MCFpbXBvcnRhbnQ7XG4gICAgYm94LXNoYWRvdzogMCA0cHggOHB4IDAgcmdiYSgwLCAwLCAwLCAwLjIpLCAwIDZweCAyMHB4IDAgcmdiYSgwLCAwLCAwLCAwLjE5KVxufVxuLm5hdmJhci1icmFuZHtcbiAgICBjb2xvcjogd2hpdGU7XG59XG4ubWF0QnRuIHtcbiAgICAtd2Via2l0LWFwcGVhcmFuY2U6IGJ1dHRvbjtcbiAgICB0ZXh0LXJlbmRlcmluZzogYXV0bztcbiAgICBjb2xvcjogYnV0dG9udGV4dDtcbiAgICBsZXR0ZXItc3BhY2luZzogbm9ybWFsO1xuICAgIHdvcmQtc3BhY2luZzogbm9ybWFsO1xuICAgIHRleHQtdHJhbnNmb3JtOiBub25lO1xuICAgIHRleHQtaW5kZW50OiAwcHg7XG4gICAgdGV4dC1zaGFkb3c6IG5vbmU7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcbiAgICBjdXJzb3I6IGRlZmF1bHQ7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogYnV0dG9uZmFjZTtcbiAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICAgIG1hcmdpbjogMGVtO1xuICAgIGZvbnQ6IDQwMCAxMXB4IHN5c3RlbS11aTtcbiAgICBwYWRkaW5nOiAxcHggN3B4IDJweDtcbiAgICBib3JkZXItd2lkdGg6IDFweDtcbiAgICBib3JkZXItc3R5bGU6IHNvbGlkO1xuICAgIGJvcmRlci1pbWFnZTogaW5pdGlhbDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMmMzZTUwO1xuICAgIGJvcmRlci1jb2xvcjogIzJjM2U1MDtcbiAgICBjb2xvcjogd2hpdGU7XG59IiwiQG1lZGlhIChtYXgtd2lkdGg6IDcwMHB4KSB7XG4gIC5uYXZiYXIge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC13cmFwOiB3cmFwO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgIHBhZGRpbmc6IC41JTtcbiAgfVxuICAuaW1hZ2Utc3R5bGUge1xuICAgIGJvcmRlcjogMDtcbiAgICBoZWlnaHQ6IDQwcHg7XG4gICAgd2lkdGg6IDQwcHg7XG4gIH1cbiAgdWwge1xuICAgIG1hcmdpbjogMDtcbiAgfVxuICBhIHtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xuICAgIC13ZWJraXQtdGV4dC1kZWNvcmF0aW9uLXNraXA6IG9iamVjdHM7XG4gICAgdGV4dC1zaXplLWFkanVzdDogaW5oZXJpdDtcbiAgfVxuICAubGlzdC1pbmxpbmUtaXRlbTpub3QoOmxhc3QtY2hpbGQpIHtcbiAgICBtYXJnaW4tcmlnaHQ6IDJweDtcbiAgfVxuICAjZml4ZWQtd2lkdGgtZmxhbWluZ28ge1xuICAgIHdpZHRoOiAyMHB4O1xuICAgIGhlaWdodDogMjBweDtcbiAgfVxufVxuXG5AbWVkaWEgKG1pbi13aWR0aDogNzAwcHgpIHtcbiAgLmltYWdlLXN0eWxlIHtcbiAgICBib3JkZXI6IDA7XG4gICAgaGVpZ2h0OiA1MHB4O1xuICAgIHdpZHRoOiA1MHB4O1xuICB9XG4gIC5uYXZiYXIge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC13cmFwOiB3cmFwO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgIHBhZGRpbmc6IC44JTtcbiAgfVxuICB1bCB7XG4gICAgbWFyZ2luOiAwO1xuICB9XG4gIGEge1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gICAgLXdlYmtpdC10ZXh0LWRlY29yYXRpb24tc2tpcDogb2JqZWN0cztcbiAgICB0ZXh0LXNpemUtYWRqdXN0OiBpbmhlcml0O1xuICB9XG4gICNmaXhlZC13aWR0aC1mbGFtaW5nbyB7XG4gICAgd2lkdGg6IDMwcHg7XG4gICAgaGVpZ2h0OiAzMHB4O1xuICB9XG59XG5cbi5iZy1wcmltYXJ5IHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzJjM2U1MCAhaW1wb3J0YW50O1xuICBib3gtc2hhZG93OiAwIDRweCA4cHggMCByZ2JhKDAsIDAsIDAsIDAuMiksIDAgNnB4IDIwcHggMCByZ2JhKDAsIDAsIDAsIDAuMTkpO1xufVxuXG4ubmF2YmFyLWJyYW5kIHtcbiAgY29sb3I6IHdoaXRlO1xufVxuXG4ubWF0QnRuIHtcbiAgLXdlYmtpdC1hcHBlYXJhbmNlOiBidXR0b247XG4gIHRleHQtcmVuZGVyaW5nOiBhdXRvO1xuICBjb2xvcjogYnV0dG9udGV4dDtcbiAgbGV0dGVyLXNwYWNpbmc6IG5vcm1hbDtcbiAgd29yZC1zcGFjaW5nOiBub3JtYWw7XG4gIHRleHQtdHJhbnNmb3JtOiBub25lO1xuICB0ZXh0LWluZGVudDogMHB4O1xuICB0ZXh0LXNoYWRvdzogbm9uZTtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xuICBjdXJzb3I6IGRlZmF1bHQ7XG4gIGJhY2tncm91bmQtY29sb3I6IGJ1dHRvbmZhY2U7XG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gIG1hcmdpbjogMGVtO1xuICBmb250OiA0MDAgMTFweCBzeXN0ZW0tdWk7XG4gIHBhZGRpbmc6IDFweCA3cHggMnB4O1xuICBib3JkZXItd2lkdGg6IDFweDtcbiAgYm9yZGVyLXN0eWxlOiBzb2xpZDtcbiAgYm9yZGVyLWltYWdlOiBpbml0aWFsO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMmMzZTUwO1xuICBib3JkZXItY29sb3I6ICMyYzNlNTA7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cbiJdfQ== */");

/***/ }),

/***/ "./src/app/app-header/app-header.component.ts":
/*!****************************************************!*\
  !*** ./src/app/app-header/app-header.component.ts ***!
  \****************************************************/
/*! exports provided: AppHeaderDesktopComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppHeaderDesktopComponent", function() { return AppHeaderDesktopComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _application_state_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../application-state.service */ "./src/app/application-state.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _header_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./header.service */ "./src/app/app-header/header.service.ts");





let AppHeaderDesktopComponent = class AppHeaderDesktopComponent {
    constructor(applicationStateService, router, headerService) {
        this.applicationStateService = applicationStateService;
        this.router = router;
        this.headerService = headerService;
        this.isRelatedTopicsPresent = false;
        this.isHome = false;
        this.isMobileResolution = this.applicationStateService.getIsMobileResolution();
    }
    ngOnInit() {
        this.initRelatedTopics();
    }
    initRelatedTopics() {
        this.headerService.relatedTopics.subscribe(relatedTopics => {
            if (relatedTopics != "relatedTopics") {
                this.relatedTopics = relatedTopics;
                this.isRelatedTopicsPresent = this.relatedTopics.length > 0;
            }
        });
        this.headerService.isHome.subscribe(isHome => {
            if (isHome != "isHome") {
                this.isHome = true;
                this.isRelatedTopicsPresent = false;
            }
            else {
                this.isHome = false;
            }
        });
    }
    openTabPage(isChild, cat, relatedLinksType) {
        if (isChild == 1) {
            this.router.navigateByUrl('/detail-list/' + cat);
        }
        else if (isChild == 2 && relatedLinksType.includes("Book")) {
            this.router.navigateByUrl('/study-met/' + cat);
        }
        else if (isChild == 2) {
            this.router.navigateByUrl('/detail/' + cat);
        }
        else if (isChild == 0) {
            this.router.navigate(['/subcat/' + cat]);
        }
    }
};
AppHeaderDesktopComponent.ctorParameters = () => [
    { type: _application_state_service__WEBPACK_IMPORTED_MODULE_2__["ApplicationStateService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _header_service__WEBPACK_IMPORTED_MODULE_4__["HeaderService"] }
];
AppHeaderDesktopComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-header',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./app-header.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/app-header/app-header.component.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./app-header.component.scss */ "./src/app/app-header/app-header.component.scss")).default]
    })
], AppHeaderDesktopComponent);



/***/ }),

/***/ "./src/app/app-header/header.service.ts":
/*!**********************************************!*\
  !*** ./src/app/app-header/header.service.ts ***!
  \**********************************************/
/*! exports provided: HeaderService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderService", function() { return HeaderService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var rxjs_BehaviorSubject__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/BehaviorSubject */ "./node_modules/rxjs-compat/_esm2015/BehaviorSubject.js");



let HeaderService = class HeaderService {
    constructor() {
        this.relatedTopics = new rxjs_BehaviorSubject__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"]('relatedTopics');
        this.isHome = new rxjs_BehaviorSubject__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"]('isHome');
    }
    setRelatedTopics(relatedTopics) {
        this.relatedTopics.next(relatedTopics);
    }
    setIsHome(isHome) {
        if (isHome) {
            this.isHome.next(isHome);
        }
        else {
            this.isHome.next("isHome");
        }
    }
};
HeaderService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
], HeaderService);



/***/ }),

/***/ "./src/app/app.component.model.ts":
/*!****************************************!*\
  !*** ./src/app/app.component.model.ts ***!
  \****************************************/
/*! exports provided: AppComponentModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponentModel", function() { return AppComponentModel; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");

class AppComponentModel {
    constructor() {
        this.isToShowUserProfile = false;
    }
    clone() {
        let model = new AppComponentModel();
        model.isToShowUserProfile = this.isToShowUserProfile;
        return model;
    }
}


/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".button2 {\n  border-radius: 4px;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Wb2x1bWVzL0ZpbmFsUmV2aXNlL2ZpbmFscmV2aXNlX3dlYnNpdGUvc3JjL2FwcC9hcHAuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUFVLGtCQUFrQjtBQ0U1QiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5idXR0b24yIHtib3JkZXItcmFkaXVzOiA0cHg7fVxuXG5cbiIsIi5idXR0b24yIHtcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xufVxuIl19 */");

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _app_component_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app.component.model */ "./src/app/app.component.model.ts");
/* harmony import */ var _angular_animations__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/animations */ "./node_modules/@angular/animations/__ivy_ngcc__/fesm2015/animations.js");
/* harmony import */ var _application_state_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./application-state.service */ "./src/app/application-state.service.ts");





let AppComponent = class AppComponent {
    constructor(applicationStateService) {
        this.applicationStateService = applicationStateService;
        this.model = new _app_component_model__WEBPACK_IMPORTED_MODULE_2__["AppComponentModel"]();
        this.myViewModel = new _app_component_model__WEBPACK_IMPORTED_MODULE_2__["AppComponentModel"]();
        this.isMobileResolution = this.applicationStateService.getIsMobileResolution();
    }
    onUserProfileClick() {
        if (this.model.isToShowUserProfile) {
            this.model.isToShowUserProfile = false;
        }
        else {
            this.model.isToShowUserProfile = true;
        }
        this.updateView();
    }
    updateView() {
        this.myViewModel = this.model.clone();
    }
};
AppComponent.ctorParameters = () => [
    { type: _application_state_service__WEBPACK_IMPORTED_MODULE_4__["ApplicationStateService"] }
];
AppComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-root',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html")).default,
        animations: [
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_3__["trigger"])('userProfileAnimation', [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_3__["transition"])('void => *', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_3__["style"])({ opacity: 0, marginTop: 500 }),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_3__["animate"])('200ms ease-in', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_3__["style"])({ opacity: 1, marginTop: 50 }))
                ]),
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_3__["transition"])('* => void', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_3__["animate"])('150ms ease-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_3__["style"])({ opacity: 0, marginTop: 500 }))
                ])
            ])
        ],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")).default]
    })
], AppComponent);



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.routing.module */ "./src/app/app.routing.module.ts");
/* harmony import */ var _about_about_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./about/about.component */ "./src/app/about/about.component.ts");
/* harmony import */ var _core_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./core.module */ "./src/app/core.module.ts");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/dialog.js");
/* harmony import */ var _app_header_app_header_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./app-header/app-header.component */ "./src/app/app-header/app-header.component.ts");
/* harmony import */ var _home_desktop_home_component_desktop__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./home/desktop/home.component.desktop */ "./src/app/home/desktop/home.component.desktop.ts");
/* harmony import */ var _home_mobile_home_component_mobile__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./home/mobile/home.component.mobile */ "./src/app/home/mobile/home.component.mobile.ts");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var _footer_footer_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./footer/footer.component */ "./src/app/footer/footer.component.ts");
/* harmony import */ var _subcategory_desktop_subcat_desktop_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./subcategory/desktop/subcat-desktop.component */ "./src/app/subcategory/desktop/subcat-desktop.component.ts");
/* harmony import */ var _subcategory_mobile_subcat_mobile_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./subcategory/mobile/subcat-mobile.component */ "./src/app/subcategory/mobile/subcat-mobile.component.ts");
/* harmony import */ var _app_header_header_service__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./app-header/header.service */ "./src/app/app-header/header.service.ts");
/* harmony import */ var _angular_material_menu__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/material/menu */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/menu.js");
/* harmony import */ var _pdf_viewer_mobile_mobile_pdf_viewer_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./pdf-viewer/mobile/mobile-pdf-viewer.component */ "./src/app/pdf-viewer/mobile/mobile-pdf-viewer.component.ts");
/* harmony import */ var _study_met_desktop_desktop_study_met_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./study-met/desktop/desktop-study-met.component */ "./src/app/study-met/desktop/desktop-study-met.component.ts");
/* harmony import */ var _study_met_mobile_mobile_study_met_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./study-met/mobile/mobile-study-met.component */ "./src/app/study-met/mobile/mobile-study-met.component.ts");
/* harmony import */ var _detail_desktop_desktop_detail_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./detail/desktop/desktop-detail.component */ "./src/app/detail/desktop/desktop-detail.component.ts");
/* harmony import */ var _detail_list_desktop_detail_list_desktop_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./detail-list/desktop/detail-list-desktop.component */ "./src/app/detail-list/desktop/detail-list-desktop.component.ts");
/* harmony import */ var _detail_mobile_mobile_detail_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./detail/mobile/mobile-detail.component */ "./src/app/detail/mobile/mobile-detail.component.ts");
/* harmony import */ var ngx_masonry__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ngx-masonry */ "./node_modules/ngx-masonry/__ivy_ngcc__/fesm2015/ngx-masonry.js");
/* harmony import */ var ngx_device_detector__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ngx-device-detector */ "./node_modules/ngx-device-detector/__ivy_ngcc__/fesm2015/ngx-device-detector.js");
/* harmony import */ var _pdf_viewer_desktop_desktop_pdf_viewer_component__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./pdf-viewer/desktop/desktop-pdf-viewer.component */ "./src/app/pdf-viewer/desktop/desktop-pdf-viewer.component.ts");
/* harmony import */ var _detail_list_mobile_detail_list_mobile_component__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ./detail-list/mobile/detail-list-mobile.component */ "./src/app/detail-list/mobile/detail-list-mobile.component.ts");
/* harmony import */ var _CustomUrlSerializer__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ./CustomUrlSerializer */ "./src/app/CustomUrlSerializer.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _contact_contact_component__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! ./contact/contact.component */ "./src/app/contact/contact.component.ts");
/* harmony import */ var _our_services_services_component__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! ./our-services/services.component */ "./src/app/our-services/services.component.ts");
/* harmony import */ var _search_search_component__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! ./search/search.component */ "./src/app/search/search.component.ts");
/* harmony import */ var _footer_fileUpload_service__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! ./footer/fileUpload.service */ "./src/app/footer/fileUpload.service.ts");
/* harmony import */ var _angular_material_card__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! @angular/material/card */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/card.js");




// import {BrowserAnimationsModule} from '@angular/platform-browser/animations';















// import { MatIconModule } from '@angular/material/icon';
// import { MatToolbarModule } from '@angular/material/toolbar';

















// import { AngularSvgIconModule } from 'angular-svg-icon';

let AppModule = class AppModule {
};
AppModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        declarations: [
            _app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"],
            _about_about_component__WEBPACK_IMPORTED_MODULE_5__["AboutComponent"],
            _app_header_app_header_component__WEBPACK_IMPORTED_MODULE_8__["AppHeaderDesktopComponent"],
            _home_desktop_home_component_desktop__WEBPACK_IMPORTED_MODULE_9__["HomeComponentDesktop"],
            _home_mobile_home_component_mobile__WEBPACK_IMPORTED_MODULE_10__["HomeComponentMobile"],
            _footer_footer_component__WEBPACK_IMPORTED_MODULE_14__["FooterComponent"],
            _subcategory_desktop_subcat_desktop_component__WEBPACK_IMPORTED_MODULE_15__["SubcatDesktopComponent"],
            _subcategory_mobile_subcat_mobile_component__WEBPACK_IMPORTED_MODULE_16__["SubcatComponentMobile"],
            _pdf_viewer_mobile_mobile_pdf_viewer_component__WEBPACK_IMPORTED_MODULE_19__["PdfViewerComponentMobile"],
            _study_met_desktop_desktop_study_met_component__WEBPACK_IMPORTED_MODULE_20__["StudyMetDesktopComponent"],
            _study_met_mobile_mobile_study_met_component__WEBPACK_IMPORTED_MODULE_21__["DialogContentDialog"],
            _detail_desktop_desktop_detail_component__WEBPACK_IMPORTED_MODULE_22__["DetailComponentDesktop"],
            _detail_mobile_mobile_detail_component__WEBPACK_IMPORTED_MODULE_24__["DetailComponentMobile"],
            _detail_list_desktop_detail_list_desktop_component__WEBPACK_IMPORTED_MODULE_23__["DetailListDesktopComponent"],
            _detail_mobile_mobile_detail_component__WEBPACK_IMPORTED_MODULE_24__["DialogContentExampleDialog"],
            _pdf_viewer_desktop_desktop_pdf_viewer_component__WEBPACK_IMPORTED_MODULE_27__["PdfViewerComponentDesktop"],
            _detail_list_mobile_detail_list_mobile_component__WEBPACK_IMPORTED_MODULE_28__["DetailListMobileComponent"],
            _about_about_component__WEBPACK_IMPORTED_MODULE_5__["AboutComponent"],
            _contact_contact_component__WEBPACK_IMPORTED_MODULE_31__["ContactComponent"],
            _our_services_services_component__WEBPACK_IMPORTED_MODULE_32__["ServicesComponent"],
            _search_search_component__WEBPACK_IMPORTED_MODULE_33__["SearchComponent"],
            _study_met_mobile_mobile_study_met_component__WEBPACK_IMPORTED_MODULE_21__["StudyMetMobileComponent"]
        ],
        imports: [
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"].withServerTransition({ appId: 'angular-multi-view' }),
            // BrowserAnimationsModule,
            _core_module__WEBPACK_IMPORTED_MODULE_6__["CoreModule"],
            _app_routing_module__WEBPACK_IMPORTED_MODULE_4__["AppRoutingModule"],
            _angular_http__WEBPACK_IMPORTED_MODULE_11__["HttpModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_12__["ReactiveFormsModule"],
            // BrowserAnimationsModule,
            _angular_common_http__WEBPACK_IMPORTED_MODULE_13__["HttpClientModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_12__["FormsModule"],
            // MatToolbarModule,
            _angular_material_menu__WEBPACK_IMPORTED_MODULE_18__["MatMenuModule"],
            // MatIconModule,
            _angular_material_dialog__WEBPACK_IMPORTED_MODULE_7__["MatDialogModule"],
            ngx_masonry__WEBPACK_IMPORTED_MODULE_25__["NgxMasonryModule"],
            _angular_material_card__WEBPACK_IMPORTED_MODULE_35__["MatCardModule"],
            // AngularSvgIconModule.forRoot(),
            ngx_device_detector__WEBPACK_IMPORTED_MODULE_26__["DeviceDetectorModule"].forRoot()
        ],
        entryComponents: [
            _home_mobile_home_component_mobile__WEBPACK_IMPORTED_MODULE_10__["HomeComponentMobile"],
            _subcategory_mobile_subcat_mobile_component__WEBPACK_IMPORTED_MODULE_16__["SubcatComponentMobile"],
            _detail_mobile_mobile_detail_component__WEBPACK_IMPORTED_MODULE_24__["DialogContentExampleDialog"],
            _study_met_mobile_mobile_study_met_component__WEBPACK_IMPORTED_MODULE_21__["DialogContentDialog"],
            _detail_mobile_mobile_detail_component__WEBPACK_IMPORTED_MODULE_24__["DetailComponentMobile"],
            _pdf_viewer_mobile_mobile_pdf_viewer_component__WEBPACK_IMPORTED_MODULE_19__["PdfViewerComponentMobile"],
            _detail_list_mobile_detail_list_mobile_component__WEBPACK_IMPORTED_MODULE_28__["DetailListMobileComponent"],
            _study_met_mobile_mobile_study_met_component__WEBPACK_IMPORTED_MODULE_21__["StudyMetMobileComponent"]
        ],
        bootstrap: [
            _app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]
        ],
        providers: [_app_header_header_service__WEBPACK_IMPORTED_MODULE_17__["HeaderService"], _footer_fileUpload_service__WEBPACK_IMPORTED_MODULE_34__["UploadFileService"],
            { provide: _angular_router__WEBPACK_IMPORTED_MODULE_30__["UrlSerializer"], useClass: _CustomUrlSerializer__WEBPACK_IMPORTED_MODULE_29__["CustomUrlSerializer"] }
        ]
    })
], AppModule);



/***/ }),

/***/ "./src/app/app.routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app.routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _about_about_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./about/about.component */ "./src/app/about/about.component.ts");
/* harmony import */ var _application_state_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./application-state.service */ "./src/app/application-state.service.ts");
/* harmony import */ var _home_desktop_home_component_desktop__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./home/desktop/home.component.desktop */ "./src/app/home/desktop/home.component.desktop.ts");
/* harmony import */ var _home_mobile_home_component_mobile__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home/mobile/home.component.mobile */ "./src/app/home/mobile/home.component.mobile.ts");
/* harmony import */ var _subcategory_desktop_subcat_desktop_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./subcategory/desktop/subcat-desktop.component */ "./src/app/subcategory/desktop/subcat-desktop.component.ts");
/* harmony import */ var _subcategory_mobile_subcat_mobile_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./subcategory/mobile/subcat-mobile.component */ "./src/app/subcategory/mobile/subcat-mobile.component.ts");
/* harmony import */ var _pdf_viewer_mobile_mobile_pdf_viewer_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./pdf-viewer/mobile/mobile-pdf-viewer.component */ "./src/app/pdf-viewer/mobile/mobile-pdf-viewer.component.ts");
/* harmony import */ var _detail_desktop_desktop_detail_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./detail/desktop/desktop-detail.component */ "./src/app/detail/desktop/desktop-detail.component.ts");
/* harmony import */ var _detail_list_desktop_detail_list_desktop_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./detail-list/desktop/detail-list-desktop.component */ "./src/app/detail-list/desktop/detail-list-desktop.component.ts");
/* harmony import */ var _detail_mobile_mobile_detail_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./detail/mobile/mobile-detail.component */ "./src/app/detail/mobile/mobile-detail.component.ts");
/* harmony import */ var _pdf_viewer_desktop_desktop_pdf_viewer_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./pdf-viewer/desktop/desktop-pdf-viewer.component */ "./src/app/pdf-viewer/desktop/desktop-pdf-viewer.component.ts");
/* harmony import */ var _detail_list_mobile_detail_list_mobile_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./detail-list/mobile/detail-list-mobile.component */ "./src/app/detail-list/mobile/detail-list-mobile.component.ts");
/* harmony import */ var _contact_contact_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./contact/contact.component */ "./src/app/contact/contact.component.ts");
/* harmony import */ var _our_services_services_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./our-services/services.component */ "./src/app/our-services/services.component.ts");
/* harmony import */ var _search_search_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./search/search.component */ "./src/app/search/search.component.ts");
/* harmony import */ var _study_met_desktop_desktop_study_met_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./study-met/desktop/desktop-study-met.component */ "./src/app/study-met/desktop/desktop-study-met.component.ts");
/* harmony import */ var _study_met_mobile_mobile_study_met_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./study-met/mobile/mobile-study-met.component */ "./src/app/study-met/mobile/mobile-study-met.component.ts");




















const desktop_routes = [
    {
        path: '', component: _home_desktop_home_component_desktop__WEBPACK_IMPORTED_MODULE_5__["HomeComponentDesktop"]
    },
    { path: 'about', component: _about_about_component__WEBPACK_IMPORTED_MODULE_3__["AboutComponent"] },
    { path: 'home', component: _home_desktop_home_component_desktop__WEBPACK_IMPORTED_MODULE_5__["HomeComponentDesktop"] },
    { path: 'subcat/:title', component: _subcategory_desktop_subcat_desktop_component__WEBPACK_IMPORTED_MODULE_7__["SubcatDesktopComponent"] },
    {
        path: "detail-list/:title",
        component: _detail_list_desktop_detail_list_desktop_component__WEBPACK_IMPORTED_MODULE_11__["DetailListDesktopComponent"],
        pathMatch: 'full'
    },
    {
        path: "detail/:title",
        component: _detail_desktop_desktop_detail_component__WEBPACK_IMPORTED_MODULE_10__["DetailComponentDesktop"],
        pathMatch: 'full'
    },
    {
        path: "pdf-viewer/:urlTitle/:title/:subId",
        component: _pdf_viewer_desktop_desktop_pdf_viewer_component__WEBPACK_IMPORTED_MODULE_13__["PdfViewerComponentDesktop"]
    },
    {
        path: "about",
        component: _about_about_component__WEBPACK_IMPORTED_MODULE_3__["AboutComponent"]
    },
    {
        path: "contact",
        component: _contact_contact_component__WEBPACK_IMPORTED_MODULE_15__["ContactComponent"]
    },
    {
        path: "our-services",
        component: _our_services_services_component__WEBPACK_IMPORTED_MODULE_16__["ServicesComponent"]
    },
    {
        path: "search/:text/:count/:type",
        component: _search_search_component__WEBPACK_IMPORTED_MODULE_17__["SearchComponent"],
    },
    {
        path: "study-met/:title",
        component: _study_met_desktop_desktop_study_met_component__WEBPACK_IMPORTED_MODULE_18__["StudyMetDesktopComponent"]
    },
    // directs all other routes to the main page
    { path: '**', redirectTo: '' }
];
const mobile_routes = [
    { path: '', component: _home_mobile_home_component_mobile__WEBPACK_IMPORTED_MODULE_6__["HomeComponentMobile"] },
    { path: 'about', component: _about_about_component__WEBPACK_IMPORTED_MODULE_3__["AboutComponent"] },
    { path: 'home', component: _home_mobile_home_component_mobile__WEBPACK_IMPORTED_MODULE_6__["HomeComponentMobile"] },
    { path: 'subcat/:title', component: _subcategory_mobile_subcat_mobile_component__WEBPACK_IMPORTED_MODULE_8__["SubcatComponentMobile"] },
    {
        path: "detail-list/:title",
        component: _detail_list_mobile_detail_list_mobile_component__WEBPACK_IMPORTED_MODULE_14__["DetailListMobileComponent"],
        pathMatch: 'full'
    },
    {
        path: "detail/:title",
        component: _detail_mobile_mobile_detail_component__WEBPACK_IMPORTED_MODULE_12__["DetailComponentMobile"],
        pathMatch: 'full'
    },
    {
        path: "pdf-viewer/:urlTitle/:title/:subId",
        component: _pdf_viewer_mobile_mobile_pdf_viewer_component__WEBPACK_IMPORTED_MODULE_9__["PdfViewerComponentMobile"]
    },
    {
        path: "about",
        component: _about_about_component__WEBPACK_IMPORTED_MODULE_3__["AboutComponent"]
    },
    {
        path: "contact",
        component: _contact_contact_component__WEBPACK_IMPORTED_MODULE_15__["ContactComponent"]
    },
    {
        path: "our-services",
        component: _our_services_services_component__WEBPACK_IMPORTED_MODULE_16__["ServicesComponent"]
    },
    {
        path: "search/:text/:count/:type",
        component: _search_search_component__WEBPACK_IMPORTED_MODULE_17__["SearchComponent"],
    },
    {
        path: "study-met/:title",
        component: _study_met_mobile_mobile_study_met_component__WEBPACK_IMPORTED_MODULE_19__["StudyMetMobileComponent"]
    },
    // directs all other routes to the main page
    { path: '**', redirectTo: '' }
];
let AppRoutingModule = class AppRoutingModule {
    constructor(router, applicationStateService) {
        this.router = router;
        this.applicationStateService = applicationStateService;
        if (applicationStateService.getIsMobileResolution()) {
            router.resetConfig(mobile_routes);
        }
    }
    /**
     * this function inject new routes for the given module instead the current routes. the operation happens on the given current routes object so after
     * this method a call to reset routes on router should be called with the the current routes object.
     * @param currentRoutes
     * @param routesToInject
     * @param childNameToReplaceRoutesUnder - the module name to replace its routes.
     */
    injectModuleRoutes(currentRoutes, routesToInject, childNameToReplaceRoutesUnder) {
        for (let i = 0; i < currentRoutes.length; i++) {
            if (currentRoutes[i].loadChildren != null &&
                currentRoutes[i].loadChildren.toString().indexOf(childNameToReplaceRoutesUnder) != -1) {
                // we found it. taking the route prefix
                let prefixRoute = currentRoutes[i].path;
                // first removing the module line
                currentRoutes.splice(i, 1);
                // now injecting the new routes
                // we need to add the prefix route first
                this.addPrefixToRoutes(routesToInject, prefixRoute);
                for (let route of routesToInject) {
                    currentRoutes.push(route);
                }
                // since we found it we can break the injection
                return;
            }
            if (currentRoutes[i].children != null) {
                this.injectModuleRoutes(currentRoutes[i].children, routesToInject, childNameToReplaceRoutesUnder);
            }
        }
    }
    addPrefixToRoutes(routes, prefix) {
        for (let i = 0; i < routes.length; i++) {
            routes[i].path = prefix + '/' + routes[i].path;
        }
    }
};
AppRoutingModule.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _application_state_service__WEBPACK_IMPORTED_MODULE_4__["ApplicationStateService"] }
];
AppRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        // as default we set the desktop routing configuration. if mobile will be started it will be replaced below.
        // note that we must specify some routes here (not an empty array) otherwise the trick below doesn't work...
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(desktop_routes, { preloadingStrategy: _angular_router__WEBPACK_IMPORTED_MODULE_2__["PreloadAllModules"], scrollPositionRestoration: 'enabled' })],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], AppRoutingModule);



/***/ }),

/***/ "./src/app/application-state.service.ts":
/*!**********************************************!*\
  !*** ./src/app/application-state.service.ts ***!
  \**********************************************/
/*! exports provided: ApplicationStateService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApplicationStateService", function() { return ApplicationStateService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var ngx_device_detector__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-device-detector */ "./node_modules/ngx-device-detector/__ivy_ngcc__/fesm2015/ngx-device-detector.js");



let ApplicationStateService = class ApplicationStateService {
    constructor(deviceService) {
        this.deviceService = deviceService;
        if (this.deviceService.isMobile()) {
            this.isMobileResolution = true;
        }
        else {
            this.isMobileResolution = false;
        }
    }
    getIsMobileResolution() {
        return this.isMobileResolution;
    }
};
ApplicationStateService.ctorParameters = () => [
    { type: ngx_device_detector__WEBPACK_IMPORTED_MODULE_2__["DeviceDetectorService"] }
];
ApplicationStateService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
], ApplicationStateService);



/***/ }),

/***/ "./src/app/contact/contact.component.css":
/*!***********************************************!*\
  !*** ./src/app/contact/contact.component.css ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".jumbotron{\n    padding:0rem 0rem;margin-bottom:0rem;background-color:#ecf0f1;border-radius:0.3rem\n}\n    .play-img{\n        width: 10%;\n        height: 5%;\n      }\n    @media (min-width: 576px){\n    .jumbotron{padding:1.25rem .2rem}\n}\n    .main-content {\n    padding-top: 7.2%;\n    padding-left: 2%;\n    padding-right: 2%;\n    min-height: 80vh;\n    padding-bottom: 2%;\n    background-image: url('/assets/images/bg.jpg');\n  }\n  \n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29udGFjdC9jb250YWN0LmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxpQkFBaUIsQ0FBQyxrQkFBa0IsQ0FBQyx3QkFBd0IsQ0FBQztBQUNsRTtJQUNJO1FBQ0ksVUFBVTtRQUNWLFVBQVU7TUFDWjtJQUNOO0lBQ0ksV0FBVyxxQkFBcUI7QUFDcEM7SUFDQTtJQUNJLGlCQUFpQjtJQUNqQixnQkFBZ0I7SUFDaEIsaUJBQWlCO0lBQ2pCLGdCQUFnQjtJQUNoQixrQkFBa0I7SUFDbEIsOENBQThDO0VBQ2hEIiwiZmlsZSI6InNyYy9hcHAvY29udGFjdC9jb250YWN0LmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuanVtYm90cm9ue1xuICAgIHBhZGRpbmc6MHJlbSAwcmVtO21hcmdpbi1ib3R0b206MHJlbTtiYWNrZ3JvdW5kLWNvbG9yOiNlY2YwZjE7Ym9yZGVyLXJhZGl1czowLjNyZW1cbn1cbiAgICAucGxheS1pbWd7XG4gICAgICAgIHdpZHRoOiAxMCU7XG4gICAgICAgIGhlaWdodDogNSU7XG4gICAgICB9XG5AbWVkaWEgKG1pbi13aWR0aDogNTc2cHgpe1xuICAgIC5qdW1ib3Ryb257cGFkZGluZzoxLjI1cmVtIC4ycmVtfVxufVxuLm1haW4tY29udGVudCB7XG4gICAgcGFkZGluZy10b3A6IDcuMiU7XG4gICAgcGFkZGluZy1sZWZ0OiAyJTtcbiAgICBwYWRkaW5nLXJpZ2h0OiAyJTtcbiAgICBtaW4taGVpZ2h0OiA4MHZoO1xuICAgIHBhZGRpbmctYm90dG9tOiAyJTtcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoJy9hc3NldHMvaW1hZ2VzL2JnLmpwZycpO1xuICB9XG4gICJdfQ== */");

/***/ }),

/***/ "./src/app/contact/contact.component.ts":
/*!**********************************************!*\
  !*** ./src/app/contact/contact.component.ts ***!
  \**********************************************/
/*! exports provided: ContactComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactComponent", function() { return ContactComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var rxjs_Rx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/Rx */ "./node_modules/rxjs-compat/_esm2015/Rx.js");



let ContactComponent = class ContactComponent {
    constructor() {
    }
};
ContactComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'contact',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./contact.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/contact/contact.component.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./contact.component.css */ "./src/app/contact/contact.component.css")).default]
    })
], ContactComponent);



/***/ }),

/***/ "./src/app/core.module.ts":
/*!********************************!*\
  !*** ./src/app/core.module.ts ***!
  \********************************/
/*! exports provided: CoreModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CoreModule", function() { return CoreModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _application_state_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./application-state.service */ "./src/app/application-state.service.ts");



/**
 * this module include services that must be singeltons. it should only be included in the app module
 * in order to work like that. see here for more info under core module:
 * https://angular.io/docs/ts/latest/guide/ngmodule.html#!#shared-module-for-root
 */
let CoreModule = class CoreModule {
};
CoreModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [],
        providers: [
            _application_state_service__WEBPACK_IMPORTED_MODULE_2__["ApplicationStateService"]
        ]
    })
], CoreModule);



/***/ }),

/***/ "./src/app/detail-list/desktop/detail-list-desktop.component.css":
/*!***********************************************************************!*\
  !*** ./src/app/detail-list/desktop/detail-list-desktop.component.css ***!
  \***********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".tableHeader{\n    margin-top: 75px;\n    table-layout: fixed;\n    width: 100%;\n    border-collapse: unset;\n}\na{\n  color: #8b0000;\n}\nli {\n  list-style-type: none;\n  text-shadow: 2px 2px 4px  rgba(0, 0, 0, 0.18);\n  font-size: large;\n}\n.card{\n  margin-right:15px;\n}\n.card-body {\n  flex: 1 1 auto;\n  padding: 0;\n  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);\n}\n.card-subtitle {\n  padding: 2%;\n}\n.card-title {\n  padding-left: 2%;\n  padding-top: 1%;\n  padding-bottom: 1%;\n  /* background: linear-gradient(110deg, #8b0000 60%, #2c3e50 60%); */\n  /* background: rgba(139, 0, 0, 0.815); */\n  background-image: linear-gradient(to left, #ff000000, #2c3e50);\n  /* background-image: linear-gradient(to right, #8b0000 , #2c3e50); */\n  color: white;\n}\n.masonry-item { \n    flex: 0 0 33.3333333333%;\n    max-width: 33.3333333333%; \n    min-width: 33.3333333333%;\n}\n.fontSize{\n  font-size: 1.7rem;\n}\n.nav-link {\n    display: block;\n    background: #8b0000;\n    margin-top: 8%;\n    margin-right: 2%;\n}\n.sbuttons {\n    bottom: 15%;\n    position: fixed;\n    margin: 1em;\n    left: 0;\n  }\n.sbutton {\n    display: block;\n    width: 60px;\n    height: 60px;\n    border-radius: 50%;\n    text-align: center;\n    color: transparent;\n    margin: 20px auto 0;\n    box-shadow: 0px 5px 11px -2px rgba(0, 0, 0, 0.18), 0px 4px 12px -7px rgba(0, 0, 0, 0.15);\n    cursor: pointer;\n    transition: all .1s ease-out;\n    position: relative;\n  }\n.sbutton > i {\n    font-size: 38px;\n    line-height: 60px;\n    transition: all .2s ease-in-out;\n    transition-delay: 2s;\n  }\n.sbutton:active,\n  .sbutton:focus,\n  .sbutton:hover {\n    box-shadow: 0 0 4px rgba(0, 0, 0, .14), 0 4px 8px rgba(0, 0, 0, .28);\n  }\n.sbutton:not(:last-child) {\n    width: 60px;\n    height: 60px;\n    margin: 20px auto 0;\n    opacity: 0;\n  }\n.sbutton:not(:last-child) > i {\n    font-size: 25px;\n    line-height: 60px;\n    transition: all .3s ease-in-out;\n  }\n.sbutton:not(:last-child) {\n    opacity: 1;\n    width: 60px;\n    height: 60px;\n    margin: 15px auto 0;\n  }\n.sbutton:nth-last-child(1) {\n    transition-delay: 25ms;\n  }\n.sbutton:not(:last-child):nth-last-child(2) {\n    transition-delay: 20ms;\n  }\n.sbutton:not(:last-child):nth-last-child(3) {\n    transition-delay: 40ms;\n  }\n.sbutton:not(:last-child):nth-last-child(4) {\n    transition-delay: 60ms;\n  }\n.sbutton:not(:last-child):nth-last-child(5) {\n    transition-delay: 80ms;\n  }\n.sbutton:not(:last-child):nth-last-child(6) {\n    transition-delay: 100ms;\n  }\n[tooltip]:before {\n    font-family: 'Roboto';\n    font-weight: 600;\n    border-radius: 2px;\n    background-color: #585858;\n    color: #fff;\n    content: attr(tooltip);\n    font-size: 12px;\n    visibility: hidden;\n    opacity: 0;\n    padding: 5px 7px;\n    margin-left: 10px;\n    position: absolute;\n    left: 100%;\n    bottom: 20%;\n    white-space: nowrap;\n  }\n.sbutton.mainsbutton {\n    background:url(/assets/images/share.jpg);\n  }\n.sbutton.gplus {\n    background:url(/assets/images/gmail60.png);\n  }\n.sbutton.twitt {\n    background:url(/assets/images/mess60.png);\n  }\n.sbutton.fb {\n    background:url(/assets/images/facebook60.png);\n  }\n.sbutton.whatsapp {\n    background:url(/assets/images/whatsapp60.png);\n  }\n.blink {\n    margin-top: 2.5%;\n    -webkit-animation: blinker 2s linear infinite;\n            animation: blinker 2s linear infinite;\n    color: #2c3e50;\n   }\n@-webkit-keyframes blinker {  \n    50% { opacity: 0; }\n   }\n@keyframes blinker {  \n    50% { opacity: 0; }\n   }\n.main-content {\n    padding-top: 6.5%;\n    padding-left: 2%;\n    padding-right: 2%;\n      /* The image used */\n  \n\n  /* Set a specific height */\n  min-height: 80vh;\n\n  /* Create the parallax scrolling effect */\n  background-attachment: fixed;\n  background-position: center;\n  background-repeat: no-repeat;\n  background-size: cover;\n  }\n.loader {\n    position: fixed;\n    left: 0px;\n    top: 0px;\n    width: 100%;\n    height: 100%;\n    z-index: 9999;\n    background: url('/assets/images/loading.gif') 50% 50% no-repeat rgb(249,249,249);\n    opacity: .6;\n  }\n.jumbotron {\n    padding: 1rem 1rem;\n    margin-bottom: 2%;\n    background-color:#ecf0f1;\n    border-radius:0.3rem;\n    box-shadow: 0px 5px 11px -2px rgba(0, 0, 0, 0.18), 0px 4px 12px -7px rgba(0, 0, 0, 0.15);\n  }\n  \n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZGV0YWlsLWxpc3QvZGVza3RvcC9kZXRhaWwtbGlzdC1kZXNrdG9wLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxnQkFBZ0I7SUFDaEIsbUJBQW1CO0lBQ25CLFdBQVc7SUFDWCxzQkFBc0I7QUFDMUI7QUFDQTtFQUNFLGNBQWM7QUFDaEI7QUFDQTtFQUNFLHFCQUFxQjtFQUNyQiw2Q0FBNkM7RUFDN0MsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxpQkFBaUI7QUFDbkI7QUFDQTtFQUdFLGNBQWM7RUFDZCxVQUFVO0VBQ1YsNEVBQTRFO0FBQzlFO0FBQ0E7RUFDRSxXQUFXO0FBQ2I7QUFDQTtFQUNFLGdCQUFnQjtFQUNoQixlQUFlO0VBQ2Ysa0JBQWtCO0VBQ2xCLG1FQUFtRTtFQUNuRSx3Q0FBd0M7RUFDeEMsOERBQThEO0VBQzlELG9FQUFvRTtFQUNwRSxZQUFZO0FBQ2Q7QUFDQTtJQUNJLHdCQUF3QjtJQUN4Qix5QkFBeUI7SUFDekIseUJBQXlCO0FBQzdCO0FBQ0E7RUFDRSxpQkFBaUI7QUFDbkI7QUFDQTtJQUNJLGNBQWM7SUFDZCxtQkFBbUI7SUFDbkIsY0FBYztJQUNkLGdCQUFnQjtBQUNwQjtBQUNBO0lBQ0ksV0FBVztJQUNYLGVBQWU7SUFDZixXQUFXO0lBQ1gsT0FBTztFQUNUO0FBQ0E7SUFDRSxjQUFjO0lBQ2QsV0FBVztJQUNYLFlBQVk7SUFDWixrQkFBa0I7SUFDbEIsa0JBQWtCO0lBQ2xCLGtCQUFrQjtJQUNsQixtQkFBbUI7SUFDbkIsd0ZBQXdGO0lBQ3hGLGVBQWU7SUFFZiw0QkFBNEI7SUFDNUIsa0JBQWtCO0VBQ3BCO0FBQ0E7SUFDRSxlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLCtCQUErQjtJQUMvQixvQkFBb0I7RUFDdEI7QUFDQTs7O0lBR0Usb0VBQW9FO0VBQ3RFO0FBQ0E7SUFDRSxXQUFXO0lBQ1gsWUFBWTtJQUNaLG1CQUFtQjtJQUNuQixVQUFVO0VBQ1o7QUFDQTtJQUNFLGVBQWU7SUFDZixpQkFBaUI7SUFDakIsK0JBQStCO0VBQ2pDO0FBQ0E7SUFDRSxVQUFVO0lBQ1YsV0FBVztJQUNYLFlBQVk7SUFDWixtQkFBbUI7RUFDckI7QUFDQTtJQUVFLHNCQUFzQjtFQUN4QjtBQUNBO0lBRUUsc0JBQXNCO0VBQ3hCO0FBQ0E7SUFFRSxzQkFBc0I7RUFDeEI7QUFDQTtJQUVFLHNCQUFzQjtFQUN4QjtBQUNBO0lBRUUsc0JBQXNCO0VBQ3hCO0FBQ0E7SUFFRSx1QkFBdUI7RUFDekI7QUFFQTtJQUNFLHFCQUFxQjtJQUNyQixnQkFBZ0I7SUFDaEIsa0JBQWtCO0lBQ2xCLHlCQUF5QjtJQUN6QixXQUFXO0lBQ1gsc0JBQXNCO0lBQ3RCLGVBQWU7SUFDZixrQkFBa0I7SUFDbEIsVUFBVTtJQUNWLGdCQUFnQjtJQUNoQixpQkFBaUI7SUFDakIsa0JBQWtCO0lBQ2xCLFVBQVU7SUFDVixXQUFXO0lBQ1gsbUJBQW1CO0VBQ3JCO0FBRUE7SUFDRSx3Q0FBd0M7RUFDMUM7QUFDQTtJQUNFLDBDQUEwQztFQUM1QztBQUNBO0lBQ0UseUNBQXlDO0VBQzNDO0FBQ0E7SUFDRSw2Q0FBNkM7RUFDL0M7QUFDQTtJQUNFLDZDQUE2QztFQUMvQztBQUVBO0lBQ0UsZ0JBQWdCO0lBQ2hCLDZDQUFxQztZQUFyQyxxQ0FBcUM7SUFDckMsY0FBYztHQUNmO0FBQ0Q7SUFDRSxNQUFNLFVBQVUsRUFBRTtHQUNuQjtBQUZEO0lBQ0UsTUFBTSxVQUFVLEVBQUU7R0FDbkI7QUFDQTtJQUNDLGlCQUFpQjtJQUNqQixnQkFBZ0I7SUFDaEIsaUJBQWlCO01BQ2YsbUJBQW1COzs7RUFHdkIsMEJBQTBCO0VBQzFCLGdCQUFnQjs7RUFFaEIseUNBQXlDO0VBQ3pDLDRCQUE0QjtFQUM1QiwyQkFBMkI7RUFDM0IsNEJBQTRCO0VBQzVCLHNCQUFzQjtFQUN0QjtBQUVBO0lBQ0UsZUFBZTtJQUNmLFNBQVM7SUFDVCxRQUFRO0lBQ1IsV0FBVztJQUNYLFlBQVk7SUFDWixhQUFhO0lBQ2IsZ0ZBQWdGO0lBQ2hGLFdBQVc7RUFDYjtBQUNBO0lBQ0Usa0JBQWtCO0lBQ2xCLGlCQUFpQjtJQUNqQix3QkFBd0I7SUFDeEIsb0JBQW9CO0lBQ3BCLHdGQUF3RjtFQUMxRiIsImZpbGUiOiJzcmMvYXBwL2RldGFpbC1saXN0L2Rlc2t0b3AvZGV0YWlsLWxpc3QtZGVza3RvcC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnRhYmxlSGVhZGVye1xuICAgIG1hcmdpbi10b3A6IDc1cHg7XG4gICAgdGFibGUtbGF5b3V0OiBmaXhlZDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBib3JkZXItY29sbGFwc2U6IHVuc2V0O1xufVxuYXtcbiAgY29sb3I6ICM4YjAwMDA7XG59XG5saSB7XG4gIGxpc3Qtc3R5bGUtdHlwZTogbm9uZTtcbiAgdGV4dC1zaGFkb3c6IDJweCAycHggNHB4ICByZ2JhKDAsIDAsIDAsIDAuMTgpO1xuICBmb250LXNpemU6IGxhcmdlO1xufVxuLmNhcmR7XG4gIG1hcmdpbi1yaWdodDoxNXB4O1xufVxuLmNhcmQtYm9keSB7XG4gIC13ZWJraXQtYm94LWZsZXg6IDE7XG4gIC1tcy1mbGV4OiAxIDEgYXV0bztcbiAgZmxleDogMSAxIGF1dG87XG4gIHBhZGRpbmc6IDA7XG4gIGJveC1zaGFkb3c6IDAgNHB4IDhweCAwIHJnYmEoMCwgMCwgMCwgMC4yKSwgMCA2cHggMjBweCAwIHJnYmEoMCwgMCwgMCwgMC4xOSk7XG59XG4uY2FyZC1zdWJ0aXRsZSB7XG4gIHBhZGRpbmc6IDIlO1xufVxuLmNhcmQtdGl0bGUge1xuICBwYWRkaW5nLWxlZnQ6IDIlO1xuICBwYWRkaW5nLXRvcDogMSU7XG4gIHBhZGRpbmctYm90dG9tOiAxJTtcbiAgLyogYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDExMGRlZywgIzhiMDAwMCA2MCUsICMyYzNlNTAgNjAlKTsgKi9cbiAgLyogYmFja2dyb3VuZDogcmdiYSgxMzksIDAsIDAsIDAuODE1KTsgKi9cbiAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KHRvIGxlZnQsICNmZjAwMDAwMCwgIzJjM2U1MCk7XG4gIC8qIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCh0byByaWdodCwgIzhiMDAwMCAsICMyYzNlNTApOyAqL1xuICBjb2xvcjogd2hpdGU7XG59XG4ubWFzb25yeS1pdGVtIHsgXG4gICAgZmxleDogMCAwIDMzLjMzMzMzMzMzMzMlO1xuICAgIG1heC13aWR0aDogMzMuMzMzMzMzMzMzMyU7IFxuICAgIG1pbi13aWR0aDogMzMuMzMzMzMzMzMzMyU7XG59XG4uZm9udFNpemV7XG4gIGZvbnQtc2l6ZTogMS43cmVtO1xufVxuLm5hdi1saW5rIHtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICBiYWNrZ3JvdW5kOiAjOGIwMDAwO1xuICAgIG1hcmdpbi10b3A6IDglO1xuICAgIG1hcmdpbi1yaWdodDogMiU7XG59XG4uc2J1dHRvbnMge1xuICAgIGJvdHRvbTogMTUlO1xuICAgIHBvc2l0aW9uOiBmaXhlZDtcbiAgICBtYXJnaW46IDFlbTtcbiAgICBsZWZ0OiAwO1xuICB9XG4gIC5zYnV0dG9uIHtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICB3aWR0aDogNjBweDtcbiAgICBoZWlnaHQ6IDYwcHg7XG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBjb2xvcjogdHJhbnNwYXJlbnQ7XG4gICAgbWFyZ2luOiAyMHB4IGF1dG8gMDtcbiAgICBib3gtc2hhZG93OiAwcHggNXB4IDExcHggLTJweCByZ2JhKDAsIDAsIDAsIDAuMTgpLCAwcHggNHB4IDEycHggLTdweCByZ2JhKDAsIDAsIDAsIDAuMTUpO1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICAtd2Via2l0LXRyYW5zaXRpb246IGFsbCAuMXMgZWFzZS1vdXQ7XG4gICAgdHJhbnNpdGlvbjogYWxsIC4xcyBlYXNlLW91dDtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIH1cbiAgLnNidXR0b24gPiBpIHtcbiAgICBmb250LXNpemU6IDM4cHg7XG4gICAgbGluZS1oZWlnaHQ6IDYwcHg7XG4gICAgdHJhbnNpdGlvbjogYWxsIC4ycyBlYXNlLWluLW91dDtcbiAgICB0cmFuc2l0aW9uLWRlbGF5OiAycztcbiAgfVxuICAuc2J1dHRvbjphY3RpdmUsXG4gIC5zYnV0dG9uOmZvY3VzLFxuICAuc2J1dHRvbjpob3ZlciB7XG4gICAgYm94LXNoYWRvdzogMCAwIDRweCByZ2JhKDAsIDAsIDAsIC4xNCksIDAgNHB4IDhweCByZ2JhKDAsIDAsIDAsIC4yOCk7XG4gIH1cbiAgLnNidXR0b246bm90KDpsYXN0LWNoaWxkKSB7XG4gICAgd2lkdGg6IDYwcHg7XG4gICAgaGVpZ2h0OiA2MHB4O1xuICAgIG1hcmdpbjogMjBweCBhdXRvIDA7XG4gICAgb3BhY2l0eTogMDtcbiAgfVxuICAuc2J1dHRvbjpub3QoOmxhc3QtY2hpbGQpID4gaSB7XG4gICAgZm9udC1zaXplOiAyNXB4O1xuICAgIGxpbmUtaGVpZ2h0OiA2MHB4O1xuICAgIHRyYW5zaXRpb246IGFsbCAuM3MgZWFzZS1pbi1vdXQ7XG4gIH1cbiAgLnNidXR0b246bm90KDpsYXN0LWNoaWxkKSB7XG4gICAgb3BhY2l0eTogMTtcbiAgICB3aWR0aDogNjBweDtcbiAgICBoZWlnaHQ6IDYwcHg7XG4gICAgbWFyZ2luOiAxNXB4IGF1dG8gMDtcbiAgfVxuICAuc2J1dHRvbjpudGgtbGFzdC1jaGlsZCgxKSB7XG4gICAgLXdlYmtpdC10cmFuc2l0aW9uLWRlbGF5OiAyNW1zO1xuICAgIHRyYW5zaXRpb24tZGVsYXk6IDI1bXM7XG4gIH1cbiAgLnNidXR0b246bm90KDpsYXN0LWNoaWxkKTpudGgtbGFzdC1jaGlsZCgyKSB7XG4gICAgLXdlYmtpdC10cmFuc2l0aW9uLWRlbGF5OiAyMG1zO1xuICAgIHRyYW5zaXRpb24tZGVsYXk6IDIwbXM7XG4gIH1cbiAgLnNidXR0b246bm90KDpsYXN0LWNoaWxkKTpudGgtbGFzdC1jaGlsZCgzKSB7XG4gICAgLXdlYmtpdC10cmFuc2l0aW9uLWRlbGF5OiA0MG1zO1xuICAgIHRyYW5zaXRpb24tZGVsYXk6IDQwbXM7XG4gIH1cbiAgLnNidXR0b246bm90KDpsYXN0LWNoaWxkKTpudGgtbGFzdC1jaGlsZCg0KSB7XG4gICAgLXdlYmtpdC10cmFuc2l0aW9uLWRlbGF5OiA2MG1zO1xuICAgIHRyYW5zaXRpb24tZGVsYXk6IDYwbXM7XG4gIH1cbiAgLnNidXR0b246bm90KDpsYXN0LWNoaWxkKTpudGgtbGFzdC1jaGlsZCg1KSB7XG4gICAgLXdlYmtpdC10cmFuc2l0aW9uLWRlbGF5OiA4MG1zO1xuICAgIHRyYW5zaXRpb24tZGVsYXk6IDgwbXM7XG4gIH1cbiAgLnNidXR0b246bm90KDpsYXN0LWNoaWxkKTpudGgtbGFzdC1jaGlsZCg2KSB7XG4gICAgLXdlYmtpdC10cmFuc2l0aW9uLWRlbGF5OiAxMDBtcztcbiAgICB0cmFuc2l0aW9uLWRlbGF5OiAxMDBtcztcbiAgfVxuICAgXG4gIFt0b29sdGlwXTpiZWZvcmUge1xuICAgIGZvbnQtZmFtaWx5OiAnUm9ib3RvJztcbiAgICBmb250LXdlaWdodDogNjAwO1xuICAgIGJvcmRlci1yYWRpdXM6IDJweDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjNTg1ODU4O1xuICAgIGNvbG9yOiAjZmZmO1xuICAgIGNvbnRlbnQ6IGF0dHIodG9vbHRpcCk7XG4gICAgZm9udC1zaXplOiAxMnB4O1xuICAgIHZpc2liaWxpdHk6IGhpZGRlbjtcbiAgICBvcGFjaXR5OiAwO1xuICAgIHBhZGRpbmc6IDVweCA3cHg7XG4gICAgbWFyZ2luLWxlZnQ6IDEwcHg7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGxlZnQ6IDEwMCU7XG4gICAgYm90dG9tOiAyMCU7XG4gICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgfVxuICAgXG4gIC5zYnV0dG9uLm1haW5zYnV0dG9uIHtcbiAgICBiYWNrZ3JvdW5kOnVybCgvYXNzZXRzL2ltYWdlcy9zaGFyZS5qcGcpO1xuICB9XG4gIC5zYnV0dG9uLmdwbHVzIHtcbiAgICBiYWNrZ3JvdW5kOnVybCgvYXNzZXRzL2ltYWdlcy9nbWFpbDYwLnBuZyk7XG4gIH1cbiAgLnNidXR0b24udHdpdHQge1xuICAgIGJhY2tncm91bmQ6dXJsKC9hc3NldHMvaW1hZ2VzL21lc3M2MC5wbmcpO1xuICB9XG4gIC5zYnV0dG9uLmZiIHtcbiAgICBiYWNrZ3JvdW5kOnVybCgvYXNzZXRzL2ltYWdlcy9mYWNlYm9vazYwLnBuZyk7XG4gIH1cbiAgLnNidXR0b24ud2hhdHNhcHAge1xuICAgIGJhY2tncm91bmQ6dXJsKC9hc3NldHMvaW1hZ2VzL3doYXRzYXBwNjAucG5nKTtcbiAgfVxuXG4gIC5ibGluayB7XG4gICAgbWFyZ2luLXRvcDogMi41JTtcbiAgICBhbmltYXRpb246IGJsaW5rZXIgMnMgbGluZWFyIGluZmluaXRlO1xuICAgIGNvbG9yOiAjMmMzZTUwO1xuICAgfVxuICBAa2V5ZnJhbWVzIGJsaW5rZXIgeyAgXG4gICAgNTAlIHsgb3BhY2l0eTogMDsgfVxuICAgfVxuICAgLm1haW4tY29udGVudCB7XG4gICAgcGFkZGluZy10b3A6IDYuNSU7XG4gICAgcGFkZGluZy1sZWZ0OiAyJTtcbiAgICBwYWRkaW5nLXJpZ2h0OiAyJTtcbiAgICAgIC8qIFRoZSBpbWFnZSB1c2VkICovXG4gIFxuXG4gIC8qIFNldCBhIHNwZWNpZmljIGhlaWdodCAqL1xuICBtaW4taGVpZ2h0OiA4MHZoO1xuXG4gIC8qIENyZWF0ZSB0aGUgcGFyYWxsYXggc2Nyb2xsaW5nIGVmZmVjdCAqL1xuICBiYWNrZ3JvdW5kLWF0dGFjaG1lbnQ6IGZpeGVkO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gIH1cblxuICAubG9hZGVyIHtcbiAgICBwb3NpdGlvbjogZml4ZWQ7XG4gICAgbGVmdDogMHB4O1xuICAgIHRvcDogMHB4O1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogMTAwJTtcbiAgICB6LWluZGV4OiA5OTk5O1xuICAgIGJhY2tncm91bmQ6IHVybCgnL2Fzc2V0cy9pbWFnZXMvbG9hZGluZy5naWYnKSA1MCUgNTAlIG5vLXJlcGVhdCByZ2IoMjQ5LDI0OSwyNDkpO1xuICAgIG9wYWNpdHk6IC42O1xuICB9XG4gIC5qdW1ib3Ryb24ge1xuICAgIHBhZGRpbmc6IDFyZW0gMXJlbTtcbiAgICBtYXJnaW4tYm90dG9tOiAyJTtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiNlY2YwZjE7XG4gICAgYm9yZGVyLXJhZGl1czowLjNyZW07XG4gICAgYm94LXNoYWRvdzogMHB4IDVweCAxMXB4IC0ycHggcmdiYSgwLCAwLCAwLCAwLjE4KSwgMHB4IDRweCAxMnB4IC03cHggcmdiYSgwLCAwLCAwLCAwLjE1KTtcbiAgfVxuICAiXX0= */");

/***/ }),

/***/ "./src/app/detail-list/desktop/detail-list-desktop.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/detail-list/desktop/detail-list-desktop.component.ts ***!
  \**********************************************************************/
/*! exports provided: DetailListDesktopComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetailListDesktopComponent", function() { return DetailListDesktopComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var rxjs_Rx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/Rx */ "./node_modules/rxjs-compat/_esm2015/Rx.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");
/* harmony import */ var _app_header_header_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../app-header/header.service */ "./src/app/app-header/header.service.ts");



;





let DetailListDesktopComponent = class DetailListDesktopComponent {
    constructor(http, activatedRoute, router, meta, title, headerService, sanitizer) {
        this.http = http;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.headerService = headerService;
        this.sanitizer = sanitizer;
        this.apiUrl = 'https://api.finalrevise.com/finalrevise/v1/subcat/';
        this.pageData = {};
        this.keys = [];
        this.nav = [];
        this.navIds = [];
        this.tabUrl = 'https://api.finalrevise.com/finalrevise/v1/relatedlinks/';
        this.whatsAppUrl = 'https://api.finalrevise.com/finalrevise/v1/whatsApp/';
        this.router.routeReuseStrategy.shouldReuseRoute = function () {
            return false;
        };
        this.title = title;
        this.meta = meta;
    }
    ngOnInit() {
        // subscribe to router event
        this.activatedRoute.params.subscribe((params) => {
            this.selectedId = params['title'];
        });
        this.apiUrl = this.apiUrl + this.selectedId;
        this.tabUrl = this.tabUrl + this.selectedId;
        // this.whatsAppUrl = this.whatsAppUrl + this.selectedId;
        this.getPageData();
        this.getTabData();
        // this.getWhatsAppUrl()
        this.selectedId = this.selectedId.split(' ').join('-');
    }
    shareClicked() {
        if (this.isClickedShare) {
            this.isClickedShare = false;
        }
        else {
            this.isClickedShare = true;
        }
    }
    openNextPage(isChild, metaTitle) {
        if (isChild) {
            this.router.navigateByUrl('/detail-list/' + metaTitle);
        }
        else if (metaTitle.includes('book')) {
            this.router.navigateByUrl('/study-met/' + metaTitle);
        }
        else {
            this.router.navigateByUrl('/detail/' + metaTitle);
        }
    }
    openPageFromNav(pageId, index) {
        if (index == 0) {
            this.router.navigateByUrl('#');
        }
        else {
            this.router.navigateByUrl('/detail-list/' + pageId);
        }
    }
    getData() {
        return this.http.get(this.apiUrl);
    }
    getPageData() {
        this.getData().subscribe(data => {
            this.pageData = data;
            if (this.pageData.data.isChild) {
                this.nav = String(this.pageData.data.pagePath).split("#");
                this.navIds = String(this.pageData.data.pagePathId).split("#");
                this.getKeys(this.pageData.data.groupedPageItems);
                this.aboutExamContent = this.sanitizer.bypassSecurityTrustHtml(this.pageData.data.article);
                this.isArticle = this.pageData.data.article;
                this.setTitleMeta();
            }
            else {
                this.openNextPage(false, this.pageData.data.cat);
            }
        });
    }
    getSvgs(urlSvgs) {
        if (urlSvgs.includes(".png")) {
            return false;
        }
        else
            true;
    }
    getWhatsAppUrl() {
        this.getWhatsAppData().subscribe(data => {
            this.whatsAppData = data.data;
        });
    }
    getWhatsAppData() {
        return this.http.get(this.whatsAppUrl);
    }
    getTData() {
        return this.http.get(this.tabUrl);
    }
    getTabData() {
        this.getTData().subscribe(data => {
            if (data) {
                this.headerService.setRelatedTopics(data.data.tabs);
            }
        });
    }
    getKeys(obj) {
        var r = [];
        for (var k in obj) {
            if (!obj.hasOwnProperty(k))
                continue;
            r.push(k);
        }
        this.keys = r;
    }
    setTitleMeta() {
        this.title.setTitle(this.pageData.data.tag);
        this.meta.addTags([
            { name: 'author', content: 'finalrevise.com' },
            { name: 'keywords', content: this.pageData.data.keywords },
            {
                name: 'description', content: this.pageData.data.description
            }
        ]);
    }
    replaceAll(str, replacement) {
        str = str.split('(').join('');
        str = str.split(')').join('');
        str = str.split('.').join('');
        str = str.split('/').join('');
        str = str.split('|').join(':');
        str = str.split(' ').join(replacement);
        str = str.split('---').join(replacement);
        return str.split('--').join(replacement);
    }
    ;
    replaceAllSocial(str) {
        var find = '-';
        var re = new RegExp(find, 'g');
        return str.replace(re, ' ');
    }
    openFacebook() {
        window.open("https://www.facebook.com/sharer/sharer.php?u=http://finalrevise.com/detail-list/" + this.selectedId);
    }
    openWhatsApp() {
        window.open("https://api.whatsapp.com/send?text=*Download%20" + this.selectedId + "*%0a%0a" + "http://finalrevise.com/detail-list/" + this.selectedId +
            "%0a%0a*Download%20FinalRevise%20App%20:%20https://play.google.com/store/apps/details?id%3Dcom.finalrevise.android*");
    }
    openMail() {
        window.open("mailto:?subject=FinalRevise%20Study%20Material%20for%20" + this.selectedId +
            "&body=Download%20" + this.replaceAllSocial(this.selectedId) + "%0a%0a" + "http://finalrevise.com/detail-list/" + this.selectedId +
            "%0a%0a Download FinalRevise App : https://play.google.com/store/apps/details?id=com.finalrevise.android");
    }
    openMessenger() {
        window.open('fb-messenger://share?link=' + encodeURIComponent("http://finalrevise.com/detail-list/" + this.selectedId) + '&app_id='
            + encodeURIComponent("358637287840510"));
    }
};
DetailListDesktopComponent.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__["Meta"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__["Title"] },
    { type: _app_header_header_service__WEBPACK_IMPORTED_MODULE_6__["HeaderService"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__["DomSanitizer"] }
];
DetailListDesktopComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-detail-list',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./detail-list-desktop.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/detail-list/desktop/detail-list-desktop.component.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./detail-list-desktop.component.css */ "./src/app/detail-list/desktop/detail-list-desktop.component.css")).default]
    })
], DetailListDesktopComponent);



/***/ }),

/***/ "./src/app/detail-list/mobile/detail-list-mobile.component.css":
/*!*********************************************************************!*\
  !*** ./src/app/detail-list/mobile/detail-list-mobile.component.css ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".tableHeader{\n    margin-top: 50px;\n    table-layout: fixed;\n    width: 100%;\n    border-collapse: unset;\n}\n.mb-3, .my-3 {\n  margin-bottom: 0 !important;\n}\n.row{\n  margin-left: 0;\n}\n#ngx-masonry{\nmargin-left: 0;\nmargin-right: 0;\n}\n.card-body {\n  flex: 1 1 auto;\n  padding-top: 0;\n  padding-right: 0;\n  padding-left: 0;\n  padding-bottom: 3%;\n  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);\n}\n.card-subtitle {\n  padding-left: 2%;\n}\n.goBack{\n  color: #8b0000;\n  font-size: large;\n  text-shadow: 2px 2px 4px  rgba(0, 0, 0, 0.18);\n}\n.card-title {\n  padding-left: 2%;\n  padding-top: 1%;\n  padding-bottom: 1%;\n  /* background: linear-gradient(110deg, #8b0000 60%, #2c3e50 60%); */\n  /* background: rgba(139, 0, 0, 0.815); */\n  background-image: linear-gradient(to left, #ff000000, #2c3e50);\n  /* background-image: linear-gradient(to right, #8b0000 , #2c3e50); */\n  color: white;\n}\n.masonry-item { \n    max-width: 48%; \n    min-width: 48%;\n    margin-left: 1%;\n    margin-right: 1%;\n    margin-bottom: 2%;\n}\n.fontSize{\n  font-size: 1.2rem;\n}\n.nav-link {\n    display: block;\n    background: #8b0000;\n    margin-top: 8%;\n    margin-right: 2%;\n}\n.sbuttons {\n    bottom: 15%;\n    position: fixed;\n    margin: 1em;\n    left: 0;\n  }\n.sbutton {\n    display: block;\n    width: 60px;\n    height: 60px;\n    border-radius: 50%;\n    text-align: center;\n    color: transparent;\n    margin: 20px auto 0;\n    box-shadow: 0px 5px 11px -2px rgba(0, 0, 0, 0.18), 0px 4px 12px -7px rgba(0, 0, 0, 0.15);\n    cursor: pointer;\n    transition: all .1s ease-out;\n    position: relative;\n  }\n.sbutton > i {\n    font-size: 38px;\n    line-height: 60px;\n    transition: all .2s ease-in-out;\n    transition-delay: 2s;\n  }\n.sbutton:active,\n  .sbutton:focus,\n  .sbutton:hover {\n    box-shadow: 0 0 4px rgba(0, 0, 0, .14), 0 4px 8px rgba(0, 0, 0, .28);\n  }\n.sbutton:not(:last-child) {\n    width: 60px;\n    height: 60px;\n    margin: 20px auto 0;\n    opacity: 0;\n  }\n.sbutton:not(:last-child) > i {\n    font-size: 25px;\n    line-height: 60px;\n    transition: all .3s ease-in-out;\n  }\n.sbutton:not(:last-child) {\n    opacity: 1;\n    width: 60px;\n    height: 60px;\n    margin: 15px auto 0;\n  }\n.sbutton:nth-last-child(1) {\n    transition-delay: 25ms;\n  }\n.sbutton:not(:last-child):nth-last-child(2) {\n    transition-delay: 20ms;\n  }\n.sbutton:not(:last-child):nth-last-child(3) {\n    transition-delay: 40ms;\n  }\n.sbutton:not(:last-child):nth-last-child(4) {\n    transition-delay: 60ms;\n  }\n.sbutton:not(:last-child):nth-last-child(5) {\n    transition-delay: 80ms;\n  }\n.sbutton:not(:last-child):nth-last-child(6) {\n    transition-delay: 100ms;\n  }\n[tooltip]:before {\n    font-family: 'Roboto';\n    font-weight: 600;\n    border-radius: 2px;\n    background-color: #585858;\n    color: #fff;\n    content: attr(tooltip);\n    font-size: 12px;\n    visibility: hidden;\n    opacity: 0;\n    padding: 5px 7px;\n    margin-left: 10px;\n    position: absolute;\n    left: 100%;\n    bottom: 20%;\n    white-space: nowrap;\n  }\n.sbutton.mainsbutton {\n    background:url(/assets/images/share.jpg);\n  }\n.sbutton.gplus {\n    background:url(/assets/images/gmail60.png);\n  }\n.sbutton.twitt {\n    background:url(/assets/images/mess60.png);\n  }\n.sbutton.fb {\n    background:url(/assets/images/facebook60.png);\n  }\n.sbutton.whatsapp {\n    background:url(/assets/images/whatsapp60.png);\n  }\n.blink {\n    margin-top: 2.5%;\n    -webkit-animation: blinker 2s linear infinite;\n            animation: blinker 2s linear infinite;\n    color: #2c3e50;\n   }\n@-webkit-keyframes blinker {  \n    50% { opacity: 0; }\n   }\n@keyframes blinker {  \n    50% { opacity: 0; }\n   }\n.main-content {\n    padding-top: 15%;\n    min-height: 80vh;\n    padding-left: 2%;\n    padding-right: 2%;\n    margin: 0;\n     /* The image used */\n     \n\n     /* Set a specific height */\n     min-height: 80vh;\n   \n     /* Create the parallax scrolling effect */\n     background-attachment: fixed;\n     background-position: center;\n     background-repeat: no-repeat;\n     background-size: cover;\n     }\n.loader {\n      background: url('/assets/images/loading_small.gif') 50% 50% no-repeat rgb(249,249,249);\n    }\n.jumbotron{\n      padding:2%;\n      margin-bottom: 2%;\n      background-color:#ecf0f1;\n      border-radius:0.3rem;\n      box-shadow: 0px 5px 11px -2px rgba(0, 0, 0, 0.18), 0px 4px 12px -7px rgba(0, 0, 0, 0.15);\n  }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZGV0YWlsLWxpc3QvbW9iaWxlL2RldGFpbC1saXN0LW1vYmlsZS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksZ0JBQWdCO0lBQ2hCLG1CQUFtQjtJQUNuQixXQUFXO0lBQ1gsc0JBQXNCO0FBQzFCO0FBQ0E7RUFDRSwyQkFBMkI7QUFDN0I7QUFDQTtFQUNFLGNBQWM7QUFDaEI7QUFDQTtBQUNBLGNBQWM7QUFDZCxlQUFlO0FBQ2Y7QUFDQTtFQUdFLGNBQWM7RUFDZCxjQUFjO0VBQ2QsZ0JBQWdCO0VBQ2hCLGVBQWU7RUFDZixrQkFBa0I7RUFDbEIsNEVBQTRFO0FBQzlFO0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGNBQWM7RUFDZCxnQkFBZ0I7RUFDaEIsNkNBQTZDO0FBQy9DO0FBQ0E7RUFDRSxnQkFBZ0I7RUFDaEIsZUFBZTtFQUNmLGtCQUFrQjtFQUNsQixtRUFBbUU7RUFDbkUsd0NBQXdDO0VBQ3hDLDhEQUE4RDtFQUM5RCxvRUFBb0U7RUFDcEUsWUFBWTtBQUNkO0FBQ0E7SUFDSSxjQUFjO0lBQ2QsY0FBYztJQUNkLGVBQWU7SUFDZixnQkFBZ0I7SUFDaEIsaUJBQWlCO0FBQ3JCO0FBQ0E7RUFDRSxpQkFBaUI7QUFDbkI7QUFFQTtJQUNJLGNBQWM7SUFDZCxtQkFBbUI7SUFDbkIsY0FBYztJQUNkLGdCQUFnQjtBQUNwQjtBQUNBO0lBQ0ksV0FBVztJQUNYLGVBQWU7SUFDZixXQUFXO0lBQ1gsT0FBTztFQUNUO0FBQ0E7SUFDRSxjQUFjO0lBQ2QsV0FBVztJQUNYLFlBQVk7SUFDWixrQkFBa0I7SUFDbEIsa0JBQWtCO0lBQ2xCLGtCQUFrQjtJQUNsQixtQkFBbUI7SUFDbkIsd0ZBQXdGO0lBQ3hGLGVBQWU7SUFFZiw0QkFBNEI7SUFDNUIsa0JBQWtCO0VBQ3BCO0FBQ0E7SUFDRSxlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLCtCQUErQjtJQUMvQixvQkFBb0I7RUFDdEI7QUFDQTs7O0lBR0Usb0VBQW9FO0VBQ3RFO0FBQ0E7SUFDRSxXQUFXO0lBQ1gsWUFBWTtJQUNaLG1CQUFtQjtJQUNuQixVQUFVO0VBQ1o7QUFDQTtJQUNFLGVBQWU7SUFDZixpQkFBaUI7SUFDakIsK0JBQStCO0VBQ2pDO0FBQ0E7SUFDRSxVQUFVO0lBQ1YsV0FBVztJQUNYLFlBQVk7SUFDWixtQkFBbUI7RUFDckI7QUFDQTtJQUVFLHNCQUFzQjtFQUN4QjtBQUNBO0lBRUUsc0JBQXNCO0VBQ3hCO0FBQ0E7SUFFRSxzQkFBc0I7RUFDeEI7QUFDQTtJQUVFLHNCQUFzQjtFQUN4QjtBQUNBO0lBRUUsc0JBQXNCO0VBQ3hCO0FBQ0E7SUFFRSx1QkFBdUI7RUFDekI7QUFFQTtJQUNFLHFCQUFxQjtJQUNyQixnQkFBZ0I7SUFDaEIsa0JBQWtCO0lBQ2xCLHlCQUF5QjtJQUN6QixXQUFXO0lBQ1gsc0JBQXNCO0lBQ3RCLGVBQWU7SUFDZixrQkFBa0I7SUFDbEIsVUFBVTtJQUNWLGdCQUFnQjtJQUNoQixpQkFBaUI7SUFDakIsa0JBQWtCO0lBQ2xCLFVBQVU7SUFDVixXQUFXO0lBQ1gsbUJBQW1CO0VBQ3JCO0FBRUE7SUFDRSx3Q0FBd0M7RUFDMUM7QUFDQTtJQUNFLDBDQUEwQztFQUM1QztBQUNBO0lBQ0UseUNBQXlDO0VBQzNDO0FBQ0E7SUFDRSw2Q0FBNkM7RUFDL0M7QUFDQTtJQUNFLDZDQUE2QztFQUMvQztBQUVBO0lBQ0UsZ0JBQWdCO0lBQ2hCLDZDQUFxQztZQUFyQyxxQ0FBcUM7SUFDckMsY0FBYztHQUNmO0FBQ0Q7SUFDRSxNQUFNLFVBQVUsRUFBRTtHQUNuQjtBQUZEO0lBQ0UsTUFBTSxVQUFVLEVBQUU7R0FDbkI7QUFDQTtJQUNDLGdCQUFnQjtJQUNoQixnQkFBZ0I7SUFDaEIsZ0JBQWdCO0lBQ2hCLGlCQUFpQjtJQUNqQixTQUFTO0tBQ1IsbUJBQW1COzs7S0FHbkIsMEJBQTBCO0tBQzFCLGdCQUFnQjs7S0FFaEIseUNBQXlDO0tBQ3pDLDRCQUE0QjtLQUM1QiwyQkFBMkI7S0FDM0IsNEJBQTRCO0tBQzVCLHNCQUFzQjtLQUN0QjtBQUVEO01BQ0Usc0ZBQXNGO0lBQ3hGO0FBRUE7TUFDRSxVQUFVO01BQ1YsaUJBQWlCO01BQ2pCLHdCQUF3QjtNQUN4QixvQkFBb0I7TUFDcEIsd0ZBQXdGO0VBQzVGIiwiZmlsZSI6InNyYy9hcHAvZGV0YWlsLWxpc3QvbW9iaWxlL2RldGFpbC1saXN0LW1vYmlsZS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnRhYmxlSGVhZGVye1xuICAgIG1hcmdpbi10b3A6IDUwcHg7XG4gICAgdGFibGUtbGF5b3V0OiBmaXhlZDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBib3JkZXItY29sbGFwc2U6IHVuc2V0O1xufVxuLm1iLTMsIC5teS0zIHtcbiAgbWFyZ2luLWJvdHRvbTogMCAhaW1wb3J0YW50O1xufVxuLnJvd3tcbiAgbWFyZ2luLWxlZnQ6IDA7XG59XG4jbmd4LW1hc29ucnl7XG5tYXJnaW4tbGVmdDogMDtcbm1hcmdpbi1yaWdodDogMDtcbn1cbi5jYXJkLWJvZHkge1xuICAtd2Via2l0LWJveC1mbGV4OiAxO1xuICAtbXMtZmxleDogMSAxIGF1dG87XG4gIGZsZXg6IDEgMSBhdXRvO1xuICBwYWRkaW5nLXRvcDogMDtcbiAgcGFkZGluZy1yaWdodDogMDtcbiAgcGFkZGluZy1sZWZ0OiAwO1xuICBwYWRkaW5nLWJvdHRvbTogMyU7XG4gIGJveC1zaGFkb3c6IDAgNHB4IDhweCAwIHJnYmEoMCwgMCwgMCwgMC4yKSwgMCA2cHggMjBweCAwIHJnYmEoMCwgMCwgMCwgMC4xOSk7XG59XG4uY2FyZC1zdWJ0aXRsZSB7XG4gIHBhZGRpbmctbGVmdDogMiU7XG59XG4uZ29CYWNre1xuICBjb2xvcjogIzhiMDAwMDtcbiAgZm9udC1zaXplOiBsYXJnZTtcbiAgdGV4dC1zaGFkb3c6IDJweCAycHggNHB4ICByZ2JhKDAsIDAsIDAsIDAuMTgpO1xufVxuLmNhcmQtdGl0bGUge1xuICBwYWRkaW5nLWxlZnQ6IDIlO1xuICBwYWRkaW5nLXRvcDogMSU7XG4gIHBhZGRpbmctYm90dG9tOiAxJTtcbiAgLyogYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDExMGRlZywgIzhiMDAwMCA2MCUsICMyYzNlNTAgNjAlKTsgKi9cbiAgLyogYmFja2dyb3VuZDogcmdiYSgxMzksIDAsIDAsIDAuODE1KTsgKi9cbiAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KHRvIGxlZnQsICNmZjAwMDAwMCwgIzJjM2U1MCk7XG4gIC8qIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCh0byByaWdodCwgIzhiMDAwMCAsICMyYzNlNTApOyAqL1xuICBjb2xvcjogd2hpdGU7XG59XG4ubWFzb25yeS1pdGVtIHsgXG4gICAgbWF4LXdpZHRoOiA0OCU7IFxuICAgIG1pbi13aWR0aDogNDglO1xuICAgIG1hcmdpbi1sZWZ0OiAxJTtcbiAgICBtYXJnaW4tcmlnaHQ6IDElO1xuICAgIG1hcmdpbi1ib3R0b206IDIlO1xufVxuLmZvbnRTaXple1xuICBmb250LXNpemU6IDEuMnJlbTtcbn1cblxuLm5hdi1saW5rIHtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICBiYWNrZ3JvdW5kOiAjOGIwMDAwO1xuICAgIG1hcmdpbi10b3A6IDglO1xuICAgIG1hcmdpbi1yaWdodDogMiU7XG59XG4uc2J1dHRvbnMge1xuICAgIGJvdHRvbTogMTUlO1xuICAgIHBvc2l0aW9uOiBmaXhlZDtcbiAgICBtYXJnaW46IDFlbTtcbiAgICBsZWZ0OiAwO1xuICB9XG4gIC5zYnV0dG9uIHtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICB3aWR0aDogNjBweDtcbiAgICBoZWlnaHQ6IDYwcHg7XG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBjb2xvcjogdHJhbnNwYXJlbnQ7XG4gICAgbWFyZ2luOiAyMHB4IGF1dG8gMDtcbiAgICBib3gtc2hhZG93OiAwcHggNXB4IDExcHggLTJweCByZ2JhKDAsIDAsIDAsIDAuMTgpLCAwcHggNHB4IDEycHggLTdweCByZ2JhKDAsIDAsIDAsIDAuMTUpO1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICAtd2Via2l0LXRyYW5zaXRpb246IGFsbCAuMXMgZWFzZS1vdXQ7XG4gICAgdHJhbnNpdGlvbjogYWxsIC4xcyBlYXNlLW91dDtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIH1cbiAgLnNidXR0b24gPiBpIHtcbiAgICBmb250LXNpemU6IDM4cHg7XG4gICAgbGluZS1oZWlnaHQ6IDYwcHg7XG4gICAgdHJhbnNpdGlvbjogYWxsIC4ycyBlYXNlLWluLW91dDtcbiAgICB0cmFuc2l0aW9uLWRlbGF5OiAycztcbiAgfVxuICAuc2J1dHRvbjphY3RpdmUsXG4gIC5zYnV0dG9uOmZvY3VzLFxuICAuc2J1dHRvbjpob3ZlciB7XG4gICAgYm94LXNoYWRvdzogMCAwIDRweCByZ2JhKDAsIDAsIDAsIC4xNCksIDAgNHB4IDhweCByZ2JhKDAsIDAsIDAsIC4yOCk7XG4gIH1cbiAgLnNidXR0b246bm90KDpsYXN0LWNoaWxkKSB7XG4gICAgd2lkdGg6IDYwcHg7XG4gICAgaGVpZ2h0OiA2MHB4O1xuICAgIG1hcmdpbjogMjBweCBhdXRvIDA7XG4gICAgb3BhY2l0eTogMDtcbiAgfVxuICAuc2J1dHRvbjpub3QoOmxhc3QtY2hpbGQpID4gaSB7XG4gICAgZm9udC1zaXplOiAyNXB4O1xuICAgIGxpbmUtaGVpZ2h0OiA2MHB4O1xuICAgIHRyYW5zaXRpb246IGFsbCAuM3MgZWFzZS1pbi1vdXQ7XG4gIH1cbiAgLnNidXR0b246bm90KDpsYXN0LWNoaWxkKSB7XG4gICAgb3BhY2l0eTogMTtcbiAgICB3aWR0aDogNjBweDtcbiAgICBoZWlnaHQ6IDYwcHg7XG4gICAgbWFyZ2luOiAxNXB4IGF1dG8gMDtcbiAgfVxuICAuc2J1dHRvbjpudGgtbGFzdC1jaGlsZCgxKSB7XG4gICAgLXdlYmtpdC10cmFuc2l0aW9uLWRlbGF5OiAyNW1zO1xuICAgIHRyYW5zaXRpb24tZGVsYXk6IDI1bXM7XG4gIH1cbiAgLnNidXR0b246bm90KDpsYXN0LWNoaWxkKTpudGgtbGFzdC1jaGlsZCgyKSB7XG4gICAgLXdlYmtpdC10cmFuc2l0aW9uLWRlbGF5OiAyMG1zO1xuICAgIHRyYW5zaXRpb24tZGVsYXk6IDIwbXM7XG4gIH1cbiAgLnNidXR0b246bm90KDpsYXN0LWNoaWxkKTpudGgtbGFzdC1jaGlsZCgzKSB7XG4gICAgLXdlYmtpdC10cmFuc2l0aW9uLWRlbGF5OiA0MG1zO1xuICAgIHRyYW5zaXRpb24tZGVsYXk6IDQwbXM7XG4gIH1cbiAgLnNidXR0b246bm90KDpsYXN0LWNoaWxkKTpudGgtbGFzdC1jaGlsZCg0KSB7XG4gICAgLXdlYmtpdC10cmFuc2l0aW9uLWRlbGF5OiA2MG1zO1xuICAgIHRyYW5zaXRpb24tZGVsYXk6IDYwbXM7XG4gIH1cbiAgLnNidXR0b246bm90KDpsYXN0LWNoaWxkKTpudGgtbGFzdC1jaGlsZCg1KSB7XG4gICAgLXdlYmtpdC10cmFuc2l0aW9uLWRlbGF5OiA4MG1zO1xuICAgIHRyYW5zaXRpb24tZGVsYXk6IDgwbXM7XG4gIH1cbiAgLnNidXR0b246bm90KDpsYXN0LWNoaWxkKTpudGgtbGFzdC1jaGlsZCg2KSB7XG4gICAgLXdlYmtpdC10cmFuc2l0aW9uLWRlbGF5OiAxMDBtcztcbiAgICB0cmFuc2l0aW9uLWRlbGF5OiAxMDBtcztcbiAgfVxuICAgXG4gIFt0b29sdGlwXTpiZWZvcmUge1xuICAgIGZvbnQtZmFtaWx5OiAnUm9ib3RvJztcbiAgICBmb250LXdlaWdodDogNjAwO1xuICAgIGJvcmRlci1yYWRpdXM6IDJweDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjNTg1ODU4O1xuICAgIGNvbG9yOiAjZmZmO1xuICAgIGNvbnRlbnQ6IGF0dHIodG9vbHRpcCk7XG4gICAgZm9udC1zaXplOiAxMnB4O1xuICAgIHZpc2liaWxpdHk6IGhpZGRlbjtcbiAgICBvcGFjaXR5OiAwO1xuICAgIHBhZGRpbmc6IDVweCA3cHg7XG4gICAgbWFyZ2luLWxlZnQ6IDEwcHg7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGxlZnQ6IDEwMCU7XG4gICAgYm90dG9tOiAyMCU7XG4gICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgfVxuICAgXG4gIC5zYnV0dG9uLm1haW5zYnV0dG9uIHtcbiAgICBiYWNrZ3JvdW5kOnVybCgvYXNzZXRzL2ltYWdlcy9zaGFyZS5qcGcpO1xuICB9XG4gIC5zYnV0dG9uLmdwbHVzIHtcbiAgICBiYWNrZ3JvdW5kOnVybCgvYXNzZXRzL2ltYWdlcy9nbWFpbDYwLnBuZyk7XG4gIH1cbiAgLnNidXR0b24udHdpdHQge1xuICAgIGJhY2tncm91bmQ6dXJsKC9hc3NldHMvaW1hZ2VzL21lc3M2MC5wbmcpO1xuICB9XG4gIC5zYnV0dG9uLmZiIHtcbiAgICBiYWNrZ3JvdW5kOnVybCgvYXNzZXRzL2ltYWdlcy9mYWNlYm9vazYwLnBuZyk7XG4gIH1cbiAgLnNidXR0b24ud2hhdHNhcHAge1xuICAgIGJhY2tncm91bmQ6dXJsKC9hc3NldHMvaW1hZ2VzL3doYXRzYXBwNjAucG5nKTtcbiAgfVxuXG4gIC5ibGluayB7XG4gICAgbWFyZ2luLXRvcDogMi41JTtcbiAgICBhbmltYXRpb246IGJsaW5rZXIgMnMgbGluZWFyIGluZmluaXRlO1xuICAgIGNvbG9yOiAjMmMzZTUwO1xuICAgfVxuICBAa2V5ZnJhbWVzIGJsaW5rZXIgeyAgXG4gICAgNTAlIHsgb3BhY2l0eTogMDsgfVxuICAgfVxuICAgLm1haW4tY29udGVudCB7XG4gICAgcGFkZGluZy10b3A6IDE1JTtcbiAgICBtaW4taGVpZ2h0OiA4MHZoO1xuICAgIHBhZGRpbmctbGVmdDogMiU7XG4gICAgcGFkZGluZy1yaWdodDogMiU7XG4gICAgbWFyZ2luOiAwO1xuICAgICAvKiBUaGUgaW1hZ2UgdXNlZCAqL1xuICAgICBcblxuICAgICAvKiBTZXQgYSBzcGVjaWZpYyBoZWlnaHQgKi9cbiAgICAgbWluLWhlaWdodDogODB2aDtcbiAgIFxuICAgICAvKiBDcmVhdGUgdGhlIHBhcmFsbGF4IHNjcm9sbGluZyBlZmZlY3QgKi9cbiAgICAgYmFja2dyb3VuZC1hdHRhY2htZW50OiBmaXhlZDtcbiAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgICB9XG5cbiAgICAubG9hZGVyIHtcbiAgICAgIGJhY2tncm91bmQ6IHVybCgnL2Fzc2V0cy9pbWFnZXMvbG9hZGluZ19zbWFsbC5naWYnKSA1MCUgNTAlIG5vLXJlcGVhdCByZ2IoMjQ5LDI0OSwyNDkpO1xuICAgIH0gXG5cbiAgICAuanVtYm90cm9ue1xuICAgICAgcGFkZGluZzoyJTtcbiAgICAgIG1hcmdpbi1ib3R0b206IDIlO1xuICAgICAgYmFja2dyb3VuZC1jb2xvcjojZWNmMGYxO1xuICAgICAgYm9yZGVyLXJhZGl1czowLjNyZW07XG4gICAgICBib3gtc2hhZG93OiAwcHggNXB4IDExcHggLTJweCByZ2JhKDAsIDAsIDAsIDAuMTgpLCAwcHggNHB4IDEycHggLTdweCByZ2JhKDAsIDAsIDAsIDAuMTUpO1xuICB9XG4iXX0= */");

/***/ }),

/***/ "./src/app/detail-list/mobile/detail-list-mobile.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/detail-list/mobile/detail-list-mobile.component.ts ***!
  \********************************************************************/
/*! exports provided: DetailListMobileComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetailListMobileComponent", function() { return DetailListMobileComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var rxjs_Rx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/Rx */ "./node_modules/rxjs-compat/_esm2015/Rx.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");
/* harmony import */ var _app_header_header_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../app-header/header.service */ "./src/app/app-header/header.service.ts");



;





let DetailListMobileComponent = class DetailListMobileComponent {
    constructor(http, activatedRoute, router, meta, title, headerService, sanitizer) {
        this.http = http;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.headerService = headerService;
        this.sanitizer = sanitizer;
        this.apiUrl = 'https://api.finalrevise.com/finalrevise/v1/subcat/';
        this.pageData = {};
        this.keys = [];
        this.nav = [];
        this.navIds = [];
        this.tabUrl = 'https://api.finalrevise.com/finalrevise/v1/relatedlinks/';
        this.router.routeReuseStrategy.shouldReuseRoute = function () {
            return false;
        };
        this.title = title;
        this.meta = meta;
    }
    ngOnInit() {
        // subscribe to router event
        this.activatedRoute.params.subscribe((params) => {
            this.selectedId = params['title'];
        });
        this.apiUrl = this.apiUrl + this.selectedId;
        this.tabUrl = this.tabUrl + this.selectedId;
        this.getPageData();
        this.getTabData();
        this.selectedId = this.selectedId.split(' ').join('-');
    }
    shareClicked() {
        if (this.isClickedShare) {
            this.isClickedShare = false;
        }
        else {
            this.isClickedShare = true;
        }
    }
    openNextPage(isChild, metaTitle) {
        if (isChild) {
            this.router.navigateByUrl('/detail-list/' + metaTitle);
        }
        else if (metaTitle.includes('book')) {
            this.router.navigateByUrl('/study-met/' + metaTitle);
        }
        else {
            this.router.navigateByUrl('/detail/' + metaTitle);
        }
    }
    openPageFromNav(pageId, index) {
        if (index == 0) {
            this.router.navigateByUrl('#');
        }
        else {
            this.router.navigateByUrl('/detail-list/' + pageId);
        }
    }
    getData() {
        return this.http.get(this.apiUrl);
    }
    getPageData() {
        this.getData().subscribe(data => {
            this.pageData = data;
            if (this.pageData.data.isChild) {
                this.nav = String(this.pageData.data.pagePath).split("#");
                this.navIds = String(this.pageData.data.pagePathId).split("#");
                this.getKeys(this.pageData.data.groupedPageItems);
                this.aboutExamContent = this.sanitizer.bypassSecurityTrustHtml(this.pageData.data.article);
                this.isArticle = this.pageData.data.article;
                this.setTitleMeta();
            }
            else {
                this.openNextPage(false, this.pageData.data.cat);
            }
            document.getElementById('loader').style.display = 'none';
        });
    }
    getTData() {
        return this.http.get(this.tabUrl);
    }
    getTabData() {
        this.getTData().subscribe(data => {
            if (data) {
                this.headerService.setRelatedTopics(data.data.tabs);
            }
        });
    }
    getKeys(obj) {
        var r = [];
        for (var k in obj) {
            if (!obj.hasOwnProperty(k))
                continue;
            r.push(k);
        }
        this.keys = r;
    }
    setTitleMeta() {
        this.title.setTitle(this.pageData.data.tag);
        this.meta.addTags([
            { name: 'author', content: 'finalrevise.com' },
            { name: 'keywords', content: this.pageData.data.keywords },
            {
                name: 'description', content: this.pageData.data.description
            }
        ]);
    }
    replaceAll(str, replacement) {
        str = str.split('(').join('');
        str = str.split(')').join('');
        str = str.split('.').join('');
        str = str.split('/').join('');
        str = str.split('|').join(':');
        str = str.split(' ').join(replacement);
        str = str.split('---').join(replacement);
        return str.split('--').join(replacement);
    }
    ;
    replaceAllSocial(str) {
        var find = '-';
        var re = new RegExp(find, 'g');
        return str.replace(re, ' ');
    }
    openFacebook() {
        window.open("https://www.facebook.com/sharer/sharer.php?u=http://finalrevise.com/detail-list/" + this.selectedId);
    }
    openWhatsApp() {
        window.open("https://api.whatsapp.com/send?text=*Download%20" + this.selectedId + "*%0a%0a" + "http://finalrevise.com/detail-list/" + this.selectedId +
            "%0a%0a*Download%20FinalRevise%20App%20:%20https://play.google.com/store/apps/details?id%3Dcom.finalrevise.android*");
    }
    openMail() {
        window.open("mailto:?subject=FinalRevise%20Study%20Material%20for%20" + this.selectedId +
            "&body=Download%20" + this.replaceAllSocial(this.selectedId) + "%0a%0a" + "http://finalrevise.com/detail-list/" + this.selectedId +
            "%0a%0a Download FinalRevise App : https://play.google.com/store/apps/details?id=com.finalrevise.android");
    }
    openMessenger() {
        window.open("https://m.me/finalrevise?u=http://finalrevise.com/detail-list/" + this.selectedId);
    }
};
DetailListMobileComponent.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__["Meta"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__["Title"] },
    { type: _app_header_header_service__WEBPACK_IMPORTED_MODULE_6__["HeaderService"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__["DomSanitizer"] }
];
DetailListMobileComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-detail-list',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./detail-list-mobile.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/detail-list/mobile/detail-list-mobile.component.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./detail-list-mobile.component.css */ "./src/app/detail-list/mobile/detail-list-mobile.component.css")).default]
    })
], DetailListMobileComponent);



/***/ }),

/***/ "./src/app/detail/data.service.ts":
/*!****************************************!*\
  !*** ./src/app/detail/data.service.ts ***!
  \****************************************/
/*! exports provided: CounterService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CounterService", function() { return CounterService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


let CounterService = class CounterService {
    constructor() {
        this.count = 0;
    }
    getCount() {
        let temp = parseInt(localStorage.getItem('count'));
        return temp ? temp : 0;
    }
    addCount() {
        let temp = parseInt(localStorage.getItem('count'));
        this.count = temp ? temp : 0;
        this.count = this.count + 1;
        localStorage.setItem('count', " + this.count);
    }
};
CounterService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
], CounterService);



/***/ }),

/***/ "./src/app/detail/desktop/desktop-detail.component.css":
/*!*************************************************************!*\
  !*** ./src/app/detail/desktop/desktop-detail.component.css ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".jumbotron{\n    padding:2%;\n    margin-bottom:0rem;\n    background-color:#ecf0f1;\n    border-radius:0.3rem;\n    box-shadow: 0px 5px 11px -2px rgba(0, 0, 0, 0.18), 0px 4px 12px -7px rgba(0, 0, 0, 0.15);\n}\nli {\n  list-style-type: none;\n  text-shadow: 2px 2px 4px  rgba(0, 0, 0, 0.18);\n  font-size: large;\n}\nul, menu, dir {\n    -webkit-margin-before: 0;\n            margin-block-start: 0;\n    -webkit-margin-after: 0;\n            margin-block-end: 0;\n    -webkit-margin-start: 15px;\n            margin-inline-start: 15px;\n    -webkit-margin-end: 0px;\n            margin-inline-end: 0px;\n    -webkit-padding-start: 0px;\n            padding-inline-start: 0px;\n}\n.bg-primary {\n    /* background-color: #dfeaec !important; */\n    animation: colorchange 5s infinite; /* animation-name followed by duration in seconds*/\n         /* you could also use milliseconds (ms) or something like 2.5s */\n      -webkit-animation: colorchange 5s infinite; /* Chrome and Safari */\n}\n#fixed-width-flamingo {  \n    width: 35px; \n    height: 35px;\n}\n.tableHeader{\n    margin-top: 50px;\n    table-layout: fixed;\n    width: 100%;\n    border-collapse: unset;\n}\n.col-sm-3{\n        padding:1rem 0rem;\n        margin-bottom:0rem;\n        background-color:#ecf0f1;\n        border-radius:0.3rem;\n        -webkit-box-flex:0;height: -webkit-fit-content;height: -moz-fit-content;height: fit-content;\n    }\n.navbar {\n        position: relative;\n        display: block;\n        flex-wrap: wrap;\n        text-align: center;\n        justify-content: space-between;\n        padding: 5px;\n    }\n.fontSize{\n      font-size: 1.2rem;\n    }\n.nav-link {\n    display: block;\n    background: #8b0000;\n    margin-bottom: 3%;\n    margin-top: 8%;\n    margin-right: 2%;\n}\n@media (max-width:720px){\n\tbody{\n\t\ttext-align: center;\n\t}\n\t.display-4{\n\t\tfont-size: 30px !important;\n\t}\n}\n@keyframes colorchange\n    {\n      0%   {background: #2c3e50;}\n      33%  {background: rebeccapurple;}\n      66%  {background: #008f72}\n      100%  {background: #8b0000;}\n    }\n@-webkit-keyframes colorchange /* Safari and Chrome - necessary duplicate */\n    {\n      0%   {background: #2c3e50;}\n      33%  {background: rebeccapurple;}\n      66%  {background: rgb(0, 143, 114)}\n      100%  {background: #8b0000;}\n    }\n.text{ \n        white-space: nowrap;  \n        overflow: hidden; \n        text-overflow: ellipsis; \n    }\n.sbuttons {\n      bottom: 15%;\n      position: fixed;\n      margin: 1em;\n      left: 0;\n    }\n.sbutton {\n      display: block;\n      width: 60px;\n      height: 60px;\n      border-radius: 50%;\n      text-align: center;\n      color: transparent;\n      margin: 20px auto 0;\n      box-shadow: 0px 5px 11px -2px rgba(0, 0, 0, 0.18), 0px 4px 12px -7px rgba(0, 0, 0, 0.15);\n      cursor: pointer;\n      transition: all .1s ease-out;\n      position: relative;\n    }\n.sbutton > i {\n      font-size: 38px;\n      line-height: 60px;\n      transition: all .2s ease-in-out;\n      transition-delay: 2s;\n    }\n.sbutton:active,\n    .sbutton:focus,\n    .sbutton:hover {\n      box-shadow: 0 0 4px rgba(0, 0, 0, .14), 0 4px 8px rgba(0, 0, 0, .28);\n    }\n.sbutton:not(:last-child) {\n      width: 60px;\n      height: 60px;\n      margin: 20px auto 0;\n      opacity: 0;\n    }\n.sbutton:not(:last-child) > i {\n      font-size: 25px;\n      line-height: 60px;\n      transition: all .3s ease-in-out;\n    }\n.sbutton:not(:last-child) {\n      opacity: 1;\n      width: 60px;\n      height: 60px;\n      margin: 15px auto 0;\n    }\n.sbutton:nth-last-child(1) {\n      transition-delay: 25ms;\n    }\n.sbutton:not(:last-child):nth-last-child(2) {\n      transition-delay: 20ms;\n    }\n.sbutton:not(:last-child):nth-last-child(3) {\n      transition-delay: 40ms;\n    }\n.sbutton:not(:last-child):nth-last-child(4) {\n      transition-delay: 60ms;\n    }\n.sbutton:not(:last-child):nth-last-child(5) {\n      transition-delay: 80ms;\n    }\n.sbutton:not(:last-child):nth-last-child(6) {\n      transition-delay: 100ms;\n    }\n[tooltip]:before {\n      font-family: 'Roboto';\n      font-weight: 600;\n      border-radius: 2px;\n      background-color: #585858;\n      color: #fff;\n      content: attr(tooltip);\n      font-size: 12px;\n      visibility: hidden;\n      opacity: 0;\n      padding: 5px 7px;\n      margin-left: 10px;\n      position: absolute;\n      left: 100%;\n      bottom: 20%;\n      white-space: nowrap;\n    }\n.sbutton.mainsbutton {\n      background:url(/assets/images/share.jpg);\n    }\n.sbutton.gplus {\n      background:url(/assets/images/gmail60.png);\n    }\n.sbutton.twitt {\n      background:url(/assets/images/mess60.png);\n    }\n.sbutton.fb {\n      background:url(/assets/images/facebook60.png);\n    }\n.sbutton.whatsapp {\n      background:url(/assets/images/whatsapp60.png);\n    }\n.blink {\n      margin-top: 2.5%;\n      -webkit-animation: blinker 2s linear infinite;\n              animation: blinker 2s linear infinite;\n     }\n@-webkit-keyframes blinker {  \n      50% { opacity: 0; }\n     }\n@keyframes blinker {  \n      50% { opacity: 0; }\n     }\n.main-content {\n    padding-top: 6.5%;\n    padding-left: 2%;\n    padding-right: 2%;\n    min-height: 80vh;\n  }\nhr{\n    margin: 0;\n  }\n.table1{\n  margin-left: 1%;\n  margin-right: 1%;\n  }\n  \n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZGV0YWlsL2Rlc2t0b3AvZGVza3RvcC1kZXRhaWwuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLFVBQVU7SUFDVixrQkFBa0I7SUFDbEIsd0JBQXdCO0lBQ3hCLG9CQUFvQjtJQUNwQix3RkFBd0Y7QUFDNUY7QUFDQTtFQUNFLHFCQUFxQjtFQUNyQiw2Q0FBNkM7RUFDN0MsZ0JBQWdCO0FBQ2xCO0FBQ0E7SUFDSSx3QkFBcUI7WUFBckIscUJBQXFCO0lBQ3JCLHVCQUFtQjtZQUFuQixtQkFBbUI7SUFDbkIsMEJBQXlCO1lBQXpCLHlCQUF5QjtJQUN6Qix1QkFBc0I7WUFBdEIsc0JBQXNCO0lBQ3RCLDBCQUF5QjtZQUF6Qix5QkFBeUI7QUFDN0I7QUFDQTtJQUNJLDBDQUEwQztJQUMxQyxrQ0FBa0MsRUFBRSxrREFBa0Q7U0FDakYsZ0VBQWdFO01BQ25FLDBDQUEwQyxFQUFFLHNCQUFzQjtBQUN4RTtBQUNBO0lBQ0ksV0FBVztJQUNYLFlBQVk7QUFDaEI7QUFDRTtJQUNFLGdCQUFnQjtJQUNoQixtQkFBbUI7SUFDbkIsV0FBVztJQUNYLHNCQUFzQjtBQUMxQjtBQUNJO1FBQ0ksaUJBQWlCO1FBQ2pCLGtCQUFrQjtRQUNsQix3QkFBd0I7UUFDeEIsb0JBQW9CO1FBQ3BCLGtCQUFrQixDQUFDLDJCQUFtQixDQUFuQix3QkFBbUIsQ0FBbkIsbUJBQW1CO0lBQzFDO0FBQ0E7UUFDSSxrQkFBa0I7UUFDbEIsY0FBYztRQUNkLGVBQWU7UUFDZixrQkFBa0I7UUFDbEIsOEJBQThCO1FBQzlCLFlBQVk7SUFDaEI7QUFDQTtNQUNFLGlCQUFpQjtJQUNuQjtBQUNKO0lBQ0ksY0FBYztJQUNkLG1CQUFtQjtJQUNuQixpQkFBaUI7SUFDakIsY0FBYztJQUNkLGdCQUFnQjtBQUNwQjtBQUVBO0NBQ0M7RUFDQyxrQkFBa0I7Q0FDbkI7Q0FDQTtFQUNDLDBCQUEwQjtDQUMzQjtBQUNEO0FBRUE7O01BRU0sTUFBTSxtQkFBbUIsQ0FBQztNQUMxQixNQUFNLHlCQUF5QixDQUFDO01BQ2hDLE1BQU0sbUJBQW1CO01BQ3pCLE9BQU8sbUJBQW1CLENBQUM7SUFDN0I7QUFFQTs7TUFFRSxNQUFNLG1CQUFtQixDQUFDO01BQzFCLE1BQU0seUJBQXlCLENBQUM7TUFDaEMsTUFBTSw0QkFBNEI7TUFDbEMsT0FBTyxtQkFBbUIsQ0FBQztJQUM3QjtBQUNBO1FBQ0ksbUJBQW1CO1FBQ25CLGdCQUFnQjtRQUNoQix1QkFBdUI7SUFDM0I7QUFFQTtNQUNFLFdBQVc7TUFDWCxlQUFlO01BQ2YsV0FBVztNQUNYLE9BQU87SUFDVDtBQUNBO01BQ0UsY0FBYztNQUNkLFdBQVc7TUFDWCxZQUFZO01BQ1osa0JBQWtCO01BQ2xCLGtCQUFrQjtNQUNsQixrQkFBa0I7TUFDbEIsbUJBQW1CO01BQ25CLHdGQUF3RjtNQUN4RixlQUFlO01BRWYsNEJBQTRCO01BQzVCLGtCQUFrQjtJQUNwQjtBQUNBO01BQ0UsZUFBZTtNQUNmLGlCQUFpQjtNQUNqQiwrQkFBK0I7TUFDL0Isb0JBQW9CO0lBQ3RCO0FBQ0E7OztNQUdFLG9FQUFvRTtJQUN0RTtBQUNBO01BQ0UsV0FBVztNQUNYLFlBQVk7TUFDWixtQkFBbUI7TUFDbkIsVUFBVTtJQUNaO0FBQ0E7TUFDRSxlQUFlO01BQ2YsaUJBQWlCO01BQ2pCLCtCQUErQjtJQUNqQztBQUNBO01BQ0UsVUFBVTtNQUNWLFdBQVc7TUFDWCxZQUFZO01BQ1osbUJBQW1CO0lBQ3JCO0FBQ0E7TUFFRSxzQkFBc0I7SUFDeEI7QUFDQTtNQUVFLHNCQUFzQjtJQUN4QjtBQUNBO01BRUUsc0JBQXNCO0lBQ3hCO0FBQ0E7TUFFRSxzQkFBc0I7SUFDeEI7QUFDQTtNQUVFLHNCQUFzQjtJQUN4QjtBQUNBO01BRUUsdUJBQXVCO0lBQ3pCO0FBRUE7TUFDRSxxQkFBcUI7TUFDckIsZ0JBQWdCO01BQ2hCLGtCQUFrQjtNQUNsQix5QkFBeUI7TUFDekIsV0FBVztNQUNYLHNCQUFzQjtNQUN0QixlQUFlO01BQ2Ysa0JBQWtCO01BQ2xCLFVBQVU7TUFDVixnQkFBZ0I7TUFDaEIsaUJBQWlCO01BQ2pCLGtCQUFrQjtNQUNsQixVQUFVO01BQ1YsV0FBVztNQUNYLG1CQUFtQjtJQUNyQjtBQUVBO01BQ0Usd0NBQXdDO0lBQzFDO0FBQ0E7TUFDRSwwQ0FBMEM7SUFDNUM7QUFDQTtNQUNFLHlDQUF5QztJQUMzQztBQUNBO01BQ0UsNkNBQTZDO0lBQy9DO0FBQ0E7TUFDRSw2Q0FBNkM7SUFDL0M7QUFFQTtNQUNFLGdCQUFnQjtNQUNoQiw2Q0FBcUM7Y0FBckMscUNBQXFDO0tBQ3RDO0FBQ0Q7TUFDRSxNQUFNLFVBQVUsRUFBRTtLQUNuQjtBQUZEO01BQ0UsTUFBTSxVQUFVLEVBQUU7S0FDbkI7QUFFRjtJQUNDLGlCQUFpQjtJQUNqQixnQkFBZ0I7SUFDaEIsaUJBQWlCO0lBQ2pCLGdCQUFnQjtFQUNsQjtBQUVBO0lBQ0UsU0FBUztFQUNYO0FBRUE7RUFDQSxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCIiwiZmlsZSI6InNyYy9hcHAvZGV0YWlsL2Rlc2t0b3AvZGVza3RvcC1kZXRhaWwuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5qdW1ib3Ryb257XG4gICAgcGFkZGluZzoyJTtcbiAgICBtYXJnaW4tYm90dG9tOjByZW07XG4gICAgYmFja2dyb3VuZC1jb2xvcjojZWNmMGYxO1xuICAgIGJvcmRlci1yYWRpdXM6MC4zcmVtO1xuICAgIGJveC1zaGFkb3c6IDBweCA1cHggMTFweCAtMnB4IHJnYmEoMCwgMCwgMCwgMC4xOCksIDBweCA0cHggMTJweCAtN3B4IHJnYmEoMCwgMCwgMCwgMC4xNSk7XG59XG5saSB7XG4gIGxpc3Qtc3R5bGUtdHlwZTogbm9uZTtcbiAgdGV4dC1zaGFkb3c6IDJweCAycHggNHB4ICByZ2JhKDAsIDAsIDAsIDAuMTgpO1xuICBmb250LXNpemU6IGxhcmdlO1xufVxudWwsIG1lbnUsIGRpciB7XG4gICAgbWFyZ2luLWJsb2NrLXN0YXJ0OiAwO1xuICAgIG1hcmdpbi1ibG9jay1lbmQ6IDA7XG4gICAgbWFyZ2luLWlubGluZS1zdGFydDogMTVweDtcbiAgICBtYXJnaW4taW5saW5lLWVuZDogMHB4O1xuICAgIHBhZGRpbmctaW5saW5lLXN0YXJ0OiAwcHg7XG59XG4uYmctcHJpbWFyeSB7XG4gICAgLyogYmFja2dyb3VuZC1jb2xvcjogI2RmZWFlYyAhaW1wb3J0YW50OyAqL1xuICAgIGFuaW1hdGlvbjogY29sb3JjaGFuZ2UgNXMgaW5maW5pdGU7IC8qIGFuaW1hdGlvbi1uYW1lIGZvbGxvd2VkIGJ5IGR1cmF0aW9uIGluIHNlY29uZHMqL1xuICAgICAgICAgLyogeW91IGNvdWxkIGFsc28gdXNlIG1pbGxpc2Vjb25kcyAobXMpIG9yIHNvbWV0aGluZyBsaWtlIDIuNXMgKi9cbiAgICAgIC13ZWJraXQtYW5pbWF0aW9uOiBjb2xvcmNoYW5nZSA1cyBpbmZpbml0ZTsgLyogQ2hyb21lIGFuZCBTYWZhcmkgKi9cbn1cbiNmaXhlZC13aWR0aC1mbGFtaW5nbyB7ICBcbiAgICB3aWR0aDogMzVweDsgXG4gICAgaGVpZ2h0OiAzNXB4O1xufVxuICAudGFibGVIZWFkZXJ7XG4gICAgbWFyZ2luLXRvcDogNTBweDtcbiAgICB0YWJsZS1sYXlvdXQ6IGZpeGVkO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGJvcmRlci1jb2xsYXBzZTogdW5zZXQ7XG59XG4gICAgLmNvbC1zbS0ze1xuICAgICAgICBwYWRkaW5nOjFyZW0gMHJlbTtcbiAgICAgICAgbWFyZ2luLWJvdHRvbTowcmVtO1xuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiNlY2YwZjE7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6MC4zcmVtO1xuICAgICAgICAtd2Via2l0LWJveC1mbGV4OjA7aGVpZ2h0OiBmaXQtY29udGVudDtcbiAgICB9XG4gICAgLm5hdmJhciB7XG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgIGZsZXgtd3JhcDogd3JhcDtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gICAgICAgIHBhZGRpbmc6IDVweDtcbiAgICB9XG4gICAgLmZvbnRTaXple1xuICAgICAgZm9udC1zaXplOiAxLjJyZW07XG4gICAgfVxuLm5hdi1saW5rIHtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICBiYWNrZ3JvdW5kOiAjOGIwMDAwO1xuICAgIG1hcmdpbi1ib3R0b206IDMlO1xuICAgIG1hcmdpbi10b3A6IDglO1xuICAgIG1hcmdpbi1yaWdodDogMiU7XG59XG5cbkBtZWRpYSAobWF4LXdpZHRoOjcyMHB4KXtcblx0Ym9keXtcblx0XHR0ZXh0LWFsaWduOiBjZW50ZXI7XG5cdH1cblx0LmRpc3BsYXktNHtcblx0XHRmb250LXNpemU6IDMwcHggIWltcG9ydGFudDtcblx0fVxufVxuXG5Aa2V5ZnJhbWVzIGNvbG9yY2hhbmdlXG4gICAge1xuICAgICAgMCUgICB7YmFja2dyb3VuZDogIzJjM2U1MDt9XG4gICAgICAzMyUgIHtiYWNrZ3JvdW5kOiByZWJlY2NhcHVycGxlO31cbiAgICAgIDY2JSAge2JhY2tncm91bmQ6ICMwMDhmNzJ9XG4gICAgICAxMDAlICB7YmFja2dyb3VuZDogIzhiMDAwMDt9XG4gICAgfVxuXG4gICAgQC13ZWJraXQta2V5ZnJhbWVzIGNvbG9yY2hhbmdlIC8qIFNhZmFyaSBhbmQgQ2hyb21lIC0gbmVjZXNzYXJ5IGR1cGxpY2F0ZSAqL1xuICAgIHtcbiAgICAgIDAlICAge2JhY2tncm91bmQ6ICMyYzNlNTA7fVxuICAgICAgMzMlICB7YmFja2dyb3VuZDogcmViZWNjYXB1cnBsZTt9XG4gICAgICA2NiUgIHtiYWNrZ3JvdW5kOiByZ2IoMCwgMTQzLCAxMTQpfVxuICAgICAgMTAwJSAge2JhY2tncm91bmQ6ICM4YjAwMDA7fVxuICAgIH1cbiAgICAudGV4dHsgXG4gICAgICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7ICBcbiAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjsgXG4gICAgICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzOyBcbiAgICB9XG5cbiAgICAuc2J1dHRvbnMge1xuICAgICAgYm90dG9tOiAxNSU7XG4gICAgICBwb3NpdGlvbjogZml4ZWQ7XG4gICAgICBtYXJnaW46IDFlbTtcbiAgICAgIGxlZnQ6IDA7XG4gICAgfVxuICAgIC5zYnV0dG9uIHtcbiAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgd2lkdGg6IDYwcHg7XG4gICAgICBoZWlnaHQ6IDYwcHg7XG4gICAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICBjb2xvcjogdHJhbnNwYXJlbnQ7XG4gICAgICBtYXJnaW46IDIwcHggYXV0byAwO1xuICAgICAgYm94LXNoYWRvdzogMHB4IDVweCAxMXB4IC0ycHggcmdiYSgwLCAwLCAwLCAwLjE4KSwgMHB4IDRweCAxMnB4IC03cHggcmdiYSgwLCAwLCAwLCAwLjE1KTtcbiAgICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICAgIC13ZWJraXQtdHJhbnNpdGlvbjogYWxsIC4xcyBlYXNlLW91dDtcbiAgICAgIHRyYW5zaXRpb246IGFsbCAuMXMgZWFzZS1vdXQ7XG4gICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgfVxuICAgIC5zYnV0dG9uID4gaSB7XG4gICAgICBmb250LXNpemU6IDM4cHg7XG4gICAgICBsaW5lLWhlaWdodDogNjBweDtcbiAgICAgIHRyYW5zaXRpb246IGFsbCAuMnMgZWFzZS1pbi1vdXQ7XG4gICAgICB0cmFuc2l0aW9uLWRlbGF5OiAycztcbiAgICB9XG4gICAgLnNidXR0b246YWN0aXZlLFxuICAgIC5zYnV0dG9uOmZvY3VzLFxuICAgIC5zYnV0dG9uOmhvdmVyIHtcbiAgICAgIGJveC1zaGFkb3c6IDAgMCA0cHggcmdiYSgwLCAwLCAwLCAuMTQpLCAwIDRweCA4cHggcmdiYSgwLCAwLCAwLCAuMjgpO1xuICAgIH1cbiAgICAuc2J1dHRvbjpub3QoOmxhc3QtY2hpbGQpIHtcbiAgICAgIHdpZHRoOiA2MHB4O1xuICAgICAgaGVpZ2h0OiA2MHB4O1xuICAgICAgbWFyZ2luOiAyMHB4IGF1dG8gMDtcbiAgICAgIG9wYWNpdHk6IDA7XG4gICAgfVxuICAgIC5zYnV0dG9uOm5vdCg6bGFzdC1jaGlsZCkgPiBpIHtcbiAgICAgIGZvbnQtc2l6ZTogMjVweDtcbiAgICAgIGxpbmUtaGVpZ2h0OiA2MHB4O1xuICAgICAgdHJhbnNpdGlvbjogYWxsIC4zcyBlYXNlLWluLW91dDtcbiAgICB9XG4gICAgLnNidXR0b246bm90KDpsYXN0LWNoaWxkKSB7XG4gICAgICBvcGFjaXR5OiAxO1xuICAgICAgd2lkdGg6IDYwcHg7XG4gICAgICBoZWlnaHQ6IDYwcHg7XG4gICAgICBtYXJnaW46IDE1cHggYXV0byAwO1xuICAgIH1cbiAgICAuc2J1dHRvbjpudGgtbGFzdC1jaGlsZCgxKSB7XG4gICAgICAtd2Via2l0LXRyYW5zaXRpb24tZGVsYXk6IDI1bXM7XG4gICAgICB0cmFuc2l0aW9uLWRlbGF5OiAyNW1zO1xuICAgIH1cbiAgICAuc2J1dHRvbjpub3QoOmxhc3QtY2hpbGQpOm50aC1sYXN0LWNoaWxkKDIpIHtcbiAgICAgIC13ZWJraXQtdHJhbnNpdGlvbi1kZWxheTogMjBtcztcbiAgICAgIHRyYW5zaXRpb24tZGVsYXk6IDIwbXM7XG4gICAgfVxuICAgIC5zYnV0dG9uOm5vdCg6bGFzdC1jaGlsZCk6bnRoLWxhc3QtY2hpbGQoMykge1xuICAgICAgLXdlYmtpdC10cmFuc2l0aW9uLWRlbGF5OiA0MG1zO1xuICAgICAgdHJhbnNpdGlvbi1kZWxheTogNDBtcztcbiAgICB9XG4gICAgLnNidXR0b246bm90KDpsYXN0LWNoaWxkKTpudGgtbGFzdC1jaGlsZCg0KSB7XG4gICAgICAtd2Via2l0LXRyYW5zaXRpb24tZGVsYXk6IDYwbXM7XG4gICAgICB0cmFuc2l0aW9uLWRlbGF5OiA2MG1zO1xuICAgIH1cbiAgICAuc2J1dHRvbjpub3QoOmxhc3QtY2hpbGQpOm50aC1sYXN0LWNoaWxkKDUpIHtcbiAgICAgIC13ZWJraXQtdHJhbnNpdGlvbi1kZWxheTogODBtcztcbiAgICAgIHRyYW5zaXRpb24tZGVsYXk6IDgwbXM7XG4gICAgfVxuICAgIC5zYnV0dG9uOm5vdCg6bGFzdC1jaGlsZCk6bnRoLWxhc3QtY2hpbGQoNikge1xuICAgICAgLXdlYmtpdC10cmFuc2l0aW9uLWRlbGF5OiAxMDBtcztcbiAgICAgIHRyYW5zaXRpb24tZGVsYXk6IDEwMG1zO1xuICAgIH1cbiAgICAgXG4gICAgW3Rvb2x0aXBdOmJlZm9yZSB7XG4gICAgICBmb250LWZhbWlseTogJ1JvYm90byc7XG4gICAgICBmb250LXdlaWdodDogNjAwO1xuICAgICAgYm9yZGVyLXJhZGl1czogMnB4O1xuICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzU4NTg1ODtcbiAgICAgIGNvbG9yOiAjZmZmO1xuICAgICAgY29udGVudDogYXR0cih0b29sdGlwKTtcbiAgICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICAgIHZpc2liaWxpdHk6IGhpZGRlbjtcbiAgICAgIG9wYWNpdHk6IDA7XG4gICAgICBwYWRkaW5nOiA1cHggN3B4O1xuICAgICAgbWFyZ2luLWxlZnQ6IDEwcHg7XG4gICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICBsZWZ0OiAxMDAlO1xuICAgICAgYm90dG9tOiAyMCU7XG4gICAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICAgIH1cbiAgICAgXG4gICAgLnNidXR0b24ubWFpbnNidXR0b24ge1xuICAgICAgYmFja2dyb3VuZDp1cmwoL2Fzc2V0cy9pbWFnZXMvc2hhcmUuanBnKTtcbiAgICB9XG4gICAgLnNidXR0b24uZ3BsdXMge1xuICAgICAgYmFja2dyb3VuZDp1cmwoL2Fzc2V0cy9pbWFnZXMvZ21haWw2MC5wbmcpO1xuICAgIH1cbiAgICAuc2J1dHRvbi50d2l0dCB7XG4gICAgICBiYWNrZ3JvdW5kOnVybCgvYXNzZXRzL2ltYWdlcy9tZXNzNjAucG5nKTtcbiAgICB9XG4gICAgLnNidXR0b24uZmIge1xuICAgICAgYmFja2dyb3VuZDp1cmwoL2Fzc2V0cy9pbWFnZXMvZmFjZWJvb2s2MC5wbmcpO1xuICAgIH1cbiAgICAuc2J1dHRvbi53aGF0c2FwcCB7XG4gICAgICBiYWNrZ3JvdW5kOnVybCgvYXNzZXRzL2ltYWdlcy93aGF0c2FwcDYwLnBuZyk7XG4gICAgfVxuICBcbiAgICAuYmxpbmsge1xuICAgICAgbWFyZ2luLXRvcDogMi41JTtcbiAgICAgIGFuaW1hdGlvbjogYmxpbmtlciAycyBsaW5lYXIgaW5maW5pdGU7XG4gICAgIH1cbiAgICBAa2V5ZnJhbWVzIGJsaW5rZXIgeyAgXG4gICAgICA1MCUgeyBvcGFjaXR5OiAwOyB9XG4gICAgIH1cblxuICAgLm1haW4tY29udGVudCB7XG4gICAgcGFkZGluZy10b3A6IDYuNSU7XG4gICAgcGFkZGluZy1sZWZ0OiAyJTtcbiAgICBwYWRkaW5nLXJpZ2h0OiAyJTtcbiAgICBtaW4taGVpZ2h0OiA4MHZoO1xuICB9XG5cbiAgaHJ7XG4gICAgbWFyZ2luOiAwO1xuICB9XG5cbiAgLnRhYmxlMXtcbiAgbWFyZ2luLWxlZnQ6IDElO1xuICBtYXJnaW4tcmlnaHQ6IDElO1xuICB9XG4gICJdfQ== */");

/***/ }),

/***/ "./src/app/detail/desktop/desktop-detail.component.ts":
/*!************************************************************!*\
  !*** ./src/app/detail/desktop/desktop-detail.component.ts ***!
  \************************************************************/
/*! exports provided: DetailComponentDesktop */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetailComponentDesktop", function() { return DetailComponentDesktop; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var rxjs_Rx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/Rx */ "./node_modules/rxjs-compat/_esm2015/Rx.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _app_header_header_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../app-header/header.service */ "./src/app/app-header/header.service.ts");



;






let DetailComponentDesktop = class DetailComponentDesktop {
    constructor(http, activatedRoute, router, meta, title, sanitizer, headerService) {
        this.http = http;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.sanitizer = sanitizer;
        this.headerService = headerService;
        this.apiUrl = 'https://api.finalrevise.com/finalrevise/v1/content/';
        this.pageData = {};
        this.keys = [];
        this.nav = [];
        this.navIds = [];
        this.tabUrl = 'https://api.finalrevise.com/finalrevise/v1/relatedlinks/';
        this.whatsAppUrl = 'https://api.finalrevise.com/finalrevise/v1/whatsApp/';
        this.router.routeReuseStrategy.shouldReuseRoute = function () {
            return false;
        };
        this.title = title;
        this.meta = meta;
    }
    ngOnInit() {
        // subscribe to router event
        this.activatedRoute.params.subscribe((params) => {
            this.titleText = params['title'];
        });
        this.apiUrl = this.apiUrl + this.titleText;
        this.tabUrl = this.tabUrl + this.titleText;
        this.whatsAppUrl = this.whatsAppUrl + this.titleText.split('/').join('-');
        this.getWhatsAppUrl();
        this.getPageData();
        this.getTabData();
    }
    shareClicked() {
        if (this.isClickedShare) {
            this.isClickedShare = false;
        }
        else {
            this.isClickedShare = true;
        }
    }
    getData() {
        return this.http.get(this.apiUrl);
    }
    getTData() {
        return this.http.get(this.tabUrl);
    }
    getTabData() {
        this.getTData().subscribe(data => {
            this.tags = data.data.related;
            this.tabs = data;
            this.headerService.setRelatedTopics(this.tabs.data.tabs);
        });
    }
    getWhatsAppUrl() {
        this.getWhatsAppData().subscribe(data => {
            this.groupUrl = data.data.whatsAppUrl;
        });
    }
    getWhatsAppData() {
        return this.http.get(this.whatsAppUrl);
    }
    getPageData() {
        this.getData().subscribe(data => {
            this.pageData = data;
            this.nav = String(this.pageData.data.pagePath).split("#");
            this.navCount = this.nav.length;
            this.navIds = String(this.pageData.data.pagePathId).split("#");
            this.aboutExamContent = this.sanitizer.bypassSecurityTrustHtml(this.pageData.data.pageIntro);
            this.linkedPageItems = this.pageData.data.linkedPageItems;
            this.pageTitle = this.pageData.data.pageTitle;
            // For DU-JAT only
            if (this.pageTitle.includes('DU-JAT')) {
                this.dujat = true;
            }
            this.setTitleMeta();
        });
    }
    openPageFromNav(pageId, index) {
        if (index == 0) {
            this.router.navigateByUrl('#');
        }
        else {
            this.router.navigateByUrl('/detail-list/' + pageId);
        }
    }
    openNextPage(link, paperName) {
        paperName = this.replaceAllString(paperName, ",", " ");
        paperName = this.replaceAllString(paperName, "(", " ");
        paperName = this.replaceAllString(paperName, ")", " ");
        paperName = this.replaceAllString(paperName, "?", " ");
        this.openPdfViewer(link, paperName);
    }
    openPdfViewer(link, paperName) {
        if (link.includes('open')) {
            this.router.navigateByUrl('/pdf-viewer/' + this.titleText + "/" + paperName + "/" + link.substring(link.indexOf("?id=") + 4));
        }
        else if (link.includes("/view")) {
            this.router.navigateByUrl('/pdf-viewer/' + this.titleText + "/" + paperName + "/" + link.replace("/view", ").substring(link.indexOf("/d/") + 3));
        }
        else {
            window.open(link);
        }
    }
    replaceAllString(str, find, replace) {
        return str.split(find).join(replace);
    }
    openTag(tab) {
        if (tab.isChild == 1) {
            this.router.navigateByUrl('/detail-list/' + tab.linkTitle);
        }
        else {
            if (tab.linkTitle.includes('book')) {
                this.router.navigateByUrl('/study-met/' + tab.linkTitle);
            }
            else {
                this.router.navigateByUrl('/detail/' + tab.linkTitle);
            }
        }
    }
    replaceAllSocial(str) {
        var find = '-';
        var re = new RegExp(find, 'g');
        return str.replace(re, ' ');
    }
    openFacebook() {
        window.open("https://www.facebook.com/sharer/sharer.php?u=http://finalrevise.com/detail/" + this.titleText);
    }
    openWhatsApp() {
        window.open("https://api.whatsapp.com/send?text=*Download%20" + this.titleText + "*%0a%0a" + "http://finalrevise.com/detail/" + this.titleText +
            "%0a%0a*Download%20FinalRevise%20App%20:%20https://play.google.com/store/apps/details?id%3Dcom.finalrevise.android*");
    }
    openMail() {
        window.open("mailto:?subject=FinalRevise%20Study%20Material%20for%20" + this.nav[this.navCount - 1] +
            "&body=Download%20" + this.replaceAllSocial(this.titleText) + "%0a%0a" + "http://finalrevise.com/detail/" + this.titleText +
            "%0a%0a Download FinalRevise App : https://play.google.com/store/apps/details?id=com.finalrevise.android");
    }
    openMessenger() {
        if (_angular_common__WEBPACK_IMPORTED_MODULE_6__["isPlatformBrowser"]) {
            window.open("https://m.me/finalrevise?u=http://finalrevise.com/detail/" + this.titleText);
        }
        else {
            window.open('fb-messenger://share?link=' + encodeURIComponent("http://finalrevise.com/detail/" + this.titleText) + '&app_id='
                + encodeURIComponent("358637287840510"));
        }
    }
    setTitleMeta() {
        this.title.setTitle(this.pageData.data.tag);
        this.meta.addTags([
            { name: 'author', content: 'finalrevise.com' },
            { name: 'keywords', content: this.pageData.data.keywords },
            {
                name: 'description', content: this.pageData.data.description
            }
        ]);
    }
    replaceAll(str, replacement) {
        str = str.split('(').join('');
        str = str.split(')').join('');
        str = str.split('.').join('');
        str = str.split('/').join('');
        str = str.split('|').join(':');
        str = str.split(' ').join(replacement);
        str = str.split('---').join(replacement);
        return str.split('--').join(replacement);
    }
    ;
};
DetailComponentDesktop.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__["Meta"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__["Title"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__["DomSanitizer"] },
    { type: _app_header_header_service__WEBPACK_IMPORTED_MODULE_7__["HeaderService"] }
];
DetailComponentDesktop = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-detail-desktop',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./desktop-detail.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/detail/desktop/desktop-detail.component.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./desktop-detail.component.css */ "./src/app/detail/desktop/desktop-detail.component.css")).default]
    })
], DetailComponentDesktop);



/***/ }),

/***/ "./src/app/detail/mobile/mobile-detail.component.css":
/*!***********************************************************!*\
  !*** ./src/app/detail/mobile/mobile-detail.component.css ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".jumbotron{\n    padding:2%;\n    margin-bottom:0rem;\n    background-color:#ecf0f1;\n    border-radius:0.3rem;\n    box-shadow: 0px 5px 11px -2px rgba(0, 0, 0, 0.18), 0px 4px 12px -7px rgba(0, 0, 0, 0.15);\n}\nh1{\n    font-weight: 100;\n}\n.tableHeader{\n    margin-top: 75px;\n    table-layout: fixed;\n    width: 100%;\n    border-collapse: unset;\n}\n.col-sm-3{\n        padding:1rem 0rem;margin-bottom:0rem;background-color:#ecf0f1;border-radius:0.3rem;\n        max-width: 35%;flex:0 0 35%;height: -webkit-fit-content;height: -moz-fit-content;height: fit-content;\n        margin-left: auto;\n    }\n.col-sm-8{\n        margin: inherit;\n    }\n.navbar {\n        position: relative;\n        display: block;\n        flex-wrap: wrap;\n        text-align: center;\n        justify-content: space-between;\n        padding: 5px;\n    }\n.fontSize{\n      font-size: 1.7rem;\n    }\nul, menu, dir {\n    -webkit-margin-before: 0;\n            margin-block-start: 0;\n    -webkit-margin-after: 0;\n            margin-block-end: 0;\n    -webkit-margin-start: 15px;\n            margin-inline-start: 15px;\n    -webkit-margin-end: 0px;\n            margin-inline-end: 0px;\n    -webkit-padding-start: 0px;\n            padding-inline-start: 0px;\n}\n.bg-primary {\n    /* background-color: #dfeaec !important; */\n    animation: colorchange 5s infinite; /* animation-name followed by duration in seconds*/\n         /* you could also use milliseconds (ms) or something like 2.5s */\n      -webkit-animation: colorchange 5s infinite; /* Chrome and Safari */\n}\n#fixed-width-flamingo {  \n    width: 35px; \n    height: 35px;\n}\n.nav-link {\n    display: block;\n    background: #8b0000;\n    margin-bottom: 3%;\n    margin-top: 8%;\n    margin-right: 2%;\n}\n@media (max-width:720px){\n\tbody{\n\t\ttext-align: center;\n\t}\n\t.display-4{\n\t\tfont-size: 30px !important;\n\t}\n}\n@keyframes colorchange\n    {\n      0%   {background: #2c3e50;}\n      33%  {background: rebeccapurple;}\n      66%  {background: rgb(0, 143, 114)}\n      100%  {background: #8b0000;}\n    }\n@-webkit-keyframes colorchange /* Safari and Chrome - necessary duplicate */\n    {\n      0%   {background: #2c3e50;}\n      33%  {background: rebeccapurple;}\n      66%  {background: #008f72}\n      100%  {background: #8b0000;}\n    }\n.text{ \n        white-space: nowrap;  \n        overflow: hidden; \n        text-overflow: ellipsis; \n    }\n.sbuttons {\n      bottom: 15%;\n      position: fixed;\n      margin: 1em;\n      left: 0;\n    }\n.sbutton {\n      display: block;\n      width: 60px;\n      height: 60px;\n      border-radius: 50%;\n      text-align: center;\n      color: transparent;\n      margin: 20px auto 0;\n      box-shadow: 0px 5px 11px -2px rgba(0, 0, 0, 0.18), 0px 4px 12px -7px rgba(0, 0, 0, 0.15);\n      cursor: pointer;\n      transition: all .1s ease-out;\n      position: relative;\n    }\n.sbutton > i {\n      font-size: 38px;\n      line-height: 60px;\n      transition: all .2s ease-in-out;\n      transition-delay: 2s;\n    }\n.sbutton:active,\n    .sbutton:focus,\n    .sbutton:hover {\n      box-shadow: 0 0 4px rgba(0, 0, 0, .14), 0 4px 8px rgba(0, 0, 0, .28);\n    }\n.sbutton:not(:last-child) {\n      width: 60px;\n      height: 60px;\n      margin: 20px auto 0;\n      opacity: 0;\n    }\n.sbutton:not(:last-child) > i {\n      font-size: 25px;\n      line-height: 60px;\n      transition: all .3s ease-in-out;\n    }\n.sbutton:not(:last-child) {\n      opacity: 1;\n      width: 60px;\n      height: 60px;\n      margin: 15px auto 0;\n    }\n.sbutton:nth-last-child(1) {\n      transition-delay: 25ms;\n    }\n.sbutton:not(:last-child):nth-last-child(2) {\n      transition-delay: 20ms;\n    }\n.sbutton:not(:last-child):nth-last-child(3) {\n      transition-delay: 40ms;\n    }\n.sbutton:not(:last-child):nth-last-child(4) {\n      transition-delay: 60ms;\n    }\n.sbutton:not(:last-child):nth-last-child(5) {\n      transition-delay: 80ms;\n    }\n.sbutton:not(:last-child):nth-last-child(6) {\n      transition-delay: 100ms;\n    }\n[tooltip]:before {\n      font-family: 'Roboto';\n      font-weight: 600;\n      border-radius: 2px;\n      background-color: #585858;\n      color: #fff;\n      content: attr(tooltip);\n      font-size: 12px;\n      visibility: hidden;\n      opacity: 0;\n      padding: 5px 7px;\n      margin-left: 10px;\n      position: absolute;\n      left: 100%;\n      bottom: 20%;\n      white-space: nowrap;\n    }\n.sbutton.mainsbutton {\n      background:url(/assets/images/share.jpg);\n    }\n.sbutton.gplus {\n      background:url(/assets/images/gmail60.png);\n    }\n.sbutton.twitt {\n      background:url(/assets/images/mess60.png);\n    }\n.sbutton.fb {\n      background:url(/assets/images/facebook60.png);\n    }\n.sbutton.whatsapp {\n      background:url(/assets/images/whatsapp60.png);\n    }\n@-webkit-keyframes blinker {  \n      50% { opacity: 0; }\n     }\n@keyframes blinker {  \n      50% { opacity: 0; }\n     }\n.main-content {\n      padding-top: 15%;\n      min-height: 80vh;\n      padding-left: 2%;\n      padding-right: 2%;\n    }\n.center {\n      display: block;\n      margin-left: auto;\n      margin-right: auto;\n      width: 85%;\n      margin-top: 2.5%;\n      -webkit-animation: blinker 2s linear infinite;\n              animation: blinker 2s linear infinite;\n    }\n.goBack{\n      color: #008f72;\n      font-size: large;\n      text-shadow: 2px 2px white;\n    }\nhr{\n      margin: 0;  \n    }\n.loader {\n      background: url('/assets/images/loading_small.gif') 50% 50% no-repeat rgb(249,249,249);\n    } \n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZGV0YWlsL21vYmlsZS9tb2JpbGUtZGV0YWlsLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxVQUFVO0lBQ1Ysa0JBQWtCO0lBQ2xCLHdCQUF3QjtJQUN4QixvQkFBb0I7SUFDcEIsd0ZBQXdGO0FBQzVGO0FBQ0E7SUFDSSxnQkFBZ0I7QUFDcEI7QUFDRTtJQUNFLGdCQUFnQjtJQUNoQixtQkFBbUI7SUFDbkIsV0FBVztJQUNYLHNCQUFzQjtBQUMxQjtBQUNJO1FBQ0ksaUJBQWlCLENBQUMsa0JBQWtCLENBQUMsd0JBQXdCLENBQUMsb0JBQW9CO1FBQ2xGLGNBQWMsQ0FBcUMsWUFBWSxDQUFDLDJCQUFtQixDQUFuQix3QkFBbUIsQ0FBbkIsbUJBQW1CO1FBQ25GLGlCQUFpQjtJQUNyQjtBQUNBO1FBQ0ksZUFBZTtJQUNuQjtBQUNBO1FBQ0ksa0JBQWtCO1FBQ2xCLGNBQWM7UUFDZCxlQUFlO1FBQ2Ysa0JBQWtCO1FBQ2xCLDhCQUE4QjtRQUM5QixZQUFZO0lBQ2hCO0FBQ0E7TUFDRSxpQkFBaUI7SUFDbkI7QUFDSjtJQUNJLHdCQUFxQjtZQUFyQixxQkFBcUI7SUFDckIsdUJBQW1CO1lBQW5CLG1CQUFtQjtJQUNuQiwwQkFBeUI7WUFBekIseUJBQXlCO0lBQ3pCLHVCQUFzQjtZQUF0QixzQkFBc0I7SUFDdEIsMEJBQXlCO1lBQXpCLHlCQUF5QjtBQUM3QjtBQUNBO0lBQ0ksMENBQTBDO0lBQzFDLGtDQUFrQyxFQUFFLGtEQUFrRDtTQUNqRixnRUFBZ0U7TUFDbkUsMENBQTBDLEVBQUUsc0JBQXNCO0FBQ3hFO0FBQ0E7SUFDSSxXQUFXO0lBQ1gsWUFBWTtBQUNoQjtBQUVBO0lBQ0ksY0FBYztJQUNkLG1CQUFtQjtJQUNuQixpQkFBaUI7SUFDakIsY0FBYztJQUNkLGdCQUFnQjtBQUNwQjtBQUVBO0NBQ0M7RUFDQyxrQkFBa0I7Q0FDbkI7Q0FDQTtFQUNDLDBCQUEwQjtDQUMzQjtBQUNEO0FBRUE7O01BRU0sTUFBTSxtQkFBbUIsQ0FBQztNQUMxQixNQUFNLHlCQUF5QixDQUFDO01BQ2hDLE1BQU0sNEJBQTRCO01BQ2xDLE9BQU8sbUJBQW1CLENBQUM7SUFDN0I7QUFFQTs7TUFFRSxNQUFNLG1CQUFtQixDQUFDO01BQzFCLE1BQU0seUJBQXlCLENBQUM7TUFDaEMsTUFBTSxtQkFBbUI7TUFDekIsT0FBTyxtQkFBbUIsQ0FBQztJQUM3QjtBQUNBO1FBQ0ksbUJBQW1CO1FBQ25CLGdCQUFnQjtRQUNoQix1QkFBdUI7SUFDM0I7QUFFQTtNQUNFLFdBQVc7TUFDWCxlQUFlO01BQ2YsV0FBVztNQUNYLE9BQU87SUFDVDtBQUNBO01BQ0UsY0FBYztNQUNkLFdBQVc7TUFDWCxZQUFZO01BQ1osa0JBQWtCO01BQ2xCLGtCQUFrQjtNQUNsQixrQkFBa0I7TUFDbEIsbUJBQW1CO01BQ25CLHdGQUF3RjtNQUN4RixlQUFlO01BRWYsNEJBQTRCO01BQzVCLGtCQUFrQjtJQUNwQjtBQUNBO01BQ0UsZUFBZTtNQUNmLGlCQUFpQjtNQUNqQiwrQkFBK0I7TUFDL0Isb0JBQW9CO0lBQ3RCO0FBQ0E7OztNQUdFLG9FQUFvRTtJQUN0RTtBQUNBO01BQ0UsV0FBVztNQUNYLFlBQVk7TUFDWixtQkFBbUI7TUFDbkIsVUFBVTtJQUNaO0FBQ0E7TUFDRSxlQUFlO01BQ2YsaUJBQWlCO01BQ2pCLCtCQUErQjtJQUNqQztBQUNBO01BQ0UsVUFBVTtNQUNWLFdBQVc7TUFDWCxZQUFZO01BQ1osbUJBQW1CO0lBQ3JCO0FBQ0E7TUFFRSxzQkFBc0I7SUFDeEI7QUFDQTtNQUVFLHNCQUFzQjtJQUN4QjtBQUNBO01BRUUsc0JBQXNCO0lBQ3hCO0FBQ0E7TUFFRSxzQkFBc0I7SUFDeEI7QUFDQTtNQUVFLHNCQUFzQjtJQUN4QjtBQUNBO01BRUUsdUJBQXVCO0lBQ3pCO0FBRUE7TUFDRSxxQkFBcUI7TUFDckIsZ0JBQWdCO01BQ2hCLGtCQUFrQjtNQUNsQix5QkFBeUI7TUFDekIsV0FBVztNQUNYLHNCQUFzQjtNQUN0QixlQUFlO01BQ2Ysa0JBQWtCO01BQ2xCLFVBQVU7TUFDVixnQkFBZ0I7TUFDaEIsaUJBQWlCO01BQ2pCLGtCQUFrQjtNQUNsQixVQUFVO01BQ1YsV0FBVztNQUNYLG1CQUFtQjtJQUNyQjtBQUVBO01BQ0Usd0NBQXdDO0lBQzFDO0FBQ0E7TUFDRSwwQ0FBMEM7SUFDNUM7QUFDQTtNQUNFLHlDQUF5QztJQUMzQztBQUNBO01BQ0UsNkNBQTZDO0lBQy9DO0FBQ0E7TUFDRSw2Q0FBNkM7SUFDL0M7QUFFQTtNQUNFLE1BQU0sVUFBVSxFQUFFO0tBQ25CO0FBRkQ7TUFDRSxNQUFNLFVBQVUsRUFBRTtLQUNuQjtBQUNBO01BQ0MsZ0JBQWdCO01BQ2hCLGdCQUFnQjtNQUNoQixnQkFBZ0I7TUFDaEIsaUJBQWlCO0lBQ25CO0FBQ0E7TUFDRSxjQUFjO01BQ2QsaUJBQWlCO01BQ2pCLGtCQUFrQjtNQUNsQixVQUFVO01BQ1YsZ0JBQWdCO01BQ2hCLDZDQUFxQztjQUFyQyxxQ0FBcUM7SUFDdkM7QUFFQTtNQUNFLGNBQWM7TUFDZCxnQkFBZ0I7TUFDaEIsMEJBQTBCO0lBQzVCO0FBQ0E7TUFDRSxTQUFTO0lBQ1g7QUFFQTtNQUNFLHNGQUFzRjtJQUN4RiIsImZpbGUiOiJzcmMvYXBwL2RldGFpbC9tb2JpbGUvbW9iaWxlLWRldGFpbC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmp1bWJvdHJvbntcbiAgICBwYWRkaW5nOjIlO1xuICAgIG1hcmdpbi1ib3R0b206MHJlbTtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiNlY2YwZjE7XG4gICAgYm9yZGVyLXJhZGl1czowLjNyZW07XG4gICAgYm94LXNoYWRvdzogMHB4IDVweCAxMXB4IC0ycHggcmdiYSgwLCAwLCAwLCAwLjE4KSwgMHB4IDRweCAxMnB4IC03cHggcmdiYSgwLCAwLCAwLCAwLjE1KTtcbn1cbmgxe1xuICAgIGZvbnQtd2VpZ2h0OiAxMDA7XG59XG4gIC50YWJsZUhlYWRlcntcbiAgICBtYXJnaW4tdG9wOiA3NXB4O1xuICAgIHRhYmxlLWxheW91dDogZml4ZWQ7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgYm9yZGVyLWNvbGxhcHNlOiB1bnNldDtcbn1cbiAgICAuY29sLXNtLTN7XG4gICAgICAgIHBhZGRpbmc6MXJlbSAwcmVtO21hcmdpbi1ib3R0b206MHJlbTtiYWNrZ3JvdW5kLWNvbG9yOiNlY2YwZjE7Ym9yZGVyLXJhZGl1czowLjNyZW07XG4gICAgICAgIG1heC13aWR0aDogMzUlOy13ZWJraXQtYm94LWZsZXg6MDstbXMtZmxleDowIDAgMzUlO2ZsZXg6MCAwIDM1JTtoZWlnaHQ6IGZpdC1jb250ZW50O1xuICAgICAgICBtYXJnaW4tbGVmdDogYXV0bztcbiAgICB9XG4gICAgLmNvbC1zbS04e1xuICAgICAgICBtYXJnaW46IGluaGVyaXQ7XG4gICAgfVxuICAgIC5uYXZiYXIge1xuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICBmbGV4LXdyYXA6IHdyYXA7XG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgICAgICBwYWRkaW5nOiA1cHg7XG4gICAgfVxuICAgIC5mb250U2l6ZXtcbiAgICAgIGZvbnQtc2l6ZTogMS43cmVtO1xuICAgIH1cbnVsLCBtZW51LCBkaXIge1xuICAgIG1hcmdpbi1ibG9jay1zdGFydDogMDtcbiAgICBtYXJnaW4tYmxvY2stZW5kOiAwO1xuICAgIG1hcmdpbi1pbmxpbmUtc3RhcnQ6IDE1cHg7XG4gICAgbWFyZ2luLWlubGluZS1lbmQ6IDBweDtcbiAgICBwYWRkaW5nLWlubGluZS1zdGFydDogMHB4O1xufVxuLmJnLXByaW1hcnkge1xuICAgIC8qIGJhY2tncm91bmQtY29sb3I6ICNkZmVhZWMgIWltcG9ydGFudDsgKi9cbiAgICBhbmltYXRpb246IGNvbG9yY2hhbmdlIDVzIGluZmluaXRlOyAvKiBhbmltYXRpb24tbmFtZSBmb2xsb3dlZCBieSBkdXJhdGlvbiBpbiBzZWNvbmRzKi9cbiAgICAgICAgIC8qIHlvdSBjb3VsZCBhbHNvIHVzZSBtaWxsaXNlY29uZHMgKG1zKSBvciBzb21ldGhpbmcgbGlrZSAyLjVzICovXG4gICAgICAtd2Via2l0LWFuaW1hdGlvbjogY29sb3JjaGFuZ2UgNXMgaW5maW5pdGU7IC8qIENocm9tZSBhbmQgU2FmYXJpICovXG59XG4jZml4ZWQtd2lkdGgtZmxhbWluZ28geyAgXG4gICAgd2lkdGg6IDM1cHg7IFxuICAgIGhlaWdodDogMzVweDtcbn1cblxuLm5hdi1saW5rIHtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICBiYWNrZ3JvdW5kOiAjOGIwMDAwO1xuICAgIG1hcmdpbi1ib3R0b206IDMlO1xuICAgIG1hcmdpbi10b3A6IDglO1xuICAgIG1hcmdpbi1yaWdodDogMiU7XG59XG5cbkBtZWRpYSAobWF4LXdpZHRoOjcyMHB4KXtcblx0Ym9keXtcblx0XHR0ZXh0LWFsaWduOiBjZW50ZXI7XG5cdH1cblx0LmRpc3BsYXktNHtcblx0XHRmb250LXNpemU6IDMwcHggIWltcG9ydGFudDtcblx0fVxufVxuXG5Aa2V5ZnJhbWVzIGNvbG9yY2hhbmdlXG4gICAge1xuICAgICAgMCUgICB7YmFja2dyb3VuZDogIzJjM2U1MDt9XG4gICAgICAzMyUgIHtiYWNrZ3JvdW5kOiByZWJlY2NhcHVycGxlO31cbiAgICAgIDY2JSAge2JhY2tncm91bmQ6IHJnYigwLCAxNDMsIDExNCl9XG4gICAgICAxMDAlICB7YmFja2dyb3VuZDogIzhiMDAwMDt9XG4gICAgfVxuXG4gICAgQC13ZWJraXQta2V5ZnJhbWVzIGNvbG9yY2hhbmdlIC8qIFNhZmFyaSBhbmQgQ2hyb21lIC0gbmVjZXNzYXJ5IGR1cGxpY2F0ZSAqL1xuICAgIHtcbiAgICAgIDAlICAge2JhY2tncm91bmQ6ICMyYzNlNTA7fVxuICAgICAgMzMlICB7YmFja2dyb3VuZDogcmViZWNjYXB1cnBsZTt9XG4gICAgICA2NiUgIHtiYWNrZ3JvdW5kOiAjMDA4ZjcyfVxuICAgICAgMTAwJSAge2JhY2tncm91bmQ6ICM4YjAwMDA7fVxuICAgIH1cbiAgICAudGV4dHsgXG4gICAgICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7ICBcbiAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjsgXG4gICAgICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzOyBcbiAgICB9XG5cbiAgICAuc2J1dHRvbnMge1xuICAgICAgYm90dG9tOiAxNSU7XG4gICAgICBwb3NpdGlvbjogZml4ZWQ7XG4gICAgICBtYXJnaW46IDFlbTtcbiAgICAgIGxlZnQ6IDA7XG4gICAgfVxuICAgIC5zYnV0dG9uIHtcbiAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgd2lkdGg6IDYwcHg7XG4gICAgICBoZWlnaHQ6IDYwcHg7XG4gICAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICBjb2xvcjogdHJhbnNwYXJlbnQ7XG4gICAgICBtYXJnaW46IDIwcHggYXV0byAwO1xuICAgICAgYm94LXNoYWRvdzogMHB4IDVweCAxMXB4IC0ycHggcmdiYSgwLCAwLCAwLCAwLjE4KSwgMHB4IDRweCAxMnB4IC03cHggcmdiYSgwLCAwLCAwLCAwLjE1KTtcbiAgICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICAgIC13ZWJraXQtdHJhbnNpdGlvbjogYWxsIC4xcyBlYXNlLW91dDtcbiAgICAgIHRyYW5zaXRpb246IGFsbCAuMXMgZWFzZS1vdXQ7XG4gICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgfVxuICAgIC5zYnV0dG9uID4gaSB7XG4gICAgICBmb250LXNpemU6IDM4cHg7XG4gICAgICBsaW5lLWhlaWdodDogNjBweDtcbiAgICAgIHRyYW5zaXRpb246IGFsbCAuMnMgZWFzZS1pbi1vdXQ7XG4gICAgICB0cmFuc2l0aW9uLWRlbGF5OiAycztcbiAgICB9XG4gICAgLnNidXR0b246YWN0aXZlLFxuICAgIC5zYnV0dG9uOmZvY3VzLFxuICAgIC5zYnV0dG9uOmhvdmVyIHtcbiAgICAgIGJveC1zaGFkb3c6IDAgMCA0cHggcmdiYSgwLCAwLCAwLCAuMTQpLCAwIDRweCA4cHggcmdiYSgwLCAwLCAwLCAuMjgpO1xuICAgIH1cbiAgICAuc2J1dHRvbjpub3QoOmxhc3QtY2hpbGQpIHtcbiAgICAgIHdpZHRoOiA2MHB4O1xuICAgICAgaGVpZ2h0OiA2MHB4O1xuICAgICAgbWFyZ2luOiAyMHB4IGF1dG8gMDtcbiAgICAgIG9wYWNpdHk6IDA7XG4gICAgfVxuICAgIC5zYnV0dG9uOm5vdCg6bGFzdC1jaGlsZCkgPiBpIHtcbiAgICAgIGZvbnQtc2l6ZTogMjVweDtcbiAgICAgIGxpbmUtaGVpZ2h0OiA2MHB4O1xuICAgICAgdHJhbnNpdGlvbjogYWxsIC4zcyBlYXNlLWluLW91dDtcbiAgICB9XG4gICAgLnNidXR0b246bm90KDpsYXN0LWNoaWxkKSB7XG4gICAgICBvcGFjaXR5OiAxO1xuICAgICAgd2lkdGg6IDYwcHg7XG4gICAgICBoZWlnaHQ6IDYwcHg7XG4gICAgICBtYXJnaW46IDE1cHggYXV0byAwO1xuICAgIH1cbiAgICAuc2J1dHRvbjpudGgtbGFzdC1jaGlsZCgxKSB7XG4gICAgICAtd2Via2l0LXRyYW5zaXRpb24tZGVsYXk6IDI1bXM7XG4gICAgICB0cmFuc2l0aW9uLWRlbGF5OiAyNW1zO1xuICAgIH1cbiAgICAuc2J1dHRvbjpub3QoOmxhc3QtY2hpbGQpOm50aC1sYXN0LWNoaWxkKDIpIHtcbiAgICAgIC13ZWJraXQtdHJhbnNpdGlvbi1kZWxheTogMjBtcztcbiAgICAgIHRyYW5zaXRpb24tZGVsYXk6IDIwbXM7XG4gICAgfVxuICAgIC5zYnV0dG9uOm5vdCg6bGFzdC1jaGlsZCk6bnRoLWxhc3QtY2hpbGQoMykge1xuICAgICAgLXdlYmtpdC10cmFuc2l0aW9uLWRlbGF5OiA0MG1zO1xuICAgICAgdHJhbnNpdGlvbi1kZWxheTogNDBtcztcbiAgICB9XG4gICAgLnNidXR0b246bm90KDpsYXN0LWNoaWxkKTpudGgtbGFzdC1jaGlsZCg0KSB7XG4gICAgICAtd2Via2l0LXRyYW5zaXRpb24tZGVsYXk6IDYwbXM7XG4gICAgICB0cmFuc2l0aW9uLWRlbGF5OiA2MG1zO1xuICAgIH1cbiAgICAuc2J1dHRvbjpub3QoOmxhc3QtY2hpbGQpOm50aC1sYXN0LWNoaWxkKDUpIHtcbiAgICAgIC13ZWJraXQtdHJhbnNpdGlvbi1kZWxheTogODBtcztcbiAgICAgIHRyYW5zaXRpb24tZGVsYXk6IDgwbXM7XG4gICAgfVxuICAgIC5zYnV0dG9uOm5vdCg6bGFzdC1jaGlsZCk6bnRoLWxhc3QtY2hpbGQoNikge1xuICAgICAgLXdlYmtpdC10cmFuc2l0aW9uLWRlbGF5OiAxMDBtcztcbiAgICAgIHRyYW5zaXRpb24tZGVsYXk6IDEwMG1zO1xuICAgIH1cbiAgICAgXG4gICAgW3Rvb2x0aXBdOmJlZm9yZSB7XG4gICAgICBmb250LWZhbWlseTogJ1JvYm90byc7XG4gICAgICBmb250LXdlaWdodDogNjAwO1xuICAgICAgYm9yZGVyLXJhZGl1czogMnB4O1xuICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzU4NTg1ODtcbiAgICAgIGNvbG9yOiAjZmZmO1xuICAgICAgY29udGVudDogYXR0cih0b29sdGlwKTtcbiAgICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICAgIHZpc2liaWxpdHk6IGhpZGRlbjtcbiAgICAgIG9wYWNpdHk6IDA7XG4gICAgICBwYWRkaW5nOiA1cHggN3B4O1xuICAgICAgbWFyZ2luLWxlZnQ6IDEwcHg7XG4gICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICBsZWZ0OiAxMDAlO1xuICAgICAgYm90dG9tOiAyMCU7XG4gICAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICAgIH1cbiAgICAgXG4gICAgLnNidXR0b24ubWFpbnNidXR0b24ge1xuICAgICAgYmFja2dyb3VuZDp1cmwoL2Fzc2V0cy9pbWFnZXMvc2hhcmUuanBnKTtcbiAgICB9XG4gICAgLnNidXR0b24uZ3BsdXMge1xuICAgICAgYmFja2dyb3VuZDp1cmwoL2Fzc2V0cy9pbWFnZXMvZ21haWw2MC5wbmcpO1xuICAgIH1cbiAgICAuc2J1dHRvbi50d2l0dCB7XG4gICAgICBiYWNrZ3JvdW5kOnVybCgvYXNzZXRzL2ltYWdlcy9tZXNzNjAucG5nKTtcbiAgICB9XG4gICAgLnNidXR0b24uZmIge1xuICAgICAgYmFja2dyb3VuZDp1cmwoL2Fzc2V0cy9pbWFnZXMvZmFjZWJvb2s2MC5wbmcpO1xuICAgIH1cbiAgICAuc2J1dHRvbi53aGF0c2FwcCB7XG4gICAgICBiYWNrZ3JvdW5kOnVybCgvYXNzZXRzL2ltYWdlcy93aGF0c2FwcDYwLnBuZyk7XG4gICAgfVxuICBcbiAgICBAa2V5ZnJhbWVzIGJsaW5rZXIgeyAgXG4gICAgICA1MCUgeyBvcGFjaXR5OiAwOyB9XG4gICAgIH1cbiAgICAgLm1haW4tY29udGVudCB7XG4gICAgICBwYWRkaW5nLXRvcDogMTUlO1xuICAgICAgbWluLWhlaWdodDogODB2aDtcbiAgICAgIHBhZGRpbmctbGVmdDogMiU7XG4gICAgICBwYWRkaW5nLXJpZ2h0OiAyJTtcbiAgICB9XG4gICAgLmNlbnRlciB7XG4gICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgIG1hcmdpbi1sZWZ0OiBhdXRvO1xuICAgICAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xuICAgICAgd2lkdGg6IDg1JTtcbiAgICAgIG1hcmdpbi10b3A6IDIuNSU7XG4gICAgICBhbmltYXRpb246IGJsaW5rZXIgMnMgbGluZWFyIGluZmluaXRlO1xuICAgIH1cblxuICAgIC5nb0JhY2t7XG4gICAgICBjb2xvcjogIzAwOGY3MjtcbiAgICAgIGZvbnQtc2l6ZTogbGFyZ2U7XG4gICAgICB0ZXh0LXNoYWRvdzogMnB4IDJweCB3aGl0ZTtcbiAgICB9XG4gICAgaHJ7XG4gICAgICBtYXJnaW46IDA7ICBcbiAgICB9XG5cbiAgICAubG9hZGVyIHtcbiAgICAgIGJhY2tncm91bmQ6IHVybCgnL2Fzc2V0cy9pbWFnZXMvbG9hZGluZ19zbWFsbC5naWYnKSA1MCUgNTAlIG5vLXJlcGVhdCByZ2IoMjQ5LDI0OSwyNDkpO1xuICAgIH0gIl19 */");

/***/ }),

/***/ "./src/app/detail/mobile/mobile-detail.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/detail/mobile/mobile-detail.component.ts ***!
  \**********************************************************/
/*! exports provided: DetailComponentMobile, DialogContentExampleDialog */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetailComponentMobile", function() { return DetailComponentMobile; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DialogContentExampleDialog", function() { return DialogContentExampleDialog; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var rxjs_Rx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/Rx */ "./node_modules/rxjs-compat/_esm2015/Rx.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/dialog.js");
/* harmony import */ var _data_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../data.service */ "./src/app/detail/data.service.ts");
/* harmony import */ var rxjs_observable_TimerObservable__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rxjs/observable/TimerObservable */ "./node_modules/rxjs-compat/_esm2015/observable/TimerObservable.js");
/* harmony import */ var _app_header_header_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../app-header/header.service */ "./src/app/app-header/header.service.ts");
/* harmony import */ var ngx_device_detector__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ngx-device-detector */ "./node_modules/ngx-device-detector/__ivy_ngcc__/fesm2015/ngx-device-detector.js");



;










let DetailComponentMobile = class DetailComponentMobile {
    constructor(http, activatedRoute, router, meta, title, sanitizer, dialog, deviceService, dataService, headerService) {
        this.http = http;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.sanitizer = sanitizer;
        this.dialog = dialog;
        this.deviceService = deviceService;
        this.dataService = dataService;
        this.headerService = headerService;
        this.apiUrl = 'https://api.finalrevise.com/finalrevise/v1/content/';
        this.pageData = {};
        this.keys = [];
        this.nav = [];
        this.navIds = [];
        this.navCount = [];
        this.tabUrl = 'https://api.finalrevise.com/finalrevise/v1/relatedlinks/';
        this.whatsAppUrl = 'https://api.finalrevise.com/finalrevise/v1/whatsApp/';
        this.deviceInfo = {};
        this.title = title;
        this.meta = meta;
    }
    ngOnInit() {
        // subscribe to router event
        this.activatedRoute.params.subscribe((params) => {
            this.titleText = params['title'];
        });
        this.apiUrl = this.apiUrl + this.titleText;
        this.tabUrl = this.tabUrl + this.titleText;
        this.whatsAppUrl = this.whatsAppUrl + this.titleText.split('/').join('-');
        this.getPageData();
        this.getTabData();
        this.getWhatsAppUrl();
    }
    shareClicked() {
        if (this.isClickedShare) {
            this.isClickedShare = false;
        }
        else {
            this.isClickedShare = true;
        }
    }
    getData() {
        return this.http.get(this.apiUrl);
    }
    getTData() {
        return this.http.get(this.tabUrl);
    }
    getTabData() {
        this.getTData().subscribe(data => {
            this.tags = data.data.related;
            this.tabs = data;
            this.headerService.setRelatedTopics(this.tabs.data.tabs);
        });
    }
    getWhatsAppUrl() {
        this.getWhatsAppData().subscribe(data => {
            this.groupUrl = data.data.whatsAppUrl;
        });
    }
    getWhatsAppData() {
        return this.http.get(this.whatsAppUrl);
    }
    getPageData() {
        this.getData().subscribe(data => {
            this.pageData = data;
            this.linkedPageItems = this.pageData.data.linkedPageItems;
            this.aboutExamContent = this.sanitizer.bypassSecurityTrustHtml(this.pageData.data.pageIntro);
            this.pageTitle = this.pageData.data.pageTitle;
            // For DU-JAT only
            if (this.pageTitle.includes('DU-JAT')) {
                this.dujat = true;
            }
            this.setTitleMeta();
        });
    }
    openNextPage(link, paperName) {
        this.deviceInfo = this.deviceService.getDeviceInfo();
        paperName = this.replaceAllString(paperName, ",", " ");
        paperName = this.replaceAllString(paperName, "(", " ");
        paperName = this.replaceAllString(paperName, ")", " ");
        paperName = this.replaceAllString(paperName, "?", " ");
        this.dataService.count = this.dataService.getCount();
        this.dataService.addCount();
        // console.log(this.dataService.count);
        if (this.dataService.count % 3 != 0 && this.dataService.count != 0) {
            this.openPdfViewer(link, paperName);
        }
        else if ("ANDROID" == (this.deviceInfo.os).toUpperCase()) {
            this.openDialog(link, this.titleText, paperName);
        }
        else {
            this.openPdfViewer(link, paperName);
        }
    }
    openPdfViewer(link, paperName) {
        if (link.includes('open')) {
            this.router.navigateByUrl('/pdf-viewer/' + this.titleText + "/" + paperName + "/" + link.substring(link.indexOf("?id=") + 4));
        }
        else if (link.includes("/view")) {
            this.router.navigateByUrl('/pdf-viewer/' + this.titleText + "/" + paperName + "/" + link.replace("/view", ").substring(link.indexOf("/d/") + 3));
        }
        else {
            window.open(link);
        }
    }
    replaceAllString(str, find, replace) {
        return str.split(find).join(replace);
    }
    openTag(tab) {
        if (tab.isChild == 1) {
            this.router.navigateByUrl('/detail-list/' + tab.linkTitle);
        }
        else {
            if (tab.linkTitle.includes('book')) {
                this.router.navigateByUrl('/study-met/' + tab.linkTitle);
            }
            else {
                this.router.navigateByUrl('/detail/' + tab.linkTitle);
            }
        }
    }
    replaceAllSocial(str) {
        var find = '-';
        var re = new RegExp(find, 'g');
        return str.replace(re, ' ');
    }
    openFacebook() {
        window.open("https://www.facebook.com/sharer/sharer.php?u=http://finalrevise.com/detail/" + this.titleText);
    }
    openWhatsApp() {
        window.open("https://api.whatsapp.com/send?text=*Download%20" + this.titleText + "*%0a%0a" + "http://finalrevise.com/detail/" + this.titleText +
            "%0a%0a*Download%20FinalRevise%20App%20:%20https://play.google.com/store/apps/details?id%3Dcom.finalrevise.android*");
    }
    openMail() {
        window.open("mailto:?subject=FinalRevise%20Study%20Material%20for%20" + this.nav[this.navCount - 1] +
            "&body=Download%20" + this.replaceAllSocial(this.titleText) + "%0a%0a" + "http://finalrevise.com/detail/" + this.titleText +
            "%0a%0a Download FinalRevise App : https://play.google.com/store/apps/details?id=com.finalrevise.android");
    }
    openMessenger() {
        if (_angular_common__WEBPACK_IMPORTED_MODULE_6__["isPlatformBrowser"]) {
            window.open("https://m.me/finalrevise?u=http://finalrevise.com/detail/" + this.titleText);
        }
        else {
            window.open('fb-messenger://share?link=' + encodeURIComponent("http://finalrevise.com/detail/" + this.titleText) + '&app_id='
                + encodeURIComponent("358637287840510"));
        }
    }
    openTabPage(pageId, isChild, tag, cat) {
        if (pageId != null) {
            if (cat == 10 && !isChild) {
                window.open('/study-met/' + this.replaceAll(tag, '-') + '/' + pageId, "_self");
            }
            else if (pageId == 1573689600004 || pageId == 10 || pageId == 13 || pageId == 1561798213495
                || pageId == 14 || pageId == 1561798213494 || pageId == 1561658659435) {
                window.open('/main-cat-list/' + this.replaceAll(tag, '-') + '/' + pageId, "_self");
            }
            else if (cat == 11) {
                window.open('/image-text-list/' + this.replaceAll(tag, '-') + '/' + pageId, "_self");
            }
            else if (isChild && pageId != null) {
                window.open('/detail-list/' + this.replaceAll(tag, '-') + '/' + pageId, "_self");
            }
            else {
                this.router.navigateByUrl('/detail/' + this.replaceAll(tag, '-') + '/' + pageId);
            }
        }
    }
    setTitleMeta() {
        this.title.setTitle(this.pageData.data.tag);
        this.meta.addTags([
            { name: 'author', content: 'finalrevise.com' },
            { name: 'keywords', content: this.pageData.data.keywords },
            {
                name: 'description', content: this.pageData.data.description
            }
        ]);
    }
    replaceAll(str, replacement) {
        str = str.split('(').join('');
        str = str.split(')').join('');
        str = str.split('.').join('');
        str = str.split('/').join('');
        str = str.split('|').join(':');
        str = str.split(' ').join(replacement);
        str = str.split('---').join(replacement);
        return str.split('--').join(replacement);
    }
    ;
    openDialog(link, titleText, paperName) {
        const dialogRef = this.dialog.open(DialogContentExampleDialog, {
            data: {
                link: link,
                titleText: titleText,
                paperName: paperName
            }
        });
    }
};
DetailComponentMobile.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__["Meta"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__["Title"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__["DomSanitizer"] },
    { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_7__["MatDialog"] },
    { type: ngx_device_detector__WEBPACK_IMPORTED_MODULE_11__["DeviceDetectorService"] },
    { type: _data_service__WEBPACK_IMPORTED_MODULE_8__["CounterService"] },
    { type: _app_header_header_service__WEBPACK_IMPORTED_MODULE_10__["HeaderService"] }
];
DetailComponentMobile = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-detail-mobile',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./mobile-detail.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/detail/mobile/mobile-detail.component.html")).default,
        providers: [_data_service__WEBPACK_IMPORTED_MODULE_8__["CounterService"]],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./mobile-detail.component.css */ "./src/app/detail/mobile/mobile-detail.component.css")).default]
    })
], DetailComponentMobile);

let DialogContentExampleDialog = class DialogContentExampleDialog {
    constructor(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.isValid = false;
        let timer = rxjs_observable_TimerObservable__WEBPACK_IMPORTED_MODULE_9__["TimerObservable"].create(0, 1000);
        this.subscription = timer.subscribe(t => {
            this.tick = t;
            if (this.tick > 15) {
                this.isValid = true;
            }
        });
    }
    ngOnInit() {
    }
    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
    openDialog() {
        this.dialogRef.close();
        this.openPdfViewer(this.data.link, this.data.titleText, this.data.paperName);
        // window.open(this.data.link)
    }
    openPdfViewer(link, titleText, paperName) {
        if (link.includes('open')) {
            window.open('/pdf-viewer/' + titleText + "/" + paperName + "/" + link.substring(link.indexOf("?id=") + 4), "_self");
        }
        else if (link.includes("/view")) {
            window.open('/pdf-viewer/' + titleText + "/" + paperName + "/" + link.replace("/view", ").substring(link.indexOf("/d/") + 3), "_self");
        }
        else {
            window.open(link);
        }
    }
    openFacebook() {
        window.open("https://www.facebook.com/sharer/sharer.php?u=http://finalrevise.com/detail/" + this.data.titleText);
    }
    openWhatsApp() {
        window.open("https://api.whatsapp.com/send?text=*Download%20" + this.data.titleText + "*%0a%0a" + "http://finalrevise.com/detail/" + this.data.titleText +
            "%0a%0a*Download%20FinalRevise%20App%20:%20https://play.google.com/store/apps/details?id%3Dcom.finalrevise.android*");
    }
    openMail() {
        window.open("mailto:?subject=FinalRevise%20Study%20material%20related%20to%20" + this.data.paperName +
            "&body=Download%20all%20Study%20material%20related%20to%20" + this.data.paperName + "%20:%20" +
            this.data.paperName + "%20" + "http://finalrevise.com/detail/" + this.data.titleText);
        this.openDialog();
    }
    openMessenger() {
        window.open('fb-messenger://share?link=' + encodeURIComponent("http://finalrevise.com/detail/" + this.data.titleText) + '&app_id='
            + encodeURIComponent("358637287840510"));
        this.openDialog();
    }
};
DialogContentExampleDialog.ctorParameters = () => [
    { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_7__["MatDialogRef"] },
    { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: [_angular_material_dialog__WEBPACK_IMPORTED_MODULE_7__["MAT_DIALOG_DATA"],] }] }
];
DialogContentExampleDialog = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'dialog-content-example-dialog',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./dialog-app-install.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/detail/mobile/dialog-app-install.html")).default,
    }),
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__param"])(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_material_dialog__WEBPACK_IMPORTED_MODULE_7__["MAT_DIALOG_DATA"]))
], DialogContentExampleDialog);



/***/ }),

/***/ "./src/app/footer/fileUpload.service.ts":
/*!**********************************************!*\
  !*** ./src/app/footer/fileUpload.service.ts ***!
  \**********************************************/
/*! exports provided: UploadFileService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UploadFileService", function() { return UploadFileService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");



let UploadFileService = class UploadFileService {
    constructor(http) {
        this.http = http;
    }
    pushFileToStorage(file) {
        const formdata = new FormData();
        formdata.append('file', file);
        const req = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpRequest"]('POST', 'http://finalrevise-1206660368.us-east-2.elb.amazonaws.com/finalrevise/file/uploadFile', formdata, {
            reportProgress: true,
            responseType: 'text'
        });
        return this.http.request(req);
    }
};
UploadFileService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
UploadFileService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
], UploadFileService);



/***/ }),

/***/ "./src/app/footer/footer.component.scss":
/*!**********************************************!*\
  !*** ./src/app/footer/footer.component.scss ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ul {\n  margin-bottom: 0rem;\n}\n\n.jumbotron {\n  padding: 0rem 0rem;\n  margin-bottom: 0rem;\n  background-color: #ecf0f1;\n  border-radius: 0.3rem;\n  text-align: right;\n}\n\n.navbar {\n  position: relative;\n  display: block;\n  flex-wrap: wrap;\n  justify-content: space-between;\n  text-align: left;\n}\n\n.bg-primary {\n  background-color: #ecf0f1 !important;\n  padding-bottom: 1%;\n  padding-top: 1%;\n  box-shadow: 0 4px 8px #2c3e50, 0 6px 20px 0 #2c3e50;\n}\n\n.align-center {\n  text-align: center;\n  background-color: #2c3e50 !important;\n  padding: 0.5%;\n}\n\n.list-item {\n  color: #ecf0f1;\n}\n\na {\n  color: #2c3e50;\n  font-weight: 500;\n}\n\n.list {\n  -webkit-padding-start: 5%;\n          padding-inline-start: 5%;\n}\n\n@media (max-width: 700px) {\n  .play-img {\n    width: 70%;\n    height: 55%;\n    padding-top: 5%;\n  }\n  .list-inline-item {\n    display: table-cell;\n  }\n  .inline1 {\n    width: 30%;\n  }\n  .inline2 {\n    width: 60%;\n  }\n}\n\n@media (min-width: 700px) {\n  .play-img {\n    width: 28%;\n    height: 12%;\n  }\n  .list-inline-item {\n    display: table-cell;\n  }\n  .inline1 {\n    width: 30%;\n  }\n  .inline2 {\n    width: 30%;\n    padding-right: 2%;\n  }\n}\n\ntable.d {\n  width: 100%;\n}\n\n.image-upload > input {\n  display: none;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Wb2x1bWVzL0ZpbmFsUmV2aXNlL2ZpbmFscmV2aXNlX3dlYnNpdGUvc3JjL2FwcC9mb290ZXIvZm9vdGVyLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9mb290ZXIvZm9vdGVyLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksbUJBQW1CO0FDQ3ZCOztBRENBO0VBQ0ksa0JBQWlCO0VBQUMsbUJBQWtCO0VBQ3BDLHlCQUF3QjtFQUN4QixxQkFBb0I7RUFDcEIsaUJBQWlCO0FDR3JCOztBRERBO0VBQ0ksa0JBQWtCO0VBQ2xCLGNBQWM7RUFDZCxlQUFlO0VBQ2YsOEJBQThCO0VBQzlCLGdCQUFnQjtBQ0lwQjs7QURGQTtFQUNJLG9DQUFtQztFQUNuQyxrQkFBa0I7RUFDbEIsZUFBZTtFQUNmLG1EQUFtRDtBQ0t2RDs7QURIQTtFQUNJLGtCQUFrQjtFQUNsQixvQ0FBbUM7RUFDbkMsYUFBYTtBQ01qQjs7QURKQTtFQUNJLGNBQWM7QUNPbEI7O0FETEE7RUFDQyxjQUFjO0VBQ2QsZ0JBQWdCO0FDUWpCOztBRE5BO0VBQ0kseUJBQXdCO1VBQXhCLHdCQUF3QjtBQ1M1Qjs7QURQQTtFQUVJO0lBQ0ksVUFBVTtJQUNWLFdBQVc7SUFDWCxlQUFlO0VDU3JCO0VEUEU7SUFDSSxtQkFBbUI7RUNTekI7RURQRTtJQUNJLFVBQVU7RUNTaEI7RURQRTtJQUNJLFVBQVU7RUNTaEI7QUFDRjs7QURQQTtFQUVJO0lBQ0ksVUFBVTtJQUNWLFdBQVc7RUNTakI7RURQQTtJQUNFLG1CQUFtQjtFQ1NyQjtFRFBGO0lBQ0ksVUFBVTtFQ1NaO0VEUEY7SUFDSSxVQUFVO0lBQ1YsaUJBQWlCO0VDU25CO0FBQ0Y7O0FEUEE7RUFDSSxXQUFXO0FDVWY7O0FEUkU7RUFDRSxhQUFhO0FDV2pCIiwiZmlsZSI6InNyYy9hcHAvZm9vdGVyL2Zvb3Rlci5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbInVse1xuICAgIG1hcmdpbi1ib3R0b206IDByZW07XG59XG4uanVtYm90cm9ue1xuICAgIHBhZGRpbmc6MHJlbSAwcmVtO21hcmdpbi1ib3R0b206MHJlbTtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiNlY2YwZjE7XG4gICAgYm9yZGVyLXJhZGl1czowLjNyZW07XG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XG59XG4ubmF2YmFyIHtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgZmxleC13cmFwOiB3cmFwO1xuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xufVxuLmJnLXByaW1hcnl7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2VjZjBmMSFpbXBvcnRhbnQ7XG4gICAgcGFkZGluZy1ib3R0b206IDElO1xuICAgIHBhZGRpbmctdG9wOiAxJTtcbiAgICBib3gtc2hhZG93OiAwIDRweCA4cHggIzJjM2U1MCwgMCA2cHggMjBweCAwICMyYzNlNTA7XG59XG4uYWxpZ24tY2VudGVye1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMmMzZTUwIWltcG9ydGFudDs7XG4gICAgcGFkZGluZzogMC41JTtcbn1cbi5saXN0LWl0ZW17XG4gICAgY29sb3I6ICNlY2YwZjE7XG59XG5he1xuIGNvbG9yOiAjMmMzZTUwO1xuIGZvbnQtd2VpZ2h0OiA1MDA7XG59XG4ubGlzdHtcbiAgICBwYWRkaW5nLWlubGluZS1zdGFydDogNSU7XG59XG5AbWVkaWEgKG1heC13aWR0aDogNzAwcHgpIFxue1xuICAgIC5wbGF5LWltZ3tcbiAgICAgICAgd2lkdGg6IDcwJTtcbiAgICAgICAgaGVpZ2h0OiA1NSU7XG4gICAgICAgIHBhZGRpbmctdG9wOiA1JTtcbiAgICAgIH1cbiAgICAubGlzdC1pbmxpbmUtaXRlbXtcbiAgICAgICAgZGlzcGxheTogdGFibGUtY2VsbDtcbiAgICB9XG4gICAgLmlubGluZTF7XG4gICAgICAgIHdpZHRoOiAzMCU7XG4gICAgfVxuICAgIC5pbmxpbmUye1xuICAgICAgICB3aWR0aDogNjAlO1xuICAgIH1cbn1cbkBtZWRpYSAobWluLXdpZHRoOiA3MDBweCkgXG57XG4gICAgLnBsYXktaW1ne1xuICAgICAgICB3aWR0aDogMjglO1xuICAgICAgICBoZWlnaHQ6IDEyJTtcbiAgICAgIH1cbiAgLmxpc3QtaW5saW5lLWl0ZW17XG4gICAgZGlzcGxheTogdGFibGUtY2VsbDtcbn1cbi5pbmxpbmUxe1xuICAgIHdpZHRoOiAzMCU7XG59XG4uaW5saW5lMntcbiAgICB3aWR0aDogMzAlO1xuICAgIHBhZGRpbmctcmlnaHQ6IDIlO1xufVxufVxudGFibGUuZCB7XG4gICAgd2lkdGg6IDEwMCU7ICBcbiAgfVxuICAuaW1hZ2UtdXBsb2FkPmlucHV0IHtcbiAgICBkaXNwbGF5OiBub25lO1xuICB9IiwidWwge1xuICBtYXJnaW4tYm90dG9tOiAwcmVtO1xufVxuXG4uanVtYm90cm9uIHtcbiAgcGFkZGluZzogMHJlbSAwcmVtO1xuICBtYXJnaW4tYm90dG9tOiAwcmVtO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWNmMGYxO1xuICBib3JkZXItcmFkaXVzOiAwLjNyZW07XG4gIHRleHQtYWxpZ246IHJpZ2h0O1xufVxuXG4ubmF2YmFyIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBkaXNwbGF5OiBibG9jaztcbiAgZmxleC13cmFwOiB3cmFwO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gIHRleHQtYWxpZ246IGxlZnQ7XG59XG5cbi5iZy1wcmltYXJ5IHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2VjZjBmMSAhaW1wb3J0YW50O1xuICBwYWRkaW5nLWJvdHRvbTogMSU7XG4gIHBhZGRpbmctdG9wOiAxJTtcbiAgYm94LXNoYWRvdzogMCA0cHggOHB4ICMyYzNlNTAsIDAgNnB4IDIwcHggMCAjMmMzZTUwO1xufVxuXG4uYWxpZ24tY2VudGVyIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMmMzZTUwICFpbXBvcnRhbnQ7XG4gIHBhZGRpbmc6IDAuNSU7XG59XG5cbi5saXN0LWl0ZW0ge1xuICBjb2xvcjogI2VjZjBmMTtcbn1cblxuYSB7XG4gIGNvbG9yOiAjMmMzZTUwO1xuICBmb250LXdlaWdodDogNTAwO1xufVxuXG4ubGlzdCB7XG4gIHBhZGRpbmctaW5saW5lLXN0YXJ0OiA1JTtcbn1cblxuQG1lZGlhIChtYXgtd2lkdGg6IDcwMHB4KSB7XG4gIC5wbGF5LWltZyB7XG4gICAgd2lkdGg6IDcwJTtcbiAgICBoZWlnaHQ6IDU1JTtcbiAgICBwYWRkaW5nLXRvcDogNSU7XG4gIH1cbiAgLmxpc3QtaW5saW5lLWl0ZW0ge1xuICAgIGRpc3BsYXk6IHRhYmxlLWNlbGw7XG4gIH1cbiAgLmlubGluZTEge1xuICAgIHdpZHRoOiAzMCU7XG4gIH1cbiAgLmlubGluZTIge1xuICAgIHdpZHRoOiA2MCU7XG4gIH1cbn1cblxuQG1lZGlhIChtaW4td2lkdGg6IDcwMHB4KSB7XG4gIC5wbGF5LWltZyB7XG4gICAgd2lkdGg6IDI4JTtcbiAgICBoZWlnaHQ6IDEyJTtcbiAgfVxuICAubGlzdC1pbmxpbmUtaXRlbSB7XG4gICAgZGlzcGxheTogdGFibGUtY2VsbDtcbiAgfVxuICAuaW5saW5lMSB7XG4gICAgd2lkdGg6IDMwJTtcbiAgfVxuICAuaW5saW5lMiB7XG4gICAgd2lkdGg6IDMwJTtcbiAgICBwYWRkaW5nLXJpZ2h0OiAyJTtcbiAgfVxufVxuXG50YWJsZS5kIHtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbi5pbWFnZS11cGxvYWQgPiBpbnB1dCB7XG4gIGRpc3BsYXk6IG5vbmU7XG59XG4iXX0= */");

/***/ }),

/***/ "./src/app/footer/footer.component.ts":
/*!********************************************!*\
  !*** ./src/app/footer/footer.component.ts ***!
  \********************************************/
/*! exports provided: FooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterComponent", function() { return FooterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _fileUpload_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./fileUpload.service */ "./src/app/footer/fileUpload.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");




let FooterComponent = class FooterComponent {
    constructor(uploadService) {
        this.uploadService = uploadService;
        this.title = 'Final Revise';
        this.uploadMsg = '';
    }
    // ----------
    selectFile(event) {
        this.selectedFiles = event.target.files;
    }
    upload() {
        var filesAmount = this.selectedFiles.length;
        for (let i = 0; i < filesAmount; i++) {
            this.currentFileUpload = this.selectedFiles.item(i);
            this.uploadService.pushFileToStorage(this.currentFileUpload).subscribe(event => {
                if (event instanceof _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpResponse"]) {
                    this.uploadMsg = this.uploadMsg + event.body;
                }
            });
        }
        this.uploadMsg = '';
        this.selectedFiles = undefined;
    }
};
FooterComponent.ctorParameters = () => [
    { type: _fileUpload_service__WEBPACK_IMPORTED_MODULE_2__["UploadFileService"] }
];
FooterComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-footer',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./footer.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/footer/footer.component.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./footer.component.scss */ "./src/app/footer/footer.component.scss")).default]
    })
], FooterComponent);



/***/ }),

/***/ "./src/app/home/desktop/home.component.desktop.scss":
/*!**********************************************************!*\
  !*** ./src/app/home/desktop/home.component.desktop.scss ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("/* Safari */\n@-webkit-keyframes spin {\n  0% {\n    -webkit-transform: rotate(0deg);\n  }\n  100% {\n    -webkit-transform: rotate(360deg);\n  }\n}\n@keyframes spin {\n  0% {\n    transform: rotate(0deg);\n  }\n  100% {\n    transform: rotate(360deg);\n  }\n}\n.jumbotron {\n  padding: .2rem 1rem;\n  text-align: center;\n  box-shadow: 0px 5px 11px -2px rgba(0, 0, 0, 0.18), 0px 4px 12px -7px rgba(0, 0, 0, 0.15);\n}\nimg {\n  width: 100%;\n  height: 75%;\n}\n.hr-sect {\n  display: flex;\n  flex-basis: 100%;\n  align-items: center;\n  color: #8b0000;\n  margin: 8px 0px;\n  text-shadow: 2px 2px 4px rgba(0, 0, 0, 0.18);\n}\n.hr-sect::before,\n.hr-sect::after {\n  content: \"\";\n  flex-grow: 1;\n  background: rgba(133, 133, 133, 0.35);\n  text-shadow: 2px 2px 4px  #8b0000;\n  height: 1px;\n  font-size: 0px;\n  line-height: 0px;\n  margin: 0px 8px;\n}\n.col-lg-2 {\n  flex: 0 0 20%;\n  max-width: 20%;\n}\n.col-lg-3 {\n  position: relative;\n  width: 33%;\n  min-height: 1px;\n  padding-right: 0;\n  padding-left: 0;\n}\n.container {\n  padding-bottom: 2%;\n  padding-left: 0%;\n  padding-right: 0%;\n}\n.my-auto {\n  text-align: -webkit-center;\n}\n.carousel-indicators li {\n  background-color: #8b0000;\n  margin-bottom: 1%;\n}\n.h5-style {\n  text-align: center;\n  margin-top: 10px;\n  color: #8b0000;\n  font-size: 140%;\n  font-weight: 500;\n  text-shadow: 2px 2px 4px rgba(0, 0, 0, 0.18);\n}\nh1 {\n  font-size: 300%;\n}\nh5 {\n  color: #2c3e50;\n}\n.parallax {\n  /* The image used */\n  background-size: cover;\n  /* Set a specific height */\n  min-height: 80vh;\n  /* Create the parallax scrolling effect */\n  background-attachment: fixed;\n  background-position: center;\n  background-repeat: no-repeat;\n  background-size: cover;\n  padding-top: 6.8%;\n  padding-left: 2%;\n  padding-right: 2%;\n  min-height: 80vh;\n}\n.mat-card-content {\n  margin-top: 3%;\n}\n.mat-card {\n  box-shadow: 3px 3px 3px #868686;\n  padding-left: .10%;\n  padding-right: .10%;\n  padding-top: .10%;\n  padding-bottom: 3%;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Wb2x1bWVzL0ZpbmFsUmV2aXNlL2ZpbmFscmV2aXNlX3dlYnNpdGUvc3JjL2FwcC9ob21lL2Rlc2t0b3AvaG9tZS5jb21wb25lbnQuZGVza3RvcC5zY3NzIiwic3JjL2FwcC9ob21lL2Rlc2t0b3AvaG9tZS5jb21wb25lbnQuZGVza3RvcC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLFdBQUE7QUFDQTtFQUNFO0lBQUssK0JBQStCO0VDRXBDO0VEREE7SUFBTyxpQ0FBaUM7RUNJeEM7QUFDRjtBREZBO0VBQ0U7SUFBSyx1QkFBdUI7RUNNNUI7RURMQTtJQUFPLHlCQUF5QjtFQ1FoQztBQUNGO0FETkU7RUFDSSxtQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLHdGQUF3RjtBQ1M5RjtBRFBFO0VBQ0UsV0FBVztFQUNYLFdBQVc7QUNVZjtBRFBBO0VBQ0EsYUFBYTtFQUNiLGdCQUFnQjtFQUNoQixtQkFBbUI7RUFDbkIsY0FBYztFQUNkLGVBQWU7RUFDZiw0Q0FBNkM7QUNVN0M7QURSQTs7RUFFQSxXQUFXO0VBQ1gsWUFBWTtFQUNaLHFDQUFxQztFQUNyQyxpQ0FBaUM7RUFDakMsV0FBVztFQUNYLGNBQWM7RUFDZCxnQkFBZ0I7RUFDaEIsZUFBZTtBQ1dmO0FEVEE7RUFFb0MsYUFBWTtFQUFDLGNBQ2pEO0FDYUE7QURaQTtFQUNFLGtCQUFrQjtFQUNsQixVQUFVO0VBQ1YsZUFBZTtFQUNkLGdCQUFnQjtFQUNqQixlQUFlO0FDZWpCO0FEYkE7RUFDRSxrQkFBa0I7RUFDbEIsZ0JBQWdCO0VBQ2hCLGlCQUFpQjtBQ2dCbkI7QURkQTtFQUNFLDBCQUEwQjtBQ2lCNUI7QURmQTtFQUNBLHlCQUF5QjtFQUN6QixpQkFBaUI7QUNrQmpCO0FEaEJBO0VBQ0Usa0JBQWlCO0VBQ2pCLGdCQUFnQjtFQUNoQixjQUFjO0VBQ2QsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQiw0Q0FBNkM7QUNtQi9DO0FEakJBO0VBQ0UsZUFBZTtBQ29CakI7QURsQkE7RUFDRSxjQUFjO0FDcUJoQjtBRG5CQTtFQUNFLG1CQUFBO0VBRUEsc0JBQXNCO0VBRXRCLDBCQUFBO0VBQ0EsZ0JBQWdCO0VBRWhCLHlDQUFBO0VBQ0EsNEJBQTRCO0VBQzVCLDJCQUEyQjtFQUMzQiw0QkFBNEI7RUFDNUIsc0JBQXNCO0VBQ3RCLGlCQUFpQjtFQUNqQixnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLGdCQUFnQjtBQ21CbEI7QURqQkE7RUFDRSxjQUFjO0FDb0JoQjtBRGxCQTtFQUNFLCtCQUErQjtFQUMvQixrQkFBa0I7RUFDbEIsbUJBQW1CO0VBQ25CLGlCQUFpQjtFQUNqQixrQkFBa0I7QUNxQnBCIiwiZmlsZSI6InNyYy9hcHAvaG9tZS9kZXNrdG9wL2hvbWUuY29tcG9uZW50LmRlc2t0b3Auc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIFNhZmFyaSAqL1xuQC13ZWJraXQta2V5ZnJhbWVzIHNwaW4ge1xuICAwJSB7IC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMGRlZyk7IH1cbiAgMTAwJSB7IC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMzYwZGVnKTsgfVxufVxuXG5Aa2V5ZnJhbWVzIHNwaW4ge1xuICAwJSB7IHRyYW5zZm9ybTogcm90YXRlKDBkZWcpOyB9XG4gIDEwMCUgeyB0cmFuc2Zvcm06IHJvdGF0ZSgzNjBkZWcpOyB9XG59XG5cbiAgLmp1bWJvdHJvbntcbiAgICAgIHBhZGRpbmc6LjJyZW0gMXJlbTtcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgIGJveC1zaGFkb3c6IDBweCA1cHggMTFweCAtMnB4IHJnYmEoMCwgMCwgMCwgMC4xOCksIDBweCA0cHggMTJweCAtN3B4IHJnYmEoMCwgMCwgMCwgMC4xNSk7XG4gIH1cbiAgaW1ne1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogNzUlO1xuICB9XG5cbi5oci1zZWN0IHtcbmRpc3BsYXk6IGZsZXg7XG5mbGV4LWJhc2lzOiAxMDAlO1xuYWxpZ24taXRlbXM6IGNlbnRlcjtcbmNvbG9yOiAjOGIwMDAwO1xubWFyZ2luOiA4cHggMHB4O1xudGV4dC1zaGFkb3c6IDJweCAycHggNHB4ICByZ2JhKDAsIDAsIDAsIDAuMTgpO1xufVxuLmhyLXNlY3Q6OmJlZm9yZSxcbi5oci1zZWN0OjphZnRlciB7XG5jb250ZW50OiBcIlwiO1xuZmxleC1ncm93OiAxO1xuYmFja2dyb3VuZDogcmdiYSgxMzMsIDEzMywgMTMzLCAwLjM1KTtcbnRleHQtc2hhZG93OiAycHggMnB4IDRweCAgIzhiMDAwMDtcbmhlaWdodDogMXB4O1xuZm9udC1zaXplOiAwcHg7XG5saW5lLWhlaWdodDogMHB4O1xubWFyZ2luOiAwcHggOHB4O1xufVxuLmNvbC1sZy0yXG57XG4td2Via2l0LWJveC1mbGV4OjA7LW1zLWZsZXg6MCAwIDIwJTtmbGV4OjAgMCAyMCU7bWF4LXdpZHRoOjIwJVxufVxuLmNvbC1sZy0ze1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIHdpZHRoOiAzMyU7XG4gIG1pbi1oZWlnaHQ6IDFweDtcbiAgIHBhZGRpbmctcmlnaHQ6IDA7XG4gIHBhZGRpbmctbGVmdDogMDtcbn1cbi5jb250YWluZXJ7XG4gIHBhZGRpbmctYm90dG9tOiAyJTtcbiAgcGFkZGluZy1sZWZ0OiAwJTtcbiAgcGFkZGluZy1yaWdodDogMCU7XG59XG4ubXktYXV0b3tcbiAgdGV4dC1hbGlnbjogLXdlYmtpdC1jZW50ZXI7XG59XG4uY2Fyb3VzZWwtaW5kaWNhdG9ycyBsaSB7XG5iYWNrZ3JvdW5kLWNvbG9yOiAjOGIwMDAwO1xubWFyZ2luLWJvdHRvbTogMSU7XG59XG4uaDUtc3R5bGV7XG4gIHRleHQtYWxpZ246Y2VudGVyOyBcbiAgbWFyZ2luLXRvcDogMTBweDtcbiAgY29sb3I6ICM4YjAwMDA7XG4gIGZvbnQtc2l6ZTogMTQwJTtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgdGV4dC1zaGFkb3c6IDJweCAycHggNHB4ICByZ2JhKDAsIDAsIDAsIDAuMTgpO1xufVxuaDF7XG4gIGZvbnQtc2l6ZTogMzAwJTtcbn1cbmg1e1xuICBjb2xvcjogIzJjM2U1MDtcbn1cbi5wYXJhbGxheCB7XG4gIC8qIFRoZSBpbWFnZSB1c2VkICovXG4gIFxuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuXG4gIC8qIFNldCBhIHNwZWNpZmljIGhlaWdodCAqL1xuICBtaW4taGVpZ2h0OiA4MHZoO1xuXG4gIC8qIENyZWF0ZSB0aGUgcGFyYWxsYXggc2Nyb2xsaW5nIGVmZmVjdCAqL1xuICBiYWNrZ3JvdW5kLWF0dGFjaG1lbnQ6IGZpeGVkO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gIHBhZGRpbmctdG9wOiA2LjglO1xuICBwYWRkaW5nLWxlZnQ6IDIlO1xuICBwYWRkaW5nLXJpZ2h0OiAyJTtcbiAgbWluLWhlaWdodDogODB2aDtcbn1cbi5tYXQtY2FyZC1jb250ZW50e1xuICBtYXJnaW4tdG9wOiAzJTtcbn1cbi5tYXQtY2FyZHtcbiAgYm94LXNoYWRvdzogM3B4IDNweCAzcHggIzg2ODY4NjtcbiAgcGFkZGluZy1sZWZ0OiAuMTAlO1xuICBwYWRkaW5nLXJpZ2h0OiAuMTAlO1xuICBwYWRkaW5nLXRvcDogLjEwJTtcbiAgcGFkZGluZy1ib3R0b206IDMlO1xufVxuIiwiLyogU2FmYXJpICovXG5ALXdlYmtpdC1rZXlmcmFtZXMgc3BpbiB7XG4gIDAlIHtcbiAgICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDBkZWcpO1xuICB9XG4gIDEwMCUge1xuICAgIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMzYwZGVnKTtcbiAgfVxufVxuXG5Aa2V5ZnJhbWVzIHNwaW4ge1xuICAwJSB7XG4gICAgdHJhbnNmb3JtOiByb3RhdGUoMGRlZyk7XG4gIH1cbiAgMTAwJSB7XG4gICAgdHJhbnNmb3JtOiByb3RhdGUoMzYwZGVnKTtcbiAgfVxufVxuXG4uanVtYm90cm9uIHtcbiAgcGFkZGluZzogLjJyZW0gMXJlbTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBib3gtc2hhZG93OiAwcHggNXB4IDExcHggLTJweCByZ2JhKDAsIDAsIDAsIDAuMTgpLCAwcHggNHB4IDEycHggLTdweCByZ2JhKDAsIDAsIDAsIDAuMTUpO1xufVxuXG5pbWcge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiA3NSU7XG59XG5cbi5oci1zZWN0IHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1iYXNpczogMTAwJTtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgY29sb3I6ICM4YjAwMDA7XG4gIG1hcmdpbjogOHB4IDBweDtcbiAgdGV4dC1zaGFkb3c6IDJweCAycHggNHB4IHJnYmEoMCwgMCwgMCwgMC4xOCk7XG59XG5cbi5oci1zZWN0OjpiZWZvcmUsXG4uaHItc2VjdDo6YWZ0ZXIge1xuICBjb250ZW50OiBcIlwiO1xuICBmbGV4LWdyb3c6IDE7XG4gIGJhY2tncm91bmQ6IHJnYmEoMTMzLCAxMzMsIDEzMywgMC4zNSk7XG4gIHRleHQtc2hhZG93OiAycHggMnB4IDRweCAgIzhiMDAwMDtcbiAgaGVpZ2h0OiAxcHg7XG4gIGZvbnQtc2l6ZTogMHB4O1xuICBsaW5lLWhlaWdodDogMHB4O1xuICBtYXJnaW46IDBweCA4cHg7XG59XG5cbi5jb2wtbGctMiB7XG4gIC13ZWJraXQtYm94LWZsZXg6IDA7XG4gIC1tcy1mbGV4OiAwIDAgMjAlO1xuICBmbGV4OiAwIDAgMjAlO1xuICBtYXgtd2lkdGg6IDIwJTtcbn1cblxuLmNvbC1sZy0zIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB3aWR0aDogMzMlO1xuICBtaW4taGVpZ2h0OiAxcHg7XG4gIHBhZGRpbmctcmlnaHQ6IDA7XG4gIHBhZGRpbmctbGVmdDogMDtcbn1cblxuLmNvbnRhaW5lciB7XG4gIHBhZGRpbmctYm90dG9tOiAyJTtcbiAgcGFkZGluZy1sZWZ0OiAwJTtcbiAgcGFkZGluZy1yaWdodDogMCU7XG59XG5cbi5teS1hdXRvIHtcbiAgdGV4dC1hbGlnbjogLXdlYmtpdC1jZW50ZXI7XG59XG5cbi5jYXJvdXNlbC1pbmRpY2F0b3JzIGxpIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzhiMDAwMDtcbiAgbWFyZ2luLWJvdHRvbTogMSU7XG59XG5cbi5oNS1zdHlsZSB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luLXRvcDogMTBweDtcbiAgY29sb3I6ICM4YjAwMDA7XG4gIGZvbnQtc2l6ZTogMTQwJTtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgdGV4dC1zaGFkb3c6IDJweCAycHggNHB4IHJnYmEoMCwgMCwgMCwgMC4xOCk7XG59XG5cbmgxIHtcbiAgZm9udC1zaXplOiAzMDAlO1xufVxuXG5oNSB7XG4gIGNvbG9yOiAjMmMzZTUwO1xufVxuXG4ucGFyYWxsYXgge1xuICAvKiBUaGUgaW1hZ2UgdXNlZCAqL1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAvKiBTZXQgYSBzcGVjaWZpYyBoZWlnaHQgKi9cbiAgbWluLWhlaWdodDogODB2aDtcbiAgLyogQ3JlYXRlIHRoZSBwYXJhbGxheCBzY3JvbGxpbmcgZWZmZWN0ICovXG4gIGJhY2tncm91bmQtYXR0YWNobWVudDogZml4ZWQ7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgcGFkZGluZy10b3A6IDYuOCU7XG4gIHBhZGRpbmctbGVmdDogMiU7XG4gIHBhZGRpbmctcmlnaHQ6IDIlO1xuICBtaW4taGVpZ2h0OiA4MHZoO1xufVxuXG4ubWF0LWNhcmQtY29udGVudCB7XG4gIG1hcmdpbi10b3A6IDMlO1xufVxuXG4ubWF0LWNhcmQge1xuICBib3gtc2hhZG93OiAzcHggM3B4IDNweCAjODY4Njg2O1xuICBwYWRkaW5nLWxlZnQ6IC4xMCU7XG4gIHBhZGRpbmctcmlnaHQ6IC4xMCU7XG4gIHBhZGRpbmctdG9wOiAuMTAlO1xuICBwYWRkaW5nLWJvdHRvbTogMyU7XG59XG4iXX0= */");

/***/ }),

/***/ "./src/app/home/desktop/home.component.desktop.ts":
/*!********************************************************!*\
  !*** ./src/app/home/desktop/home.component.desktop.ts ***!
  \********************************************************/
/*! exports provided: HomeComponentDesktop */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponentDesktop", function() { return HomeComponentDesktop; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var rxjs_Rx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/Rx */ "./node_modules/rxjs-compat/_esm2015/Rx.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");
/* harmony import */ var _app_header_header_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../app-header/header.service */ "./src/app/app-header/header.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");







let HomeComponentDesktop = class HomeComponentDesktop {
    constructor(meta, title, router, headerService) {
        this.router = router;
        this.headerService = headerService;
        this.homePageData = {};
        this.title = 'FinalRevise';
        this.signupfrm = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({
            name: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]()
        });
        title.setTitle(this.title + "FinalRevise-Previous Year questions/notes/study material/placement & interview question");
        meta.addTags([
            { name: 'author', content: 'finalrevise.com' },
            { name: 'keywords', content: 'State and Centre Boards Previous Year question Papers, Competitive Exams Previous Year Papers and answer, Entrance Exams question paper, University Semesters Previous Year question Papers, Companies Placement Papers download, Companies Interview Questions answer, International Exams Paper' },
            { name: 'description', content: 'Get study material for competitive exams, universities semesters, international competitive exam, school board, government job papers and placement papers.' }
        ]);
    }
    ngOnInit() {
        this.headerService.setIsHome(true);
    }
    getSearchResult() {
        this.router.navigateByUrl('/search/' + this.signupfrm.value.name + '/0' + "/0");
    }
    openNextPage(titleText) {
        this.router.navigateByUrl('/subcat/' + this.replaceAll(titleText, '-'));
    }
    replaceAll(str, replacement) {
        return str.split(' ').join(replacement);
    }
};
HomeComponentDesktop.ctorParameters = () => [
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__["Meta"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__["Title"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"] },
    { type: _app_header_header_service__WEBPACK_IMPORTED_MODULE_5__["HeaderService"] }
];
HomeComponentDesktop = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'home-desktop',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./home.component.desktop.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/desktop/home.component.desktop.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./home.component.desktop.scss */ "./src/app/home/desktop/home.component.desktop.scss")).default]
    })
], HomeComponentDesktop);



/***/ }),

/***/ "./src/app/home/mobile/home.component.mobile.scss":
/*!********************************************************!*\
  !*** ./src/app/home/mobile/home.component.mobile.scss ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".jumbotron {\n  padding: 0rem 0rem;\n  margin-bottom: 0rem;\n  background-color: #ecf0f1;\n  border-radius: 0.3rem;\n  text-align: center;\n  box-shadow: 0px 5px 11px -2px rgba(0, 0, 0, 0.18), 0px 4px 12px -7px rgba(0, 0, 0, 0.15);\n}\n\n.hr-sect {\n  display: flex;\n  flex-basis: 100%;\n  align-items: center;\n  color: #2c3e50;\n  margin: 8px 0px;\n}\n\n.hr-sect::before,\n.hr-sect::after {\n  content: \"\";\n  flex-grow: 1;\n  background: rgba(133, 133, 133, 0.35);\n  height: 1px;\n  font-size: 0px;\n  line-height: 0px;\n  margin: 0px 8px;\n}\n\n.col-lg-2 {\n  flex: 0 0 20%;\n  max-width: 20%;\n}\n\n.col-lg-3 {\n  position: relative;\n  width: 33%;\n  min-height: 1px;\n  padding: 1%;\n}\n\n.container {\n  padding: 0%;\n}\n\n.row {\n  margin: 0%;\n}\n\n.carousel {\n  max-height: 40%;\n}\n\n.h5-style {\n  text-align: center;\n  margin-top: 10px;\n  color: #8b0000;\n  font-size: 125%;\n  text-shadow: 2px 2px 4px rgba(0, 0, 0, 0.18);\n}\n\n.main-content {\n  padding-top: 18%;\n  padding-bottom: 18%;\n  min-height: 80vh;\n  /* The image used */\n  background-size: cover;\n  /* Create the parallax scrolling effect */\n  background-attachment: fixed;\n  background-position: center;\n  background-repeat: no-repeat;\n  background-size: cover;\n  min-height: 80vh;\n}\n\na {\n  text-decoration: none !important;\n}\n\ntable {\n  width: 100%;\n  border-collapse: collapse;\n  padding: 1%;\n}\n\ntd {\n  width: 50%;\n  text-align: left;\n  padding: 1%;\n}\n\n#pics {\n  width: 100% !important;\n}\n\nimage {\n  x: -2;\n  y: -99;\n}\n\n.mat-card {\n  box-shadow: 3px 3px 3px #868686;\n  padding-left: .10%;\n  padding-right: .10%;\n  padding-top: .10%;\n  padding-bottom: 3%;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Wb2x1bWVzL0ZpbmFsUmV2aXNlL2ZpbmFscmV2aXNlX3dlYnNpdGUvc3JjL2FwcC9ob21lL21vYmlsZS9ob21lLmNvbXBvbmVudC5tb2JpbGUuc2NzcyIsInNyYy9hcHAvaG9tZS9tb2JpbGUvaG9tZS5jb21wb25lbnQubW9iaWxlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxrQkFBaUI7RUFBQyxtQkFBa0I7RUFBQyx5QkFBd0I7RUFDN0QscUJBQW9CO0VBQ3BCLGtCQUFrQjtFQUNsQix3RkFBd0Y7QUNHMUY7O0FEREE7RUFDQSxhQUFhO0VBQ2IsZ0JBQWdCO0VBQ2hCLG1CQUFtQjtFQUNuQixjQUFjO0VBQ2QsZUFBZTtBQ0lmOztBREZBOztFQUVBLFdBQVc7RUFDWCxZQUFZO0VBQ1oscUNBQXFDO0VBQ3JDLFdBQVc7RUFDWCxjQUFjO0VBQ2QsZ0JBQWdCO0VBQ2hCLGVBQWU7QUNLZjs7QURIQTtFQUVvQyxhQUFZO0VBQUMsY0FDakQ7QUNPQTs7QUROQTtFQUNFLGtCQUFrQjtFQUNsQixVQUFVO0VBQ1YsZUFBZTtFQUNmLFdBQVc7QUNTYjs7QURQQTtFQUNFLFdBQVc7QUNVYjs7QURSQTtFQUNFLFVBQVU7QUNXWjs7QURUQTtFQUNFLGVBQWU7QUNZakI7O0FEVkE7RUFDRSxrQkFBaUI7RUFDakIsZ0JBQWdCO0VBQ2hCLGNBQWM7RUFDZCxlQUFlO0VBQ2YsNENBQTZDO0FDYS9DOztBRFhBO0VBQ0UsZ0JBQWdCO0VBQ2hCLG1CQUFtQjtFQUNuQixnQkFBZ0I7RUFDZCxtQkFBQTtFQUVBLHNCQUFzQjtFQUV0Qix5Q0FBQTtFQUNBLDRCQUE0QjtFQUM1QiwyQkFBMkI7RUFDM0IsNEJBQTRCO0VBQzVCLHNCQUFzQjtFQUN0QixnQkFBZ0I7QUNZcEI7O0FEVkU7RUFDRSxnQ0FBZ0M7QUNhcEM7O0FEWEE7RUFDRSxXQUFXO0VBQ1gseUJBQXdCO0VBQ3hCLFdBQVc7QUNjYjs7QURYQTtFQUNFLFVBQVU7RUFDVixnQkFBZ0I7RUFDaEIsV0FBVztBQ2NiOztBRFpBO0VBQ0Usc0JBQXNCO0FDZXhCOztBRGJBO0VBQ0UsS0FBSztFQUNMLE1BQU07QUNnQlI7O0FEZEE7RUFDRSwrQkFBK0I7RUFDL0Isa0JBQWtCO0VBQ2xCLG1CQUFtQjtFQUNuQixpQkFBaUI7RUFDakIsa0JBQWtCO0FDaUJwQiIsImZpbGUiOiJzcmMvYXBwL2hvbWUvbW9iaWxlL2hvbWUuY29tcG9uZW50Lm1vYmlsZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmp1bWJvdHJvbntcbiAgcGFkZGluZzowcmVtIDByZW07bWFyZ2luLWJvdHRvbTowcmVtO2JhY2tncm91bmQtY29sb3I6I2VjZjBmMTtcbiAgYm9yZGVyLXJhZGl1czowLjNyZW07XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgYm94LXNoYWRvdzogMHB4IDVweCAxMXB4IC0ycHggcmdiYSgwLCAwLCAwLCAwLjE4KSwgMHB4IDRweCAxMnB4IC03cHggcmdiYSgwLCAwLCAwLCAwLjE1KTtcbn1cbi5oci1zZWN0IHtcbmRpc3BsYXk6IGZsZXg7XG5mbGV4LWJhc2lzOiAxMDAlO1xuYWxpZ24taXRlbXM6IGNlbnRlcjtcbmNvbG9yOiAjMmMzZTUwO1xubWFyZ2luOiA4cHggMHB4O1xufVxuLmhyLXNlY3Q6OmJlZm9yZSxcbi5oci1zZWN0OjphZnRlciB7XG5jb250ZW50OiBcIlwiO1xuZmxleC1ncm93OiAxO1xuYmFja2dyb3VuZDogcmdiYSgxMzMsIDEzMywgMTMzLCAwLjM1KTtcbmhlaWdodDogMXB4O1xuZm9udC1zaXplOiAwcHg7XG5saW5lLWhlaWdodDogMHB4O1xubWFyZ2luOiAwcHggOHB4O1xufVxuLmNvbC1sZy0yXG57XG4td2Via2l0LWJveC1mbGV4OjA7LW1zLWZsZXg6MCAwIDIwJTtmbGV4OjAgMCAyMCU7bWF4LXdpZHRoOjIwJVxufVxuLmNvbC1sZy0ze1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIHdpZHRoOiAzMyU7XG4gIG1pbi1oZWlnaHQ6IDFweDtcbiAgcGFkZGluZzogMSU7XG59XG4uY29udGFpbmVye1xuICBwYWRkaW5nOiAwJTtcbn1cbi5yb3d7XG4gIG1hcmdpbjogMCU7XG59XG4uY2Fyb3VzZWx7XG4gIG1heC1oZWlnaHQ6IDQwJTtcbn1cbi5oNS1zdHlsZXtcbiAgdGV4dC1hbGlnbjpjZW50ZXI7IFxuICBtYXJnaW4tdG9wOiAxMHB4O1xuICBjb2xvcjogIzhiMDAwMDtcbiAgZm9udC1zaXplOiAxMjUlO1xuICB0ZXh0LXNoYWRvdzogMnB4IDJweCA0cHggIHJnYmEoMCwgMCwgMCwgMC4xOCk7XG59XG4ubWFpbi1jb250ZW50IHtcbiAgcGFkZGluZy10b3A6IDE4JTtcbiAgcGFkZGluZy1ib3R0b206IDE4JTtcbiAgbWluLWhlaWdodDogODB2aDtcbiAgICAvKiBUaGUgaW1hZ2UgdXNlZCAqL1xuICAgIFxuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gIFxuICAgIC8qIENyZWF0ZSB0aGUgcGFyYWxsYXggc2Nyb2xsaW5nIGVmZmVjdCAqL1xuICAgIGJhY2tncm91bmQtYXR0YWNobWVudDogZml4ZWQ7XG4gICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICBtaW4taGVpZ2h0OiA4MHZoO1xuICB9XG4gIGEge1xuICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZSAhaW1wb3J0YW50O1xufVxudGFibGV7XG4gIHdpZHRoOiAxMDAlO1xuICBib3JkZXItY29sbGFwc2U6Y29sbGFwc2U7XG4gIHBhZGRpbmc6IDElO1xufVxuXG50ZHtcbiAgd2lkdGg6IDUwJTtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbiAgcGFkZGluZzogMSU7XG59XG4jcGljc3tcbiAgd2lkdGg6IDEwMCUgIWltcG9ydGFudDtcbn1cbmltYWdle1xuICB4OiAtMjtcbiAgeTogLTk5O1xufVxuLm1hdC1jYXJke1xuICBib3gtc2hhZG93OiAzcHggM3B4IDNweCAjODY4Njg2O1xuICBwYWRkaW5nLWxlZnQ6IC4xMCU7XG4gIHBhZGRpbmctcmlnaHQ6IC4xMCU7XG4gIHBhZGRpbmctdG9wOiAuMTAlO1xuICBwYWRkaW5nLWJvdHRvbTogMyU7XG59IiwiLmp1bWJvdHJvbiB7XG4gIHBhZGRpbmc6IDByZW0gMHJlbTtcbiAgbWFyZ2luLWJvdHRvbTogMHJlbTtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2VjZjBmMTtcbiAgYm9yZGVyLXJhZGl1czogMC4zcmVtO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGJveC1zaGFkb3c6IDBweCA1cHggMTFweCAtMnB4IHJnYmEoMCwgMCwgMCwgMC4xOCksIDBweCA0cHggMTJweCAtN3B4IHJnYmEoMCwgMCwgMCwgMC4xNSk7XG59XG5cbi5oci1zZWN0IHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1iYXNpczogMTAwJTtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgY29sb3I6ICMyYzNlNTA7XG4gIG1hcmdpbjogOHB4IDBweDtcbn1cblxuLmhyLXNlY3Q6OmJlZm9yZSxcbi5oci1zZWN0OjphZnRlciB7XG4gIGNvbnRlbnQ6IFwiXCI7XG4gIGZsZXgtZ3JvdzogMTtcbiAgYmFja2dyb3VuZDogcmdiYSgxMzMsIDEzMywgMTMzLCAwLjM1KTtcbiAgaGVpZ2h0OiAxcHg7XG4gIGZvbnQtc2l6ZTogMHB4O1xuICBsaW5lLWhlaWdodDogMHB4O1xuICBtYXJnaW46IDBweCA4cHg7XG59XG5cbi5jb2wtbGctMiB7XG4gIC13ZWJraXQtYm94LWZsZXg6IDA7XG4gIC1tcy1mbGV4OiAwIDAgMjAlO1xuICBmbGV4OiAwIDAgMjAlO1xuICBtYXgtd2lkdGg6IDIwJTtcbn1cblxuLmNvbC1sZy0zIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB3aWR0aDogMzMlO1xuICBtaW4taGVpZ2h0OiAxcHg7XG4gIHBhZGRpbmc6IDElO1xufVxuXG4uY29udGFpbmVyIHtcbiAgcGFkZGluZzogMCU7XG59XG5cbi5yb3cge1xuICBtYXJnaW46IDAlO1xufVxuXG4uY2Fyb3VzZWwge1xuICBtYXgtaGVpZ2h0OiA0MCU7XG59XG5cbi5oNS1zdHlsZSB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luLXRvcDogMTBweDtcbiAgY29sb3I6ICM4YjAwMDA7XG4gIGZvbnQtc2l6ZTogMTI1JTtcbiAgdGV4dC1zaGFkb3c6IDJweCAycHggNHB4IHJnYmEoMCwgMCwgMCwgMC4xOCk7XG59XG5cbi5tYWluLWNvbnRlbnQge1xuICBwYWRkaW5nLXRvcDogMTglO1xuICBwYWRkaW5nLWJvdHRvbTogMTglO1xuICBtaW4taGVpZ2h0OiA4MHZoO1xuICAvKiBUaGUgaW1hZ2UgdXNlZCAqL1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAvKiBDcmVhdGUgdGhlIHBhcmFsbGF4IHNjcm9sbGluZyBlZmZlY3QgKi9cbiAgYmFja2dyb3VuZC1hdHRhY2htZW50OiBmaXhlZDtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICBtaW4taGVpZ2h0OiA4MHZoO1xufVxuXG5hIHtcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lICFpbXBvcnRhbnQ7XG59XG5cbnRhYmxlIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGJvcmRlci1jb2xsYXBzZTogY29sbGFwc2U7XG4gIHBhZGRpbmc6IDElO1xufVxuXG50ZCB7XG4gIHdpZHRoOiA1MCU7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG4gIHBhZGRpbmc6IDElO1xufVxuXG4jcGljcyB7XG4gIHdpZHRoOiAxMDAlICFpbXBvcnRhbnQ7XG59XG5cbmltYWdlIHtcbiAgeDogLTI7XG4gIHk6IC05OTtcbn1cblxuLm1hdC1jYXJkIHtcbiAgYm94LXNoYWRvdzogM3B4IDNweCAzcHggIzg2ODY4NjtcbiAgcGFkZGluZy1sZWZ0OiAuMTAlO1xuICBwYWRkaW5nLXJpZ2h0OiAuMTAlO1xuICBwYWRkaW5nLXRvcDogLjEwJTtcbiAgcGFkZGluZy1ib3R0b206IDMlO1xufVxuIl19 */");

/***/ }),

/***/ "./src/app/home/mobile/home.component.mobile.ts":
/*!******************************************************!*\
  !*** ./src/app/home/mobile/home.component.mobile.ts ***!
  \******************************************************/
/*! exports provided: HomeComponentMobile */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponentMobile", function() { return HomeComponentMobile; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var rxjs_Rx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/Rx */ "./node_modules/rxjs-compat/_esm2015/Rx.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");
/* harmony import */ var _app_header_header_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../app-header/header.service */ "./src/app/app-header/header.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");







let HomeComponentMobile = class HomeComponentMobile {
    constructor(meta, title, router, headerService) {
        this.router = router;
        this.headerService = headerService;
        this.homePageData = {};
        this.title = 'FinalRevise';
        this.signupfrm = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({
            name: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]()
        });
        title.setTitle(this.title + "FinalRevise-Previous Year questions/notes/study material/placement & interview question");
        meta.addTags([
            { name: 'author', content: 'finalrevise.com' },
            { name: 'keywords', content: 'State and Centre Boards Previous Year question Papers, Competitive Exams Previous Year Papers and answer, Entrance Exams question paper, University Semesters Previous Year question Papers, Companies Placement Papers download, Companies Interview Questions answer, International Exams Paper' },
            { name: 'description', content: 'Get study material for competitive exams, universities semesters, international competitive exam, school board, government job papers and placement papers.' }
        ]);
    }
    ngOnInit() {
        this.headerService.setIsHome(true);
    }
    getSearchResult() {
        this.router.navigateByUrl('/search/' + this.signupfrm.value.name + '/0' + "/0");
    }
    openNextPage(titleText) {
        this.router.navigateByUrl('/subcat/' + this.replaceAll(titleText, '-'));
    }
    replaceAll(str, replacement) {
        return str.split(' ').join(replacement);
    }
    ;
    scrollToPrevious() {
        var elmnt = document.getElementById("previous");
        elmnt.scrollIntoView();
    }
};
HomeComponentMobile.ctorParameters = () => [
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__["Meta"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__["Title"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"] },
    { type: _app_header_header_service__WEBPACK_IMPORTED_MODULE_5__["HeaderService"] }
];
HomeComponentMobile = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'home-mobile',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./home.component.mobile.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/mobile/home.component.mobile.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./home.component.mobile.scss */ "./src/app/home/mobile/home.component.mobile.scss")).default]
    })
], HomeComponentMobile);



/***/ }),

/***/ "./src/app/our-services/services.component.css":
/*!*****************************************************!*\
  !*** ./src/app/our-services/services.component.css ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".jumbotron{\n    padding:0rem 0rem;margin-bottom:0rem;background-color:#ecf0f1;border-radius:0.3rem\n  }\n  @media (min-width: 576px){\n    .main-content {\n      padding-top: 7.2%;\n      padding-left: 7%;\n      padding-right: 7%;\n      min-height: 80vh;\n      padding-bottom: 2%;\n      background-image: url('/assets/images/bg.jpg');\n\n  /* Set a specific height */\n  min-height: 80vh;\n\n  /* Create the parallax scrolling effect */\n  background-attachment: fixed;\n  background-position: center;\n  background-repeat: no-repeat;\n  background-size: cover;\n  \n    }\n    img{\n      width: 70%; height: 65%;\n    }\n  }\n  @media (max-width: 576px){\n    .main-content {\n      padding-top: 15%;\n      padding-left: 2%;\n      padding-right: 2%;\n      min-height: 80vh;\n      padding-bottom: 5%;\n      background-image: url('/assets/images/bg.jpg');\n      \n  /* Set a specific height */\n  min-height: 80vh;\n\n  /* Create the parallax scrolling effect */\n  background-attachment: fixed;\n  background-position: center;\n  background-repeat: no-repeat;\n  background-size: cover;\n\n    } \n    img{\n      width: 50%; height: 90%;\n    }\n  }\n  .hr-sect {\n    display: flex;\n    flex-basis: 100%;\n    align-items: center;\n    color: #2c3e50;\n    margin: 8px 0px;\n  }\n  .hr-sect::before,\n  .hr-sect::after {\n    content: \"\";\n    flex-grow: 1;\n    background: rgba(133, 133, 133, 0.35);\n    height: 1px;\n    font-size: 0px;\n    line-height: 0px;\n    margin: 0px 8px;\n  }\n  .container{\n    padding-bottom: 2%;\n    padding-left: 0%;\n    padding-right: 0%;\n  }\n  .my-auto{\n    text-align: -webkit-center;\n  }\n  .carousel-indicators li {\n  background-color: #8b0000;\n  margin-bottom: 1%;\n}\n  .row{\n  margin: 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvb3VyLXNlcnZpY2VzL3NlcnZpY2VzLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxpQkFBaUIsQ0FBQyxrQkFBa0IsQ0FBQyx3QkFBd0IsQ0FBQztFQUNoRTtFQUNBO0lBQ0U7TUFDRSxpQkFBaUI7TUFDakIsZ0JBQWdCO01BQ2hCLGlCQUFpQjtNQUNqQixnQkFBZ0I7TUFDaEIsa0JBQWtCO01BQ2xCLDhDQUE4Qzs7RUFFbEQsMEJBQTBCO0VBQzFCLGdCQUFnQjs7RUFFaEIseUNBQXlDO0VBQ3pDLDRCQUE0QjtFQUM1QiwyQkFBMkI7RUFDM0IsNEJBQTRCO0VBQzVCLHNCQUFzQjs7SUFFcEI7SUFDQTtNQUNFLFVBQVUsRUFBRSxXQUFXO0lBQ3pCO0VBQ0Y7RUFDQTtJQUNFO01BQ0UsZ0JBQWdCO01BQ2hCLGdCQUFnQjtNQUNoQixpQkFBaUI7TUFDakIsZ0JBQWdCO01BQ2hCLGtCQUFrQjtNQUNsQiw4Q0FBOEM7O0VBRWxELDBCQUEwQjtFQUMxQixnQkFBZ0I7O0VBRWhCLHlDQUF5QztFQUN6Qyw0QkFBNEI7RUFDNUIsMkJBQTJCO0VBQzNCLDRCQUE0QjtFQUM1QixzQkFBc0I7O0lBRXBCO0lBQ0E7TUFDRSxVQUFVLEVBQUUsV0FBVztJQUN6QjtFQUNGO0VBQ0E7SUFDRSxhQUFhO0lBQ2IsZ0JBQWdCO0lBQ2hCLG1CQUFtQjtJQUNuQixjQUFjO0lBQ2QsZUFBZTtFQUNqQjtFQUNBOztJQUVFLFdBQVc7SUFDWCxZQUFZO0lBQ1oscUNBQXFDO0lBQ3JDLFdBQVc7SUFDWCxjQUFjO0lBQ2QsZ0JBQWdCO0lBQ2hCLGVBQWU7RUFDakI7RUFDQTtJQUNFLGtCQUFrQjtJQUNsQixnQkFBZ0I7SUFDaEIsaUJBQWlCO0VBQ25CO0VBQ0E7SUFDRSwwQkFBMEI7RUFDNUI7RUFDRjtFQUNFLHlCQUF5QjtFQUN6QixpQkFBaUI7QUFDbkI7RUFDQTtFQUNFLFNBQVM7QUFDWCIsImZpbGUiOiJzcmMvYXBwL291ci1zZXJ2aWNlcy9zZXJ2aWNlcy5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmp1bWJvdHJvbntcbiAgICBwYWRkaW5nOjByZW0gMHJlbTttYXJnaW4tYm90dG9tOjByZW07YmFja2dyb3VuZC1jb2xvcjojZWNmMGYxO2JvcmRlci1yYWRpdXM6MC4zcmVtXG4gIH1cbiAgQG1lZGlhIChtaW4td2lkdGg6IDU3NnB4KXtcbiAgICAubWFpbi1jb250ZW50IHtcbiAgICAgIHBhZGRpbmctdG9wOiA3LjIlO1xuICAgICAgcGFkZGluZy1sZWZ0OiA3JTtcbiAgICAgIHBhZGRpbmctcmlnaHQ6IDclO1xuICAgICAgbWluLWhlaWdodDogODB2aDtcbiAgICAgIHBhZGRpbmctYm90dG9tOiAyJTtcbiAgICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybCgnL2Fzc2V0cy9pbWFnZXMvYmcuanBnJyk7XG5cbiAgLyogU2V0IGEgc3BlY2lmaWMgaGVpZ2h0ICovXG4gIG1pbi1oZWlnaHQ6IDgwdmg7XG5cbiAgLyogQ3JlYXRlIHRoZSBwYXJhbGxheCBzY3JvbGxpbmcgZWZmZWN0ICovXG4gIGJhY2tncm91bmQtYXR0YWNobWVudDogZml4ZWQ7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgXG4gICAgfVxuICAgIGltZ3tcbiAgICAgIHdpZHRoOiA3MCU7IGhlaWdodDogNjUlO1xuICAgIH1cbiAgfVxuICBAbWVkaWEgKG1heC13aWR0aDogNTc2cHgpe1xuICAgIC5tYWluLWNvbnRlbnQge1xuICAgICAgcGFkZGluZy10b3A6IDE1JTtcbiAgICAgIHBhZGRpbmctbGVmdDogMiU7XG4gICAgICBwYWRkaW5nLXJpZ2h0OiAyJTtcbiAgICAgIG1pbi1oZWlnaHQ6IDgwdmg7XG4gICAgICBwYWRkaW5nLWJvdHRvbTogNSU7XG4gICAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoJy9hc3NldHMvaW1hZ2VzL2JnLmpwZycpO1xuICAgICAgXG4gIC8qIFNldCBhIHNwZWNpZmljIGhlaWdodCAqL1xuICBtaW4taGVpZ2h0OiA4MHZoO1xuXG4gIC8qIENyZWF0ZSB0aGUgcGFyYWxsYXggc2Nyb2xsaW5nIGVmZmVjdCAqL1xuICBiYWNrZ3JvdW5kLWF0dGFjaG1lbnQ6IGZpeGVkO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG5cbiAgICB9IFxuICAgIGltZ3tcbiAgICAgIHdpZHRoOiA1MCU7IGhlaWdodDogOTAlO1xuICAgIH1cbiAgfVxuICAuaHItc2VjdCB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWJhc2lzOiAxMDAlO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgY29sb3I6ICMyYzNlNTA7XG4gICAgbWFyZ2luOiA4cHggMHB4O1xuICB9XG4gIC5oci1zZWN0OjpiZWZvcmUsXG4gIC5oci1zZWN0OjphZnRlciB7XG4gICAgY29udGVudDogXCJcIjtcbiAgICBmbGV4LWdyb3c6IDE7XG4gICAgYmFja2dyb3VuZDogcmdiYSgxMzMsIDEzMywgMTMzLCAwLjM1KTtcbiAgICBoZWlnaHQ6IDFweDtcbiAgICBmb250LXNpemU6IDBweDtcbiAgICBsaW5lLWhlaWdodDogMHB4O1xuICAgIG1hcmdpbjogMHB4IDhweDtcbiAgfVxuICAuY29udGFpbmVye1xuICAgIHBhZGRpbmctYm90dG9tOiAyJTtcbiAgICBwYWRkaW5nLWxlZnQ6IDAlO1xuICAgIHBhZGRpbmctcmlnaHQ6IDAlO1xuICB9XG4gIC5teS1hdXRve1xuICAgIHRleHQtYWxpZ246IC13ZWJraXQtY2VudGVyO1xuICB9XG4uY2Fyb3VzZWwtaW5kaWNhdG9ycyBsaSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICM4YjAwMDA7XG4gIG1hcmdpbi1ib3R0b206IDElO1xufVxuLnJvd3tcbiAgbWFyZ2luOiAwO1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/our-services/services.component.ts":
/*!****************************************************!*\
  !*** ./src/app/our-services/services.component.ts ***!
  \****************************************************/
/*! exports provided: ServicesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServicesComponent", function() { return ServicesComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var rxjs_Rx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/Rx */ "./node_modules/rxjs-compat/_esm2015/Rx.js");



// import {NgbCarouselConfig} from '@ng-bootstrap/ng-bootstrap';
let ServicesComponent = class ServicesComponent {
    constructor() {
        this.event_list = [
            {
                event: ' Event 1',
                eventLocation: 'Fox',
                eventImage: 'https://images.unsplash.com/photo-1563210080-dfe35c83e2eb?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjExMDk0fQ&auto=format&fit=crop&w=500&q=60',
                eventDescription: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
                eventStartDate: new Date(),
                eventEndingDate: new Date()
            },
            {
                event: ' Event 2',
                eventLocation: 'Cat',
                eventImage: 'https://images.unsplash.com/photo-1564012948891-27965af38a81?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60',
                eventDescription: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
                eventStartDate: new Date(),
                eventEndingDate: new Date()
            },
            {
                event: ' Event 3',
                eventLocation: 'Monkey',
                eventImage: "https://images.unsplash.com/photo-1563049009-2fc38eb88540?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60",
                eventDescription: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\' standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
                eventStartDate: new Date(),
                eventEndingDate: new Date()
            }
        ];
        this.current_events = this.event_list.filter(event => (event.eventStartDate >= new Date() && (event.eventEndingDate <= new Date())));
        // customize default values of carousels used by this component tree
        // config.showNavigationArrows = false;
        // config.wrap = true;
        // config.showNavigationIndicators = true;
    }
};
ServicesComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'our-services',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./services.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/our-services/services.component.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./services.component.css */ "./src/app/our-services/services.component.css")).default]
    })
], ServicesComponent);



/***/ }),

/***/ "./src/app/pdf-viewer/desktop/desktop-pdf-viewer.component.css":
/*!*********************************************************************!*\
  !*** ./src/app/pdf-viewer/desktop/desktop-pdf-viewer.component.css ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("iframe{\n    max-width: 100%;\n    width: 100%;\n    height: 650px;\n}\n.jumbotron{\n    padding:0rem 0rem;\n    margin-bottom:0rem;\n    background-color:#ecf0f1;\n    border-radius:0.3rem;\n    box-shadow: 0px 5px 11px -2px rgba(0, 0, 0, 0.18), 0px 4px 12px -7px rgba(0, 0, 0, 0.15);\n}\n.tableHeader{\n    margin-top: 75px;\n    table-layout: fixed;\n    width: 100%;\n    border-collapse: unset;\n}\n.col-sm-3{\n        padding:1rem 0rem;margin-bottom:0rem;background-color:#ecf0f1;border-radius:0.3rem;\n        max-width: 35%;flex:0 0 35%;height: -webkit-fit-content;height: -moz-fit-content;height: fit-content;\n        margin-left: auto;\n    }\n.col-sm-8{\n        margin: inherit;\n    }\n.navbar {\n        position: relative;\n        display: block;\n        flex-wrap: wrap;\n        text-align: center;\n        justify-content: space-between;\n        padding: 5px;\n    }\n.fontSize{\n      font-size: 1.7rem;\n    }\nul, menu, dir {\n    -webkit-margin-before: 0;\n            margin-block-start: 0;\n    -webkit-margin-after: 0;\n            margin-block-end: 0;\n    -webkit-margin-start: 15px;\n            margin-inline-start: 15px;\n    -webkit-margin-end: 0px;\n            margin-inline-end: 0px;\n    -webkit-padding-start: 0px;\n            padding-inline-start: 0px;\n}\n.bg-primary {\n    /* background-color: #dfeaec !important; */\n    animation: colorchange 5s infinite; /* animation-name followed by duration in seconds*/\n         /* you could also use milliseconds (ms) or something like 2.5s */\n      -webkit-animation: colorchange 5s infinite; /* Chrome and Safari */\n}\n#fixed-width-flamingo {  \n    width: 35px; \n    height: 35px;\n}\n.nav-link {\n    display: block;\n    background: #8b0000;\n    margin-bottom: 3%;\n    margin-top: 8%;\n    margin-right: 2%;\n}\n@media (max-width:720px){\n\tbody{\n\t\ttext-align: center;\n\t}\n\t.display-4{\n\t\tfont-size: 30px !important;\n\t}\n}\n@keyframes colorchange\n    {\n      0%   {background: #2c3e50;}\n      33%  {background: rebeccapurple;}\n      66%  {background: rgb(0, 143, 114)}\n      100%  {background: #8b0000;}\n    }\n@-webkit-keyframes colorchange /* Safari and Chrome - necessary duplicate */\n    {\n      0%   {background: #2c3e50;}\n      33%  {background: rebeccapurple;}\n      66%  {background: rgb(0, 143, 114)}\n      100%  {background: #8b0000;}\n    }\n.sbuttons {\n      bottom: 15%;\n      position: fixed;\n      margin: 1em;\n      left: 0;\n    }\n.sbutton {\n      display: block;\n      width: 60px;\n      height: 60px;\n      border-radius: 50%;\n      text-align: center;\n      color: transparent;\n      margin: 20px auto 0;\n      box-shadow: 0px 5px 11px -2px rgba(0, 0, 0, 0.18), 0px 4px 12px -7px rgba(0, 0, 0, 0.15);\n      cursor: pointer;\n      transition: all .1s ease-out;\n      position: relative;\n    }\n.sbutton > i {\n      font-size: 38px;\n      line-height: 60px;\n      transition: all .2s ease-in-out;\n      transition-delay: 2s;\n    }\n.sbutton:active,\n    .sbutton:focus,\n    .sbutton:hover {\n      box-shadow: 0 0 4px rgba(0, 0, 0, .14), 0 4px 8px rgba(0, 0, 0, .28);\n    }\n.sbutton:not(:last-child) {\n      width: 60px;\n      height: 60px;\n      margin: 20px auto 0;\n      opacity: 0;\n    }\n.sbutton:not(:last-child) > i {\n      font-size: 25px;\n      line-height: 60px;\n      transition: all .3s ease-in-out;\n    }\n.sbutton:not(:last-child) {\n      opacity: 1;\n      width: 60px;\n      height: 60px;\n      margin: 15px auto 0;\n    }\n.sbutton:nth-last-child(1) {\n      transition-delay: 25ms;\n    }\n.sbutton:not(:last-child):nth-last-child(2) {\n      transition-delay: 20ms;\n    }\n.sbutton:not(:last-child):nth-last-child(3) {\n      transition-delay: 40ms;\n    }\n.sbutton:not(:last-child):nth-last-child(4) {\n      transition-delay: 60ms;\n    }\n.sbutton:not(:last-child):nth-last-child(5) {\n      transition-delay: 80ms;\n    }\n.sbutton:not(:last-child):nth-last-child(6) {\n      transition-delay: 100ms;\n    }\n[tooltip]:before {\n      font-family: 'Roboto';\n      font-weight: 600;\n      border-radius: 2px;\n      background-color: #585858;\n      color: #fff;\n      content: attr(tooltip);\n      font-size: 12px;\n      visibility: hidden;\n      opacity: 0;\n      padding: 5px 7px;\n      margin-left: 10px;\n      position: absolute;\n      left: 100%;\n      bottom: 20%;\n      white-space: nowrap;\n    }\n.sbutton.mainsbutton {\n      background:url(/assets/images/share.jpg);\n    }\n.sbutton.gplus {\n      background:url(/assets/images/gmail60.png);\n    }\n.sbutton.twitt {\n      background:url(/assets/images/mess60.png);\n    }\n.sbutton.fb {\n      background:url(/assets/images/facebook60.png);\n    }\n.sbutton.whatsapp {\n      background:url(/assets/images/whatsapp60.png);\n    }\n.blink {\n      margin-top: 2.5%;\n      -webkit-animation: blinker 2s linear infinite;\n              animation: blinker 2s linear infinite;\n      color: #2c3e50;\n     }\n@-webkit-keyframes blinker {  \n      50% { opacity: 0; }\n     }\n@keyframes blinker {  \n      50% { opacity: 0; }\n     }\n.main-content {\n      padding-top: 6.5%;\n      padding-left: 2%;\n      padding-right: 2%;\n      min-height: 80vh;\n    }\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGRmLXZpZXdlci9kZXNrdG9wL2Rlc2t0b3AtcGRmLXZpZXdlci5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksZUFBZTtJQUNmLFdBQVc7SUFDWCxhQUFhO0FBQ2pCO0FBQ0E7SUFDSSxpQkFBaUI7SUFDakIsa0JBQWtCO0lBQ2xCLHdCQUF3QjtJQUN4QixvQkFBb0I7SUFDcEIsd0ZBQXdGO0FBQzVGO0FBQ0U7SUFDRSxnQkFBZ0I7SUFDaEIsbUJBQW1CO0lBQ25CLFdBQVc7SUFDWCxzQkFBc0I7QUFDMUI7QUFDSTtRQUNJLGlCQUFpQixDQUFDLGtCQUFrQixDQUFDLHdCQUF3QixDQUFDLG9CQUFvQjtRQUNsRixjQUFjLENBQXFDLFlBQVksQ0FBQywyQkFBbUIsQ0FBbkIsd0JBQW1CLENBQW5CLG1CQUFtQjtRQUNuRixpQkFBaUI7SUFDckI7QUFDQTtRQUNJLGVBQWU7SUFDbkI7QUFDQTtRQUNJLGtCQUFrQjtRQUNsQixjQUFjO1FBQ2QsZUFBZTtRQUNmLGtCQUFrQjtRQUNsQiw4QkFBOEI7UUFDOUIsWUFBWTtJQUNoQjtBQUNBO01BQ0UsaUJBQWlCO0lBQ25CO0FBQ0o7SUFDSSx3QkFBcUI7WUFBckIscUJBQXFCO0lBQ3JCLHVCQUFtQjtZQUFuQixtQkFBbUI7SUFDbkIsMEJBQXlCO1lBQXpCLHlCQUF5QjtJQUN6Qix1QkFBc0I7WUFBdEIsc0JBQXNCO0lBQ3RCLDBCQUF5QjtZQUF6Qix5QkFBeUI7QUFDN0I7QUFDQTtJQUNJLDBDQUEwQztJQUMxQyxrQ0FBa0MsRUFBRSxrREFBa0Q7U0FDakYsZ0VBQWdFO01BQ25FLDBDQUEwQyxFQUFFLHNCQUFzQjtBQUN4RTtBQUNBO0lBQ0ksV0FBVztJQUNYLFlBQVk7QUFDaEI7QUFDQTtJQUNJLGNBQWM7SUFDZCxtQkFBbUI7SUFDbkIsaUJBQWlCO0lBQ2pCLGNBQWM7SUFDZCxnQkFBZ0I7QUFDcEI7QUFFQTtDQUNDO0VBQ0Msa0JBQWtCO0NBQ25CO0NBQ0E7RUFDQywwQkFBMEI7Q0FDM0I7QUFDRDtBQUVBOztNQUVNLE1BQU0sbUJBQW1CLENBQUM7TUFDMUIsTUFBTSx5QkFBeUIsQ0FBQztNQUNoQyxNQUFNLDRCQUE0QjtNQUNsQyxPQUFPLG1CQUFtQixDQUFDO0lBQzdCO0FBRUE7O01BRUUsTUFBTSxtQkFBbUIsQ0FBQztNQUMxQixNQUFNLHlCQUF5QixDQUFDO01BQ2hDLE1BQU0sNEJBQTRCO01BQ2xDLE9BQU8sbUJBQW1CLENBQUM7SUFDN0I7QUFFQTtNQUNFLFdBQVc7TUFDWCxlQUFlO01BQ2YsV0FBVztNQUNYLE9BQU87SUFDVDtBQUNBO01BQ0UsY0FBYztNQUNkLFdBQVc7TUFDWCxZQUFZO01BQ1osa0JBQWtCO01BQ2xCLGtCQUFrQjtNQUNsQixrQkFBa0I7TUFDbEIsbUJBQW1CO01BQ25CLHdGQUF3RjtNQUN4RixlQUFlO01BRWYsNEJBQTRCO01BQzVCLGtCQUFrQjtJQUNwQjtBQUNBO01BQ0UsZUFBZTtNQUNmLGlCQUFpQjtNQUNqQiwrQkFBK0I7TUFDL0Isb0JBQW9CO0lBQ3RCO0FBQ0E7OztNQUdFLG9FQUFvRTtJQUN0RTtBQUNBO01BQ0UsV0FBVztNQUNYLFlBQVk7TUFDWixtQkFBbUI7TUFDbkIsVUFBVTtJQUNaO0FBQ0E7TUFDRSxlQUFlO01BQ2YsaUJBQWlCO01BQ2pCLCtCQUErQjtJQUNqQztBQUNBO01BQ0UsVUFBVTtNQUNWLFdBQVc7TUFDWCxZQUFZO01BQ1osbUJBQW1CO0lBQ3JCO0FBQ0E7TUFFRSxzQkFBc0I7SUFDeEI7QUFDQTtNQUVFLHNCQUFzQjtJQUN4QjtBQUNBO01BRUUsc0JBQXNCO0lBQ3hCO0FBQ0E7TUFFRSxzQkFBc0I7SUFDeEI7QUFDQTtNQUVFLHNCQUFzQjtJQUN4QjtBQUNBO01BRUUsdUJBQXVCO0lBQ3pCO0FBRUE7TUFDRSxxQkFBcUI7TUFDckIsZ0JBQWdCO01BQ2hCLGtCQUFrQjtNQUNsQix5QkFBeUI7TUFDekIsV0FBVztNQUNYLHNCQUFzQjtNQUN0QixlQUFlO01BQ2Ysa0JBQWtCO01BQ2xCLFVBQVU7TUFDVixnQkFBZ0I7TUFDaEIsaUJBQWlCO01BQ2pCLGtCQUFrQjtNQUNsQixVQUFVO01BQ1YsV0FBVztNQUNYLG1CQUFtQjtJQUNyQjtBQUVBO01BQ0Usd0NBQXdDO0lBQzFDO0FBQ0E7TUFDRSwwQ0FBMEM7SUFDNUM7QUFDQTtNQUNFLHlDQUF5QztJQUMzQztBQUNBO01BQ0UsNkNBQTZDO0lBQy9DO0FBQ0E7TUFDRSw2Q0FBNkM7SUFDL0M7QUFFQTtNQUNFLGdCQUFnQjtNQUNoQiw2Q0FBcUM7Y0FBckMscUNBQXFDO01BQ3JDLGNBQWM7S0FDZjtBQUNEO01BQ0UsTUFBTSxVQUFVLEVBQUU7S0FDbkI7QUFGRDtNQUNFLE1BQU0sVUFBVSxFQUFFO0tBQ25CO0FBQ0E7TUFDQyxpQkFBaUI7TUFDakIsZ0JBQWdCO01BQ2hCLGlCQUFpQjtNQUNqQixnQkFBZ0I7SUFDbEIiLCJmaWxlIjoic3JjL2FwcC9wZGYtdmlld2VyL2Rlc2t0b3AvZGVza3RvcC1wZGYtdmlld2VyLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpZnJhbWV7XG4gICAgbWF4LXdpZHRoOiAxMDAlO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogNjUwcHg7XG59XG4uanVtYm90cm9ue1xuICAgIHBhZGRpbmc6MHJlbSAwcmVtO1xuICAgIG1hcmdpbi1ib3R0b206MHJlbTtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiNlY2YwZjE7XG4gICAgYm9yZGVyLXJhZGl1czowLjNyZW07XG4gICAgYm94LXNoYWRvdzogMHB4IDVweCAxMXB4IC0ycHggcmdiYSgwLCAwLCAwLCAwLjE4KSwgMHB4IDRweCAxMnB4IC03cHggcmdiYSgwLCAwLCAwLCAwLjE1KTtcbn1cbiAgLnRhYmxlSGVhZGVye1xuICAgIG1hcmdpbi10b3A6IDc1cHg7XG4gICAgdGFibGUtbGF5b3V0OiBmaXhlZDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBib3JkZXItY29sbGFwc2U6IHVuc2V0O1xufVxuICAgIC5jb2wtc20tM3tcbiAgICAgICAgcGFkZGluZzoxcmVtIDByZW07bWFyZ2luLWJvdHRvbTowcmVtO2JhY2tncm91bmQtY29sb3I6I2VjZjBmMTtib3JkZXItcmFkaXVzOjAuM3JlbTtcbiAgICAgICAgbWF4LXdpZHRoOiAzNSU7LXdlYmtpdC1ib3gtZmxleDowOy1tcy1mbGV4OjAgMCAzNSU7ZmxleDowIDAgMzUlO2hlaWdodDogZml0LWNvbnRlbnQ7XG4gICAgICAgIG1hcmdpbi1sZWZ0OiBhdXRvO1xuICAgIH1cbiAgICAuY29sLXNtLTh7XG4gICAgICAgIG1hcmdpbjogaW5oZXJpdDtcbiAgICB9XG4gICAgLm5hdmJhciB7XG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgIGZsZXgtd3JhcDogd3JhcDtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gICAgICAgIHBhZGRpbmc6IDVweDtcbiAgICB9XG4gICAgLmZvbnRTaXple1xuICAgICAgZm9udC1zaXplOiAxLjdyZW07XG4gICAgfVxudWwsIG1lbnUsIGRpciB7XG4gICAgbWFyZ2luLWJsb2NrLXN0YXJ0OiAwO1xuICAgIG1hcmdpbi1ibG9jay1lbmQ6IDA7XG4gICAgbWFyZ2luLWlubGluZS1zdGFydDogMTVweDtcbiAgICBtYXJnaW4taW5saW5lLWVuZDogMHB4O1xuICAgIHBhZGRpbmctaW5saW5lLXN0YXJ0OiAwcHg7XG59XG4uYmctcHJpbWFyeSB7XG4gICAgLyogYmFja2dyb3VuZC1jb2xvcjogI2RmZWFlYyAhaW1wb3J0YW50OyAqL1xuICAgIGFuaW1hdGlvbjogY29sb3JjaGFuZ2UgNXMgaW5maW5pdGU7IC8qIGFuaW1hdGlvbi1uYW1lIGZvbGxvd2VkIGJ5IGR1cmF0aW9uIGluIHNlY29uZHMqL1xuICAgICAgICAgLyogeW91IGNvdWxkIGFsc28gdXNlIG1pbGxpc2Vjb25kcyAobXMpIG9yIHNvbWV0aGluZyBsaWtlIDIuNXMgKi9cbiAgICAgIC13ZWJraXQtYW5pbWF0aW9uOiBjb2xvcmNoYW5nZSA1cyBpbmZpbml0ZTsgLyogQ2hyb21lIGFuZCBTYWZhcmkgKi9cbn1cbiNmaXhlZC13aWR0aC1mbGFtaW5nbyB7ICBcbiAgICB3aWR0aDogMzVweDsgXG4gICAgaGVpZ2h0OiAzNXB4O1xufVxuLm5hdi1saW5rIHtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICBiYWNrZ3JvdW5kOiAjOGIwMDAwO1xuICAgIG1hcmdpbi1ib3R0b206IDMlO1xuICAgIG1hcmdpbi10b3A6IDglO1xuICAgIG1hcmdpbi1yaWdodDogMiU7XG59XG5cbkBtZWRpYSAobWF4LXdpZHRoOjcyMHB4KXtcblx0Ym9keXtcblx0XHR0ZXh0LWFsaWduOiBjZW50ZXI7XG5cdH1cblx0LmRpc3BsYXktNHtcblx0XHRmb250LXNpemU6IDMwcHggIWltcG9ydGFudDtcblx0fVxufVxuXG5Aa2V5ZnJhbWVzIGNvbG9yY2hhbmdlXG4gICAge1xuICAgICAgMCUgICB7YmFja2dyb3VuZDogIzJjM2U1MDt9XG4gICAgICAzMyUgIHtiYWNrZ3JvdW5kOiByZWJlY2NhcHVycGxlO31cbiAgICAgIDY2JSAge2JhY2tncm91bmQ6IHJnYigwLCAxNDMsIDExNCl9XG4gICAgICAxMDAlICB7YmFja2dyb3VuZDogIzhiMDAwMDt9XG4gICAgfVxuXG4gICAgQC13ZWJraXQta2V5ZnJhbWVzIGNvbG9yY2hhbmdlIC8qIFNhZmFyaSBhbmQgQ2hyb21lIC0gbmVjZXNzYXJ5IGR1cGxpY2F0ZSAqL1xuICAgIHtcbiAgICAgIDAlICAge2JhY2tncm91bmQ6ICMyYzNlNTA7fVxuICAgICAgMzMlICB7YmFja2dyb3VuZDogcmViZWNjYXB1cnBsZTt9XG4gICAgICA2NiUgIHtiYWNrZ3JvdW5kOiByZ2IoMCwgMTQzLCAxMTQpfVxuICAgICAgMTAwJSAge2JhY2tncm91bmQ6ICM4YjAwMDA7fVxuICAgIH1cblxuICAgIC5zYnV0dG9ucyB7XG4gICAgICBib3R0b206IDE1JTtcbiAgICAgIHBvc2l0aW9uOiBmaXhlZDtcbiAgICAgIG1hcmdpbjogMWVtO1xuICAgICAgbGVmdDogMDtcbiAgICB9XG4gICAgLnNidXR0b24ge1xuICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICB3aWR0aDogNjBweDtcbiAgICAgIGhlaWdodDogNjBweDtcbiAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgIGNvbG9yOiB0cmFuc3BhcmVudDtcbiAgICAgIG1hcmdpbjogMjBweCBhdXRvIDA7XG4gICAgICBib3gtc2hhZG93OiAwcHggNXB4IDExcHggLTJweCByZ2JhKDAsIDAsIDAsIDAuMTgpLCAwcHggNHB4IDEycHggLTdweCByZ2JhKDAsIDAsIDAsIDAuMTUpO1xuICAgICAgY3Vyc29yOiBwb2ludGVyO1xuICAgICAgLXdlYmtpdC10cmFuc2l0aW9uOiBhbGwgLjFzIGVhc2Utb3V0O1xuICAgICAgdHJhbnNpdGlvbjogYWxsIC4xcyBlYXNlLW91dDtcbiAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICB9XG4gICAgLnNidXR0b24gPiBpIHtcbiAgICAgIGZvbnQtc2l6ZTogMzhweDtcbiAgICAgIGxpbmUtaGVpZ2h0OiA2MHB4O1xuICAgICAgdHJhbnNpdGlvbjogYWxsIC4ycyBlYXNlLWluLW91dDtcbiAgICAgIHRyYW5zaXRpb24tZGVsYXk6IDJzO1xuICAgIH1cbiAgICAuc2J1dHRvbjphY3RpdmUsXG4gICAgLnNidXR0b246Zm9jdXMsXG4gICAgLnNidXR0b246aG92ZXIge1xuICAgICAgYm94LXNoYWRvdzogMCAwIDRweCByZ2JhKDAsIDAsIDAsIC4xNCksIDAgNHB4IDhweCByZ2JhKDAsIDAsIDAsIC4yOCk7XG4gICAgfVxuICAgIC5zYnV0dG9uOm5vdCg6bGFzdC1jaGlsZCkge1xuICAgICAgd2lkdGg6IDYwcHg7XG4gICAgICBoZWlnaHQ6IDYwcHg7XG4gICAgICBtYXJnaW46IDIwcHggYXV0byAwO1xuICAgICAgb3BhY2l0eTogMDtcbiAgICB9XG4gICAgLnNidXR0b246bm90KDpsYXN0LWNoaWxkKSA+IGkge1xuICAgICAgZm9udC1zaXplOiAyNXB4O1xuICAgICAgbGluZS1oZWlnaHQ6IDYwcHg7XG4gICAgICB0cmFuc2l0aW9uOiBhbGwgLjNzIGVhc2UtaW4tb3V0O1xuICAgIH1cbiAgICAuc2J1dHRvbjpub3QoOmxhc3QtY2hpbGQpIHtcbiAgICAgIG9wYWNpdHk6IDE7XG4gICAgICB3aWR0aDogNjBweDtcbiAgICAgIGhlaWdodDogNjBweDtcbiAgICAgIG1hcmdpbjogMTVweCBhdXRvIDA7XG4gICAgfVxuICAgIC5zYnV0dG9uOm50aC1sYXN0LWNoaWxkKDEpIHtcbiAgICAgIC13ZWJraXQtdHJhbnNpdGlvbi1kZWxheTogMjVtcztcbiAgICAgIHRyYW5zaXRpb24tZGVsYXk6IDI1bXM7XG4gICAgfVxuICAgIC5zYnV0dG9uOm5vdCg6bGFzdC1jaGlsZCk6bnRoLWxhc3QtY2hpbGQoMikge1xuICAgICAgLXdlYmtpdC10cmFuc2l0aW9uLWRlbGF5OiAyMG1zO1xuICAgICAgdHJhbnNpdGlvbi1kZWxheTogMjBtcztcbiAgICB9XG4gICAgLnNidXR0b246bm90KDpsYXN0LWNoaWxkKTpudGgtbGFzdC1jaGlsZCgzKSB7XG4gICAgICAtd2Via2l0LXRyYW5zaXRpb24tZGVsYXk6IDQwbXM7XG4gICAgICB0cmFuc2l0aW9uLWRlbGF5OiA0MG1zO1xuICAgIH1cbiAgICAuc2J1dHRvbjpub3QoOmxhc3QtY2hpbGQpOm50aC1sYXN0LWNoaWxkKDQpIHtcbiAgICAgIC13ZWJraXQtdHJhbnNpdGlvbi1kZWxheTogNjBtcztcbiAgICAgIHRyYW5zaXRpb24tZGVsYXk6IDYwbXM7XG4gICAgfVxuICAgIC5zYnV0dG9uOm5vdCg6bGFzdC1jaGlsZCk6bnRoLWxhc3QtY2hpbGQoNSkge1xuICAgICAgLXdlYmtpdC10cmFuc2l0aW9uLWRlbGF5OiA4MG1zO1xuICAgICAgdHJhbnNpdGlvbi1kZWxheTogODBtcztcbiAgICB9XG4gICAgLnNidXR0b246bm90KDpsYXN0LWNoaWxkKTpudGgtbGFzdC1jaGlsZCg2KSB7XG4gICAgICAtd2Via2l0LXRyYW5zaXRpb24tZGVsYXk6IDEwMG1zO1xuICAgICAgdHJhbnNpdGlvbi1kZWxheTogMTAwbXM7XG4gICAgfVxuICAgICBcbiAgICBbdG9vbHRpcF06YmVmb3JlIHtcbiAgICAgIGZvbnQtZmFtaWx5OiAnUm9ib3RvJztcbiAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgICBib3JkZXItcmFkaXVzOiAycHg7XG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjNTg1ODU4O1xuICAgICAgY29sb3I6ICNmZmY7XG4gICAgICBjb250ZW50OiBhdHRyKHRvb2x0aXApO1xuICAgICAgZm9udC1zaXplOiAxMnB4O1xuICAgICAgdmlzaWJpbGl0eTogaGlkZGVuO1xuICAgICAgb3BhY2l0eTogMDtcbiAgICAgIHBhZGRpbmc6IDVweCA3cHg7XG4gICAgICBtYXJnaW4tbGVmdDogMTBweDtcbiAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgIGxlZnQ6IDEwMCU7XG4gICAgICBib3R0b206IDIwJTtcbiAgICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gICAgfVxuICAgICBcbiAgICAuc2J1dHRvbi5tYWluc2J1dHRvbiB7XG4gICAgICBiYWNrZ3JvdW5kOnVybCgvYXNzZXRzL2ltYWdlcy9zaGFyZS5qcGcpO1xuICAgIH1cbiAgICAuc2J1dHRvbi5ncGx1cyB7XG4gICAgICBiYWNrZ3JvdW5kOnVybCgvYXNzZXRzL2ltYWdlcy9nbWFpbDYwLnBuZyk7XG4gICAgfVxuICAgIC5zYnV0dG9uLnR3aXR0IHtcbiAgICAgIGJhY2tncm91bmQ6dXJsKC9hc3NldHMvaW1hZ2VzL21lc3M2MC5wbmcpO1xuICAgIH1cbiAgICAuc2J1dHRvbi5mYiB7XG4gICAgICBiYWNrZ3JvdW5kOnVybCgvYXNzZXRzL2ltYWdlcy9mYWNlYm9vazYwLnBuZyk7XG4gICAgfVxuICAgIC5zYnV0dG9uLndoYXRzYXBwIHtcbiAgICAgIGJhY2tncm91bmQ6dXJsKC9hc3NldHMvaW1hZ2VzL3doYXRzYXBwNjAucG5nKTtcbiAgICB9XG4gIFxuICAgIC5ibGluayB7XG4gICAgICBtYXJnaW4tdG9wOiAyLjUlO1xuICAgICAgYW5pbWF0aW9uOiBibGlua2VyIDJzIGxpbmVhciBpbmZpbml0ZTtcbiAgICAgIGNvbG9yOiAjMmMzZTUwO1xuICAgICB9XG4gICAgQGtleWZyYW1lcyBibGlua2VyIHsgIFxuICAgICAgNTAlIHsgb3BhY2l0eTogMDsgfVxuICAgICB9XG4gICAgIC5tYWluLWNvbnRlbnQge1xuICAgICAgcGFkZGluZy10b3A6IDYuNSU7XG4gICAgICBwYWRkaW5nLWxlZnQ6IDIlO1xuICAgICAgcGFkZGluZy1yaWdodDogMiU7XG4gICAgICBtaW4taGVpZ2h0OiA4MHZoO1xuICAgIH0iXX0= */");

/***/ }),

/***/ "./src/app/pdf-viewer/desktop/desktop-pdf-viewer.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/pdf-viewer/desktop/desktop-pdf-viewer.component.ts ***!
  \********************************************************************/
/*! exports provided: PdfViewerComponentDesktop */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PdfViewerComponentDesktop", function() { return PdfViewerComponentDesktop; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var rxjs_Rx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/Rx */ "./node_modules/rxjs-compat/_esm2015/Rx.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _app_header_header_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../app-header/header.service */ "./src/app/app-header/header.service.ts");



;






let PdfViewerComponentDesktop = class PdfViewerComponentDesktop {
    constructor(http, activatedRoute, router, meta, title, sanitizer, headerService) {
        this.http = http;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.sanitizer = sanitizer;
        this.headerService = headerService;
        this.keys = [];
        this.nav = [];
        this.navIds = [];
        this.navCount = [];
        this.deviceInfo = {};
        this.tabUrl = 'https://api.finalrevise.com/finalrevise/v1/relatedlinks/';
        this.whatsAppUrl = 'https://api.finalrevise.com/finalrevise/v1/whatsApp/';
        this.metaUrl = 'https://api.finalrevise.com/finalrevise/v1/meta/';
        this.title = title;
        this.meta = meta;
    }
    transform(url) {
        return this.sanitizer.bypassSecurityTrustResourceUrl(url);
    }
    ngOnInit() {
        // subscribe to router event
        this.activatedRoute.params.subscribe((params) => {
            this.titleText = params['title'];
            this.urlTitle = params['urlTitle'];
            this.subId = params['subId'];
            this.pdfUrl = "https://drive.google.com/file/d/" + this.subId + "/preview";
        });
        this.tabUrl = this.tabUrl + this.urlTitle;
        this.whatsAppUrl = this.whatsAppUrl + this.urlTitle;
        this.metaUrl = this.metaUrl + this.subId;
        this.getTabData();
        this.getWhatsAppUrl();
        this.getMetaUrl();
    }
    shareClicked() {
        if (this.isClickedShare) {
            this.isClickedShare = false;
        }
        else {
            this.isClickedShare = true;
        }
    }
    getWhatsAppUrl() {
        this.getWhatsAppData().subscribe(data => {
            this.groupUrl = data.data.whatsAppUrl;
        });
    }
    getWhatsAppData() {
        return this.http.get(this.whatsAppUrl);
    }
    getTData() {
        return this.http.get(this.tabUrl);
    }
    getMetaUrl() {
        this.getMetaData().subscribe(data => {
            this.metaData = data;
            this.setTitleMeta();
        });
    }
    getMetaData() {
        return this.http.get(this.metaUrl);
    }
    getTabData() {
        this.getTData().subscribe(data => {
            this.tags = data.data.related;
            if (data) {
                this.headerService.setRelatedTopics(data);
            }
        });
    }
    openTag(tab) {
        if (tab.isChild == 1) {
            this.router.navigateByUrl('/detail-list/' + tab.linkTitle);
        }
        else {
            if (tab.linkTitle.includes('book')) {
                this.router.navigateByUrl('/study-met/' + tab.linkTitle);
            }
            else {
                this.router.navigateByUrl('/detail/' + tab.linkTitle);
            }
        }
    }
    replaceAllSocial(str) {
        var find = '-';
        var re = new RegExp(find, 'g');
        return str.replace(re, ' ');
    }
    openFacebook() {
        window.open("https://www.facebook.com/sharer/sharer.php?u=http://finalrevise.com/detail/" + this.titleText);
    }
    openWhatsApp() {
        window.open("https://api.whatsapp.com/send?text=*Download%20" + this.titleText + "*%0a%0a" + "http://finalrevise.com/detail/" + this.titleText + "/" +
            "%0a%0a*Download%20FinalRevise%20App%20:%20https://play.google.com/store/apps/details?id%3Dcom.finalrevise.android*");
    }
    openMail() {
        window.open("mailto:?subject=FinalRevise%20Study%20Material%20for%20" + this.nav[this.navCount - 1] +
            "&body=Download%20" + this.replaceAllSocial(this.titleText) + "%0a%0a" + "http://finalrevise.com/detail/" + this.titleText +
            "%0a%0a Download FinalRevise App : https://play.google.com/store/apps/details?id=com.finalrevise.android");
    }
    openMessenger() {
        if (_angular_common__WEBPACK_IMPORTED_MODULE_6__["isPlatformBrowser"]) {
            window.open("https://m.me/finalrevise?u=http://finalrevise.com/detail/" + this.titleText);
        }
        else {
            window.open('fb-messenger://share?link=' + encodeURIComponent("http://finalrevise.com/detail/" + this.titleText) + '&app_id='
                + encodeURIComponent("358637287840510"));
        }
    }
    setTitleMeta() {
        this.title.setTitle(this.metaData.title);
        this.meta.addTags([
            { name: 'author', content: 'finalrevise.com' },
            { name: 'keywords', content: this.metaData.metaKeywords },
            {
                name: 'description', content: this.metaData.metaDesc
            }
        ]);
    }
    replaceAll(str, replacement) {
        str = str.split('(').join('');
        str = str.split(')').join('');
        str = str.split('.').join('');
        str = str.split('/').join('');
        str = str.split('|').join(':');
        str = str.split(' ').join(replacement);
        str = str.split('---').join(replacement);
        return str.split('--').join(replacement);
    }
    ;
};
PdfViewerComponentDesktop.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__["Meta"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__["Title"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__["DomSanitizer"] },
    { type: _app_header_header_service__WEBPACK_IMPORTED_MODULE_7__["HeaderService"] }
];
PdfViewerComponentDesktop = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'pdf-viewer-desktop',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./desktop-pdf-viewer.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pdf-viewer/desktop/desktop-pdf-viewer.component.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./desktop-pdf-viewer.component.css */ "./src/app/pdf-viewer/desktop/desktop-pdf-viewer.component.css")).default]
    })
], PdfViewerComponentDesktop);



/***/ }),

/***/ "./src/app/pdf-viewer/mobile/mobile-pdf-viewer.component.css":
/*!*******************************************************************!*\
  !*** ./src/app/pdf-viewer/mobile/mobile-pdf-viewer.component.css ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("iframe{\n    max-width: 100%;\n    width: 100%;\n    height: 650px;\n}\n.jumbotron{\n    padding:0rem 0rem;\n    margin-bottom:0rem;\n    background-color:#ecf0f1;\n    border-radius:0.3rem;\n    box-shadow: 0px 5px 11px -2px rgba(0, 0, 0, 0.18), 0px 4px 12px -7px rgba(0, 0, 0, 0.15);\n}\nul, menu, dir {\n    -webkit-margin-before: 0;\n            margin-block-start: 0;\n    -webkit-margin-after: 0;\n            margin-block-end: 0;\n    -webkit-margin-start: 15px;\n            margin-inline-start: 15px;\n    -webkit-margin-end: 0px;\n            margin-inline-end: 0px;\n    -webkit-padding-start: 0px;\n            padding-inline-start: 0px;\n}\n.bg-primary {\n    /* background-color: #dfeaec !important; */\n    animation: colorchange 5s infinite; /* animation-name followed by duration in seconds*/\n         /* you could also use milliseconds (ms) or something like 2.5s */\n      -webkit-animation: colorchange 5s infinite; /* Chrome and Safari */\n}\n#fixed-width-flamingo {  \n    width: 35px; \n    height: 35px;\n}\n.tableHeader{\n    margin-top: 50px;\n    table-layout: fixed;\n    width: 100%;\n    border-collapse: unset;\n}\n.col-sm-3{\n        padding:1rem 0rem;margin-bottom:0rem;background-color:#ecf0f1;border-radius:0.3rem;\n        -webkit-box-flex:0;height: -webkit-fit-content;height: -moz-fit-content;height: fit-content;margin: 3%;\n    }\n.col-sm-8{\n        padding:0rem 0rem;\n        margin-left: 3%;\n        margin-right: 3%;\n    }\n.navbar {\n        position: relative;\n        display: block;\n        flex-wrap: wrap;\n        text-align: center;\n        justify-content: space-between;\n        padding: 5px;\n    }\n.container-fluid{\n      padding: 0%;\n    }\n.fontSize{\n      font-size: 1.2rem;\n    }\n.nav-link {\n    display: block;\n    background: #8b0000;\n    margin-bottom: 3%;\n    margin-top: 8%;\n    margin-right: 2%;\n}\n@media (max-width:720px){\n\tbody{\n\t\ttext-align: center;\n\t}\n\t.display-4{\n\t\tfont-size: 30px !important;\n\t}\n}\n@keyframes colorchange\n    {\n      0%   {background: #2c3e50;}\n      33%  {background: rebeccapurple;}\n      66%  {background: rgb(0, 143, 114)}\n      100%  {background: #8b0000;}\n    }\n@-webkit-keyframes colorchange /* Safari and Chrome - necessary duplicate */\n    {\n      0%   {background: #2c3e50;}\n      33%  {background: rebeccapurple;}\n      66%  {background: rgb(0, 143, 114)}\n      100%  {background: #8b0000;}\n    }\n.sbuttons {\n      bottom: 15%;\n      position: fixed;\n      margin: 1em;\n      left: 0;\n    }\n.sbutton {\n      display: block;\n      width: 60px;\n      height: 60px;\n      border-radius: 50%;\n      text-align: center;\n      color: transparent;\n      margin: 20px auto 0;\n      box-shadow: 0px 5px 11px -2px rgba(0, 0, 0, 0.18), 0px 4px 12px -7px rgba(0, 0, 0, 0.15);\n      cursor: pointer;\n      transition: all .1s ease-out;\n      position: relative;\n    }\n.sbutton > i {\n      font-size: 38px;\n      line-height: 60px;\n      transition: all .2s ease-in-out;\n      transition-delay: 2s;\n    }\n.sbutton:active,\n    .sbutton:focus,\n    .sbutton:hover {\n      box-shadow: 0 0 4px rgba(0, 0, 0, .14), 0 4px 8px rgba(0, 0, 0, .28);\n    }\n.sbutton:not(:last-child) {\n      width: 60px;\n      height: 60px;\n      margin: 20px auto 0;\n      opacity: 0;\n    }\n.sbutton:not(:last-child) > i {\n      font-size: 25px;\n      line-height: 60px;\n      transition: all .3s ease-in-out;\n    }\n.sbutton:not(:last-child) {\n      opacity: 1;\n      width: 60px;\n      height: 60px;\n      margin: 15px auto 0;\n    }\n.sbutton:nth-last-child(1) {\n      transition-delay: 25ms;\n    }\n.sbutton:not(:last-child):nth-last-child(2) {\n      transition-delay: 20ms;\n    }\n.sbutton:not(:last-child):nth-last-child(3) {\n      transition-delay: 40ms;\n    }\n.sbutton:not(:last-child):nth-last-child(4) {\n      transition-delay: 60ms;\n    }\n.sbutton:not(:last-child):nth-last-child(5) {\n      transition-delay: 80ms;\n    }\n.sbutton:not(:last-child):nth-last-child(6) {\n      transition-delay: 100ms;\n    }\n[tooltip]:before {\n      font-family: 'Roboto';\n      font-weight: 600;\n      border-radius: 2px;\n      background-color: #585858;\n      color: #fff;\n      content: attr(tooltip);\n      font-size: 12px;\n      visibility: hidden;\n      opacity: 0;\n      padding: 5px 7px;\n      margin-left: 10px;\n      position: absolute;\n      left: 100%;\n      bottom: 20%;\n      white-space: nowrap;\n    }\n.sbutton.mainsbutton {\n      background:url(/assets/images/share.jpg);\n    }\n.sbutton.gplus {\n      background:url(/assets/images/gmail60.png);\n    }\n.sbutton.twitt {\n      background:url(/assets/images/mess60.png);\n    }\n.sbutton.fb {\n      background:url(/assets/images/facebook60.png);\n    }\n.sbutton.whatsapp {\n      background:url(/assets/images/whatsapp60.png);\n    }\n.blink {\n      margin-top: 2.5%;\n      -webkit-animation: blinker 2s linear infinite;\n              animation: blinker 2s linear infinite;\n      color: #2c3e50;\n     }\n@-webkit-keyframes blinker {  \n      50% { opacity: 0; }\n     }\n@keyframes blinker {  \n      50% { opacity: 0; }\n     }\n.main-content {\n      padding-top: 15%;\n      min-height: 80vh;\n      padding-left: 5%;\n      padding-right: 5%;\n      background-image: url('/assets/images/bg.jpg');\n      padding-bottom: 5%;\n    }\n.center {\n      display: block;\n      margin-left: auto;\n      margin-right: auto;\n      width: 85%;\n      margin-top: 2.5%;\n      -webkit-animation: blinker 2s linear infinite;\n              animation: blinker 2s linear infinite;\n    }\n.goBack{\n      color: #008f72;\n      font-size: large;\n      text-shadow: 2px 2px white;\n    }\nhr{\n      margin: 0;  \n    }\n.loader {\n      background: url('/assets/images/loading_small.gif') 50% 50% no-repeat rgb(249,249,249);\n    } \n  \n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGRmLXZpZXdlci9tb2JpbGUvbW9iaWxlLXBkZi12aWV3ZXIuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLGVBQWU7SUFDZixXQUFXO0lBQ1gsYUFBYTtBQUNqQjtBQUNBO0lBQ0ksaUJBQWlCO0lBQ2pCLGtCQUFrQjtJQUNsQix3QkFBd0I7SUFDeEIsb0JBQW9CO0lBQ3BCLHdGQUF3RjtBQUM1RjtBQUNBO0lBQ0ksd0JBQXFCO1lBQXJCLHFCQUFxQjtJQUNyQix1QkFBbUI7WUFBbkIsbUJBQW1CO0lBQ25CLDBCQUF5QjtZQUF6Qix5QkFBeUI7SUFDekIsdUJBQXNCO1lBQXRCLHNCQUFzQjtJQUN0QiwwQkFBeUI7WUFBekIseUJBQXlCO0FBQzdCO0FBQ0E7SUFDSSwwQ0FBMEM7SUFDMUMsa0NBQWtDLEVBQUUsa0RBQWtEO1NBQ2pGLGdFQUFnRTtNQUNuRSwwQ0FBMEMsRUFBRSxzQkFBc0I7QUFDeEU7QUFDQTtJQUNJLFdBQVc7SUFDWCxZQUFZO0FBQ2hCO0FBQ0U7SUFDRSxnQkFBZ0I7SUFDaEIsbUJBQW1CO0lBQ25CLFdBQVc7SUFDWCxzQkFBc0I7QUFDMUI7QUFDSTtRQUNJLGlCQUFpQixDQUFDLGtCQUFrQixDQUFDLHdCQUF3QixDQUFDLG9CQUFvQjtRQUNsRixrQkFBa0IsQ0FBQywyQkFBbUIsQ0FBbkIsd0JBQW1CLENBQW5CLG1CQUFtQixDQUFDLFVBQVU7SUFDckQ7QUFDQTtRQUNJLGlCQUFpQjtRQUNqQixlQUFlO1FBQ2YsZ0JBQWdCO0lBQ3BCO0FBQ0E7UUFDSSxrQkFBa0I7UUFDbEIsY0FBYztRQUNkLGVBQWU7UUFDZixrQkFBa0I7UUFDbEIsOEJBQThCO1FBQzlCLFlBQVk7SUFDaEI7QUFDQTtNQUNFLFdBQVc7SUFDYjtBQUNBO01BQ0UsaUJBQWlCO0lBQ25CO0FBQ0o7SUFDSSxjQUFjO0lBQ2QsbUJBQW1CO0lBQ25CLGlCQUFpQjtJQUNqQixjQUFjO0lBQ2QsZ0JBQWdCO0FBQ3BCO0FBRUE7Q0FDQztFQUNDLGtCQUFrQjtDQUNuQjtDQUNBO0VBQ0MsMEJBQTBCO0NBQzNCO0FBQ0Q7QUFFQTs7TUFFTSxNQUFNLG1CQUFtQixDQUFDO01BQzFCLE1BQU0seUJBQXlCLENBQUM7TUFDaEMsTUFBTSw0QkFBNEI7TUFDbEMsT0FBTyxtQkFBbUIsQ0FBQztJQUM3QjtBQUVBOztNQUVFLE1BQU0sbUJBQW1CLENBQUM7TUFDMUIsTUFBTSx5QkFBeUIsQ0FBQztNQUNoQyxNQUFNLDRCQUE0QjtNQUNsQyxPQUFPLG1CQUFtQixDQUFDO0lBQzdCO0FBRUE7TUFDRSxXQUFXO01BQ1gsZUFBZTtNQUNmLFdBQVc7TUFDWCxPQUFPO0lBQ1Q7QUFDQTtNQUNFLGNBQWM7TUFDZCxXQUFXO01BQ1gsWUFBWTtNQUNaLGtCQUFrQjtNQUNsQixrQkFBa0I7TUFDbEIsa0JBQWtCO01BQ2xCLG1CQUFtQjtNQUNuQix3RkFBd0Y7TUFDeEYsZUFBZTtNQUVmLDRCQUE0QjtNQUM1QixrQkFBa0I7SUFDcEI7QUFDQTtNQUNFLGVBQWU7TUFDZixpQkFBaUI7TUFDakIsK0JBQStCO01BQy9CLG9CQUFvQjtJQUN0QjtBQUNBOzs7TUFHRSxvRUFBb0U7SUFDdEU7QUFDQTtNQUNFLFdBQVc7TUFDWCxZQUFZO01BQ1osbUJBQW1CO01BQ25CLFVBQVU7SUFDWjtBQUNBO01BQ0UsZUFBZTtNQUNmLGlCQUFpQjtNQUNqQiwrQkFBK0I7SUFDakM7QUFDQTtNQUNFLFVBQVU7TUFDVixXQUFXO01BQ1gsWUFBWTtNQUNaLG1CQUFtQjtJQUNyQjtBQUNBO01BRUUsc0JBQXNCO0lBQ3hCO0FBQ0E7TUFFRSxzQkFBc0I7SUFDeEI7QUFDQTtNQUVFLHNCQUFzQjtJQUN4QjtBQUNBO01BRUUsc0JBQXNCO0lBQ3hCO0FBQ0E7TUFFRSxzQkFBc0I7SUFDeEI7QUFDQTtNQUVFLHVCQUF1QjtJQUN6QjtBQUVBO01BQ0UscUJBQXFCO01BQ3JCLGdCQUFnQjtNQUNoQixrQkFBa0I7TUFDbEIseUJBQXlCO01BQ3pCLFdBQVc7TUFDWCxzQkFBc0I7TUFDdEIsZUFBZTtNQUNmLGtCQUFrQjtNQUNsQixVQUFVO01BQ1YsZ0JBQWdCO01BQ2hCLGlCQUFpQjtNQUNqQixrQkFBa0I7TUFDbEIsVUFBVTtNQUNWLFdBQVc7TUFDWCxtQkFBbUI7SUFDckI7QUFFQTtNQUNFLHdDQUF3QztJQUMxQztBQUNBO01BQ0UsMENBQTBDO0lBQzVDO0FBQ0E7TUFDRSx5Q0FBeUM7SUFDM0M7QUFDQTtNQUNFLDZDQUE2QztJQUMvQztBQUNBO01BQ0UsNkNBQTZDO0lBQy9DO0FBRUE7TUFDRSxnQkFBZ0I7TUFDaEIsNkNBQXFDO2NBQXJDLHFDQUFxQztNQUNyQyxjQUFjO0tBQ2Y7QUFDRDtNQUNFLE1BQU0sVUFBVSxFQUFFO0tBQ25CO0FBRkQ7TUFDRSxNQUFNLFVBQVUsRUFBRTtLQUNuQjtBQUNBO01BQ0MsZ0JBQWdCO01BQ2hCLGdCQUFnQjtNQUNoQixnQkFBZ0I7TUFDaEIsaUJBQWlCO01BQ2pCLDhDQUE4QztNQUM5QyxrQkFBa0I7SUFDcEI7QUFDQTtNQUNFLGNBQWM7TUFDZCxpQkFBaUI7TUFDakIsa0JBQWtCO01BQ2xCLFVBQVU7TUFDVixnQkFBZ0I7TUFDaEIsNkNBQXFDO2NBQXJDLHFDQUFxQztJQUN2QztBQUNBO01BQ0UsY0FBYztNQUNkLGdCQUFnQjtNQUNoQiwwQkFBMEI7SUFDNUI7QUFDQTtNQUNFLFNBQVM7SUFDWDtBQUVBO01BQ0Usc0ZBQXNGO0lBQ3hGIiwiZmlsZSI6InNyYy9hcHAvcGRmLXZpZXdlci9tb2JpbGUvbW9iaWxlLXBkZi12aWV3ZXIuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbImlmcmFtZXtcbiAgICBtYXgtd2lkdGg6IDEwMCU7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgaGVpZ2h0OiA2NTBweDtcbn1cbi5qdW1ib3Ryb257XG4gICAgcGFkZGluZzowcmVtIDByZW07XG4gICAgbWFyZ2luLWJvdHRvbTowcmVtO1xuICAgIGJhY2tncm91bmQtY29sb3I6I2VjZjBmMTtcbiAgICBib3JkZXItcmFkaXVzOjAuM3JlbTtcbiAgICBib3gtc2hhZG93OiAwcHggNXB4IDExcHggLTJweCByZ2JhKDAsIDAsIDAsIDAuMTgpLCAwcHggNHB4IDEycHggLTdweCByZ2JhKDAsIDAsIDAsIDAuMTUpO1xufVxudWwsIG1lbnUsIGRpciB7XG4gICAgbWFyZ2luLWJsb2NrLXN0YXJ0OiAwO1xuICAgIG1hcmdpbi1ibG9jay1lbmQ6IDA7XG4gICAgbWFyZ2luLWlubGluZS1zdGFydDogMTVweDtcbiAgICBtYXJnaW4taW5saW5lLWVuZDogMHB4O1xuICAgIHBhZGRpbmctaW5saW5lLXN0YXJ0OiAwcHg7XG59XG4uYmctcHJpbWFyeSB7XG4gICAgLyogYmFja2dyb3VuZC1jb2xvcjogI2RmZWFlYyAhaW1wb3J0YW50OyAqL1xuICAgIGFuaW1hdGlvbjogY29sb3JjaGFuZ2UgNXMgaW5maW5pdGU7IC8qIGFuaW1hdGlvbi1uYW1lIGZvbGxvd2VkIGJ5IGR1cmF0aW9uIGluIHNlY29uZHMqL1xuICAgICAgICAgLyogeW91IGNvdWxkIGFsc28gdXNlIG1pbGxpc2Vjb25kcyAobXMpIG9yIHNvbWV0aGluZyBsaWtlIDIuNXMgKi9cbiAgICAgIC13ZWJraXQtYW5pbWF0aW9uOiBjb2xvcmNoYW5nZSA1cyBpbmZpbml0ZTsgLyogQ2hyb21lIGFuZCBTYWZhcmkgKi9cbn1cbiNmaXhlZC13aWR0aC1mbGFtaW5nbyB7ICBcbiAgICB3aWR0aDogMzVweDsgXG4gICAgaGVpZ2h0OiAzNXB4O1xufVxuICAudGFibGVIZWFkZXJ7XG4gICAgbWFyZ2luLXRvcDogNTBweDtcbiAgICB0YWJsZS1sYXlvdXQ6IGZpeGVkO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGJvcmRlci1jb2xsYXBzZTogdW5zZXQ7XG59XG4gICAgLmNvbC1zbS0ze1xuICAgICAgICBwYWRkaW5nOjFyZW0gMHJlbTttYXJnaW4tYm90dG9tOjByZW07YmFja2dyb3VuZC1jb2xvcjojZWNmMGYxO2JvcmRlci1yYWRpdXM6MC4zcmVtO1xuICAgICAgICAtd2Via2l0LWJveC1mbGV4OjA7aGVpZ2h0OiBmaXQtY29udGVudDttYXJnaW46IDMlO1xuICAgIH1cbiAgICAuY29sLXNtLTh7XG4gICAgICAgIHBhZGRpbmc6MHJlbSAwcmVtO1xuICAgICAgICBtYXJnaW4tbGVmdDogMyU7XG4gICAgICAgIG1hcmdpbi1yaWdodDogMyU7XG4gICAgfVxuICAgIC5uYXZiYXIge1xuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICBmbGV4LXdyYXA6IHdyYXA7XG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgICAgICBwYWRkaW5nOiA1cHg7XG4gICAgfVxuICAgIC5jb250YWluZXItZmx1aWR7XG4gICAgICBwYWRkaW5nOiAwJTtcbiAgICB9XG4gICAgLmZvbnRTaXple1xuICAgICAgZm9udC1zaXplOiAxLjJyZW07XG4gICAgfVxuLm5hdi1saW5rIHtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICBiYWNrZ3JvdW5kOiAjOGIwMDAwO1xuICAgIG1hcmdpbi1ib3R0b206IDMlO1xuICAgIG1hcmdpbi10b3A6IDglO1xuICAgIG1hcmdpbi1yaWdodDogMiU7XG59XG5cbkBtZWRpYSAobWF4LXdpZHRoOjcyMHB4KXtcblx0Ym9keXtcblx0XHR0ZXh0LWFsaWduOiBjZW50ZXI7XG5cdH1cblx0LmRpc3BsYXktNHtcblx0XHRmb250LXNpemU6IDMwcHggIWltcG9ydGFudDtcblx0fVxufVxuXG5Aa2V5ZnJhbWVzIGNvbG9yY2hhbmdlXG4gICAge1xuICAgICAgMCUgICB7YmFja2dyb3VuZDogIzJjM2U1MDt9XG4gICAgICAzMyUgIHtiYWNrZ3JvdW5kOiByZWJlY2NhcHVycGxlO31cbiAgICAgIDY2JSAge2JhY2tncm91bmQ6IHJnYigwLCAxNDMsIDExNCl9XG4gICAgICAxMDAlICB7YmFja2dyb3VuZDogIzhiMDAwMDt9XG4gICAgfVxuXG4gICAgQC13ZWJraXQta2V5ZnJhbWVzIGNvbG9yY2hhbmdlIC8qIFNhZmFyaSBhbmQgQ2hyb21lIC0gbmVjZXNzYXJ5IGR1cGxpY2F0ZSAqL1xuICAgIHtcbiAgICAgIDAlICAge2JhY2tncm91bmQ6ICMyYzNlNTA7fVxuICAgICAgMzMlICB7YmFja2dyb3VuZDogcmViZWNjYXB1cnBsZTt9XG4gICAgICA2NiUgIHtiYWNrZ3JvdW5kOiByZ2IoMCwgMTQzLCAxMTQpfVxuICAgICAgMTAwJSAge2JhY2tncm91bmQ6ICM4YjAwMDA7fVxuICAgIH1cblxuICAgIC5zYnV0dG9ucyB7XG4gICAgICBib3R0b206IDE1JTtcbiAgICAgIHBvc2l0aW9uOiBmaXhlZDtcbiAgICAgIG1hcmdpbjogMWVtO1xuICAgICAgbGVmdDogMDtcbiAgICB9XG4gICAgLnNidXR0b24ge1xuICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICB3aWR0aDogNjBweDtcbiAgICAgIGhlaWdodDogNjBweDtcbiAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgIGNvbG9yOiB0cmFuc3BhcmVudDtcbiAgICAgIG1hcmdpbjogMjBweCBhdXRvIDA7XG4gICAgICBib3gtc2hhZG93OiAwcHggNXB4IDExcHggLTJweCByZ2JhKDAsIDAsIDAsIDAuMTgpLCAwcHggNHB4IDEycHggLTdweCByZ2JhKDAsIDAsIDAsIDAuMTUpO1xuICAgICAgY3Vyc29yOiBwb2ludGVyO1xuICAgICAgLXdlYmtpdC10cmFuc2l0aW9uOiBhbGwgLjFzIGVhc2Utb3V0O1xuICAgICAgdHJhbnNpdGlvbjogYWxsIC4xcyBlYXNlLW91dDtcbiAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICB9XG4gICAgLnNidXR0b24gPiBpIHtcbiAgICAgIGZvbnQtc2l6ZTogMzhweDtcbiAgICAgIGxpbmUtaGVpZ2h0OiA2MHB4O1xuICAgICAgdHJhbnNpdGlvbjogYWxsIC4ycyBlYXNlLWluLW91dDtcbiAgICAgIHRyYW5zaXRpb24tZGVsYXk6IDJzO1xuICAgIH1cbiAgICAuc2J1dHRvbjphY3RpdmUsXG4gICAgLnNidXR0b246Zm9jdXMsXG4gICAgLnNidXR0b246aG92ZXIge1xuICAgICAgYm94LXNoYWRvdzogMCAwIDRweCByZ2JhKDAsIDAsIDAsIC4xNCksIDAgNHB4IDhweCByZ2JhKDAsIDAsIDAsIC4yOCk7XG4gICAgfVxuICAgIC5zYnV0dG9uOm5vdCg6bGFzdC1jaGlsZCkge1xuICAgICAgd2lkdGg6IDYwcHg7XG4gICAgICBoZWlnaHQ6IDYwcHg7XG4gICAgICBtYXJnaW46IDIwcHggYXV0byAwO1xuICAgICAgb3BhY2l0eTogMDtcbiAgICB9XG4gICAgLnNidXR0b246bm90KDpsYXN0LWNoaWxkKSA+IGkge1xuICAgICAgZm9udC1zaXplOiAyNXB4O1xuICAgICAgbGluZS1oZWlnaHQ6IDYwcHg7XG4gICAgICB0cmFuc2l0aW9uOiBhbGwgLjNzIGVhc2UtaW4tb3V0O1xuICAgIH1cbiAgICAuc2J1dHRvbjpub3QoOmxhc3QtY2hpbGQpIHtcbiAgICAgIG9wYWNpdHk6IDE7XG4gICAgICB3aWR0aDogNjBweDtcbiAgICAgIGhlaWdodDogNjBweDtcbiAgICAgIG1hcmdpbjogMTVweCBhdXRvIDA7XG4gICAgfVxuICAgIC5zYnV0dG9uOm50aC1sYXN0LWNoaWxkKDEpIHtcbiAgICAgIC13ZWJraXQtdHJhbnNpdGlvbi1kZWxheTogMjVtcztcbiAgICAgIHRyYW5zaXRpb24tZGVsYXk6IDI1bXM7XG4gICAgfVxuICAgIC5zYnV0dG9uOm5vdCg6bGFzdC1jaGlsZCk6bnRoLWxhc3QtY2hpbGQoMikge1xuICAgICAgLXdlYmtpdC10cmFuc2l0aW9uLWRlbGF5OiAyMG1zO1xuICAgICAgdHJhbnNpdGlvbi1kZWxheTogMjBtcztcbiAgICB9XG4gICAgLnNidXR0b246bm90KDpsYXN0LWNoaWxkKTpudGgtbGFzdC1jaGlsZCgzKSB7XG4gICAgICAtd2Via2l0LXRyYW5zaXRpb24tZGVsYXk6IDQwbXM7XG4gICAgICB0cmFuc2l0aW9uLWRlbGF5OiA0MG1zO1xuICAgIH1cbiAgICAuc2J1dHRvbjpub3QoOmxhc3QtY2hpbGQpOm50aC1sYXN0LWNoaWxkKDQpIHtcbiAgICAgIC13ZWJraXQtdHJhbnNpdGlvbi1kZWxheTogNjBtcztcbiAgICAgIHRyYW5zaXRpb24tZGVsYXk6IDYwbXM7XG4gICAgfVxuICAgIC5zYnV0dG9uOm5vdCg6bGFzdC1jaGlsZCk6bnRoLWxhc3QtY2hpbGQoNSkge1xuICAgICAgLXdlYmtpdC10cmFuc2l0aW9uLWRlbGF5OiA4MG1zO1xuICAgICAgdHJhbnNpdGlvbi1kZWxheTogODBtcztcbiAgICB9XG4gICAgLnNidXR0b246bm90KDpsYXN0LWNoaWxkKTpudGgtbGFzdC1jaGlsZCg2KSB7XG4gICAgICAtd2Via2l0LXRyYW5zaXRpb24tZGVsYXk6IDEwMG1zO1xuICAgICAgdHJhbnNpdGlvbi1kZWxheTogMTAwbXM7XG4gICAgfVxuICAgICBcbiAgICBbdG9vbHRpcF06YmVmb3JlIHtcbiAgICAgIGZvbnQtZmFtaWx5OiAnUm9ib3RvJztcbiAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgICBib3JkZXItcmFkaXVzOiAycHg7XG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjNTg1ODU4O1xuICAgICAgY29sb3I6ICNmZmY7XG4gICAgICBjb250ZW50OiBhdHRyKHRvb2x0aXApO1xuICAgICAgZm9udC1zaXplOiAxMnB4O1xuICAgICAgdmlzaWJpbGl0eTogaGlkZGVuO1xuICAgICAgb3BhY2l0eTogMDtcbiAgICAgIHBhZGRpbmc6IDVweCA3cHg7XG4gICAgICBtYXJnaW4tbGVmdDogMTBweDtcbiAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgIGxlZnQ6IDEwMCU7XG4gICAgICBib3R0b206IDIwJTtcbiAgICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gICAgfVxuICAgICBcbiAgICAuc2J1dHRvbi5tYWluc2J1dHRvbiB7XG4gICAgICBiYWNrZ3JvdW5kOnVybCgvYXNzZXRzL2ltYWdlcy9zaGFyZS5qcGcpO1xuICAgIH1cbiAgICAuc2J1dHRvbi5ncGx1cyB7XG4gICAgICBiYWNrZ3JvdW5kOnVybCgvYXNzZXRzL2ltYWdlcy9nbWFpbDYwLnBuZyk7XG4gICAgfVxuICAgIC5zYnV0dG9uLnR3aXR0IHtcbiAgICAgIGJhY2tncm91bmQ6dXJsKC9hc3NldHMvaW1hZ2VzL21lc3M2MC5wbmcpO1xuICAgIH1cbiAgICAuc2J1dHRvbi5mYiB7XG4gICAgICBiYWNrZ3JvdW5kOnVybCgvYXNzZXRzL2ltYWdlcy9mYWNlYm9vazYwLnBuZyk7XG4gICAgfVxuICAgIC5zYnV0dG9uLndoYXRzYXBwIHtcbiAgICAgIGJhY2tncm91bmQ6dXJsKC9hc3NldHMvaW1hZ2VzL3doYXRzYXBwNjAucG5nKTtcbiAgICB9XG4gIFxuICAgIC5ibGluayB7XG4gICAgICBtYXJnaW4tdG9wOiAyLjUlO1xuICAgICAgYW5pbWF0aW9uOiBibGlua2VyIDJzIGxpbmVhciBpbmZpbml0ZTtcbiAgICAgIGNvbG9yOiAjMmMzZTUwO1xuICAgICB9XG4gICAgQGtleWZyYW1lcyBibGlua2VyIHsgIFxuICAgICAgNTAlIHsgb3BhY2l0eTogMDsgfVxuICAgICB9XG4gICAgIC5tYWluLWNvbnRlbnQge1xuICAgICAgcGFkZGluZy10b3A6IDE1JTtcbiAgICAgIG1pbi1oZWlnaHQ6IDgwdmg7XG4gICAgICBwYWRkaW5nLWxlZnQ6IDUlO1xuICAgICAgcGFkZGluZy1yaWdodDogNSU7XG4gICAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoJy9hc3NldHMvaW1hZ2VzL2JnLmpwZycpO1xuICAgICAgcGFkZGluZy1ib3R0b206IDUlO1xuICAgIH1cbiAgICAuY2VudGVyIHtcbiAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgbWFyZ2luLWxlZnQ6IGF1dG87XG4gICAgICBtYXJnaW4tcmlnaHQ6IGF1dG87XG4gICAgICB3aWR0aDogODUlO1xuICAgICAgbWFyZ2luLXRvcDogMi41JTtcbiAgICAgIGFuaW1hdGlvbjogYmxpbmtlciAycyBsaW5lYXIgaW5maW5pdGU7XG4gICAgfVxuICAgIC5nb0JhY2t7XG4gICAgICBjb2xvcjogIzAwOGY3MjtcbiAgICAgIGZvbnQtc2l6ZTogbGFyZ2U7XG4gICAgICB0ZXh0LXNoYWRvdzogMnB4IDJweCB3aGl0ZTtcbiAgICB9XG4gICAgaHJ7XG4gICAgICBtYXJnaW46IDA7ICBcbiAgICB9XG5cbiAgICAubG9hZGVyIHtcbiAgICAgIGJhY2tncm91bmQ6IHVybCgnL2Fzc2V0cy9pbWFnZXMvbG9hZGluZ19zbWFsbC5naWYnKSA1MCUgNTAlIG5vLXJlcGVhdCByZ2IoMjQ5LDI0OSwyNDkpO1xuICAgIH0gXG4gICJdfQ== */");

/***/ }),

/***/ "./src/app/pdf-viewer/mobile/mobile-pdf-viewer.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/pdf-viewer/mobile/mobile-pdf-viewer.component.ts ***!
  \******************************************************************/
/*! exports provided: PdfViewerComponentMobile */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PdfViewerComponentMobile", function() { return PdfViewerComponentMobile; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var rxjs_Rx__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/Rx */ "./node_modules/rxjs-compat/_esm2015/Rx.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _app_header_header_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../app-header/header.service */ "./src/app/app-header/header.service.ts");




;






let PdfViewerComponentMobile = class PdfViewerComponentMobile {
    constructor(http, activatedRoute, router, meta, title, sanitizer, headerService) {
        this.http = http;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.sanitizer = sanitizer;
        this.headerService = headerService;
        this.keys = [];
        this.nav = [];
        this.navIds = [];
        this.navCount = [];
        this.deviceInfo = {};
        this.tabUrl = 'https://api.finalrevise.com/finalrevise/v1/relatedlinks/';
        this.whatsAppUrl = 'https://api.finalrevise.com/finalrevise/v1/whatsApp/';
        this.metaUrl = 'https://api.finalrevise.com/finalrevise/v1/meta/';
        this.title = title;
        this.meta = meta;
    }
    transform(url) {
        return this.sanitizer.bypassSecurityTrustResourceUrl(url);
    }
    ngOnInit() {
        // subscribe to router event
        this.activatedRoute.params.subscribe((params) => {
            this.titleText = params['title'];
            this.urlTitle = params['urlTitle'];
            this.subId = params['subId'];
            this.pdfUrl = "https://drive.google.com/file/d/" + this.subId + "/preview";
        });
        this.tabUrl = this.tabUrl + this.urlTitle;
        this.whatsAppUrl = this.whatsAppUrl + this.urlTitle;
        this.metaUrl = this.metaUrl + this.subId;
        this.getTabData();
        this.getWhatsAppUrl();
        this.getMetaUrl();
    }
    shareClicked() {
        if (this.isClickedShare) {
            this.isClickedShare = false;
        }
        else {
            this.isClickedShare = true;
        }
    }
    getWhatsAppUrl() {
        this.getWhatsAppData().subscribe(data => {
            this.groupUrl = data.data.whatsAppUrl;
        });
    }
    getWhatsAppData() {
        return this.http.get(this.whatsAppUrl);
    }
    getTData() {
        return this.http.get(this.tabUrl);
    }
    getTabData() {
        this.getTData().subscribe(data => {
            this.tags = data.data.related;
            if (data) {
                this.headerService.setRelatedTopics(data);
            }
        });
    }
    openTag(tab) {
        if (tab.isChild == 1) {
            this.router.navigateByUrl('/detail-list/' + tab.linkTitle);
        }
        else {
            if (tab.linkTitle.includes('book')) {
                this.router.navigateByUrl('/study-met/' + tab.linkTitle);
            }
            else {
                this.router.navigateByUrl('/detail/' + tab.linkTitle);
            }
        }
    }
    getMetaUrl() {
        this.getMetaData().subscribe(data => {
            this.metaData = data;
            this.setTitleMeta();
        });
    }
    getMetaData() {
        return this.http.get(this.metaUrl);
    }
    replaceAllSocial(str) {
        var find = '-';
        var re = new RegExp(find, 'g');
        return str.replace(re, ' ');
    }
    openFacebook() {
        window.open("https://www.facebook.com/sharer/sharer.php?u=http://finalrevise.com/detail/" + this.titleText);
    }
    openWhatsApp() {
        window.open("https://api.whatsapp.com/send?text=*Download%20" + this.titleText + "*%0a%0a" + "http://finalrevise.com/detail/" + this.titleText + "/" +
            "%0a%0a*Download%20FinalRevise%20App%20:%20https://play.google.com/store/apps/details?id%3Dcom.finalrevise.android*");
    }
    openMail() {
        window.open("mailto:?subject=FinalRevise%20Study%20Material%20for%20" + this.nav[this.navCount - 1] +
            "&body=Download%20" + this.replaceAllSocial(this.titleText) + "%0a%0a" + "http://finalrevise.com/detail/" + this.titleText +
            "%0a%0a Download FinalRevise App : https://play.google.com/store/apps/details?id=com.finalrevise.android");
    }
    openMessenger() {
        if (_angular_common__WEBPACK_IMPORTED_MODULE_6__["isPlatformBrowser"]) {
            window.open("https://m.me/finalrevise?u=http://finalrevise.com/detail/" + this.titleText);
        }
        else {
            window.open('fb-messenger://share?link=' + encodeURIComponent("http://finalrevise.com/detail/" + this.titleText) + '&app_id='
                + encodeURIComponent("358637287840510"));
        }
    }
    setTitleMeta() {
        this.title.setTitle(this.metaData.title);
        this.meta.addTags([
            { name: 'author', content: 'finalrevise.com' },
            { name: 'keywords', content: this.metaData.metaKeywords },
            {
                name: 'description', content: this.metaData.metaDesc
            }
        ]);
    }
    replaceAll(str, replacement) {
        str = str.split('(').join('');
        str = str.split(')').join('');
        str = str.split('.').join('');
        str = str.split('/').join('');
        str = str.split('|').join(':');
        str = str.split(' ').join(replacement);
        str = str.split('---').join(replacement);
        return str.split('--').join(replacement);
    }
    ;
};
PdfViewerComponentMobile.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__["Meta"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__["Title"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__["DomSanitizer"] },
    { type: _app_header_header_service__WEBPACK_IMPORTED_MODULE_7__["HeaderService"] }
];
PdfViewerComponentMobile = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: 'pdf-viewer-mobile',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./mobile-pdf-viewer.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pdf-viewer/mobile/mobile-pdf-viewer.component.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./mobile-pdf-viewer.component.css */ "./src/app/pdf-viewer/mobile/mobile-pdf-viewer.component.css")).default]
    })
], PdfViewerComponentMobile);



/***/ }),

/***/ "./src/app/search/search.component.css":
/*!*********************************************!*\
  !*** ./src/app/search/search.component.css ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("@media (min-width: 576px){\n    .jumbotron{padding:1.25rem .2rem}\n    .tableHeader{\n        margin-top: 75px;\n        table-layout: fixed;\n        width: 100%;\n        border-collapse: unset;\n    }\n\n    .main-content {\n        padding-top: 5%;\n        padding-left: 2%;\n        padding-right: 2%;\n        padding-bottom: 5%;\n          /* The image used */\n      \n    \n      /* Set a specific height */\n      min-height: 80vh;\n    \n      /* Create the parallax scrolling effect */\n      background-attachment: fixed;\n      background-position: center;\n      background-repeat: no-repeat;\n      background-size: cover;\n      }\n}\n\n.previous {\n    background-color: #f1f1f1;\n    color: black;\n}\n\n.next {\n    background-color: #20c997;\n    color: white;\n}\n\n@media (max-width: 576px) \n{\n  .jumbotron{\n    padding: 2%;\n    margin:1%;\n    background-color:#ecf0f1;\n    border-radius:0.3rem\n}\n  .tableHeader{\n    margin-top: 50px;\n    table-layout: fixed;\n    width: 100%;\n    border-collapse: unset;\n}\n.loader {\n    background: url('/assets/images/loading_small.gif') 50% 50% no-repeat rgb(249,249,249);\n  } \n\n  .main-content {\n    padding-top: 15%;\n    padding-left: 2%;\n    padding-right: 2%;\n    padding-bottom: 10%;\n      /* The image used */\n  \n\n  /* Set a specific height */\n  min-height: 80vh;\n\n  /* Create the parallax scrolling effect */\n  background-attachment: fixed;\n  background-position: center;\n  background-repeat: no-repeat;\n  background-size: cover;\n  }\n}\n\n\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2VhcmNoL3NlYXJjaC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksV0FBVyxxQkFBcUI7SUFDaEM7UUFDSSxnQkFBZ0I7UUFDaEIsbUJBQW1CO1FBQ25CLFdBQVc7UUFDWCxzQkFBc0I7SUFDMUI7O0lBRUE7UUFDSSxlQUFlO1FBQ2YsZ0JBQWdCO1FBQ2hCLGlCQUFpQjtRQUNqQixrQkFBa0I7VUFDaEIsbUJBQW1COzs7TUFHdkIsMEJBQTBCO01BQzFCLGdCQUFnQjs7TUFFaEIseUNBQXlDO01BQ3pDLDRCQUE0QjtNQUM1QiwyQkFBMkI7TUFDM0IsNEJBQTRCO01BQzVCLHNCQUFzQjtNQUN0QjtBQUNOOztBQUVBO0lBQ0kseUJBQXlCO0lBQ3pCLFlBQVk7QUFDaEI7O0FBRUE7SUFDSSx5QkFBeUI7SUFDekIsWUFBWTtBQUNoQjs7QUFDQTs7RUFFRTtJQUNFLFdBQVc7SUFDWCxTQUFTO0lBQ1Qsd0JBQXdCO0lBQ3hCO0FBQ0o7RUFDRTtJQUNFLGdCQUFnQjtJQUNoQixtQkFBbUI7SUFDbkIsV0FBVztJQUNYLHNCQUFzQjtBQUMxQjtBQUNBO0lBQ0ksc0ZBQXNGO0VBQ3hGOztFQUVBO0lBQ0UsZ0JBQWdCO0lBQ2hCLGdCQUFnQjtJQUNoQixpQkFBaUI7SUFDakIsbUJBQW1CO01BQ2pCLG1CQUFtQjs7O0VBR3ZCLDBCQUEwQjtFQUMxQixnQkFBZ0I7O0VBRWhCLHlDQUF5QztFQUN6Qyw0QkFBNEI7RUFDNUIsMkJBQTJCO0VBQzNCLDRCQUE0QjtFQUM1QixzQkFBc0I7RUFDdEI7QUFDRiIsImZpbGUiOiJzcmMvYXBwL3NlYXJjaC9zZWFyY2guY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIkBtZWRpYSAobWluLXdpZHRoOiA1NzZweCl7XG4gICAgLmp1bWJvdHJvbntwYWRkaW5nOjEuMjVyZW0gLjJyZW19XG4gICAgLnRhYmxlSGVhZGVye1xuICAgICAgICBtYXJnaW4tdG9wOiA3NXB4O1xuICAgICAgICB0YWJsZS1sYXlvdXQ6IGZpeGVkO1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgYm9yZGVyLWNvbGxhcHNlOiB1bnNldDtcbiAgICB9XG5cbiAgICAubWFpbi1jb250ZW50IHtcbiAgICAgICAgcGFkZGluZy10b3A6IDUlO1xuICAgICAgICBwYWRkaW5nLWxlZnQ6IDIlO1xuICAgICAgICBwYWRkaW5nLXJpZ2h0OiAyJTtcbiAgICAgICAgcGFkZGluZy1ib3R0b206IDUlO1xuICAgICAgICAgIC8qIFRoZSBpbWFnZSB1c2VkICovXG4gICAgICBcbiAgICBcbiAgICAgIC8qIFNldCBhIHNwZWNpZmljIGhlaWdodCAqL1xuICAgICAgbWluLWhlaWdodDogODB2aDtcbiAgICBcbiAgICAgIC8qIENyZWF0ZSB0aGUgcGFyYWxsYXggc2Nyb2xsaW5nIGVmZmVjdCAqL1xuICAgICAgYmFja2dyb3VuZC1hdHRhY2htZW50OiBmaXhlZDtcbiAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgICAgfVxufVxuXG4ucHJldmlvdXMge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNmMWYxZjE7XG4gICAgY29sb3I6IGJsYWNrO1xufVxuXG4ubmV4dCB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzIwYzk5NztcbiAgICBjb2xvcjogd2hpdGU7XG59XG5AbWVkaWEgKG1heC13aWR0aDogNTc2cHgpIFxue1xuICAuanVtYm90cm9ue1xuICAgIHBhZGRpbmc6IDIlO1xuICAgIG1hcmdpbjoxJTtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiNlY2YwZjE7XG4gICAgYm9yZGVyLXJhZGl1czowLjNyZW1cbn1cbiAgLnRhYmxlSGVhZGVye1xuICAgIG1hcmdpbi10b3A6IDUwcHg7XG4gICAgdGFibGUtbGF5b3V0OiBmaXhlZDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBib3JkZXItY29sbGFwc2U6IHVuc2V0O1xufVxuLmxvYWRlciB7XG4gICAgYmFja2dyb3VuZDogdXJsKCcvYXNzZXRzL2ltYWdlcy9sb2FkaW5nX3NtYWxsLmdpZicpIDUwJSA1MCUgbm8tcmVwZWF0IHJnYigyNDksMjQ5LDI0OSk7XG4gIH0gXG5cbiAgLm1haW4tY29udGVudCB7XG4gICAgcGFkZGluZy10b3A6IDE1JTtcbiAgICBwYWRkaW5nLWxlZnQ6IDIlO1xuICAgIHBhZGRpbmctcmlnaHQ6IDIlO1xuICAgIHBhZGRpbmctYm90dG9tOiAxMCU7XG4gICAgICAvKiBUaGUgaW1hZ2UgdXNlZCAqL1xuICBcblxuICAvKiBTZXQgYSBzcGVjaWZpYyBoZWlnaHQgKi9cbiAgbWluLWhlaWdodDogODB2aDtcblxuICAvKiBDcmVhdGUgdGhlIHBhcmFsbGF4IHNjcm9sbGluZyBlZmZlY3QgKi9cbiAgYmFja2dyb3VuZC1hdHRhY2htZW50OiBmaXhlZDtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICB9XG59XG5cblxuIl19 */");

/***/ }),

/***/ "./src/app/search/search.component.ts":
/*!********************************************!*\
  !*** ./src/app/search/search.component.ts ***!
  \********************************************/
/*! exports provided: SearchComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchComponent", function() { return SearchComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var rxjs_Rx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/Rx */ "./node_modules/rxjs-compat/_esm2015/Rx.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");



;



let SearchComponent = class SearchComponent {
    constructor(http, activatedRoute, router, meta, title) {
        this.http = http;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.apiUrl = 'https://api.finalrevise.com/finalrevise/v1/search/';
        this.pageData = {};
        this.keys = [];
        this.nav = [];
        this.navIds = [];
        this.offset = 10;
        this.count = 0;
        this.type = 0;
        title.setTitle("FinalRevise  - Previous Year Papers");
        this.meta = meta;
    }
    ngOnInit() {
        // subscribe to router event
        this.activatedRoute.params.subscribe((params) => {
            this.searchTxt = params['text'];
            this.count = params['count'];
            this.type = params['type'];
        });
        this.apiUrl = this.apiUrl + this.searchTxt + "/" + this.offset + "/" + this.count + "/" + this.type;
        this.getData();
        this.getPageData();
    }
    getData() {
        return this.http.get(this.apiUrl);
    }
    getPageData() {
        this.meta.addTags([
            { name: 'author', content: 'finalrevise.com' },
            { name: 'keywords', content: 'All Papers related to : ' + this.searchTxt },
            {
                name: 'description', content: 'All Previous Year Question Papers for Different Competitive Exams, ' +
                    'Universities : Delhi University/JNU/BHU/Amity/IP etc..., International Papers : ELETS/TOFEL/GMAT/GRE etc..., Company interview questions, School 10th 12th exams, Government Jobs Exams : Bank Papers/SSC papers/graduate level exams'
            }
        ]);
        this.getData().subscribe(data => {
            this.pageData = data;
            this.nav = String('Home#' + this.searchTxt).split("#");
            this.navIds = String('1#10').split("#");
        });
    }
    openPageFromNav(pageId) {
        if (pageId > 0 && pageId < 15) {
            this.router.navigateByUrl('');
        }
    }
    openPreviousPage() {
        if (this.count > 0) {
            this.count = Number(this.count) - 1;
            window.open('/search/' + this.searchTxt + '/' + this.count + '/' + this.type, "_self");
        }
    }
    openNextPage() {
        this.count = Number(this.count) + 1;
        window.open('/search/' + this.searchTxt + '/' + this.count + '/' + this.type, "_self");
    }
    openPage(id, type, child) {
        if (child == 1) {
            window.open('detail-list/' + id, "_self");
        }
        else if (child == 2) {
            window.open('detail/' + id, "_self");
        }
    }
    replaceAll(str, replacement) {
        str = str.split('(').join('');
        str = str.split(')').join('');
        str = str.split('.').join('');
        str = str.split('/').join('');
        str = str.split('|').join('');
        str = str.split(' ').join(replacement);
        str = str.split('---').join(replacement);
        return str.split('--').join(replacement);
    }
    ;
    selectName() {
        window.open('/search/' + this.searchTxt + '/' + this.count + '/' + this.nameId, "_self");
    }
};
SearchComponent.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__["Meta"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__["Title"] }
];
SearchComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-search',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./search.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/search/search.component.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./search.component.css */ "./src/app/search/search.component.css")).default]
    })
], SearchComponent);



/***/ }),

/***/ "./src/app/study-met/desktop/desktop-study-met.component.css":
/*!*******************************************************************!*\
  !*** ./src/app/study-met/desktop/desktop-study-met.component.css ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".jumbotron{\n  padding-top:2%;\n  padding-bottom: 2%;\n  margin-bottom:0rem;\n  background-color:#ecf0f1;\n  border-radius:0.3rem;\n  box-shadow: 0px 5px 11px -2px rgba(0, 0, 0, 0.18), 0px 4px 12px -7px rgba(0, 0, 0, 0.15);\n}\n  .tableHeader{\n    margin-top: 75px;\n    table-layout: fixed;\n    width: 100%;\n    border-collapse: unset;\n}\n  .col-sm-3{\n        padding:1rem 0rem;margin-bottom:0rem;background-color:#ecf0f1;border-radius:0.3rem;\n        max-width: 35%;flex:0 0 35%;height: -webkit-fit-content;height: -moz-fit-content;height: fit-content;\n        margin-left: auto;\n    }\n  .col-sm-8{\n        margin: inherit;\n    }\n  .navbar {\n        position: relative;\n        display: block;\n        flex-wrap: wrap;\n        text-align: center;\n        justify-content: space-between;\n        padding: 5px;\n    }\n  section{\n        margin: 5px 0px;\n        background: linear-gradient(90deg, rgba(144,71,168,0.5), rgba(238,216,185,1));\n        padding: 0 15%;\n    \n    }\n  @media (min-width: 1200px){\n    .card{\n        border: 1px solid #112;\n        width: 20%;\n        display: inline-block;\n        text-align: center;\n        margin: 1% 1%;\n        box-shadow: 1px 1px 3px #111;\n    }\n  }\n  @media (max-width: 1200px) \n{\n\n  .card{\n    border: 1px solid #112;\n    width: 30%;\n    display: inline-block;\n    text-align: center;\n    margin: 1% 1%;\n    box-shadow: 1px 1px 3px #111;\n}\n\n}\n  .banner {\n        display: inline-block;\n        width: 100%;\n        height: 300px;\n        background-color:whitesmoke;\n        font-size: 0px;\n    }\n  .banner>img{\n        width:100%;\n        height: 300px;\n        border-radius: 10px 10px 0 0;\n    \n    }\n  .descri {\n        padding: .5%;\n        background-color: #8b0000;\n        color: white;\n        height: 50px;\n        overflow-y: hidden;\n        border-radius: 0px 0px 10px 10px;\n    }\n  .card,.banner{\n        border-radius: 10px;\n    }\n  .fontSize{\n      font-size: 1.7rem;\n    }\n  ul, menu, dir {\n    list-style-type: disc;\n    -webkit-margin-before: 0;\n            margin-block-start: 0;\n    -webkit-margin-after: 0;\n            margin-block-end: 0;\n    -webkit-margin-start: 15px;\n            margin-inline-start: 15px;\n    -webkit-margin-end: 0px;\n            margin-inline-end: 0px;\n    -webkit-padding-start: 0px;\n            padding-inline-start: 0px;\n}\n  .bg-primary {\n    /* background-color: #dfeaec !important; */\n    animation: colorchange 5s infinite; /* animation-name followed by duration in seconds*/\n         /* you could also use milliseconds (ms) or something like 2.5s */\n      -webkit-animation: colorchange 5s infinite; /* Chrome and Safari */\n}\n  #fixed-width-flamingo {  \n    width: 35px; \n    height: 35px;\n}\n  .nav-link {\n    display: block;\n    background: #8b0000;\n    margin-bottom: 3%;\n    margin-top: 8%;\n    margin-right: 2%;\n}\n  @media (max-width:720px){\n\tbody{\n\t\ttext-align: center;\n\t}\n\t.display-4{\n\t\tfont-size: 30px !important;\n\t}\n}\n  @keyframes colorchange\n    {\n      0%   {background: #2c3e50;}\n      33%  {background: rebeccapurple;}\n      66%  {background: rgb(0, 143, 114)}\n      100%  {background: #8b0000;}\n    }\n  @-webkit-keyframes colorchange /* Safari and Chrome - necessary duplicate */\n    {\n      0%   {background: #2c3e50;}\n      33%  {background: rebeccapurple;}\n      66%  {background: rgb(0, 143, 114)}\n      100%  {background: #8b0000;}\n    }\n  .sbuttons {\n      bottom: 5%;\n      position: fixed;\n      margin: 1em;\n      left: 0;\n    }\n  .sbutton {\n      display: block;\n      width: 60px;\n      height: 60px;\n      border-radius: 50%;\n      text-align: center;\n      color: transparent;\n      margin: 20px auto 0;\n      box-shadow: 0px 5px 11px -2px rgba(0, 0, 0, 0.18), 0px 4px 12px -7px rgba(0, 0, 0, 0.15);\n      cursor: pointer;\n      transition: all .1s ease-out;\n      position: relative;\n    }\n  .sbutton > i {\n      font-size: 38px;\n      line-height: 60px;\n      transition: all .2s ease-in-out;\n      transition-delay: 2s;\n    }\n  .sbutton:active,\n    .sbutton:focus,\n    .sbutton:hover {\n      box-shadow: 0 0 4px rgba(0, 0, 0, .14), 0 4px 8px rgba(0, 0, 0, .28);\n    }\n  .sbutton:not(:last-child) {\n      width: 60px;\n      height: 60px;\n      margin: 20px auto 0;\n      opacity: 0;\n    }\n  .sbutton:not(:last-child) > i {\n      font-size: 25px;\n      line-height: 60px;\n      transition: all .3s ease-in-out;\n    }\n  .sbutton:not(:last-child) {\n      opacity: 1;\n      width: 60px;\n      height: 60px;\n      margin: 15px auto 0;\n    }\n  .sbutton:nth-last-child(1) {\n      transition-delay: 25ms;\n    }\n  .sbutton:not(:last-child):nth-last-child(2) {\n      transition-delay: 20ms;\n    }\n  .sbutton:not(:last-child):nth-last-child(3) {\n      transition-delay: 40ms;\n    }\n  .sbutton:not(:last-child):nth-last-child(4) {\n      transition-delay: 60ms;\n    }\n  .sbutton:not(:last-child):nth-last-child(5) {\n      transition-delay: 80ms;\n    }\n  .sbutton:not(:last-child):nth-last-child(6) {\n      transition-delay: 100ms;\n    }\n  [tooltip]:before {\n      font-family: 'Roboto';\n      font-weight: 600;\n      border-radius: 2px;\n      background-color: #585858;\n      color: #fff;\n      content: attr(tooltip);\n      font-size: 12px;\n      visibility: hidden;\n      opacity: 0;\n      padding: 5px 7px;\n      margin-left: 10px;\n      position: absolute;\n      left: 100%;\n      bottom: 20%;\n      white-space: nowrap;\n    }\n  .sbutton.mainsbutton {\n      background:url(/assets/images/share.jpg);\n    }\n  .sbutton.gplus {\n      background:url(/assets/images/gmail60.png);\n    }\n  .sbutton.twitt {\n      background:url(/assets/images/mess60.png);\n    }\n  .sbutton.fb {\n      background:url(/assets/images/facebook60.png);\n    }\n  .sbutton.whatsapp {\n      background:url(/assets/images/whatsapp60.png);\n    }\n  .blink {\n      margin-top: 2.5%;\n      -webkit-animation: blinker 2s linear infinite;\n              animation: blinker 2s linear infinite;\n      color: #2c3e50;\n     }\n  @-webkit-keyframes blinker {  \n      50% { opacity: 0; }\n     }\n  @keyframes blinker {  \n      50% { opacity: 0; }\n     }\n  .main-content {\n    padding-top: 6.5%;\n    padding-left: 2%;\n    padding-right: 2%;\n    min-height: 80vh;\n  }\n  hr{\n    margin: 0;\n  }\n  \n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc3R1ZHktbWV0L2Rlc2t0b3AvZGVza3RvcC1zdHVkeS1tZXQuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGNBQWM7RUFDZCxrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLHdCQUF3QjtFQUN4QixvQkFBb0I7RUFDcEIsd0ZBQXdGO0FBQzFGO0VBQ0U7SUFDRSxnQkFBZ0I7SUFDaEIsbUJBQW1CO0lBQ25CLFdBQVc7SUFDWCxzQkFBc0I7QUFDMUI7RUFDSTtRQUNJLGlCQUFpQixDQUFDLGtCQUFrQixDQUFDLHdCQUF3QixDQUFDLG9CQUFvQjtRQUNsRixjQUFjLENBQXFDLFlBQVksQ0FBQywyQkFBbUIsQ0FBbkIsd0JBQW1CLENBQW5CLG1CQUFtQjtRQUNuRixpQkFBaUI7SUFDckI7RUFDQTtRQUNJLGVBQWU7SUFDbkI7RUFDQTtRQUNJLGtCQUFrQjtRQUNsQixjQUFjO1FBQ2QsZUFBZTtRQUNmLGtCQUFrQjtRQUNsQiw4QkFBOEI7UUFDOUIsWUFBWTtJQUNoQjtFQUNBO1FBQ0ksZUFBZTtRQUNmLDZFQUE2RTtRQUM3RSxjQUFjOztJQUVsQjtFQUNBO0lBQ0E7UUFDSSxzQkFBc0I7UUFDdEIsVUFBVTtRQUNWLHFCQUFxQjtRQUNyQixrQkFBa0I7UUFDbEIsYUFBYTtRQUNiLDRCQUE0QjtJQUNoQztFQUNGO0VBQ0E7OztFQUdBO0lBQ0Usc0JBQXNCO0lBQ3RCLFVBQVU7SUFDVixxQkFBcUI7SUFDckIsa0JBQWtCO0lBQ2xCLGFBQWE7SUFDYiw0QkFBNEI7QUFDaEM7O0FBRUE7RUFFSTtRQUNJLHFCQUFxQjtRQUNyQixXQUFXO1FBQ1gsYUFBYTtRQUNiLDJCQUEyQjtRQUMzQixjQUFjO0lBQ2xCO0VBQ0E7UUFDSSxVQUFVO1FBQ1YsYUFBYTtRQUNiLDRCQUE0Qjs7SUFFaEM7RUFDQTtRQUNJLFlBQVk7UUFDWix5QkFBeUI7UUFDekIsWUFBWTtRQUNaLFlBQVk7UUFDWixrQkFBa0I7UUFDbEIsZ0NBQWdDO0lBQ3BDO0VBQ0E7UUFDSSxtQkFBbUI7SUFDdkI7RUFDQTtNQUNFLGlCQUFpQjtJQUNuQjtFQUNKO0lBQ0kscUJBQXFCO0lBQ3JCLHdCQUFxQjtZQUFyQixxQkFBcUI7SUFDckIsdUJBQW1CO1lBQW5CLG1CQUFtQjtJQUNuQiwwQkFBeUI7WUFBekIseUJBQXlCO0lBQ3pCLHVCQUFzQjtZQUF0QixzQkFBc0I7SUFDdEIsMEJBQXlCO1lBQXpCLHlCQUF5QjtBQUM3QjtFQUNBO0lBQ0ksMENBQTBDO0lBQzFDLGtDQUFrQyxFQUFFLGtEQUFrRDtTQUNqRixnRUFBZ0U7TUFDbkUsMENBQTBDLEVBQUUsc0JBQXNCO0FBQ3hFO0VBQ0E7SUFDSSxXQUFXO0lBQ1gsWUFBWTtBQUNoQjtFQUVBO0lBQ0ksY0FBYztJQUNkLG1CQUFtQjtJQUNuQixpQkFBaUI7SUFDakIsY0FBYztJQUNkLGdCQUFnQjtBQUNwQjtFQUVBO0NBQ0M7RUFDQyxrQkFBa0I7Q0FDbkI7Q0FDQTtFQUNDLDBCQUEwQjtDQUMzQjtBQUNEO0VBRUE7O01BRU0sTUFBTSxtQkFBbUIsQ0FBQztNQUMxQixNQUFNLHlCQUF5QixDQUFDO01BQ2hDLE1BQU0sNEJBQTRCO01BQ2xDLE9BQU8sbUJBQW1CLENBQUM7SUFDN0I7RUFFQTs7TUFFRSxNQUFNLG1CQUFtQixDQUFDO01BQzFCLE1BQU0seUJBQXlCLENBQUM7TUFDaEMsTUFBTSw0QkFBNEI7TUFDbEMsT0FBTyxtQkFBbUIsQ0FBQztJQUM3QjtFQUVBO01BQ0UsVUFBVTtNQUNWLGVBQWU7TUFDZixXQUFXO01BQ1gsT0FBTztJQUNUO0VBQ0E7TUFDRSxjQUFjO01BQ2QsV0FBVztNQUNYLFlBQVk7TUFDWixrQkFBa0I7TUFDbEIsa0JBQWtCO01BQ2xCLGtCQUFrQjtNQUNsQixtQkFBbUI7TUFDbkIsd0ZBQXdGO01BQ3hGLGVBQWU7TUFFZiw0QkFBNEI7TUFDNUIsa0JBQWtCO0lBQ3BCO0VBQ0E7TUFDRSxlQUFlO01BQ2YsaUJBQWlCO01BQ2pCLCtCQUErQjtNQUMvQixvQkFBb0I7SUFDdEI7RUFDQTs7O01BR0Usb0VBQW9FO0lBQ3RFO0VBQ0E7TUFDRSxXQUFXO01BQ1gsWUFBWTtNQUNaLG1CQUFtQjtNQUNuQixVQUFVO0lBQ1o7RUFDQTtNQUNFLGVBQWU7TUFDZixpQkFBaUI7TUFDakIsK0JBQStCO0lBQ2pDO0VBQ0E7TUFDRSxVQUFVO01BQ1YsV0FBVztNQUNYLFlBQVk7TUFDWixtQkFBbUI7SUFDckI7RUFDQTtNQUVFLHNCQUFzQjtJQUN4QjtFQUNBO01BRUUsc0JBQXNCO0lBQ3hCO0VBQ0E7TUFFRSxzQkFBc0I7SUFDeEI7RUFDQTtNQUVFLHNCQUFzQjtJQUN4QjtFQUNBO01BRUUsc0JBQXNCO0lBQ3hCO0VBQ0E7TUFFRSx1QkFBdUI7SUFDekI7RUFFQTtNQUNFLHFCQUFxQjtNQUNyQixnQkFBZ0I7TUFDaEIsa0JBQWtCO01BQ2xCLHlCQUF5QjtNQUN6QixXQUFXO01BQ1gsc0JBQXNCO01BQ3RCLGVBQWU7TUFDZixrQkFBa0I7TUFDbEIsVUFBVTtNQUNWLGdCQUFnQjtNQUNoQixpQkFBaUI7TUFDakIsa0JBQWtCO01BQ2xCLFVBQVU7TUFDVixXQUFXO01BQ1gsbUJBQW1CO0lBQ3JCO0VBRUE7TUFDRSx3Q0FBd0M7SUFDMUM7RUFDQTtNQUNFLDBDQUEwQztJQUM1QztFQUNBO01BQ0UseUNBQXlDO0lBQzNDO0VBQ0E7TUFDRSw2Q0FBNkM7SUFDL0M7RUFDQTtNQUNFLDZDQUE2QztJQUMvQztFQUNBO01BQ0UsZ0JBQWdCO01BQ2hCLDZDQUFxQztjQUFyQyxxQ0FBcUM7TUFDckMsY0FBYztLQUNmO0VBQ0Q7TUFDRSxNQUFNLFVBQVUsRUFBRTtLQUNuQjtFQUZEO01BQ0UsTUFBTSxVQUFVLEVBQUU7S0FDbkI7RUFFRjtJQUNDLGlCQUFpQjtJQUNqQixnQkFBZ0I7SUFDaEIsaUJBQWlCO0lBQ2pCLGdCQUFnQjtFQUNsQjtFQUVBO0lBQ0UsU0FBUztFQUNYIiwiZmlsZSI6InNyYy9hcHAvc3R1ZHktbWV0L2Rlc2t0b3AvZGVza3RvcC1zdHVkeS1tZXQuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5qdW1ib3Ryb257XG4gIHBhZGRpbmctdG9wOjIlO1xuICBwYWRkaW5nLWJvdHRvbTogMiU7XG4gIG1hcmdpbi1ib3R0b206MHJlbTtcbiAgYmFja2dyb3VuZC1jb2xvcjojZWNmMGYxO1xuICBib3JkZXItcmFkaXVzOjAuM3JlbTtcbiAgYm94LXNoYWRvdzogMHB4IDVweCAxMXB4IC0ycHggcmdiYSgwLCAwLCAwLCAwLjE4KSwgMHB4IDRweCAxMnB4IC03cHggcmdiYSgwLCAwLCAwLCAwLjE1KTtcbn1cbiAgLnRhYmxlSGVhZGVye1xuICAgIG1hcmdpbi10b3A6IDc1cHg7XG4gICAgdGFibGUtbGF5b3V0OiBmaXhlZDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBib3JkZXItY29sbGFwc2U6IHVuc2V0O1xufVxuICAgIC5jb2wtc20tM3tcbiAgICAgICAgcGFkZGluZzoxcmVtIDByZW07bWFyZ2luLWJvdHRvbTowcmVtO2JhY2tncm91bmQtY29sb3I6I2VjZjBmMTtib3JkZXItcmFkaXVzOjAuM3JlbTtcbiAgICAgICAgbWF4LXdpZHRoOiAzNSU7LXdlYmtpdC1ib3gtZmxleDowOy1tcy1mbGV4OjAgMCAzNSU7ZmxleDowIDAgMzUlO2hlaWdodDogZml0LWNvbnRlbnQ7XG4gICAgICAgIG1hcmdpbi1sZWZ0OiBhdXRvO1xuICAgIH1cbiAgICAuY29sLXNtLTh7XG4gICAgICAgIG1hcmdpbjogaW5oZXJpdDtcbiAgICB9XG4gICAgLm5hdmJhciB7XG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgIGZsZXgtd3JhcDogd3JhcDtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gICAgICAgIHBhZGRpbmc6IDVweDtcbiAgICB9XG4gICAgc2VjdGlvbntcbiAgICAgICAgbWFyZ2luOiA1cHggMHB4O1xuICAgICAgICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoOTBkZWcsIHJnYmEoMTQ0LDcxLDE2OCwwLjUpLCByZ2JhKDIzOCwyMTYsMTg1LDEpKTtcbiAgICAgICAgcGFkZGluZzogMCAxNSU7XG4gICAgXG4gICAgfVxuICAgIEBtZWRpYSAobWluLXdpZHRoOiAxMjAwcHgpe1xuICAgIC5jYXJke1xuICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjMTEyO1xuICAgICAgICB3aWR0aDogMjAlO1xuICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgbWFyZ2luOiAxJSAxJTtcbiAgICAgICAgYm94LXNoYWRvdzogMXB4IDFweCAzcHggIzExMTtcbiAgICB9XG4gIH1cbiAgQG1lZGlhIChtYXgtd2lkdGg6IDEyMDBweCkgXG57XG5cbiAgLmNhcmR7XG4gICAgYm9yZGVyOiAxcHggc29saWQgIzExMjtcbiAgICB3aWR0aDogMzAlO1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgbWFyZ2luOiAxJSAxJTtcbiAgICBib3gtc2hhZG93OiAxcHggMXB4IDNweCAjMTExO1xufVxuXG59XG4gICAgXG4gICAgLmJhbm5lciB7XG4gICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIGhlaWdodDogMzAwcHg7XG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6d2hpdGVzbW9rZTtcbiAgICAgICAgZm9udC1zaXplOiAwcHg7XG4gICAgfVxuICAgIC5iYW5uZXI+aW1ne1xuICAgICAgICB3aWR0aDoxMDAlO1xuICAgICAgICBoZWlnaHQ6IDMwMHB4O1xuICAgICAgICBib3JkZXItcmFkaXVzOiAxMHB4IDEwcHggMCAwO1xuICAgIFxuICAgIH1cbiAgICAuZGVzY3JpIHtcbiAgICAgICAgcGFkZGluZzogLjUlO1xuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjOGIwMDAwO1xuICAgICAgICBjb2xvcjogd2hpdGU7XG4gICAgICAgIGhlaWdodDogNTBweDtcbiAgICAgICAgb3ZlcmZsb3cteTogaGlkZGVuO1xuICAgICAgICBib3JkZXItcmFkaXVzOiAwcHggMHB4IDEwcHggMTBweDtcbiAgICB9XG4gICAgLmNhcmQsLmJhbm5lcntcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgICB9XG4gICAgLmZvbnRTaXple1xuICAgICAgZm9udC1zaXplOiAxLjdyZW07XG4gICAgfVxudWwsIG1lbnUsIGRpciB7XG4gICAgbGlzdC1zdHlsZS10eXBlOiBkaXNjO1xuICAgIG1hcmdpbi1ibG9jay1zdGFydDogMDtcbiAgICBtYXJnaW4tYmxvY2stZW5kOiAwO1xuICAgIG1hcmdpbi1pbmxpbmUtc3RhcnQ6IDE1cHg7XG4gICAgbWFyZ2luLWlubGluZS1lbmQ6IDBweDtcbiAgICBwYWRkaW5nLWlubGluZS1zdGFydDogMHB4O1xufVxuLmJnLXByaW1hcnkge1xuICAgIC8qIGJhY2tncm91bmQtY29sb3I6ICNkZmVhZWMgIWltcG9ydGFudDsgKi9cbiAgICBhbmltYXRpb246IGNvbG9yY2hhbmdlIDVzIGluZmluaXRlOyAvKiBhbmltYXRpb24tbmFtZSBmb2xsb3dlZCBieSBkdXJhdGlvbiBpbiBzZWNvbmRzKi9cbiAgICAgICAgIC8qIHlvdSBjb3VsZCBhbHNvIHVzZSBtaWxsaXNlY29uZHMgKG1zKSBvciBzb21ldGhpbmcgbGlrZSAyLjVzICovXG4gICAgICAtd2Via2l0LWFuaW1hdGlvbjogY29sb3JjaGFuZ2UgNXMgaW5maW5pdGU7IC8qIENocm9tZSBhbmQgU2FmYXJpICovXG59XG4jZml4ZWQtd2lkdGgtZmxhbWluZ28geyAgXG4gICAgd2lkdGg6IDM1cHg7IFxuICAgIGhlaWdodDogMzVweDtcbn1cblxuLm5hdi1saW5rIHtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICBiYWNrZ3JvdW5kOiAjOGIwMDAwO1xuICAgIG1hcmdpbi1ib3R0b206IDMlO1xuICAgIG1hcmdpbi10b3A6IDglO1xuICAgIG1hcmdpbi1yaWdodDogMiU7XG59XG5cbkBtZWRpYSAobWF4LXdpZHRoOjcyMHB4KXtcblx0Ym9keXtcblx0XHR0ZXh0LWFsaWduOiBjZW50ZXI7XG5cdH1cblx0LmRpc3BsYXktNHtcblx0XHRmb250LXNpemU6IDMwcHggIWltcG9ydGFudDtcblx0fVxufVxuXG5Aa2V5ZnJhbWVzIGNvbG9yY2hhbmdlXG4gICAge1xuICAgICAgMCUgICB7YmFja2dyb3VuZDogIzJjM2U1MDt9XG4gICAgICAzMyUgIHtiYWNrZ3JvdW5kOiByZWJlY2NhcHVycGxlO31cbiAgICAgIDY2JSAge2JhY2tncm91bmQ6IHJnYigwLCAxNDMsIDExNCl9XG4gICAgICAxMDAlICB7YmFja2dyb3VuZDogIzhiMDAwMDt9XG4gICAgfVxuXG4gICAgQC13ZWJraXQta2V5ZnJhbWVzIGNvbG9yY2hhbmdlIC8qIFNhZmFyaSBhbmQgQ2hyb21lIC0gbmVjZXNzYXJ5IGR1cGxpY2F0ZSAqL1xuICAgIHtcbiAgICAgIDAlICAge2JhY2tncm91bmQ6ICMyYzNlNTA7fVxuICAgICAgMzMlICB7YmFja2dyb3VuZDogcmViZWNjYXB1cnBsZTt9XG4gICAgICA2NiUgIHtiYWNrZ3JvdW5kOiByZ2IoMCwgMTQzLCAxMTQpfVxuICAgICAgMTAwJSAge2JhY2tncm91bmQ6ICM4YjAwMDA7fVxuICAgIH1cbiAgICBcbiAgICAuc2J1dHRvbnMge1xuICAgICAgYm90dG9tOiA1JTtcbiAgICAgIHBvc2l0aW9uOiBmaXhlZDtcbiAgICAgIG1hcmdpbjogMWVtO1xuICAgICAgbGVmdDogMDtcbiAgICB9XG4gICAgLnNidXR0b24ge1xuICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICB3aWR0aDogNjBweDtcbiAgICAgIGhlaWdodDogNjBweDtcbiAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgIGNvbG9yOiB0cmFuc3BhcmVudDtcbiAgICAgIG1hcmdpbjogMjBweCBhdXRvIDA7XG4gICAgICBib3gtc2hhZG93OiAwcHggNXB4IDExcHggLTJweCByZ2JhKDAsIDAsIDAsIDAuMTgpLCAwcHggNHB4IDEycHggLTdweCByZ2JhKDAsIDAsIDAsIDAuMTUpO1xuICAgICAgY3Vyc29yOiBwb2ludGVyO1xuICAgICAgLXdlYmtpdC10cmFuc2l0aW9uOiBhbGwgLjFzIGVhc2Utb3V0O1xuICAgICAgdHJhbnNpdGlvbjogYWxsIC4xcyBlYXNlLW91dDtcbiAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICB9XG4gICAgLnNidXR0b24gPiBpIHtcbiAgICAgIGZvbnQtc2l6ZTogMzhweDtcbiAgICAgIGxpbmUtaGVpZ2h0OiA2MHB4O1xuICAgICAgdHJhbnNpdGlvbjogYWxsIC4ycyBlYXNlLWluLW91dDtcbiAgICAgIHRyYW5zaXRpb24tZGVsYXk6IDJzO1xuICAgIH1cbiAgICAuc2J1dHRvbjphY3RpdmUsXG4gICAgLnNidXR0b246Zm9jdXMsXG4gICAgLnNidXR0b246aG92ZXIge1xuICAgICAgYm94LXNoYWRvdzogMCAwIDRweCByZ2JhKDAsIDAsIDAsIC4xNCksIDAgNHB4IDhweCByZ2JhKDAsIDAsIDAsIC4yOCk7XG4gICAgfVxuICAgIC5zYnV0dG9uOm5vdCg6bGFzdC1jaGlsZCkge1xuICAgICAgd2lkdGg6IDYwcHg7XG4gICAgICBoZWlnaHQ6IDYwcHg7XG4gICAgICBtYXJnaW46IDIwcHggYXV0byAwO1xuICAgICAgb3BhY2l0eTogMDtcbiAgICB9XG4gICAgLnNidXR0b246bm90KDpsYXN0LWNoaWxkKSA+IGkge1xuICAgICAgZm9udC1zaXplOiAyNXB4O1xuICAgICAgbGluZS1oZWlnaHQ6IDYwcHg7XG4gICAgICB0cmFuc2l0aW9uOiBhbGwgLjNzIGVhc2UtaW4tb3V0O1xuICAgIH1cbiAgICAuc2J1dHRvbjpub3QoOmxhc3QtY2hpbGQpIHtcbiAgICAgIG9wYWNpdHk6IDE7XG4gICAgICB3aWR0aDogNjBweDtcbiAgICAgIGhlaWdodDogNjBweDtcbiAgICAgIG1hcmdpbjogMTVweCBhdXRvIDA7XG4gICAgfVxuICAgIC5zYnV0dG9uOm50aC1sYXN0LWNoaWxkKDEpIHtcbiAgICAgIC13ZWJraXQtdHJhbnNpdGlvbi1kZWxheTogMjVtcztcbiAgICAgIHRyYW5zaXRpb24tZGVsYXk6IDI1bXM7XG4gICAgfVxuICAgIC5zYnV0dG9uOm5vdCg6bGFzdC1jaGlsZCk6bnRoLWxhc3QtY2hpbGQoMikge1xuICAgICAgLXdlYmtpdC10cmFuc2l0aW9uLWRlbGF5OiAyMG1zO1xuICAgICAgdHJhbnNpdGlvbi1kZWxheTogMjBtcztcbiAgICB9XG4gICAgLnNidXR0b246bm90KDpsYXN0LWNoaWxkKTpudGgtbGFzdC1jaGlsZCgzKSB7XG4gICAgICAtd2Via2l0LXRyYW5zaXRpb24tZGVsYXk6IDQwbXM7XG4gICAgICB0cmFuc2l0aW9uLWRlbGF5OiA0MG1zO1xuICAgIH1cbiAgICAuc2J1dHRvbjpub3QoOmxhc3QtY2hpbGQpOm50aC1sYXN0LWNoaWxkKDQpIHtcbiAgICAgIC13ZWJraXQtdHJhbnNpdGlvbi1kZWxheTogNjBtcztcbiAgICAgIHRyYW5zaXRpb24tZGVsYXk6IDYwbXM7XG4gICAgfVxuICAgIC5zYnV0dG9uOm5vdCg6bGFzdC1jaGlsZCk6bnRoLWxhc3QtY2hpbGQoNSkge1xuICAgICAgLXdlYmtpdC10cmFuc2l0aW9uLWRlbGF5OiA4MG1zO1xuICAgICAgdHJhbnNpdGlvbi1kZWxheTogODBtcztcbiAgICB9XG4gICAgLnNidXR0b246bm90KDpsYXN0LWNoaWxkKTpudGgtbGFzdC1jaGlsZCg2KSB7XG4gICAgICAtd2Via2l0LXRyYW5zaXRpb24tZGVsYXk6IDEwMG1zO1xuICAgICAgdHJhbnNpdGlvbi1kZWxheTogMTAwbXM7XG4gICAgfVxuICAgICBcbiAgICBbdG9vbHRpcF06YmVmb3JlIHtcbiAgICAgIGZvbnQtZmFtaWx5OiAnUm9ib3RvJztcbiAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgICBib3JkZXItcmFkaXVzOiAycHg7XG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjNTg1ODU4O1xuICAgICAgY29sb3I6ICNmZmY7XG4gICAgICBjb250ZW50OiBhdHRyKHRvb2x0aXApO1xuICAgICAgZm9udC1zaXplOiAxMnB4O1xuICAgICAgdmlzaWJpbGl0eTogaGlkZGVuO1xuICAgICAgb3BhY2l0eTogMDtcbiAgICAgIHBhZGRpbmc6IDVweCA3cHg7XG4gICAgICBtYXJnaW4tbGVmdDogMTBweDtcbiAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgIGxlZnQ6IDEwMCU7XG4gICAgICBib3R0b206IDIwJTtcbiAgICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gICAgfVxuICAgICBcbiAgICAuc2J1dHRvbi5tYWluc2J1dHRvbiB7XG4gICAgICBiYWNrZ3JvdW5kOnVybCgvYXNzZXRzL2ltYWdlcy9zaGFyZS5qcGcpO1xuICAgIH1cbiAgICAuc2J1dHRvbi5ncGx1cyB7XG4gICAgICBiYWNrZ3JvdW5kOnVybCgvYXNzZXRzL2ltYWdlcy9nbWFpbDYwLnBuZyk7XG4gICAgfVxuICAgIC5zYnV0dG9uLnR3aXR0IHtcbiAgICAgIGJhY2tncm91bmQ6dXJsKC9hc3NldHMvaW1hZ2VzL21lc3M2MC5wbmcpO1xuICAgIH1cbiAgICAuc2J1dHRvbi5mYiB7XG4gICAgICBiYWNrZ3JvdW5kOnVybCgvYXNzZXRzL2ltYWdlcy9mYWNlYm9vazYwLnBuZyk7XG4gICAgfVxuICAgIC5zYnV0dG9uLndoYXRzYXBwIHtcbiAgICAgIGJhY2tncm91bmQ6dXJsKC9hc3NldHMvaW1hZ2VzL3doYXRzYXBwNjAucG5nKTtcbiAgICB9XG4gICAgLmJsaW5rIHtcbiAgICAgIG1hcmdpbi10b3A6IDIuNSU7XG4gICAgICBhbmltYXRpb246IGJsaW5rZXIgMnMgbGluZWFyIGluZmluaXRlO1xuICAgICAgY29sb3I6ICMyYzNlNTA7XG4gICAgIH1cbiAgICBAa2V5ZnJhbWVzIGJsaW5rZXIgeyAgXG4gICAgICA1MCUgeyBvcGFjaXR5OiAwOyB9XG4gICAgIH1cblxuICAgLm1haW4tY29udGVudCB7XG4gICAgcGFkZGluZy10b3A6IDYuNSU7XG4gICAgcGFkZGluZy1sZWZ0OiAyJTtcbiAgICBwYWRkaW5nLXJpZ2h0OiAyJTtcbiAgICBtaW4taGVpZ2h0OiA4MHZoO1xuICB9XG5cbiAgaHJ7XG4gICAgbWFyZ2luOiAwO1xuICB9XG4gICJdfQ== */");

/***/ }),

/***/ "./src/app/study-met/desktop/desktop-study-met.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/study-met/desktop/desktop-study-met.component.ts ***!
  \******************************************************************/
/*! exports provided: StudyMetDesktopComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StudyMetDesktopComponent", function() { return StudyMetDesktopComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var rxjs_Rx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/Rx */ "./node_modules/rxjs-compat/_esm2015/Rx.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _app_header_header_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../app-header/header.service */ "./src/app/app-header/header.service.ts");



;






let StudyMetDesktopComponent = class StudyMetDesktopComponent {
    constructor(http, activatedRoute, router, meta, title, sanitizer, headerService) {
        this.http = http;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.sanitizer = sanitizer;
        this.headerService = headerService;
        this.apiUrl = 'https://api.finalrevise.com/finalrevise/v1/content/';
        this.pageData = {};
        this.keys = [];
        this.nav = [];
        this.navIds = [];
        this.tabUrl = 'https://api.finalrevise.com/finalrevise/v1/relatedlinks/';
        this.whatsAppUrl = 'https://api.finalrevise.com/finalrevise/v1/whatsApp/';
        this.router.routeReuseStrategy.shouldReuseRoute = function () {
            return false;
        };
        this.title = title;
        this.meta = meta;
    }
    ngOnInit() {
        // subscribe to router event
        this.activatedRoute.params.subscribe((params) => {
            this.titleText = params['title'];
        });
        this.apiUrl = this.apiUrl + this.titleText;
        this.tabUrl = this.tabUrl + this.titleText;
        this.whatsAppUrl = this.whatsAppUrl + this.titleText;
        this.getWhatsAppUrl();
        this.getPageData();
        this.getTabData();
    }
    shareClicked() {
        if (this.isClickedShare) {
            this.isClickedShare = false;
        }
        else {
            this.isClickedShare = true;
        }
    }
    getData() {
        return this.http.get(this.apiUrl);
    }
    getTData() {
        return this.http.get(this.tabUrl);
    }
    getTabData() {
        this.getTData().subscribe(data => {
            this.tags = data.data.related;
            if (data) {
                this.headerService.setRelatedTopics(data);
            }
        });
    }
    getWhatsAppUrl() {
        this.getWhatsAppData().subscribe(data => {
            this.groupUrl = data.data.whatsAppUrl;
        });
    }
    getWhatsAppData() {
        return this.http.get(this.whatsAppUrl);
    }
    getPageData() {
        this.getData().subscribe(data => {
            this.pageData = data;
            this.nav = String(this.pageData.data.pagePath).split("#");
            this.navCount = this.nav.length;
            this.navIds = String(this.pageData.data.pagePathId).split("#");
            this.aboutExamContent = this.sanitizer.bypassSecurityTrustHtml(this.pageData.data.pageIntro);
            this.linkedPageItems = this.pageData.data.linkedPageItems;
            this.pageTitle = this.pageData.data.pageTitle;
            this.setTitleMeta();
        });
    }
    openPageFromNav(pageId, index) {
        if (index == 0) {
            this.router.navigateByUrl('#');
        }
        else {
            this.router.navigateByUrl('/detail-list/' + pageId);
        }
    }
    openNextPage(link, paperName) {
        paperName = this.replaceAllString(paperName, ",", " ");
        paperName = this.replaceAllString(paperName, "(", " ");
        paperName = this.replaceAllString(paperName, ")", " ");
        paperName = this.replaceAllString(paperName, "?", " ");
        this.openPdfViewer(link, paperName);
    }
    openPdfViewer(link, paperName) {
        if (link.includes('open')) {
            this.router.navigateByUrl('/pdf-viewer/' + this.titleText + "/" + paperName + "/" + link.substring(link.indexOf("?id=") + 4));
        }
        else if (link.includes("/view")) {
            this.router.navigateByUrl('/pdf-viewer/' + this.titleText + "/" + paperName + "/" + link.replace("/view", ").substring(link.indexOf("/d/") + 3));
        }
        else {
            window.open(link);
        }
    }
    replaceAllString(str, find, replace) {
        return str.split(find).join(replace);
    }
    openTag(tab) {
        if (tab.isChild == 1) {
            this.router.navigateByUrl('/detail-list/' + tab.linkTitle);
        }
        else {
            if (tab.linkTitle.includes('book')) {
                this.router.navigateByUrl('/study-met/' + tab.linkTitle);
            }
            else {
                this.router.navigateByUrl('/detail/' + tab.linkTitle);
            }
        }
    }
    replaceAllSocial(str) {
        var find = '-';
        var re = new RegExp(find, 'g');
        return str.replace(re, ' ');
    }
    openFacebook() {
        window.open("https://www.facebook.com/sharer/sharer.php?u=http://finalrevise.com/detail/" + this.titleText);
    }
    openWhatsApp() {
        window.open("https://api.whatsapp.com/send?text=*Download%20" + this.titleText + "*%0a%0a" + "http://finalrevise.com/detail/" + this.titleText +
            "%0a%0a*Download%20FinalRevise%20App%20:%20https://play.google.com/store/apps/details?id%3Dcom.finalrevise.android*");
    }
    openMail() {
        window.open("mailto:?subject=FinalRevise%20Study%20Material%20for%20" + this.nav[this.navCount - 1] +
            "&body=Download%20" + this.replaceAllSocial(this.titleText) + "%0a%0a" + "http://finalrevise.com/detail/" + this.titleText +
            "%0a%0a Download FinalRevise App : https://play.google.com/store/apps/details?id=com.finalrevise.android");
    }
    openMessenger() {
        if (_angular_common__WEBPACK_IMPORTED_MODULE_6__["isPlatformBrowser"]) {
            window.open("https://m.me/finalrevise?u=http://finalrevise.com/detail/" + this.titleText);
        }
        else {
            window.open('fb-messenger://share?link=' + encodeURIComponent("http://finalrevise.com/detail/" + this.titleText) + '&app_id='
                + encodeURIComponent("358637287840510"));
        }
    }
    openTabPage(pageId, isChild, tag, cat) {
        if (pageId != null) {
            if (cat == 10 && !isChild) {
                window.open('/study-met/' + this.replaceAll(tag, '-') + '/' + pageId, "_self");
            }
            else if (pageId == 1573689600004 || pageId == 10 || pageId == 13 || pageId == 1561798213495
                || pageId == 14 || pageId == 1561798213494 || pageId == 1561658659435) {
                window.open('/main-cat-list/' + this.replaceAll(tag, '-') + '/' + pageId, "_self");
            }
            else if (cat == 11) {
                window.open('/image-text-list/' + this.replaceAll(tag, '-') + '/' + pageId, "_self");
            }
            else if (isChild && pageId != null) {
                window.open('/detail-list/' + this.replaceAll(tag, '-') + '/' + pageId, "_self");
            }
            else {
                this.router.navigateByUrl('/detail/' + this.replaceAll(tag, '-') + '/' + pageId);
            }
        }
    }
    setTitleMeta() {
        this.title.setTitle(this.pageData.data.tag);
        this.meta.addTags([
            { name: 'author', content: 'finalrevise.com' },
            { name: 'keywords', content: this.pageData.data.keywords },
            {
                name: 'description', content: this.pageData.data.description
            }
        ]);
    }
    replaceAll(str, replacement) {
        str = str.split('(').join('');
        str = str.split(')').join('');
        str = str.split('.').join('');
        str = str.split('/').join('');
        str = str.split('|').join(':');
        str = str.split(' ').join(replacement);
        str = str.split('---').join(replacement);
        return str.split('--').join(replacement);
    }
    ;
};
StudyMetDesktopComponent.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__["Meta"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__["Title"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__["DomSanitizer"] },
    { type: _app_header_header_service__WEBPACK_IMPORTED_MODULE_7__["HeaderService"] }
];
StudyMetDesktopComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'study-met',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./desktop-study-met.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/study-met/desktop/desktop-study-met.component.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./desktop-study-met.component.css */ "./src/app/study-met/desktop/desktop-study-met.component.css")).default]
    })
], StudyMetDesktopComponent);



/***/ }),

/***/ "./src/app/study-met/mobile/data.service.ts":
/*!**************************************************!*\
  !*** ./src/app/study-met/mobile/data.service.ts ***!
  \**************************************************/
/*! exports provided: CounterService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CounterService", function() { return CounterService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


let CounterService = class CounterService {
    constructor() {
        this.count = 0;
    }
    getCount() {
        let temp = parseInt(localStorage.getItem('count'));
        return temp ? temp : 0;
    }
    addCount() {
        let temp = parseInt(localStorage.getItem('count'));
        this.count = temp ? temp : 0;
        this.count = this.count + 1;
        localStorage.setItem('count', " + this.count);
    }
};
CounterService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
], CounterService);



/***/ }),

/***/ "./src/app/study-met/mobile/mobile-study-met.component.css":
/*!*****************************************************************!*\
  !*** ./src/app/study-met/mobile/mobile-study-met.component.css ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".jumbotron{\n    padding:0rem 0rem;\n    margin-bottom:0rem;\n    background-color:#ecf0f1;\n    border-radius:0.3rem\n}\n\nul, menu, dir {\n    list-style-type: disc;\n    -webkit-margin-before: 0;\n            margin-block-start: 0;\n    -webkit-margin-after: 0;\n            margin-block-end: 0;\n    -webkit-margin-start: 15px;\n            margin-inline-start: 15px;\n    -webkit-margin-end: 0px;\n            margin-inline-end: 0px;\n    -webkit-padding-start: 0px;\n            padding-inline-start: 0px;\n}\n\n.bg-primary {\n    /* background-color: #dfeaec !important; */\n    animation: colorchange 5s infinite; /* animation-name followed by duration in seconds*/\n         /* you could also use milliseconds (ms) or something like 2.5s */\n      -webkit-animation: colorchange 5s infinite; /* Chrome and Safari */\n}\n\n#fixed-width-flamingo {  \n    width: 35px; \n    height: 35px;\n}\n\n.tableHeader{\n    margin-top: 50px;\n    table-layout: fixed;\n    width: 100%;\n    border-collapse: unset;\n}\n\n.col-sm-3{\n        padding:1rem 0rem;margin-bottom:0rem;background-color:#ecf0f1;border-radius:0.3rem;\n        -webkit-box-flex:0;height: -webkit-fit-content;height: -moz-fit-content;height: fit-content;margin: 3%;\n    }\n\n.col-sm-8{\n        padding:0rem;\n        margin-left: 3%;\n        margin-right: 3%;\n    }\n\n.navbar {\n        position: relative;\n        display: block;\n        flex-wrap: wrap;\n        text-align: center;\n        justify-content: space-between;\n        padding: 5px;\n    }\n\nsection{\n        margin: 5px 0px;\n        background: linear-gradient(90deg, rgba(144,71,168,0.5), rgba(238,216,185,1));\n        padding: 0 15%;\n    \n    }\n\n.card{\n        border: 1px solid #112;\n        width: 45%;\n        display: inline-block;\n        text-align: center;\n        margin: 1.5% 1.5%;\n        box-shadow: 1px 1px 3px #111;\n    }\n\n.banner {\n        display: inline-block;\n        width: 100%;\n        height: 200px;\n        background-color:white;\n        font-size: 0px;\n    }\n\n.banner>img{\n        width:100%;\n        height: 200px;\n        border-radius: 10px 10px 0 0;\n    \n    }\n\n.descri {\n        padding: 0;\n        background-color: #8b0000;\n        color: white;\n        height: 50px;\n        overflow-y: hidden;\n        border-radius: 0px 0px 10px 10px;\n    }\n\n.card,.banner{\n        border-radius: 10px;\n    }\n\n.fontSize{\n      font-size: 1.2rem;\n    }\n\n.nav-link {\n    display: block;\n    background: #8b0000;\n    margin-bottom: 3%;\n    margin-top: 8%;\n    margin-right: 2%;\n}\n\n@media (max-width:720px){\n\tbody{\n\t\ttext-align: center;\n\t}\n\t.display-4{\n\t\tfont-size: 30px !important;\n\t}\n}\n\n@keyframes colorchange\n    {\n      0%   {background: #2c3e50;}\n      33%  {background: rebeccapurple;}\n      66%  {background: rgb(0, 143, 114)}\n      100%  {background: #8b0000;}\n    }\n\n@-webkit-keyframes colorchange /* Safari and Chrome - necessary duplicate */\n    {\n      0%   {background: #2c3e50;}\n      33%  {background: rebeccapurple;}\n      66%  {background: rgb(0, 143, 114)}\n      100%  {background: #8b0000;}\n    }\n\n.sbuttons {\n      bottom: 5%;\n      position: fixed;\n      margin: 1em;\n      left: 0;\n    }\n\n.sbutton {\n      display: block;\n      width: 60px;\n      height: 60px;\n      border-radius: 50%;\n      text-align: center;\n      color: transparent;\n      margin: 20px auto 0;\n      box-shadow: 0px 5px 11px -2px rgba(0, 0, 0, 0.18), 0px 4px 12px -7px rgba(0, 0, 0, 0.15);\n      cursor: pointer;\n      transition: all .1s ease-out;\n      position: relative;\n    }\n\n.sbutton > i {\n      font-size: 38px;\n      line-height: 60px;\n      transition: all .2s ease-in-out;\n      transition-delay: 2s;\n    }\n\n.sbutton:active,\n    .sbutton:focus,\n    .sbutton:hover {\n      box-shadow: 0 0 4px rgba(0, 0, 0, .14), 0 4px 8px rgba(0, 0, 0, .28);\n    }\n\n.sbutton:not(:last-child) {\n      width: 60px;\n      height: 60px;\n      margin: 20px auto 0;\n      opacity: 0;\n    }\n\n.sbutton:not(:last-child) > i {\n      font-size: 25px;\n      line-height: 60px;\n      transition: all .3s ease-in-out;\n    }\n\n.sbutton:not(:last-child) {\n      opacity: 1;\n      width: 60px;\n      height: 60px;\n      margin: 15px auto 0;\n    }\n\n.sbutton:nth-last-child(1) {\n      transition-delay: 25ms;\n    }\n\n.sbutton:not(:last-child):nth-last-child(2) {\n      transition-delay: 20ms;\n    }\n\n.sbutton:not(:last-child):nth-last-child(3) {\n      transition-delay: 40ms;\n    }\n\n.sbutton:not(:last-child):nth-last-child(4) {\n      transition-delay: 60ms;\n    }\n\n.sbutton:not(:last-child):nth-last-child(5) {\n      transition-delay: 80ms;\n    }\n\n.sbutton:not(:last-child):nth-last-child(6) {\n      transition-delay: 100ms;\n    }\n\n[tooltip]:before {\n      font-family: 'Roboto';\n      font-weight: 600;\n      border-radius: 2px;\n      background-color: #585858;\n      color: #fff;\n      content: attr(tooltip);\n      font-size: 12px;\n      visibility: hidden;\n      opacity: 0;\n      padding: 5px 7px;\n      margin-left: 10px;\n      position: absolute;\n      left: 100%;\n      bottom: 20%;\n      white-space: nowrap;\n    }\n\n.sbutton.mainsbutton {\n      background:url(/assets/images/share.jpg);\n    }\n\n.sbutton.gplus {\n      background:url(/assets/images/gmail60.png);\n    }\n\n.sbutton.twitt {\n      background:url(/assets/images/mess60.png);\n    }\n\n.sbutton.fb {\n      background:url(/assets/images/facebook60.png);\n    }\n\n.sbutton.whatsapp {\n      background:url(/assets/images/whatsapp60.png);\n    }\n\n.blink {\n      margin-top: 2.5%;\n      -webkit-animation: blinker 2s linear infinite;\n              animation: blinker 2s linear infinite;\n      color: #2c3e50;\n     }\n\n@-webkit-keyframes blinker {  \n      50% { opacity: 0; }\n     }\n\n@keyframes blinker {  \n      50% { opacity: 0; }\n     }\n\n.main-content {\n    padding-top: 15%;\n    min-height: 80vh;\n    padding-left: 2%;\n    padding-right: 2%;\n  }\n\n.center {\n    display: block;\n    margin-left: auto;\n    margin-right: auto;\n    width: 85%;\n    margin-top: 2.5%;\n    -webkit-animation: blinker 2s linear infinite;\n            animation: blinker 2s linear infinite;\n  }\n\n.goBack{\n    color: #008f72;\n    font-size: large;\n    text-shadow: 2px 2px white;\n  }\n\nhr{\n    margin: 0;  \n  }\n\n.loader {\n    background: url('/assets/images/loading_small.gif') 50% 50% no-repeat rgb(249,249,249);\n  } \n  \n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc3R1ZHktbWV0L21vYmlsZS9tb2JpbGUtc3R1ZHktbWV0LmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxpQkFBaUI7SUFDakIsa0JBQWtCO0lBQ2xCLHdCQUF3QjtJQUN4QjtBQUNKOztBQUVBO0lBQ0kscUJBQXFCO0lBQ3JCLHdCQUFxQjtZQUFyQixxQkFBcUI7SUFDckIsdUJBQW1CO1lBQW5CLG1CQUFtQjtJQUNuQiwwQkFBeUI7WUFBekIseUJBQXlCO0lBQ3pCLHVCQUFzQjtZQUF0QixzQkFBc0I7SUFDdEIsMEJBQXlCO1lBQXpCLHlCQUF5QjtBQUM3Qjs7QUFDQTtJQUNJLDBDQUEwQztJQUMxQyxrQ0FBa0MsRUFBRSxrREFBa0Q7U0FDakYsZ0VBQWdFO01BQ25FLDBDQUEwQyxFQUFFLHNCQUFzQjtBQUN4RTs7QUFDQTtJQUNJLFdBQVc7SUFDWCxZQUFZO0FBQ2hCOztBQUNFO0lBQ0UsZ0JBQWdCO0lBQ2hCLG1CQUFtQjtJQUNuQixXQUFXO0lBQ1gsc0JBQXNCO0FBQzFCOztBQUNJO1FBQ0ksaUJBQWlCLENBQUMsa0JBQWtCLENBQUMsd0JBQXdCLENBQUMsb0JBQW9CO1FBQ2xGLGtCQUFrQixDQUFDLDJCQUFtQixDQUFuQix3QkFBbUIsQ0FBbkIsbUJBQW1CLENBQUMsVUFBVTtJQUNyRDs7QUFDQTtRQUNJLFlBQVk7UUFDWixlQUFlO1FBQ2YsZ0JBQWdCO0lBQ3BCOztBQUNBO1FBQ0ksa0JBQWtCO1FBQ2xCLGNBQWM7UUFDZCxlQUFlO1FBQ2Ysa0JBQWtCO1FBQ2xCLDhCQUE4QjtRQUM5QixZQUFZO0lBQ2hCOztBQUNBO1FBQ0ksZUFBZTtRQUNmLDZFQUE2RTtRQUM3RSxjQUFjOztJQUVsQjs7QUFFQTtRQUNJLHNCQUFzQjtRQUN0QixVQUFVO1FBQ1YscUJBQXFCO1FBQ3JCLGtCQUFrQjtRQUNsQixpQkFBaUI7UUFDakIsNEJBQTRCO0lBQ2hDOztBQUVBO1FBQ0kscUJBQXFCO1FBQ3JCLFdBQVc7UUFDWCxhQUFhO1FBQ2Isc0JBQXNCO1FBQ3RCLGNBQWM7SUFDbEI7O0FBQ0E7UUFDSSxVQUFVO1FBQ1YsYUFBYTtRQUNiLDRCQUE0Qjs7SUFFaEM7O0FBQ0E7UUFDSSxVQUFVO1FBQ1YseUJBQXlCO1FBQ3pCLFlBQVk7UUFDWixZQUFZO1FBQ1osa0JBQWtCO1FBQ2xCLGdDQUFnQztJQUNwQzs7QUFDQTtRQUNJLG1CQUFtQjtJQUN2Qjs7QUFDQTtNQUNFLGlCQUFpQjtJQUNuQjs7QUFFSjtJQUNJLGNBQWM7SUFDZCxtQkFBbUI7SUFDbkIsaUJBQWlCO0lBQ2pCLGNBQWM7SUFDZCxnQkFBZ0I7QUFDcEI7O0FBRUE7Q0FDQztFQUNDLGtCQUFrQjtDQUNuQjtDQUNBO0VBQ0MsMEJBQTBCO0NBQzNCO0FBQ0Q7O0FBRUE7O01BRU0sTUFBTSxtQkFBbUIsQ0FBQztNQUMxQixNQUFNLHlCQUF5QixDQUFDO01BQ2hDLE1BQU0sNEJBQTRCO01BQ2xDLE9BQU8sbUJBQW1CLENBQUM7SUFDN0I7O0FBRUE7O01BRUUsTUFBTSxtQkFBbUIsQ0FBQztNQUMxQixNQUFNLHlCQUF5QixDQUFDO01BQ2hDLE1BQU0sNEJBQTRCO01BQ2xDLE9BQU8sbUJBQW1CLENBQUM7SUFDN0I7O0FBRUE7TUFDRSxVQUFVO01BQ1YsZUFBZTtNQUNmLFdBQVc7TUFDWCxPQUFPO0lBQ1Q7O0FBQ0E7TUFDRSxjQUFjO01BQ2QsV0FBVztNQUNYLFlBQVk7TUFDWixrQkFBa0I7TUFDbEIsa0JBQWtCO01BQ2xCLGtCQUFrQjtNQUNsQixtQkFBbUI7TUFDbkIsd0ZBQXdGO01BQ3hGLGVBQWU7TUFFZiw0QkFBNEI7TUFDNUIsa0JBQWtCO0lBQ3BCOztBQUNBO01BQ0UsZUFBZTtNQUNmLGlCQUFpQjtNQUNqQiwrQkFBK0I7TUFDL0Isb0JBQW9CO0lBQ3RCOztBQUNBOzs7TUFHRSxvRUFBb0U7SUFDdEU7O0FBQ0E7TUFDRSxXQUFXO01BQ1gsWUFBWTtNQUNaLG1CQUFtQjtNQUNuQixVQUFVO0lBQ1o7O0FBQ0E7TUFDRSxlQUFlO01BQ2YsaUJBQWlCO01BQ2pCLCtCQUErQjtJQUNqQzs7QUFDQTtNQUNFLFVBQVU7TUFDVixXQUFXO01BQ1gsWUFBWTtNQUNaLG1CQUFtQjtJQUNyQjs7QUFDQTtNQUVFLHNCQUFzQjtJQUN4Qjs7QUFDQTtNQUVFLHNCQUFzQjtJQUN4Qjs7QUFDQTtNQUVFLHNCQUFzQjtJQUN4Qjs7QUFDQTtNQUVFLHNCQUFzQjtJQUN4Qjs7QUFDQTtNQUVFLHNCQUFzQjtJQUN4Qjs7QUFDQTtNQUVFLHVCQUF1QjtJQUN6Qjs7QUFFQTtNQUNFLHFCQUFxQjtNQUNyQixnQkFBZ0I7TUFDaEIsa0JBQWtCO01BQ2xCLHlCQUF5QjtNQUN6QixXQUFXO01BQ1gsc0JBQXNCO01BQ3RCLGVBQWU7TUFDZixrQkFBa0I7TUFDbEIsVUFBVTtNQUNWLGdCQUFnQjtNQUNoQixpQkFBaUI7TUFDakIsa0JBQWtCO01BQ2xCLFVBQVU7TUFDVixXQUFXO01BQ1gsbUJBQW1CO0lBQ3JCOztBQUVBO01BQ0Usd0NBQXdDO0lBQzFDOztBQUNBO01BQ0UsMENBQTBDO0lBQzVDOztBQUNBO01BQ0UseUNBQXlDO0lBQzNDOztBQUNBO01BQ0UsNkNBQTZDO0lBQy9DOztBQUNBO01BQ0UsNkNBQTZDO0lBQy9DOztBQUNBO01BQ0UsZ0JBQWdCO01BQ2hCLDZDQUFxQztjQUFyQyxxQ0FBcUM7TUFDckMsY0FBYztLQUNmOztBQUNEO01BQ0UsTUFBTSxVQUFVLEVBQUU7S0FDbkI7O0FBRkQ7TUFDRSxNQUFNLFVBQVUsRUFBRTtLQUNuQjs7QUFDRjtJQUNDLGdCQUFnQjtJQUNoQixnQkFBZ0I7SUFDaEIsZ0JBQWdCO0lBQ2hCLGlCQUFpQjtFQUNuQjs7QUFDQTtJQUNFLGNBQWM7SUFDZCxpQkFBaUI7SUFDakIsa0JBQWtCO0lBQ2xCLFVBQVU7SUFDVixnQkFBZ0I7SUFDaEIsNkNBQXFDO1lBQXJDLHFDQUFxQztFQUN2Qzs7QUFFQTtJQUNFLGNBQWM7SUFDZCxnQkFBZ0I7SUFDaEIsMEJBQTBCO0VBQzVCOztBQUNBO0lBQ0UsU0FBUztFQUNYOztBQUNBO0lBQ0Usc0ZBQXNGO0VBQ3hGIiwiZmlsZSI6InNyYy9hcHAvc3R1ZHktbWV0L21vYmlsZS9tb2JpbGUtc3R1ZHktbWV0LmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuanVtYm90cm9ue1xuICAgIHBhZGRpbmc6MHJlbSAwcmVtO1xuICAgIG1hcmdpbi1ib3R0b206MHJlbTtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiNlY2YwZjE7XG4gICAgYm9yZGVyLXJhZGl1czowLjNyZW1cbn1cblxudWwsIG1lbnUsIGRpciB7XG4gICAgbGlzdC1zdHlsZS10eXBlOiBkaXNjO1xuICAgIG1hcmdpbi1ibG9jay1zdGFydDogMDtcbiAgICBtYXJnaW4tYmxvY2stZW5kOiAwO1xuICAgIG1hcmdpbi1pbmxpbmUtc3RhcnQ6IDE1cHg7XG4gICAgbWFyZ2luLWlubGluZS1lbmQ6IDBweDtcbiAgICBwYWRkaW5nLWlubGluZS1zdGFydDogMHB4O1xufVxuLmJnLXByaW1hcnkge1xuICAgIC8qIGJhY2tncm91bmQtY29sb3I6ICNkZmVhZWMgIWltcG9ydGFudDsgKi9cbiAgICBhbmltYXRpb246IGNvbG9yY2hhbmdlIDVzIGluZmluaXRlOyAvKiBhbmltYXRpb24tbmFtZSBmb2xsb3dlZCBieSBkdXJhdGlvbiBpbiBzZWNvbmRzKi9cbiAgICAgICAgIC8qIHlvdSBjb3VsZCBhbHNvIHVzZSBtaWxsaXNlY29uZHMgKG1zKSBvciBzb21ldGhpbmcgbGlrZSAyLjVzICovXG4gICAgICAtd2Via2l0LWFuaW1hdGlvbjogY29sb3JjaGFuZ2UgNXMgaW5maW5pdGU7IC8qIENocm9tZSBhbmQgU2FmYXJpICovXG59XG4jZml4ZWQtd2lkdGgtZmxhbWluZ28geyAgXG4gICAgd2lkdGg6IDM1cHg7IFxuICAgIGhlaWdodDogMzVweDtcbn1cbiAgLnRhYmxlSGVhZGVye1xuICAgIG1hcmdpbi10b3A6IDUwcHg7XG4gICAgdGFibGUtbGF5b3V0OiBmaXhlZDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBib3JkZXItY29sbGFwc2U6IHVuc2V0O1xufVxuICAgIC5jb2wtc20tM3tcbiAgICAgICAgcGFkZGluZzoxcmVtIDByZW07bWFyZ2luLWJvdHRvbTowcmVtO2JhY2tncm91bmQtY29sb3I6I2VjZjBmMTtib3JkZXItcmFkaXVzOjAuM3JlbTtcbiAgICAgICAgLXdlYmtpdC1ib3gtZmxleDowO2hlaWdodDogZml0LWNvbnRlbnQ7bWFyZ2luOiAzJTtcbiAgICB9XG4gICAgLmNvbC1zbS04e1xuICAgICAgICBwYWRkaW5nOjByZW07XG4gICAgICAgIG1hcmdpbi1sZWZ0OiAzJTtcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiAzJTtcbiAgICB9XG4gICAgLm5hdmJhciB7XG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgIGZsZXgtd3JhcDogd3JhcDtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gICAgICAgIHBhZGRpbmc6IDVweDtcbiAgICB9XG4gICAgc2VjdGlvbntcbiAgICAgICAgbWFyZ2luOiA1cHggMHB4O1xuICAgICAgICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoOTBkZWcsIHJnYmEoMTQ0LDcxLDE2OCwwLjUpLCByZ2JhKDIzOCwyMTYsMTg1LDEpKTtcbiAgICAgICAgcGFkZGluZzogMCAxNSU7XG4gICAgXG4gICAgfVxuICAgIFxuICAgIC5jYXJke1xuICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjMTEyO1xuICAgICAgICB3aWR0aDogNDUlO1xuICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgbWFyZ2luOiAxLjUlIDEuNSU7XG4gICAgICAgIGJveC1zaGFkb3c6IDFweCAxcHggM3B4ICMxMTE7XG4gICAgfVxuICAgIFxuICAgIC5iYW5uZXIge1xuICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICBoZWlnaHQ6IDIwMHB4O1xuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOndoaXRlO1xuICAgICAgICBmb250LXNpemU6IDBweDtcbiAgICB9XG4gICAgLmJhbm5lcj5pbWd7XG4gICAgICAgIHdpZHRoOjEwMCU7XG4gICAgICAgIGhlaWdodDogMjAwcHg7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDEwcHggMTBweCAwIDA7XG4gICAgXG4gICAgfVxuICAgIC5kZXNjcmkge1xuICAgICAgICBwYWRkaW5nOiAwO1xuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjOGIwMDAwO1xuICAgICAgICBjb2xvcjogd2hpdGU7XG4gICAgICAgIGhlaWdodDogNTBweDtcbiAgICAgICAgb3ZlcmZsb3cteTogaGlkZGVuO1xuICAgICAgICBib3JkZXItcmFkaXVzOiAwcHggMHB4IDEwcHggMTBweDtcbiAgICB9XG4gICAgLmNhcmQsLmJhbm5lcntcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgICB9XG4gICAgLmZvbnRTaXple1xuICAgICAgZm9udC1zaXplOiAxLjJyZW07XG4gICAgfVxuXG4ubmF2LWxpbmsge1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIGJhY2tncm91bmQ6ICM4YjAwMDA7XG4gICAgbWFyZ2luLWJvdHRvbTogMyU7XG4gICAgbWFyZ2luLXRvcDogOCU7XG4gICAgbWFyZ2luLXJpZ2h0OiAyJTtcbn1cblxuQG1lZGlhIChtYXgtd2lkdGg6NzIwcHgpe1xuXHRib2R5e1xuXHRcdHRleHQtYWxpZ246IGNlbnRlcjtcblx0fVxuXHQuZGlzcGxheS00e1xuXHRcdGZvbnQtc2l6ZTogMzBweCAhaW1wb3J0YW50O1xuXHR9XG59XG5cbkBrZXlmcmFtZXMgY29sb3JjaGFuZ2VcbiAgICB7XG4gICAgICAwJSAgIHtiYWNrZ3JvdW5kOiAjMmMzZTUwO31cbiAgICAgIDMzJSAge2JhY2tncm91bmQ6IHJlYmVjY2FwdXJwbGU7fVxuICAgICAgNjYlICB7YmFja2dyb3VuZDogcmdiKDAsIDE0MywgMTE0KX1cbiAgICAgIDEwMCUgIHtiYWNrZ3JvdW5kOiAjOGIwMDAwO31cbiAgICB9XG5cbiAgICBALXdlYmtpdC1rZXlmcmFtZXMgY29sb3JjaGFuZ2UgLyogU2FmYXJpIGFuZCBDaHJvbWUgLSBuZWNlc3NhcnkgZHVwbGljYXRlICovXG4gICAge1xuICAgICAgMCUgICB7YmFja2dyb3VuZDogIzJjM2U1MDt9XG4gICAgICAzMyUgIHtiYWNrZ3JvdW5kOiByZWJlY2NhcHVycGxlO31cbiAgICAgIDY2JSAge2JhY2tncm91bmQ6IHJnYigwLCAxNDMsIDExNCl9XG4gICAgICAxMDAlICB7YmFja2dyb3VuZDogIzhiMDAwMDt9XG4gICAgfVxuICAgIFxuICAgIC5zYnV0dG9ucyB7XG4gICAgICBib3R0b206IDUlO1xuICAgICAgcG9zaXRpb246IGZpeGVkO1xuICAgICAgbWFyZ2luOiAxZW07XG4gICAgICBsZWZ0OiAwO1xuICAgIH1cbiAgICAuc2J1dHRvbiB7XG4gICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgIHdpZHRoOiA2MHB4O1xuICAgICAgaGVpZ2h0OiA2MHB4O1xuICAgICAgYm9yZGVyLXJhZGl1czogNTAlO1xuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgY29sb3I6IHRyYW5zcGFyZW50O1xuICAgICAgbWFyZ2luOiAyMHB4IGF1dG8gMDtcbiAgICAgIGJveC1zaGFkb3c6IDBweCA1cHggMTFweCAtMnB4IHJnYmEoMCwgMCwgMCwgMC4xOCksIDBweCA0cHggMTJweCAtN3B4IHJnYmEoMCwgMCwgMCwgMC4xNSk7XG4gICAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgICAtd2Via2l0LXRyYW5zaXRpb246IGFsbCAuMXMgZWFzZS1vdXQ7XG4gICAgICB0cmFuc2l0aW9uOiBhbGwgLjFzIGVhc2Utb3V0O1xuICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIH1cbiAgICAuc2J1dHRvbiA+IGkge1xuICAgICAgZm9udC1zaXplOiAzOHB4O1xuICAgICAgbGluZS1oZWlnaHQ6IDYwcHg7XG4gICAgICB0cmFuc2l0aW9uOiBhbGwgLjJzIGVhc2UtaW4tb3V0O1xuICAgICAgdHJhbnNpdGlvbi1kZWxheTogMnM7XG4gICAgfVxuICAgIC5zYnV0dG9uOmFjdGl2ZSxcbiAgICAuc2J1dHRvbjpmb2N1cyxcbiAgICAuc2J1dHRvbjpob3ZlciB7XG4gICAgICBib3gtc2hhZG93OiAwIDAgNHB4IHJnYmEoMCwgMCwgMCwgLjE0KSwgMCA0cHggOHB4IHJnYmEoMCwgMCwgMCwgLjI4KTtcbiAgICB9XG4gICAgLnNidXR0b246bm90KDpsYXN0LWNoaWxkKSB7XG4gICAgICB3aWR0aDogNjBweDtcbiAgICAgIGhlaWdodDogNjBweDtcbiAgICAgIG1hcmdpbjogMjBweCBhdXRvIDA7XG4gICAgICBvcGFjaXR5OiAwO1xuICAgIH1cbiAgICAuc2J1dHRvbjpub3QoOmxhc3QtY2hpbGQpID4gaSB7XG4gICAgICBmb250LXNpemU6IDI1cHg7XG4gICAgICBsaW5lLWhlaWdodDogNjBweDtcbiAgICAgIHRyYW5zaXRpb246IGFsbCAuM3MgZWFzZS1pbi1vdXQ7XG4gICAgfVxuICAgIC5zYnV0dG9uOm5vdCg6bGFzdC1jaGlsZCkge1xuICAgICAgb3BhY2l0eTogMTtcbiAgICAgIHdpZHRoOiA2MHB4O1xuICAgICAgaGVpZ2h0OiA2MHB4O1xuICAgICAgbWFyZ2luOiAxNXB4IGF1dG8gMDtcbiAgICB9XG4gICAgLnNidXR0b246bnRoLWxhc3QtY2hpbGQoMSkge1xuICAgICAgLXdlYmtpdC10cmFuc2l0aW9uLWRlbGF5OiAyNW1zO1xuICAgICAgdHJhbnNpdGlvbi1kZWxheTogMjVtcztcbiAgICB9XG4gICAgLnNidXR0b246bm90KDpsYXN0LWNoaWxkKTpudGgtbGFzdC1jaGlsZCgyKSB7XG4gICAgICAtd2Via2l0LXRyYW5zaXRpb24tZGVsYXk6IDIwbXM7XG4gICAgICB0cmFuc2l0aW9uLWRlbGF5OiAyMG1zO1xuICAgIH1cbiAgICAuc2J1dHRvbjpub3QoOmxhc3QtY2hpbGQpOm50aC1sYXN0LWNoaWxkKDMpIHtcbiAgICAgIC13ZWJraXQtdHJhbnNpdGlvbi1kZWxheTogNDBtcztcbiAgICAgIHRyYW5zaXRpb24tZGVsYXk6IDQwbXM7XG4gICAgfVxuICAgIC5zYnV0dG9uOm5vdCg6bGFzdC1jaGlsZCk6bnRoLWxhc3QtY2hpbGQoNCkge1xuICAgICAgLXdlYmtpdC10cmFuc2l0aW9uLWRlbGF5OiA2MG1zO1xuICAgICAgdHJhbnNpdGlvbi1kZWxheTogNjBtcztcbiAgICB9XG4gICAgLnNidXR0b246bm90KDpsYXN0LWNoaWxkKTpudGgtbGFzdC1jaGlsZCg1KSB7XG4gICAgICAtd2Via2l0LXRyYW5zaXRpb24tZGVsYXk6IDgwbXM7XG4gICAgICB0cmFuc2l0aW9uLWRlbGF5OiA4MG1zO1xuICAgIH1cbiAgICAuc2J1dHRvbjpub3QoOmxhc3QtY2hpbGQpOm50aC1sYXN0LWNoaWxkKDYpIHtcbiAgICAgIC13ZWJraXQtdHJhbnNpdGlvbi1kZWxheTogMTAwbXM7XG4gICAgICB0cmFuc2l0aW9uLWRlbGF5OiAxMDBtcztcbiAgICB9XG4gICAgIFxuICAgIFt0b29sdGlwXTpiZWZvcmUge1xuICAgICAgZm9udC1mYW1pbHk6ICdSb2JvdG8nO1xuICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICAgIGJvcmRlci1yYWRpdXM6IDJweDtcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICM1ODU4NTg7XG4gICAgICBjb2xvcjogI2ZmZjtcbiAgICAgIGNvbnRlbnQ6IGF0dHIodG9vbHRpcCk7XG4gICAgICBmb250LXNpemU6IDEycHg7XG4gICAgICB2aXNpYmlsaXR5OiBoaWRkZW47XG4gICAgICBvcGFjaXR5OiAwO1xuICAgICAgcGFkZGluZzogNXB4IDdweDtcbiAgICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xuICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgbGVmdDogMTAwJTtcbiAgICAgIGJvdHRvbTogMjAlO1xuICAgICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgICB9XG4gICAgIFxuICAgIC5zYnV0dG9uLm1haW5zYnV0dG9uIHtcbiAgICAgIGJhY2tncm91bmQ6dXJsKC9hc3NldHMvaW1hZ2VzL3NoYXJlLmpwZyk7XG4gICAgfVxuICAgIC5zYnV0dG9uLmdwbHVzIHtcbiAgICAgIGJhY2tncm91bmQ6dXJsKC9hc3NldHMvaW1hZ2VzL2dtYWlsNjAucG5nKTtcbiAgICB9XG4gICAgLnNidXR0b24udHdpdHQge1xuICAgICAgYmFja2dyb3VuZDp1cmwoL2Fzc2V0cy9pbWFnZXMvbWVzczYwLnBuZyk7XG4gICAgfVxuICAgIC5zYnV0dG9uLmZiIHtcbiAgICAgIGJhY2tncm91bmQ6dXJsKC9hc3NldHMvaW1hZ2VzL2ZhY2Vib29rNjAucG5nKTtcbiAgICB9XG4gICAgLnNidXR0b24ud2hhdHNhcHAge1xuICAgICAgYmFja2dyb3VuZDp1cmwoL2Fzc2V0cy9pbWFnZXMvd2hhdHNhcHA2MC5wbmcpO1xuICAgIH1cbiAgICAuYmxpbmsge1xuICAgICAgbWFyZ2luLXRvcDogMi41JTtcbiAgICAgIGFuaW1hdGlvbjogYmxpbmtlciAycyBsaW5lYXIgaW5maW5pdGU7XG4gICAgICBjb2xvcjogIzJjM2U1MDtcbiAgICAgfVxuICAgIEBrZXlmcmFtZXMgYmxpbmtlciB7ICBcbiAgICAgIDUwJSB7IG9wYWNpdHk6IDA7IH1cbiAgICAgfVxuICAgLm1haW4tY29udGVudCB7XG4gICAgcGFkZGluZy10b3A6IDE1JTtcbiAgICBtaW4taGVpZ2h0OiA4MHZoO1xuICAgIHBhZGRpbmctbGVmdDogMiU7XG4gICAgcGFkZGluZy1yaWdodDogMiU7XG4gIH1cbiAgLmNlbnRlciB7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgbWFyZ2luLWxlZnQ6IGF1dG87XG4gICAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xuICAgIHdpZHRoOiA4NSU7XG4gICAgbWFyZ2luLXRvcDogMi41JTtcbiAgICBhbmltYXRpb246IGJsaW5rZXIgMnMgbGluZWFyIGluZmluaXRlO1xuICB9XG5cbiAgLmdvQmFja3tcbiAgICBjb2xvcjogIzAwOGY3MjtcbiAgICBmb250LXNpemU6IGxhcmdlO1xuICAgIHRleHQtc2hhZG93OiAycHggMnB4IHdoaXRlO1xuICB9XG4gIGhye1xuICAgIG1hcmdpbjogMDsgIFxuICB9XG4gIC5sb2FkZXIge1xuICAgIGJhY2tncm91bmQ6IHVybCgnL2Fzc2V0cy9pbWFnZXMvbG9hZGluZ19zbWFsbC5naWYnKSA1MCUgNTAlIG5vLXJlcGVhdCByZ2IoMjQ5LDI0OSwyNDkpO1xuICB9IFxuICAiXX0= */");

/***/ }),

/***/ "./src/app/study-met/mobile/mobile-study-met.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/study-met/mobile/mobile-study-met.component.ts ***!
  \****************************************************************/
/*! exports provided: StudyMetMobileComponent, DialogContentDialog */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StudyMetMobileComponent", function() { return StudyMetMobileComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DialogContentDialog", function() { return DialogContentDialog; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var rxjs_Rx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/Rx */ "./node_modules/rxjs-compat/_esm2015/Rx.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/dialog.js");
/* harmony import */ var ngx_device_detector__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ngx-device-detector */ "./node_modules/ngx-device-detector/__ivy_ngcc__/fesm2015/ngx-device-detector.js");
/* harmony import */ var _data_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./data.service */ "./src/app/study-met/mobile/data.service.ts");
/* harmony import */ var rxjs_observable_TimerObservable__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! rxjs/observable/TimerObservable */ "./node_modules/rxjs-compat/_esm2015/observable/TimerObservable.js");
/* harmony import */ var _app_header_header_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../app-header/header.service */ "./src/app/app-header/header.service.ts");



;










let StudyMetMobileComponent = class StudyMetMobileComponent {
    constructor(http, activatedRoute, router, meta, title, sanitizer, dialog, deviceService, dataService, headerService) {
        this.http = http;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.sanitizer = sanitizer;
        this.deviceService = deviceService;
        this.dataService = dataService;
        this.headerService = headerService;
        this.apiUrl = 'https://api.finalrevise.com/finalrevise/v1/content/';
        this.pageData = {};
        this.keys = [];
        this.nav = [];
        this.navIds = [];
        this.tabUrl = 'https://api.finalrevise.com/finalrevise/v1/relatedlinks/';
        this.whatsAppUrl = 'https://api.finalrevise.com/finalrevise/v1/whatsApp/';
        this.deviceInfo = {};
        this.title = title;
        this.meta = meta;
        this.dialog = dialog;
    }
    ngOnInit() {
        // subscribe to router event
        this.activatedRoute.params.subscribe((params) => {
            this.titleText = params['title'];
        });
        this.apiUrl = this.apiUrl + this.titleText;
        this.tabUrl = this.tabUrl + this.titleText;
        this.whatsAppUrl = this.whatsAppUrl + this.titleText;
        this.getWhatsAppUrl();
        this.getPageData();
        this.getTabData();
    }
    shareClicked() {
        if (this.isClickedShare) {
            this.isClickedShare = false;
        }
        else {
            this.isClickedShare = true;
        }
    }
    getData() {
        return this.http.get(this.apiUrl);
    }
    getTData() {
        return this.http.get(this.tabUrl);
    }
    getTabData() {
        this.getTData().subscribe(data => {
            this.tags = data.data.related;
            if (data) {
                this.headerService.setRelatedTopics(data);
            }
        });
    }
    getWhatsAppUrl() {
        this.getWhatsAppData().subscribe(data => {
            this.groupUrl = data.data.whatsAppUrl;
        });
    }
    getWhatsAppData() {
        return this.http.get(this.whatsAppUrl);
    }
    getPageData() {
        this.getData().subscribe(data => {
            this.pageData = data;
            this.linkedPageItems = this.pageData.data.linkedPageItems;
            this.aboutExamContent = this.sanitizer.bypassSecurityTrustHtml(this.pageData.data.pageIntro);
            this.pageTitle = this.pageData.data.pageTitle;
            this.setTitleMeta();
        });
    }
    openNextPage(link, paperName) {
        this.deviceInfo = this.deviceService.getDeviceInfo();
        paperName = this.replaceAllString(paperName, ",", " ");
        paperName = this.replaceAllString(paperName, "(", " ");
        paperName = this.replaceAllString(paperName, ")", " ");
        paperName = this.replaceAllString(paperName, "?", " ");
        this.dataService.count = this.dataService.getCount();
        this.dataService.addCount();
        // console.log(this.dataService.count);
        if (this.dataService.count % 3 != 0 && this.dataService.count != 0) {
            this.openPdfViewer(link, paperName);
        }
        else if ("ANDROID" == (this.deviceInfo.os).toUpperCase()) {
            this.openDialog(link, this.titleText, paperName);
        }
        else {
            this.openPdfViewer(link, paperName);
        }
    }
    openPdfViewer(link, paperName) {
        if (link.includes('open')) {
            this.router.navigateByUrl('/pdf-viewer/' + this.titleText + "/" + paperName + "/" + link.substring(link.indexOf("?id=") + 4));
        }
        else if (link.includes("/view")) {
            this.router.navigateByUrl('/pdf-viewer/' + this.titleText + "/" + paperName + "/" + link.replace("/view", ").substring(link.indexOf("/d/") + 3));
        }
        else {
            window.open(link);
        }
    }
    replaceAllString(str, find, replace) {
        return str.split(find).join(replace);
    }
    openTag(tab) {
        if (tab.isChild == 1) {
            this.router.navigateByUrl('/detail-list/' + tab.linkTitle);
        }
        else {
            if (tab.linkTitle.includes('book')) {
                this.router.navigateByUrl('/study-met/' + tab.linkTitle);
            }
            else {
                this.router.navigateByUrl('/detail/' + tab.linkTitle);
            }
        }
    }
    replaceAllSocial(str) {
        var find = '-';
        var re = new RegExp(find, 'g');
        return str.replace(re, ' ');
    }
    openFacebook() {
        window.open("https://www.facebook.com/sharer/sharer.php?u=http://finalrevise.com/detail/" + this.titleText);
    }
    openWhatsApp() {
        window.open("https://api.whatsapp.com/send?text=*Download%20" + this.titleText + "*%0a%0a" + "http://finalrevise.com/detail/" + this.titleText +
            "%0a%0a*Download%20FinalRevise%20App%20:%20https://play.google.com/store/apps/details?id%3Dcom.finalrevise.android*");
    }
    openMail() {
        window.open("mailto:?subject=FinalRevise%20Study%20Material%20for%20" + this.nav[this.navCount - 1] +
            "&body=Download%20" + this.replaceAllSocial(this.titleText) + "%0a%0a" + "http://finalrevise.com/detail/" + this.titleText +
            "%0a%0a Download FinalRevise App : https://play.google.com/store/apps/details?id=com.finalrevise.android");
    }
    openMessenger() {
        if (_angular_common__WEBPACK_IMPORTED_MODULE_6__["isPlatformBrowser"]) {
            window.open("https://m.me/finalrevise?u=http://finalrevise.com/detail/" + this.titleText);
        }
        else {
            window.open('fb-messenger://share?link=' + encodeURIComponent("http://finalrevise.com/detail/" + this.titleText) + '&app_id='
                + encodeURIComponent("358637287840510"));
        }
    }
    setTitleMeta() {
        this.title.setTitle(this.pageData.data.tag);
        this.meta.addTags([
            { name: 'author', content: 'finalrevise.com' },
            { name: 'keywords', content: this.pageData.data.keywords },
            {
                name: 'description', content: this.pageData.data.description
            }
        ]);
    }
    replaceAll(str, replacement) {
        str = str.split('(').join('');
        str = str.split(')').join('');
        str = str.split('.').join('');
        str = str.split('/').join('');
        str = str.split('|').join(':');
        str = str.split(' ').join(replacement);
        str = str.split('---').join(replacement);
        return str.split('--').join(replacement);
    }
    ;
    openDialog(link, titleText, paperName) {
        const dialogRef = this.dialog.open(DialogContentDialog, {
            data: {
                link: link,
                titleText: titleText,
                paperName: paperName
            }
        });
    }
};
StudyMetMobileComponent.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__["Meta"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__["Title"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__["DomSanitizer"] },
    { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_7__["MatDialog"] },
    { type: ngx_device_detector__WEBPACK_IMPORTED_MODULE_8__["DeviceDetectorService"] },
    { type: _data_service__WEBPACK_IMPORTED_MODULE_9__["CounterService"] },
    { type: _app_header_header_service__WEBPACK_IMPORTED_MODULE_11__["HeaderService"] }
];
StudyMetMobileComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'study-met',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./mobile-study-met.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/study-met/mobile/mobile-study-met.component.html")).default,
        providers: [_data_service__WEBPACK_IMPORTED_MODULE_9__["CounterService"]],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./mobile-study-met.component.css */ "./src/app/study-met/mobile/mobile-study-met.component.css")).default]
    })
], StudyMetMobileComponent);

let DialogContentDialog = class DialogContentDialog {
    constructor(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.isValid = false;
        let timer = rxjs_observable_TimerObservable__WEBPACK_IMPORTED_MODULE_10__["TimerObservable"].create(0, 1000);
        this.subscription = timer.subscribe(t => {
            this.tick = t;
            if (this.tick > 15) {
                this.isValid = true;
            }
        });
    }
    ngOnInit() {
    }
    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
    openDialog() {
        this.dialogRef.close();
        this.openPdfViewer(this.data.link, this.data.titleText, this.data.paperName);
        // window.open(this.data.link)
    }
    openPdfViewer(link, titleText, paperName) {
        if (link.includes('open')) {
            window.open('/pdf-viewer/' + titleText + "/" + paperName + "/" + link.substring(link.indexOf("?id=") + 4), "_self");
        }
        else if (link.includes("/view")) {
            window.open('/pdf-viewer/' + titleText + "/" + paperName + "/" + link.replace("/view", ").substring(link.indexOf("/d/") + 3), "_self");
        }
        else {
            window.open(link);
        }
    }
    openFacebook() {
        window.open("https://www.facebook.com/sharer/sharer.php?u=http://finalrevise.com/detail/" + this.data.titleText);
    }
    openWhatsApp() {
        window.open("https://api.whatsapp.com/send?text=*Download%20" + this.data.titleText + "*%0a%0a" + "http://finalrevise.com/detail/" + this.data.titleText +
            "%0a%0a*Download%20FinalRevise%20App%20:%20https://play.google.com/store/apps/details?id%3Dcom.finalrevise.android*");
    }
    openMail() {
        window.open("mailto:?subject=FinalRevise%20Study%20material%20related%20to%20" + this.data.paperName +
            "&body=Download%20all%20Study%20material%20related%20to%20" + this.data.paperName + "%20:%20" +
            this.data.paperName + "%20" + "http://finalrevise.com/detail/" + this.data.titleText);
        this.openDialog();
    }
    openMessenger() {
        window.open('fb-messenger://share?link=' + encodeURIComponent("http://finalrevise.com/detail/" + this.data.titleText) + '&app_id='
            + encodeURIComponent("358637287840510"));
        this.openDialog();
    }
};
DialogContentDialog.ctorParameters = () => [
    { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_7__["MatDialogRef"] },
    { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: [_angular_material_dialog__WEBPACK_IMPORTED_MODULE_7__["MAT_DIALOG_DATA"],] }] }
];
DialogContentDialog = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'dialog-content-example-dialog',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./dialog-app-install.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/study-met/mobile/dialog-app-install.html")).default,
    }),
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__param"])(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_material_dialog__WEBPACK_IMPORTED_MODULE_7__["MAT_DIALOG_DATA"]))
], DialogContentDialog);



/***/ }),

/***/ "./src/app/subcategory/desktop/subcat-desktop.component.scss":
/*!*******************************************************************!*\
  !*** ./src/app/subcategory/desktop/subcat-desktop.component.scss ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("section {\n  margin: 5px 0px;\n  background: linear-gradient(90deg, rgba(144, 71, 168, 0.5), #eed8b9);\n  padding: 0 15%;\n}\n\n.descri {\n  margin-top: -4px;\n  padding: 2%;\n  background-color: #ecf0f1;\n  color: #8b0000;\n  height: 50px;\n  overflow-y: hidden;\n  border-radius: 0px 0px 10px 10px;\n  font-size: 120%;\n  text-shadow: 2px 2px 4px rgba(0, 0, 0, 0.18);\n}\n\n.tableHeader {\n  margin-top: 75px;\n  table-layout: fixed;\n  width: 100%;\n  border-collapse: unset;\n}\n\n.card {\n  width: 45%;\n  display: inline-block;\n  text-align: center;\n  margin-left: 1.1%;\n  margin-right: 1.1%;\n  margin-bottom: 2%;\n  background-color: #ecf0f1;\n  box-shadow: 0 0 4px rgba(0, 0, 0, 0.14), 0 4px 8px rgba(0, 0, 0, 0.28);\n}\n\nimg {\n  width: 100%;\n  height: 100%;\n}\n\n.nav-link {\n  display: block;\n  background: #8b0000;\n  margin-top: 8%;\n  margin-right: 2%;\n}\n\n.col-sm-8 {\n  margin-top: 5%;\n}\n\n.row {\n  margin-top: 2%;\n}\n\n.parallax {\n  /* The image used */\n  /* Set a specific height */\n  min-height: 80vh;\n  /* Create the parallax scrolling effect */\n  background-attachment: fixed;\n  background-position: center;\n  background-repeat: no-repeat;\n  background-size: cover;\n  padding-top: 7.5%;\n  padding-left: 2%;\n  padding-right: 2%;\n  min-height: 80vh;\n}\n\n/* Zoom In #1 */\n\n.hover01 figure img {\n  transform: scale(1);\n  transition: .3s ease-in-out;\n}\n\n.hover01 figure:hover img {\n  transform: scale(1.3);\n}\n\n.column::after {\n  content: '';\n  clear: both;\n  display: block;\n}\n\n.column div:first-child {\n  margin-left: 0;\n}\n\n.column div span {\n  position: absolute;\n  bottom: -20px;\n  left: 0;\n  z-index: -1;\n  display: block;\n  width: 300px;\n  margin: 0;\n  padding: 0;\n  color: #444;\n  font-size: 18px;\n  text-decoration: none;\n  text-align: center;\n  transition: .3s ease-in-out;\n  opacity: 0;\n}\n\nfigure {\n  margin: 0;\n  padding: 0;\n  background: #fff;\n  overflow: hidden;\n}\n\nfigure:hover + span {\n  bottom: -36px;\n  opacity: 1;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Wb2x1bWVzL0ZpbmFsUmV2aXNlL2ZpbmFscmV2aXNlX3dlYnNpdGUvc3JjL2FwcC9zdWJjYXRlZ29yeS9kZXNrdG9wL3N1YmNhdC1kZXNrdG9wLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9zdWJjYXRlZ29yeS9kZXNrdG9wL3N1YmNhdC1kZXNrdG9wLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsZUFBZTtFQUNmLG9FQUE2RTtFQUM3RSxjQUFjO0FDQ2hCOztBREVBO0VBQ0UsZ0JBQWdCO0VBQ2hCLFdBQVc7RUFDWCx5QkFBeUI7RUFDekIsY0FBYztFQUNkLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsZ0NBQWdDO0VBQ2hDLGVBQWU7RUFDZiw0Q0FBNkM7QUNDL0M7O0FEQ0E7RUFDRSxnQkFBZ0I7RUFDaEIsbUJBQW1CO0VBQ25CLFdBQVc7RUFDWCxzQkFBc0I7QUNFeEI7O0FEQUE7RUFDRSxVQUFVO0VBQ1YscUJBQXFCO0VBQ3JCLGtCQUFrQjtFQUNsQixpQkFBaUI7RUFDakIsa0JBQWtCO0VBQ2xCLGlCQUFpQjtFQUNqQix5QkFBeUI7RUFDekIsc0VBQW9FO0FDR3RFOztBRERBO0VBQ0UsV0FBVztFQUNYLFlBQVk7QUNJZDs7QURGQTtFQUNFLGNBQWM7RUFDZCxtQkFBbUI7RUFDbkIsY0FBYztFQUNkLGdCQUFnQjtBQ0tsQjs7QURIQTtFQUNFLGNBQWM7QUNNaEI7O0FESkE7RUFDRSxjQUFjO0FDT2hCOztBRExBO0VBQ0UsbUJBQUE7RUFHQSwwQkFBQTtFQUNBLGdCQUFnQjtFQUVoQix5Q0FBQTtFQUNBLDRCQUE0QjtFQUM1QiwyQkFBMkI7RUFDM0IsNEJBQTRCO0VBQzVCLHNCQUFzQjtFQUN0QixpQkFBaUI7RUFDakIsZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQixnQkFBZ0I7QUNLbEI7O0FEQUEsZUFBQTs7QUFDQTtFQUVDLG1CQUFtQjtFQUVuQiwyQkFBMkI7QUNHNUI7O0FEREE7RUFFQyxxQkFBcUI7QUNJdEI7O0FEREE7RUFDQyxXQUFXO0VBQ1gsV0FBVztFQUNYLGNBQWM7QUNJZjs7QUREQTtFQUNDLGNBQWM7QUNJZjs7QURGQTtFQUNDLGtCQUFrQjtFQUNsQixhQUFhO0VBQ2IsT0FBTztFQUNQLFdBQVc7RUFDWCxjQUFjO0VBQ2QsWUFBWTtFQUNaLFNBQVM7RUFDVCxVQUFVO0VBQ1YsV0FBVztFQUNYLGVBQWU7RUFDZixxQkFBcUI7RUFDckIsa0JBQWtCO0VBRWxCLDJCQUEyQjtFQUMzQixVQUFVO0FDS1g7O0FESEE7RUFDQyxTQUFTO0VBQ1QsVUFBVTtFQUNWLGdCQUFnQjtFQUNoQixnQkFBZ0I7QUNNakI7O0FESkE7RUFDQyxhQUFhO0VBQ2IsVUFBVTtBQ09YIiwiZmlsZSI6InNyYy9hcHAvc3ViY2F0ZWdvcnkvZGVza3RvcC9zdWJjYXQtZGVza3RvcC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbInNlY3Rpb257XG4gIG1hcmdpbjogNXB4IDBweDtcbiAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDkwZGVnLCByZ2JhKDE0NCw3MSwxNjgsMC41KSwgcmdiYSgyMzgsMjE2LDE4NSwxKSk7XG4gIHBhZGRpbmc6IDAgMTUlO1xufVxuXG4uZGVzY3JpIHtcbiAgbWFyZ2luLXRvcDogLTRweDtcbiAgcGFkZGluZzogMiU7XG4gIGJhY2tncm91bmQtY29sb3I6ICNlY2YwZjE7XG4gIGNvbG9yOiAjOGIwMDAwO1xuICBoZWlnaHQ6IDUwcHg7XG4gIG92ZXJmbG93LXk6IGhpZGRlbjtcbiAgYm9yZGVyLXJhZGl1czogMHB4IDBweCAxMHB4IDEwcHg7XG4gIGZvbnQtc2l6ZTogMTIwJTtcbiAgdGV4dC1zaGFkb3c6IDJweCAycHggNHB4ICByZ2JhKDAsIDAsIDAsIDAuMTgpO1xufVxuLnRhYmxlSGVhZGVye1xuICBtYXJnaW4tdG9wOiA3NXB4O1xuICB0YWJsZS1sYXlvdXQ6IGZpeGVkO1xuICB3aWR0aDogMTAwJTtcbiAgYm9yZGVyLWNvbGxhcHNlOiB1bnNldDtcbn1cbi5jYXJke1xuICB3aWR0aDogNDUlO1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luLWxlZnQ6IDEuMSU7XG4gIG1hcmdpbi1yaWdodDogMS4xJTtcbiAgbWFyZ2luLWJvdHRvbTogMiU7XG4gIGJhY2tncm91bmQtY29sb3I6ICNlY2YwZjE7XG4gIGJveC1zaGFkb3c6IDAgMCA0cHggcmdiYSgwLCAwLCAwLCAuMTQpLCAwIDRweCA4cHggcmdiYSgwLCAwLCAwLCAuMjgpO1xufVxuaW1ne1xuICB3aWR0aDogMTAwJTsgXG4gIGhlaWdodDogMTAwJTtcbn1cbi5uYXYtbGluayB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBiYWNrZ3JvdW5kOiAjOGIwMDAwO1xuICBtYXJnaW4tdG9wOiA4JTtcbiAgbWFyZ2luLXJpZ2h0OiAyJTtcbn1cbi5jb2wtc20tOHtcbiAgbWFyZ2luLXRvcDogNSU7XG59XG4ucm93e1xuICBtYXJnaW4tdG9wOiAyJTtcbn1cbi5wYXJhbGxheCB7XG4gIC8qIFRoZSBpbWFnZSB1c2VkICovXG4gIFxuXG4gIC8qIFNldCBhIHNwZWNpZmljIGhlaWdodCAqL1xuICBtaW4taGVpZ2h0OiA4MHZoO1xuXG4gIC8qIENyZWF0ZSB0aGUgcGFyYWxsYXggc2Nyb2xsaW5nIGVmZmVjdCAqL1xuICBiYWNrZ3JvdW5kLWF0dGFjaG1lbnQ6IGZpeGVkO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gIHBhZGRpbmctdG9wOiA3LjUlO1xuICBwYWRkaW5nLWxlZnQ6IDIlO1xuICBwYWRkaW5nLXJpZ2h0OiAyJTtcbiAgbWluLWhlaWdodDogODB2aDtcbn1cblxuXG5cbi8qIFpvb20gSW4gIzEgKi9cbi5ob3ZlcjAxIGZpZ3VyZSBpbWcge1xuXHQtd2Via2l0LXRyYW5zZm9ybTogc2NhbGUoMSk7XG5cdHRyYW5zZm9ybTogc2NhbGUoMSk7XG5cdC13ZWJraXQtdHJhbnNpdGlvbjogLjNzIGVhc2UtaW4tb3V0O1xuXHR0cmFuc2l0aW9uOiAuM3MgZWFzZS1pbi1vdXQ7XG59XG4uaG92ZXIwMSBmaWd1cmU6aG92ZXIgaW1nIHtcblx0LXdlYmtpdC10cmFuc2Zvcm06IHNjYWxlKDEuMyk7XG5cdHRyYW5zZm9ybTogc2NhbGUoMS4zKTtcbn1cblxuLmNvbHVtbjo6YWZ0ZXIge1xuXHRjb250ZW50OiAnJztcblx0Y2xlYXI6IGJvdGg7XG5cdGRpc3BsYXk6IGJsb2NrO1xufVxuXG4uY29sdW1uIGRpdjpmaXJzdC1jaGlsZCB7XG5cdG1hcmdpbi1sZWZ0OiAwO1xufVxuLmNvbHVtbiBkaXYgc3BhbiB7XG5cdHBvc2l0aW9uOiBhYnNvbHV0ZTtcblx0Ym90dG9tOiAtMjBweDtcblx0bGVmdDogMDtcblx0ei1pbmRleDogLTE7XG5cdGRpc3BsYXk6IGJsb2NrO1xuXHR3aWR0aDogMzAwcHg7XG5cdG1hcmdpbjogMDtcblx0cGFkZGluZzogMDtcblx0Y29sb3I6ICM0NDQ7XG5cdGZvbnQtc2l6ZTogMThweDtcblx0dGV4dC1kZWNvcmF0aW9uOiBub25lO1xuXHR0ZXh0LWFsaWduOiBjZW50ZXI7XG5cdC13ZWJraXQtdHJhbnNpdGlvbjogLjNzIGVhc2UtaW4tb3V0O1xuXHR0cmFuc2l0aW9uOiAuM3MgZWFzZS1pbi1vdXQ7XG5cdG9wYWNpdHk6IDA7XG59XG5maWd1cmUge1xuXHRtYXJnaW46IDA7XG5cdHBhZGRpbmc6IDA7XG5cdGJhY2tncm91bmQ6ICNmZmY7XG5cdG92ZXJmbG93OiBoaWRkZW47XG59XG5maWd1cmU6aG92ZXIrc3BhbiB7XG5cdGJvdHRvbTogLTM2cHg7XG5cdG9wYWNpdHk6IDE7XG59Iiwic2VjdGlvbiB7XG4gIG1hcmdpbjogNXB4IDBweDtcbiAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDkwZGVnLCByZ2JhKDE0NCwgNzEsIDE2OCwgMC41KSwgI2VlZDhiOSk7XG4gIHBhZGRpbmc6IDAgMTUlO1xufVxuXG4uZGVzY3JpIHtcbiAgbWFyZ2luLXRvcDogLTRweDtcbiAgcGFkZGluZzogMiU7XG4gIGJhY2tncm91bmQtY29sb3I6ICNlY2YwZjE7XG4gIGNvbG9yOiAjOGIwMDAwO1xuICBoZWlnaHQ6IDUwcHg7XG4gIG92ZXJmbG93LXk6IGhpZGRlbjtcbiAgYm9yZGVyLXJhZGl1czogMHB4IDBweCAxMHB4IDEwcHg7XG4gIGZvbnQtc2l6ZTogMTIwJTtcbiAgdGV4dC1zaGFkb3c6IDJweCAycHggNHB4IHJnYmEoMCwgMCwgMCwgMC4xOCk7XG59XG5cbi50YWJsZUhlYWRlciB7XG4gIG1hcmdpbi10b3A6IDc1cHg7XG4gIHRhYmxlLWxheW91dDogZml4ZWQ7XG4gIHdpZHRoOiAxMDAlO1xuICBib3JkZXItY29sbGFwc2U6IHVuc2V0O1xufVxuXG4uY2FyZCB7XG4gIHdpZHRoOiA0NSU7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW4tbGVmdDogMS4xJTtcbiAgbWFyZ2luLXJpZ2h0OiAxLjElO1xuICBtYXJnaW4tYm90dG9tOiAyJTtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2VjZjBmMTtcbiAgYm94LXNoYWRvdzogMCAwIDRweCByZ2JhKDAsIDAsIDAsIDAuMTQpLCAwIDRweCA4cHggcmdiYSgwLCAwLCAwLCAwLjI4KTtcbn1cblxuaW1nIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwJTtcbn1cblxuLm5hdi1saW5rIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGJhY2tncm91bmQ6ICM4YjAwMDA7XG4gIG1hcmdpbi10b3A6IDglO1xuICBtYXJnaW4tcmlnaHQ6IDIlO1xufVxuXG4uY29sLXNtLTgge1xuICBtYXJnaW4tdG9wOiA1JTtcbn1cblxuLnJvdyB7XG4gIG1hcmdpbi10b3A6IDIlO1xufVxuXG4ucGFyYWxsYXgge1xuICAvKiBUaGUgaW1hZ2UgdXNlZCAqL1xuICAvKiBTZXQgYSBzcGVjaWZpYyBoZWlnaHQgKi9cbiAgbWluLWhlaWdodDogODB2aDtcbiAgLyogQ3JlYXRlIHRoZSBwYXJhbGxheCBzY3JvbGxpbmcgZWZmZWN0ICovXG4gIGJhY2tncm91bmQtYXR0YWNobWVudDogZml4ZWQ7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgcGFkZGluZy10b3A6IDcuNSU7XG4gIHBhZGRpbmctbGVmdDogMiU7XG4gIHBhZGRpbmctcmlnaHQ6IDIlO1xuICBtaW4taGVpZ2h0OiA4MHZoO1xufVxuXG4vKiBab29tIEluICMxICovXG4uaG92ZXIwMSBmaWd1cmUgaW1nIHtcbiAgLXdlYmtpdC10cmFuc2Zvcm06IHNjYWxlKDEpO1xuICB0cmFuc2Zvcm06IHNjYWxlKDEpO1xuICAtd2Via2l0LXRyYW5zaXRpb246IC4zcyBlYXNlLWluLW91dDtcbiAgdHJhbnNpdGlvbjogLjNzIGVhc2UtaW4tb3V0O1xufVxuXG4uaG92ZXIwMSBmaWd1cmU6aG92ZXIgaW1nIHtcbiAgLXdlYmtpdC10cmFuc2Zvcm06IHNjYWxlKDEuMyk7XG4gIHRyYW5zZm9ybTogc2NhbGUoMS4zKTtcbn1cblxuLmNvbHVtbjo6YWZ0ZXIge1xuICBjb250ZW50OiAnJztcbiAgY2xlYXI6IGJvdGg7XG4gIGRpc3BsYXk6IGJsb2NrO1xufVxuXG4uY29sdW1uIGRpdjpmaXJzdC1jaGlsZCB7XG4gIG1hcmdpbi1sZWZ0OiAwO1xufVxuXG4uY29sdW1uIGRpdiBzcGFuIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBib3R0b206IC0yMHB4O1xuICBsZWZ0OiAwO1xuICB6LWluZGV4OiAtMTtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHdpZHRoOiAzMDBweDtcbiAgbWFyZ2luOiAwO1xuICBwYWRkaW5nOiAwO1xuICBjb2xvcjogIzQ0NDtcbiAgZm9udC1zaXplOiAxOHB4O1xuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgLXdlYmtpdC10cmFuc2l0aW9uOiAuM3MgZWFzZS1pbi1vdXQ7XG4gIHRyYW5zaXRpb246IC4zcyBlYXNlLWluLW91dDtcbiAgb3BhY2l0eTogMDtcbn1cblxuZmlndXJlIHtcbiAgbWFyZ2luOiAwO1xuICBwYWRkaW5nOiAwO1xuICBiYWNrZ3JvdW5kOiAjZmZmO1xuICBvdmVyZmxvdzogaGlkZGVuO1xufVxuXG5maWd1cmU6aG92ZXIgKyBzcGFuIHtcbiAgYm90dG9tOiAtMzZweDtcbiAgb3BhY2l0eTogMTtcbn1cbiJdfQ== */");

/***/ }),

/***/ "./src/app/subcategory/desktop/subcat-desktop.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/subcategory/desktop/subcat-desktop.component.ts ***!
  \*****************************************************************/
/*! exports provided: SubcatDesktopComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SubcatDesktopComponent", function() { return SubcatDesktopComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var rxjs_Rx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/Rx */ "./node_modules/rxjs-compat/_esm2015/Rx.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");
/* harmony import */ var _app_header_header_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../app-header/header.service */ "./src/app/app-header/header.service.ts");







let SubcatDesktopComponent = class SubcatDesktopComponent {
    constructor(http, activatedRoute, router, meta, title, headerService) {
        this.http = http;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.headerService = headerService;
        this.pageTitle = ";
        this.apiUrl = 'https://api.finalrevise.com/finalrevise/v1/entity/';
        this.pageData = {};
        this.keys = [];
        this.nav = [];
        this.navIds = [];
        this.tabUrl = 'https://api.finalrevise.com/finalrevise/v1/relatedlinks/';
        this.router.routeReuseStrategy.shouldReuseRoute = function () {
            return false;
        };
        this.title = title;
        this.meta = meta;
    }
    ngOnInit() {
        this.headerService.setIsHome(false);
        // subscribe to router event
        this.activatedRoute.params.subscribe((params) => {
            this.pageTitle = params['title'];
        });
        this.apiUrl = this.apiUrl + this.pageTitle;
        this.tabUrl = this.tabUrl + this.pageTitle;
        this.getPageData();
        this.getTabData();
    }
    openNextPage(tag) {
        if (tag.includes('Placement')) {
            this.router.navigateByUrl('/detail-list/placement-papers');
        }
        else {
            this.router.navigateByUrl('/detail-list/' + tag);
        }
    }
    getData() {
        return this.http.get(this.apiUrl);
    }
    getPageData() {
        this.getData().subscribe(data => {
            this.pageData = data;
            this.nav = String(this.pageData.data.pagePath).split("#");
            this.navIds = String(this.pageData.data.pagePathId).split("#");
            this.getKeys(this.pageData.data.groupedPageItems);
            this.setTitleMeta();
        });
    }
    getTData() {
        return this.http.get(this.tabUrl);
    }
    getTabData() {
        this.getTData().subscribe(data => {
            this.tabs = data.data.tabs;
            this.headerService.setRelatedTopics(this.tabs);
        });
    }
    getKeys(obj) {
        var r = [];
        for (var k in obj) {
            if (!obj.hasOwnProperty(k))
                continue;
            r.push(k);
        }
        this.keys = r;
    }
    setTitleMeta() {
        this.title.setTitle(this.pageData.data.tag);
        this.meta.addTags([
            { name: 'author', content: 'finalrevise.com' },
            { name: 'keywords', content: this.pageData.data.keywords },
            {
                name: 'description', content: this.pageData.data.description
            }
        ]);
    }
    replaceAll(str, replacement) {
        str = str.split('(').join('');
        str = str.split(')').join('');
        str = str.split('.').join('');
        str = str.split('/').join('');
        str = str.split('|').join(':');
        str = str.split(' ').join(replacement);
        str = str.split('---').join(replacement);
        return str.split('--').join(replacement);
    }
    ;
};
SubcatDesktopComponent.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__["Meta"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__["Title"] },
    { type: _app_header_header_service__WEBPACK_IMPORTED_MODULE_6__["HeaderService"] }
];
SubcatDesktopComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'subcat-desktop',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./subcat-desktop.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/subcategory/desktop/subcat-desktop.component.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./subcat-desktop.component.scss */ "./src/app/subcategory/desktop/subcat-desktop.component.scss")).default]
    })
], SubcatDesktopComponent);



/***/ }),

/***/ "./src/app/subcategory/mobile/subcat-mobile.component.scss":
/*!*****************************************************************!*\
  !*** ./src/app/subcategory/mobile/subcat-mobile.component.scss ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("section {\n  margin: 5px 0px;\n  background: linear-gradient(90deg, rgba(144, 71, 168, 0.5), #eed8b9);\n  padding: 0 15%;\n}\n\n.descri {\n  margin-top: -4px;\n  padding: 2%;\n  background-color: #ecf0f1;\n  color: #8b0000;\n  height: 35px;\n  overflow-y: hidden;\n  border-radius: 0px 0px 10px 10px;\n  text-shadow: 2px 2px 4px rgba(0, 0, 0, 0.18);\n}\n\n.tableHeader {\n  margin-top: 75px;\n  table-layout: fixed;\n  width: 100%;\n  border-collapse: unset;\n}\n\n.card {\n  display: inline-block;\n  text-align: center;\n  margin-bottom: 3%;\n  background-color: #ecf0f1;\n  box-shadow: 0 0 4px rgba(0, 0, 0, 0.14), 0 4px 8px rgba(0, 0, 0, 0.28);\n}\n\nimg {\n  width: 100%;\n  height: 100%;\n}\n\n.nav-link {\n  display: block;\n  background: #8b0000;\n  margin-top: 8%;\n  margin-right: 2%;\n}\n\n.col-sm-8 {\n  margin-top: 5%;\n}\n\n.main-content {\n  padding-top: 18%;\n  min-height: 80vh;\n  padding-left: 3%;\n  padding-right: 3%;\n  /* The image used */\n  /* Create the parallax scrolling effect */\n  background-attachment: fixed;\n  background-position: center;\n  background-repeat: no-repeat;\n  background-size: cover;\n}\n\n.loader {\n  background: url(\"/assets/images/loading_small.gif\") 50% 50% no-repeat #f9f9f9;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Wb2x1bWVzL0ZpbmFsUmV2aXNlL2ZpbmFscmV2aXNlX3dlYnNpdGUvc3JjL2FwcC9zdWJjYXRlZ29yeS9tb2JpbGUvc3ViY2F0LW1vYmlsZS5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvc3ViY2F0ZWdvcnkvbW9iaWxlL3N1YmNhdC1tb2JpbGUuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxlQUFlO0VBQ2Ysb0VBQTZFO0VBQzdFLGNBQWM7QUNDaEI7O0FERUE7RUFDRSxnQkFBZ0I7RUFDaEIsV0FBVztFQUNYLHlCQUF5QjtFQUN6QixjQUFjO0VBQ2QsWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixnQ0FBZ0M7RUFDaEMsNENBQTZDO0FDQy9DOztBRENBO0VBQ0UsZ0JBQWdCO0VBQ2hCLG1CQUFtQjtFQUNuQixXQUFXO0VBQ1gsc0JBQXNCO0FDRXhCOztBREFBO0VBQ0UscUJBQXFCO0VBQ3JCLGtCQUFrQjtFQUNsQixpQkFBaUI7RUFDakIseUJBQXlCO0VBQ3pCLHNFQUFvRTtBQ0d0RTs7QUREQTtFQUNFLFdBQVc7RUFDWCxZQUFZO0FDSWQ7O0FERkE7RUFDRSxjQUFjO0VBQ2QsbUJBQW1CO0VBQ25CLGNBQWM7RUFDZCxnQkFBZ0I7QUNLbEI7O0FESEE7RUFDRSxjQUFjO0FDTWhCOztBREpBO0VBQ0UsZ0JBQWdCO0VBQ2hCLGdCQUFnQjtFQUNoQixnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLG1CQUFBO0VBR0EseUNBQUE7RUFDQSw0QkFBNEI7RUFDNUIsMkJBQTJCO0VBQzNCLDRCQUE0QjtFQUM1QixzQkFBc0I7QUNLeEI7O0FESEE7RUFDRSw2RUFBc0Y7QUNNeEYiLCJmaWxlIjoic3JjL2FwcC9zdWJjYXRlZ29yeS9tb2JpbGUvc3ViY2F0LW1vYmlsZS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbInNlY3Rpb257XG4gIG1hcmdpbjogNXB4IDBweDtcbiAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDkwZGVnLCByZ2JhKDE0NCw3MSwxNjgsMC41KSwgcmdiYSgyMzgsMjE2LDE4NSwxKSk7XG4gIHBhZGRpbmc6IDAgMTUlO1xufVxuXG4uZGVzY3JpIHtcbiAgbWFyZ2luLXRvcDogLTRweDtcbiAgcGFkZGluZzogMiU7XG4gIGJhY2tncm91bmQtY29sb3I6ICNlY2YwZjE7XG4gIGNvbG9yOiAjOGIwMDAwO1xuICBoZWlnaHQ6IDM1cHg7XG4gIG92ZXJmbG93LXk6IGhpZGRlbjtcbiAgYm9yZGVyLXJhZGl1czogMHB4IDBweCAxMHB4IDEwcHg7XG4gIHRleHQtc2hhZG93OiAycHggMnB4IDRweCAgcmdiYSgwLCAwLCAwLCAwLjE4KTtcbn1cbi50YWJsZUhlYWRlcntcbiAgbWFyZ2luLXRvcDogNzVweDtcbiAgdGFibGUtbGF5b3V0OiBmaXhlZDtcbiAgd2lkdGg6IDEwMCU7XG4gIGJvcmRlci1jb2xsYXBzZTogdW5zZXQ7XG59XG4uY2FyZHtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1hcmdpbi1ib3R0b206IDMlO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWNmMGYxO1xuICBib3gtc2hhZG93OiAwIDAgNHB4IHJnYmEoMCwgMCwgMCwgLjE0KSwgMCA0cHggOHB4IHJnYmEoMCwgMCwgMCwgLjI4KTtcbn1cbmltZ3tcbiAgd2lkdGg6IDEwMCU7IFxuICBoZWlnaHQ6IDEwMCU7XG59XG4ubmF2LWxpbmsge1xuICBkaXNwbGF5OiBibG9jaztcbiAgYmFja2dyb3VuZDogIzhiMDAwMDtcbiAgbWFyZ2luLXRvcDogOCU7XG4gIG1hcmdpbi1yaWdodDogMiU7XG59XG4uY29sLXNtLTh7XG4gIG1hcmdpbi10b3A6IDUlO1xufVxuLm1haW4tY29udGVudCB7XG4gIHBhZGRpbmctdG9wOiAxOCU7XG4gIG1pbi1oZWlnaHQ6IDgwdmg7XG4gIHBhZGRpbmctbGVmdDogMyU7XG4gIHBhZGRpbmctcmlnaHQ6IDMlO1xuICAvKiBUaGUgaW1hZ2UgdXNlZCAqL1xuICBcblxuICAvKiBDcmVhdGUgdGhlIHBhcmFsbGF4IHNjcm9sbGluZyBlZmZlY3QgKi9cbiAgYmFja2dyb3VuZC1hdHRhY2htZW50OiBmaXhlZDtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xufVxuLmxvYWRlciB7XG4gIGJhY2tncm91bmQ6IHVybCgnL2Fzc2V0cy9pbWFnZXMvbG9hZGluZ19zbWFsbC5naWYnKSA1MCUgNTAlIG5vLXJlcGVhdCByZ2IoMjQ5LDI0OSwyNDkpO1xufSAiLCJzZWN0aW9uIHtcbiAgbWFyZ2luOiA1cHggMHB4O1xuICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoOTBkZWcsIHJnYmEoMTQ0LCA3MSwgMTY4LCAwLjUpLCAjZWVkOGI5KTtcbiAgcGFkZGluZzogMCAxNSU7XG59XG5cbi5kZXNjcmkge1xuICBtYXJnaW4tdG9wOiAtNHB4O1xuICBwYWRkaW5nOiAyJTtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2VjZjBmMTtcbiAgY29sb3I6ICM4YjAwMDA7XG4gIGhlaWdodDogMzVweDtcbiAgb3ZlcmZsb3cteTogaGlkZGVuO1xuICBib3JkZXItcmFkaXVzOiAwcHggMHB4IDEwcHggMTBweDtcbiAgdGV4dC1zaGFkb3c6IDJweCAycHggNHB4IHJnYmEoMCwgMCwgMCwgMC4xOCk7XG59XG5cbi50YWJsZUhlYWRlciB7XG4gIG1hcmdpbi10b3A6IDc1cHg7XG4gIHRhYmxlLWxheW91dDogZml4ZWQ7XG4gIHdpZHRoOiAxMDAlO1xuICBib3JkZXItY29sbGFwc2U6IHVuc2V0O1xufVxuXG4uY2FyZCB7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW4tYm90dG9tOiAzJTtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2VjZjBmMTtcbiAgYm94LXNoYWRvdzogMCAwIDRweCByZ2JhKDAsIDAsIDAsIDAuMTQpLCAwIDRweCA4cHggcmdiYSgwLCAwLCAwLCAwLjI4KTtcbn1cblxuaW1nIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwJTtcbn1cblxuLm5hdi1saW5rIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGJhY2tncm91bmQ6ICM4YjAwMDA7XG4gIG1hcmdpbi10b3A6IDglO1xuICBtYXJnaW4tcmlnaHQ6IDIlO1xufVxuXG4uY29sLXNtLTgge1xuICBtYXJnaW4tdG9wOiA1JTtcbn1cblxuLm1haW4tY29udGVudCB7XG4gIHBhZGRpbmctdG9wOiAxOCU7XG4gIG1pbi1oZWlnaHQ6IDgwdmg7XG4gIHBhZGRpbmctbGVmdDogMyU7XG4gIHBhZGRpbmctcmlnaHQ6IDMlO1xuICAvKiBUaGUgaW1hZ2UgdXNlZCAqL1xuICAvKiBDcmVhdGUgdGhlIHBhcmFsbGF4IHNjcm9sbGluZyBlZmZlY3QgKi9cbiAgYmFja2dyb3VuZC1hdHRhY2htZW50OiBmaXhlZDtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xufVxuXG4ubG9hZGVyIHtcbiAgYmFja2dyb3VuZDogdXJsKFwiL2Fzc2V0cy9pbWFnZXMvbG9hZGluZ19zbWFsbC5naWZcIikgNTAlIDUwJSBuby1yZXBlYXQgI2Y5ZjlmOTtcbn1cbiJdfQ== */");

/***/ }),

/***/ "./src/app/subcategory/mobile/subcat-mobile.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/subcategory/mobile/subcat-mobile.component.ts ***!
  \***************************************************************/
/*! exports provided: SubcatComponentMobile */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SubcatComponentMobile", function() { return SubcatComponentMobile; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var rxjs_Rx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/Rx */ "./node_modules/rxjs-compat/_esm2015/Rx.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");
/* harmony import */ var _app_header_header_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../app-header/header.service */ "./src/app/app-header/header.service.ts");



;




let SubcatComponentMobile = class SubcatComponentMobile {
    constructor(http, activatedRoute, router, meta, title, headerService) {
        this.http = http;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.headerService = headerService;
        this.pageTitle = ";
        this.apiUrl = 'https://api.finalrevise.com/finalrevise/v1/entity/';
        this.pageData = {};
        this.keys = [];
        this.nav = [];
        this.navIds = [];
        this.tabUrl = 'https://api.finalrevise.com/finalrevise/v1/relatedlinks/';
        this.router.routeReuseStrategy.shouldReuseRoute = function () {
            return false;
        };
        this.title = title;
        this.meta = meta;
    }
    ngOnInit() {
        this.headerService.setIsHome(false);
        // subscribe to router event
        this.activatedRoute.params.subscribe((params) => {
            this.pageTitle = params['title'];
        });
        this.apiUrl = this.apiUrl + this.pageTitle;
        this.tabUrl = this.tabUrl + this.pageTitle;
        this.getPageData();
        this.getTabData();
    }
    openNextPage(tag) {
        if (tag.includes('Placement')) {
            this.router.navigateByUrl('/detail-list/placement-papers');
        }
        else {
            this.router.navigateByUrl('/detail-list/' + tag);
        }
    }
    getData() {
        return this.http.get(this.apiUrl);
    }
    getPageData() {
        this.getData().subscribe(data => {
            this.pageData = data;
            this.nav = String(this.pageData.data.pagePath).split("#");
            this.navIds = String(this.pageData.data.pagePathId).split("#");
            this.getKeys(this.pageData.data.groupedPageItems);
            this.setTitleMeta();
            document.getElementById('loader').style.display = 'none';
        });
    }
    getTData() {
        return this.http.get(this.tabUrl);
    }
    getTabData() {
        this.getTData().subscribe(data => {
            this.tabs = data.data.tabs;
            this.headerService.setRelatedTopics(this.tabs);
        });
    }
    getKeys(obj) {
        var r = [];
        for (var k in obj) {
            if (!obj.hasOwnProperty(k))
                continue;
            r.push(k);
        }
        this.keys = r;
    }
    setTitleMeta() {
        this.title.setTitle(this.pageData.data.tag);
        this.meta.addTags([
            { name: 'author', content: 'finalrevise.com' },
            { name: 'keywords', content: this.pageData.data.keywords },
            {
                name: 'description', content: this.pageData.data.description
            }
        ]);
    }
    replaceAll(str, replacement) {
        str = str.split('(').join('');
        str = str.split(')').join('');
        str = str.split('.').join('');
        str = str.split('/').join('');
        str = str.split('|').join(':');
        str = str.split(' ').join(replacement);
        str = str.split('---').join(replacement);
        return str.split('--').join(replacement);
    }
    ;
};
SubcatComponentMobile.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__["Meta"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__["Title"] },
    { type: _app_header_header_service__WEBPACK_IMPORTED_MODULE_6__["HeaderService"] }
];
SubcatComponentMobile = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'subcat-mobile',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./subcat-mobile.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/subcategory/mobile/subcat-mobile.component.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./subcat-mobile.component.scss */ "./src/app/subcategory/mobile/subcat-mobile.component.scss")).default]
    })
], SubcatComponentMobile);



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

const environment = {
    production: false
};


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/__ivy_ngcc__/fesm2015/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");





if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
}
document.addEventListener('DOMContentLoaded', () => {
    Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"])
        .catch(err => console.error(err));
});


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Volumes/FinalRevise/finalrevise_website/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map