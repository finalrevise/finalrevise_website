import { Component } from '@angular/core';
import { UploadFileService } from './fileUpload.service';
import { HttpClient, HttpResponse, HttpEventType, HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent {

  title = 'Final Revise';
  selectedFiles: FileList;
	currentFileUpload: File;
	uploadMsg: any = '';

  constructor(private uploadService: UploadFileService){

  }

  // ----------

	selectFile(event) {
		this.selectedFiles = event.target.files;
	}
	upload() {
		var filesAmount = this.selectedFiles.length;
		for (let i = 0; i < filesAmount; i++) {

			this.currentFileUpload = this.selectedFiles.item(i);
			this.uploadService.pushFileToStorage(this.currentFileUpload).subscribe(event => {
				if (event instanceof HttpResponse) {
					this.uploadMsg = this.uploadMsg + event.body;
				} 
			});
		}
		this.uploadMsg = '';
		this.selectedFiles = undefined;
	}
}
