import {Injectable} from '@angular/core';
import { DeviceDetectorService } from 'ngx-device-detector';

@Injectable()
export class ApplicationStateService {

  private isMobileResolution: boolean;

  constructor(private deviceService: DeviceDetectorService) {
    if (this.deviceService.isMobile()) {
      this.isMobileResolution = true;
    } else {
      this.isMobileResolution = false;
    }
  }

  public getIsMobileResolution(): boolean {
    return this.isMobileResolution;
  }
}
