import { Component, OnInit } from '@angular/core';
import 'rxjs/Rx';
import { Meta, Title } from "@angular/platform-browser";

@Component({
	selector: 'contact',
	templateUrl: './contact.component.html',
	styleUrls: ['./contact.component.css']
})

export class ContactComponent {
	
	constructor(meta: Meta, title: Title) {
		title.setTitle("Contact to FinalRevise.com");

		meta.addTags([
			{ name: 'author', content: 'finalrevise.com' },
			{ name: 'keywords', content: 'contact us, finalrevise' },
			{ name: 'description', content: 'Get our phone number, mail id and social media accounts.' }
		]);
	}
	
}
