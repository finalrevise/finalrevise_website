import { Component, OnInit } from '@angular/core';
import { HttpClient} from '@angular/common/http';;
import 'rxjs/Rx';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Meta, Title } from "@angular/platform-browser";
import { HeaderService } from '../../app-header/header.service';

@Component({
  selector: 'subcat-mobile',
  templateUrl: './subcat-mobile.component.html',
  styleUrls: ['./subcat-mobile.component.scss']
})
export class SubcatComponentMobile implements OnInit {

  pageTitle: any = "";
  private apiUrl = 'https://api.finalrevise.com/finalrevise/v1/entity/';
  pageData: any = {};
  keys: any = [];
  nav: any = [];
  navIds: any = [];
  title: Title;
  meta: Meta;
  private tabUrl = 'https://api.finalrevise.com/finalrevise/v1/relatedlinks/';
  tabs: any;

  ngOnInit() {
    this.headerService.setIsHome(false);
    // subscribe to router event
    this.activatedRoute.params.subscribe((params: Params) => {
      this.pageTitle = params['title'];
    });
    this.apiUrl = this.apiUrl + this.pageTitle;
    this.tabUrl = this.tabUrl + this.pageTitle;
    this.getPageData()
    this.getTabData()
  }

  constructor(private http: HttpClient, private activatedRoute: ActivatedRoute, private router: Router, meta: Meta, title: Title,
    private headerService: HeaderService) {
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };
    this.title = title;
    this.meta = meta;
  }

  openNextPage(tag) {
    if(tag.includes('Placement')){
      this.router.navigateByUrl('/detail-list/placement-papers')
    }else{
      this.router.navigateByUrl('/detail-list/' + tag)
    }
}

  getData() {
    return this.http.get<any>(this.apiUrl)
  }

  getPageData() {
    this.getData().subscribe(data => {
      this.pageData = data
      this.nav = String(this.pageData.data.pagePath).split("#");
      this.navIds = String(this.pageData.data.pagePathId).split("#");
      this.getKeys(this.pageData.data.groupedPageItems);
      this.setTitleMeta();
      document.getElementById('loader').style.display = 'none';
    },
		error => this.router.navigateByUrl('404'))
  }

  getTData() {
    return this.http.get<any>(this.tabUrl)
  }

  getTabData() {
    this.getTData().subscribe(data => {
      this.tabs = data.data.tabs
      this.headerService.setRelatedTopics(this.tabs);
    })
  }

  getKeys(obj) {
    var r = []
    for (var k in obj) {
      if (!obj.hasOwnProperty(k))
        continue
      r.push(k)
    }
    this.keys = r;
  }

  setTitleMeta() {
    this.title.setTitle(this.pageData.data.tag);
    this.meta.addTags([
      { name: 'author', content: 'finalrevise.com' },
      { name: 'keywords', content: this.pageData.data.keywords },
      {
        name: 'description', content: this.pageData.data.description
      }
    ]);
  }

  replaceAll(str, replacement) {
    str = str.split('(').join('')
    str = str.split(')').join('')
    str = str.split('.').join('')
    str = str.split('/').join('')
    str = str.split('|').join(':')
    str = str.split(' ').join(replacement)
    str = str.split('---').join(replacement)
    return str.split('--').join(replacement)
  };
}
