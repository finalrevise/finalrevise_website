import { Component, OnInit } from '@angular/core';
import 'rxjs/Rx';
import { Meta, Title } from "@angular/platform-browser";

@Component({
	selector: 'our-services',
	templateUrl: './services.component.html',
	styleUrls: ['./services.component.css']
})
export class ServicesComponent {

	event_list = [
		{
		  event: ' Event 1',
		  eventLocation: 'Fox',
		  eventImage: 'https://images.unsplash.com/photo-1563210080-dfe35c83e2eb?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjExMDk0fQ&auto=format&fit=crop&w=500&q=60',
		  eventDescription: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
		  eventStartDate: new Date(),
		  eventEndingDate: new Date()
		},
		{
		  event: ' Event 2',
		  eventLocation: 'Cat',
		  eventImage: 'https://images.unsplash.com/photo-1564012948891-27965af38a81?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60',
		  eventDescription: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
		  eventStartDate: new Date(),
		  eventEndingDate: new Date()
		},
		{
		  event: ' Event 3',
		  eventLocation: 'Monkey',
		  eventImage: "https://images.unsplash.com/photo-1563049009-2fc38eb88540?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60",
		  eventDescription: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\' standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
		  eventStartDate: new Date(),
		  eventEndingDate: new Date()
		}
	  ];

	current_events =  this.event_list.filter( event => (event.eventStartDate >= new Date() && (event.eventEndingDate <= new Date())));

	constructor(meta: Meta, title: Title) {
		title.setTitle("Services offered by FinalRevise.com");

		meta.addTags([
			{ name: 'author', content: 'finalrevise.com' },
			{ name: 'keywords', content: 'finalrevise services, PG/Hostel booking, advertisement, tour and traveling, Event management and printing' },
			{ name: 'description', content: 'Finalrevise.com is providing wide range of service options from PG booking, advadvertising with us, tours to event management and printing.' }
		]);
	}

}