import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class HeaderService {
  public relatedTopics = new BehaviorSubject('relatedTopics');
  public isHome = new BehaviorSubject('isHome');

  constructor() { }

  setRelatedTopics(relatedTopics) {
    this.relatedTopics.next(relatedTopics);
  }

  setIsHome(isHome) {
    if(isHome){
      this.isHome.next(isHome);
    }
    else{
      this.isHome.next("isHome");
    }

  }
}