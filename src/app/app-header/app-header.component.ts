import { Component, OnInit } from '@angular/core';
import { ApplicationStateService } from '../application-state.service';
import { Router } from '@angular/router';
import { HeaderService } from './header.service';

@Component({
  selector: 'app-header',
  templateUrl: './app-header.component.html',
  styleUrls: ['./app-header.component.scss']
})
export class AppHeaderDesktopComponent implements OnInit {

  public isMobileResolution: boolean;
  relatedTopics: any;
  public isRelatedTopicsPresent: boolean = false;
  isHome: boolean = false;

  constructor(private applicationStateService: ApplicationStateService, private router: Router,
    private headerService: HeaderService) {
    this.isMobileResolution = this.applicationStateService.getIsMobileResolution();
  }

  public ngOnInit() {
    this.initRelatedTopics();
  }

  initRelatedTopics() {
    this.headerService.relatedTopics.subscribe(relatedTopics => {
      if (relatedTopics != "relatedTopics") {
        this.relatedTopics = relatedTopics;
        this.isRelatedTopicsPresent = this.relatedTopics.length > 0;
      }
    });
    this.headerService.isHome.subscribe(isHome => {
      if (isHome != "isHome") {
        this.isHome = true;
        this.isRelatedTopicsPresent = false;
      } else {
        this.isHome = false;
      }
    });
  }

  openTabPage(isChild, cat, relatedLinksType) {
    if (isChild == 1) {
      this.router.navigateByUrl('/detail-list/' + cat)
    }
    else if (isChild == 2 && relatedLinksType.includes("Book")) {
      this.router.navigateByUrl('/study-met/' + cat)
    } 
    else if(isChild == 2){
      this.router.navigateByUrl('/detail/' + cat)
    }
    else if (isChild == 0) {
      this.router.navigate(['/subcat/' + cat]);
    }
  }
}
